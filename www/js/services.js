//var mainApp = angular.module('starter.services')

var baseUrl = 'http://www.fitnessbasecamp.com/services';
// var baseUrl = 'http://192.168.171.16/fitnessbasecamp/services';


var siteUrl = 'http://www.fitnessbasecamp.com/';

var uuid = '4';

var selectPackage = ''

var GuserData = '';
// services start

// login service

angular.module('starter').factory('focus', function($timeout, $window) {
                              return function(id) {
                                // timeout makes sure that is invoked after any other event has been triggered.
                                // e.g. click events that need to run before the focus or
                                // inputs elements that are in a disabled state but are enabled when those events
                                // are triggered.
                                $timeout(function() {
                                  var element = $window.document.getElementById(id);
                                  //$window.scrollTo(0, 0);
                                  if(element)
                                    element.focus();

                                });
                              };
                            });
angular.module('starter').factory('blur', function($timeout, $window) {
  return function(id) {
    // timeout makes sure that is invoked after any other event has been triggered.
    // e.g. click events that need to run before the focus or
    // inputs elements that are in a disabled state but are enabled when those events
    // are triggered.
    $timeout(function() {
      var element = $window.document.getElementById(id);
      //$window.scrollTo(0, 0);
      if(element)
        element.blur();

    });
  };
});
angular.module('starter').factory('loginService', ['$http', '$ionicLoading', function ($http, $ionicLoading) {

    var loginService = {};

    loginService.doLoginService = function (username, password, gcm_id,loginFrom) {

      var link = baseUrl+'/login/login_user';
      var formData = {email : username, password : password, gcm_id : gcm_id, loginFrom: loginFrom}
      var postData = 'myData='+JSON.stringify(formData);
      $ionicLoading.show({
                template: '<ion-spinner icon="ios"></ion-spinner>'

            });
      return $http({
              method : 'POST',
              url : link,
              data: postData,
              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

      }).success(function(res){
        // $ionicLoading.hide();
        loginService = res;
        // console.log(res.user_data);
      })
      .error(function(error){
               //alert('error ==' + error);
      });


    };


    loginService.getUserData = function () {
        return loginService;
    };

    return loginService;

}]);

angular.module('starter').factory('checkNetwork', ['$http', '$ionicLoading','$cordovaNetwork', function ($http, $ionicLoading, $cordovaNetwork) {

  var checkNetwork = {};
  checkNetwork.isOnline = function () {

    return $cordovaNetwork.isOnline();
  };

  return checkNetwork;

}]);
angular.module('starter').factory('quote', ['$http', '$ionicLoading','checkNetwork', function ($http, $ionicLoading, checkNetwork) {

    var quote = {};
    var ifNotOnline = true;
    quote.getQuote = function (uuid, userID) {
      ifNotOnline = checkNetwork.isOnline();
      // console.log(ifNotOnline);
      if(ifNotOnline == true){
        var link = baseUrl+'/dashboard/quotes_return';
        var formData = {user_id : userID, uu_id : uuid};
        var postData = 'myData='+JSON.stringify(formData);
        $ionicLoading.show({
          template: '<ion-spinner icon="ios"></ion-spinner>'

        });
        return $http({
          method : 'POST',
          url : link,
          data: postData,
          headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

        }).success(function(res){
          // $ionicLoading.hide();
          // console.log(res);
        })
          .error(function(error){
            //alert('error ==' + error);
          });
      } else{
      return ifNotOnline;
      }



    };

        return quote;

}]);



//notification service


angular.module('starter').factory('notification', ['$http', '$ionicLoading', function ($http, $ionicLoading) {

    var notification = {};

    notification.get = function (uuid, userID) {

      var link = baseUrl+'/dashboard/notification_log';
      var formData = {user_id : userID, uu_id : uuid}
      var postData = 'myData='+JSON.stringify(formData);
      $ionicLoading.show({
                template: '<ion-spinner icon="ios"></ion-spinner>'

            });
      return $http({
              method : 'POST',
              url : link,
              data: postData,
              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

      }).success(function(res){
        $ionicLoading.hide();
        // console.log(res);
      })
      .error(function(error){
               //alert('error ==' + error);
      });


    };

        return notification;

}]);


//get friends list
angular.module('starter').factory('friendsList', ['$http', '$ionicLoading', function ($http, $ionicLoading) {

  var friendsList = {};

  friendsList.get = function (uuid, userID) {
    var link = baseUrl+'/friends/myfriends';
    // console.log('userID---'+ userID + '--UUID--' + uuid);
    var formData = {uu_id : uuid, user_id : userID};
    var postData = 'myData='+JSON.stringify(formData);

    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner>'

    });
    return $http({
      method : 'POST',
      url : link,
      data: postData,
      headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

    }).success(function(res){

    })



  };

  return friendsList;

}]);

//all friends activity

angular.module('starter').factory('friendsActivity', ['$http', '$rootScope', function ($http, $rootScope) {

  var friendsActivity = {};

  friendsActivity.get = function (uuid, userID, start) {
    var link = baseUrl+'/dashboard/all_friendsactivities';
    // console.log('userID---'+ userID + '--UUID--' + uuid);
    var formData = {uu_id : uuid, user_id : userID, start : start};
    var postData = 'myData='+JSON.stringify(formData);

    // $ionicLoading.show({
    //   template: '<ion-spinner icon="ios"></ion-spinner>'
    //
    // });

    $rootScope.showLoader = 'showLoader';
    return $http({
      method : 'POST',
      url : link,
      data: postData,
      headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

    }).success(function(res){

    })



  };

  return friendsActivity;

}]);

angular.module('starter').directive('ngLoad', ['$parse', function($parse){

    return {
      restrict: 'A',
      compile: function($element, attr) {
        var fn = $parse(attr['ngLoad']);

        return function(scope, element, attr) {
          element.on('load', function(event) {
            scope.$apply(function() {
              fn(scope, {$event:event});
            });
          });
        };

      }
    };

  }]);
//terms and conditions

angular.module('starter').factory('termsConditions', ['$http', '$ionicLoading', function ($http, $ionicLoading) {

  var termsConditions = {};

  termsConditions.get = function () {
    var link = baseUrl+'/termsConditions/termsAndConditions';
    var postData = '';

    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner>'

    });
    return $http({
      method : 'POST',
      url : link,
      data: postData,
      headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

    })



  };

  return termsConditions;

}]);
angular.module('starter').directive('equals', function() {
  return {
    restrict: 'A', // only activate on element attribute
    require: '?ngModel', // get a hold of NgModelController
    link: function(scope, elem, attrs, ngModel) {
      if(!ngModel) return; // do nothing if no ng-model

      // watch own value and re-validate on change
      scope.$watch(attrs.ngModel, function() {
        validate();
      });

      // observe the other value and re-validate on change
      attrs.$observe('equals', function (val) {
        validate();
      });

      var validate = function() {
        // values
        var val1 = ngModel.$viewValue;
        var val2 = attrs.equals;

        // set validity
        ngModel.$setValidity('equals', ! val1 || ! val2 || val1 === val2);
      };
    }
  }
});


angular.module('starter').directive('adult', function () {
  return {
    require: 'ngModel',
    link: function (scope, element, attributes, control) {
      control.$validators.adult = function (modelValue, viewValue) {

        if (control.$isEmpty(modelValue)) // if empty, correct value
        {
          return true;
        }

        var age = viewValue;
        var output = age.split(" ").pop();


        if (output >= 2000 && output <=2019) // correct value
        {
          return false;
        }
        return true; // wrong value
      };
    }
  };
});



angular.module('starter').directive('stringToNumber', [function () {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseFloat(value);
      });
    }
  };
}]);
//get device information

angular.module('starter').factory('deviceInformation', ['$http', '$cordovaDevice', '$ionicPlatform', function ($http, $cordovaDevice, $ionicPlatform) {

      var allDeviceInfo = {}

      allDeviceInfo.getInfo = function(){

        return $cordovaDevice.getDevice();
        // console.log($cordovaDevice.getDevice());

      };
      return allDeviceInfo;


}]);
var player =[];
angular.module('starter').directive('vimeo', function() {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        //Assumes that true means the video is playing
        controlBoolean: '='
      },
      template: '<iframe id="{{id}}" height="{{height}}" width="{{width}}"> {{text}} </iframe>',
      link: function postLink(scope, element, attrs) {
        var url = "http://player.vimeo.com/video/" + attrs.vid + "?title=0&byline=0&portrait=0&api=1";
        element.attr('src', url);

        var iframe = element[0];
        player[attrs.totalExercises] = $f(iframe);

        scope.$watch('controlBoolean', function(){
          if(scope.controlBoolean){
            player[attrs.totalExercises].api('play');
            player[attrs.totalExercises].addEvent("playProgress", function(data,event){

          // console.log("Seconds Played "+data.seconds);
        });


          }
          else{
            player[attrs.totalExercises].api('pause');
          }
        });
      }
    };
  });

angular.module('starter').directive('iframeOnload', [function(){
return {
    scope: {
        callBack: '&iframeOnload'
    },
    link: function(scope, element, attrs){
        element.on('load', function(){
            return scope.callBack();
        })
    }
}}])
angular.module('starter').directive('fancybox', [function() {
  return {
    link: function($scope, element, attrs) {
      element.fancybox({
        hideOnOverlayClick:false,
        hideOnContentClick:false,
        enableEscapeButton:false,
        showNavArrows:false,
        onComplete: function(){
          $timeout(function(){
            $compile($("#fancybox-content"))($scope);
            $scope.$apply();
            $.fancybox.resize();
          })
        }
      });
    }
  }
}]);
angular.module('starter').service('$cordovaScreenshot', ['$q', function($q) {
    return {
        capture: function(filename, extension, quality) {
            extension = extension || 'jpg';
            quality = quality || '100';

            var defer = $q.defer();

            navigator.screenshot.save(function(error, res) {
                if (error) {
                    console.error(error);
                    defer.reject(error);
                } else {
                    // console.log('screenshot saved in: ', res.filePath);
                    defer.resolve(res.filePath);
                }
            }, extension, quality, filename);

            return defer.promise;
        }
    };
}]);
angular.module('starter').filter('trusted', ['$sce', function ($sce) {
  return function(url) {
    return $sce.trustAsResourceUrl(url);
  };
}]);
