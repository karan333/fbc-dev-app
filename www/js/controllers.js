
/**
 * Purpose:
 *@This file is used to show manage Multiple Controllers
 *
 **/
var baseUrl = 'http://www.fitnessbasecamp.com/services';
// var baseUrl = 'http://192.168.171.16/fitnessbasecamp/services';
var siteUrl = 'http://www.fitnessbasecamp.com/';
var uuid = '4';
var selectPackage = '';
var GCM = '';
angular.module('starter').controller('AppCtrl', function ($cordovaToast, $cordovaNetwork, $cordovaPreferences, deviceInformation, $cordovaDevice, $rootScope, $ionicPlatform, $http, $window, $scope, $ionicPopup, $state, $ionicModal, $ionicPopover, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading) {

  $ionicPlatform.ready(function () {
    TTS.speak({
      text: '',
      locale: 'en-GB',
      rate: 1
    }, function () {
      // alert('success');
    }, function (reason) {
      // alert(reason);
    });
    $scope.FBUserId = "";
    var userData = $window.localStorage['userData'];
    $scope.userData = angular.fromJson(userData);

    if ($scope.userData !== undefined) {

      $scope.username = $scope.userData.user_fname;
      $scope.userImg = $scope.userData.image;
      var user_id = $scope.userData.user_id;
      $scope.isOnline = '';

      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();


        $rootScope.$on('$cordovaNetwork:online', function (event, networkState) {
          $scope.isOnline = true;
          $scope.network = $cordovaNetwork.getNetwork();
          $cordovaToast.showLongBottom('You are Online !')

        })

        // listen for Offline event
        $rootScope.$on('$cordovaNetwork:offline', function (event, networkState) {

          $scope.isOnline = false;
          $scope.network = $cordovaNetwork.getNetwork();
          $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })


      }, false);
      /**
       * @name logout
       * @todo To Logout The User
       * @return void function
       *
       **/
      $scope.logout = function () {
        //  if ($scope.isOnline == true){
        $scope.showConfirm = function () {
          var confirmPopup = $ionicPopup.confirm({
            title: 'Warning',
            template: 'Are you sure you want to logout?',
            cssClass: 'loginErrorPopUp logoutCpop'

          });

          confirmPopup.then(function (res) {
            if (res) {
              var link = baseUrl + '/login/logout';

              var formData = {uu_id: uuid, user_id: user_id};
              var postData = 'myData=' + JSON.stringify(formData);

              $ionicLoading.show({
                template: '<ion-spinner icon="ios"></ion-spinner>'

              });
              $http({
                method: 'POST',
                method: 'POST',
                url: link,
                data: postData,
                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

              }).success(function (res) {
                $ionicLoading.hide();
                if (res.status == true) {
                  $window.localStorage['logout'] = true;
                  // $window.localStorage['username'] = 'notAvailable';
                  $ionicSideMenuDelegate.toggleLeft();

                  var alertPopup = $ionicPopup.alert({
                    title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                    template: res.message,
                    cssClass: 'loginErrorPopUp hideOk'
                  });

                  $timeout(function () {
                    alertPopup.close();

                  }, 2000);
                  $state.go('app.login');
                }


              })
            } else {

            }
          });
        };
        $scope.showConfirm();
//                         }else{
//                          $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
//                         }
      };




      FCMPlugin.getToken(
        function (token) {
          GCM = token;
          // console.log("FCM" + token);
        },
        function (err) {
          // console.log('error retrieving token: ' + err);
        }
      );

      // console.log("gcm : " + GCM);




      $scope.deviceInfo = deviceInformation.getInfo();
      var uuid = $scope.deviceInfo.uuid;
      $scope.userData = [];
//



      FCMPlugin.onNotification(function(data){
        if(data.wasTapped){
          //Notification was received on device tray and tapped by the user.
          // console.log(data);
        }else{
          //Notification was received in foreground. Maybe the user needs to be notified.
          // console.log(data);
        }
      });


      /////////////////////////////////////////////////////////


//});
    }

  });
  $scope.userData = [];
  $rootScope.$on('$stateChangeSuccess',
    function (event, toState, toParams, fromState, fromParams, options) {



      var userData = $window.localStorage['userData'];
      $scope.userData = angular.fromJson(userData);

      if ($scope.userData !== undefined) {
        $scope.username = $scope.userData.user_fname;
        $scope.userImg = $scope.userData.image;
        var user_id = $scope.userData.user_id;


        $scope.logout = function () {
          //  if ($scope.isOnline == true){
          $scope.showConfirm = function () {
            var confirmPopup = $ionicPopup.confirm({
              title: 'Warning',
              template: 'Are you sure you want to logout?',
              cssClass: 'loginErrorPopUp logoutCpop'

            });

            confirmPopup.then(function (res) {
              if (res) {
                var link = baseUrl + '/login/logout';

                var formData = {uu_id: uuid, user_id: user_id};
                var postData = 'myData=' + JSON.stringify(formData);

                $ionicLoading.show({
                  template: '<ion-spinner icon="ios"></ion-spinner>'

                });
                $http({
                  method: 'POST',
                  url: link,
                  data: postData,
                  headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

                }).success(function (res) {
                  $ionicLoading.hide();
                  if (res.status == true) {
                    $window.localStorage['logout'] = true;
                    // $window.localStorage['username'] = 'notAvailable';
                    $ionicSideMenuDelegate.toggleLeft();

                    var alertPopup = $ionicPopup.alert({
                      title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                      template: res.message,
                      cssClass: 'loginErrorPopUp hideOk'
                    });

                    $timeout(function () {
                      alertPopup.close();

                    }, 2000);
                    $state.go('app.login');
                  }


                })
              } else {

              }
            });
          };
          $scope.showConfirm();
          //                         }else{
          //                          $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
          //                         }
        }
      }


    })


});
/**
 * @name LoginCtrl
 * @todo Main Purpose of this controller is to Login the User
 * @return void function
 *
 **/
angular.module('starter').controller('LoginCtrl', function ($cordovaToast, $rootScope, $cordovaNetwork, $cordovaPreferences, deviceInformation, $ionicPlatform, $cordovaSQLite, $window, loginService, $state, $ionicSideMenuDelegate, $scope, $stateParams, $ionicPopover, $cordovaNetwork, $ionicPopup, $ionicModal, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicHistory, $rootScope, $ionicLoading)
{
  $ionicSideMenuDelegate.canDragContent(false);

  $scope.isOnline = [];
  $scope.fbEmailGot = false;
  document.addEventListener("deviceready", function () {
    $scope.network = $cordovaNetwork.getNetwork();
    $scope.isOnline = $cordovaNetwork.isOnline();


    // listen for Online event
    $rootScope.$on('$cordovaNetwork:online', function (event, networkState) {
      $scope.isOnline = true;
      $scope.network = $cordovaNetwork.getNetwork();
      $cordovaToast.showLongBottom('You are Online !')

    });

    // listen for Offline event
    $rootScope.$on('$cordovaNetwork:offline', function (event, networkState) {

      $scope.isOnline = false;
      $scope.network = $cordovaNetwork.getNetwork();
      $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
    });

  }, false);

  $ionicPlatform.ready(function () {
    $scope.deviceInfo = deviceInformation.getInfo();
    var uuid = $scope.deviceInfo.uuid;
  });
  $scope.rememberMe = {
    value: true
  };

  $scope.login = {
    username : $window.localStorage['username'],
    password : $window.localStorage['password']
  };
  $scope.status = [];
  // console.log($window.localStorage['logout']);
  if ($window.localStorage['logout'] === 'false') {
    // console.log('in home state');
    $state.go('app.home', {notify: false, reload: true}).then(function () {
      $rootScope.$broadcast('$stateChangeSuccess');
    });

  } else {
    // console.log('in login state');
    $state.go('app.login');

  }
  $scope.doLogin = function () {

    if ($scope.isOnline == true) {
      var username = $scope.login.username;
      var password = $scope.login.password;
      var gcm_id = GCM;
      var isRemember = $scope.rememberMe.value;

      if(username == undefined && password == undefined){
        var alertPopup = $ionicPopup.alert({
          title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
          template: 'Please Enter Username and Password',
          cssClass: 'loginErrorPopUp hideOk'
        });

        $timeout(function () {
//                       $ionicLoading.hide();
          alertPopup.close();

        }, 3000);
      } else{
        $ionicLoading.show({
          template: '<ion-spinner icon="ios"></ion-spinner>'

        });
        loginService.doLoginService(username, password, gcm_id,'normal')
          .success(function (res) {
            // $ionicLoading.hide()

            $scope.status = res.status;
            $scope.code = res.code;
            var useridL = parseInt(res.user_id);


            $timeout(function () {

              if ($scope.code == '402') {
                $ionicLoading.hide();
                $scope.showConfirm = function () {
                  var confirmPopup = $ionicPopup.confirm({
                    title: 'Warning',
                    template: res.message,
                    cssClass: 'loginErrorPopUp'

                  });

                  confirmPopup.then(function (res) {
                    if (res) {

                      // logout service start
                      var link = baseUrl + '/login/logout';
                      var formData = {uu_id: uuid, user_id: useridL}
                      var postData = 'myData=' + JSON.stringify(formData);
                      $ionicLoading.show({
                        template: '<ion-spinner icon="ios"></ion-spinner>'

                      });
                      return $http({
                        method: 'POST',
                        url: link,
                        data: postData,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

                      }).success(function (res) {

                        var alertPopupLogout = $ionicPopup.alert({
                          title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                          template: res.message,
                          cssClass: 'loginErrorPopUp hideOk'
                        });

                        $timeout(function () {
                          alertPopupLogout.close();

                        }, 2000);
                        $ionicLoading.hide();

                      })
                        .error(function (error) {
                          $ionicLoading.hide();
                          $cordovaToast.showLongBottom('Internet Connection is slow ! Please Connect to Internet and retry later')
                          // alert('error ==' + res.message);
                        }).finally(function ($ionicLoading) {
                          // On both cases hide the loading
                          $scope.hide($ionicLoading);
                        });

                      // logout service end


                    }
                  });
                };
                $scope.showConfirm();
              }

              else if ($scope.status == true) {
                if (isRemember == true) {
                  //        alert('true');
                  $window.localStorage['username'] = username;
                  $window.localStorage['password'] = password;
                  $window.localStorage['isRemember'] = isRemember;
                  $window.localStorage['logout'] = false;
                } else {
                  //                                          alert('false');
                  $window.localStorage['username'] = '';
                  $window.localStorage['password'] = '';
                  $window.localStorage['isRemember'] = false;
                }

                var alertPopup = $ionicPopup.alert({
                  title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                  template: res.message,
                  cssClass: 'loginErrorPopUp hideOk'
                });

                $timeout(function () {
//                       $ionicLoading.hide();
                  alertPopup.close();

                }, 2000);

                var userData = JSON.stringify(res.user_data);
                $window.localStorage['userData'] = userData;

                // $timeout(function() {
                $state.go('app.home');

                // }, 5000);


              } else if ($scope.status == false) {
                var alertPopup = $ionicPopup.alert({
                  title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                  template: res.message,
                  cssClass: 'loginErrorPopUp hideOk'
                });


                $timeout(function () {
                  alertPopup.close();
                  $ionicLoading.hide();
                }, 3000);


              }

            }, 3000);


          }).finally(function ($ionicLoading) {
          $ionicLoading.hide()
        });
      }

    } else {
      $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
    }
  }


// Set Motion
  $timeout(function () {
    ionicMaterialMotion.fadeSlideIn({
      selector: '.animate-fade-slide-in .item'
    });
  }, 200);

  // Set Ink
  ionicMaterialInk.displayEffect();

  $scope.getFBemail = function(){
    $scope.fbEmailGot = 'stuck';
    facebookConnectPlugin.api($scope.FBUserId + "/?fields=id,email", ["user_birthday"],
      function onSuccess(resultFB) {
        $scope.fbEmailGot = true;
        $ionicLoading.hide();

        var fbUserEmail = resultFB.email;
        var fbUserPassword = "";
        var gcm_id = GCM;
        loginService.doLoginService(fbUserEmail, fbUserPassword, gcm_id,'facebook')
          .success(function (res) {

            isRemember = true;

            // $ionicLoading.hide()

            $scope.status = res.status;
            $scope.code = res.code;
            var useridL = parseInt(res.user_id);


            $timeout(function () {

              if ($scope.code == '490') {
                $ionicLoading.hide();
                var alertPopupLogout = $ionicPopup.alert({
                  title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                  template: 'Email Does not Exist. Please Create a New Account or Login With Existing Account' ,
                  cssClass: 'loginErrorPopUp hideOk'
                });

                $timeout(function () {
                  alertPopupLogout.close();
                  $state.go('app.selectPackage', {cid: fbUserEmail});
                }, 3500);

              }else if ($scope.code == '402') {
                $ionicLoading.hide();
                $scope.showConfirm = function () {
                  var confirmPopup = $ionicPopup.confirm({
                    title: 'Warning',
                    template: res.message,
                    cssClass: 'loginErrorPopUp'

                  });

                  confirmPopup.then(function (res) {
                    if (res) {

                      // logout service start
                      var link = baseUrl + '/login/logout';
                      var formData = {uu_id: uuid, user_id: useridL}
                      var postData = 'myData=' + JSON.stringify(formData);
                      $ionicLoading.show({
                        template: '<ion-spinner icon="ios"></ion-spinner>'

                      });
                      return $http({
                        method: 'POST',
                        url: link,
                        data: postData,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

                      }).success(function (res) {

                        var alertPopupLogout = $ionicPopup.alert({
                          title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                          template: res.message,
                          cssClass: 'loginErrorPopUp hideOk'
                        });

                        $timeout(function () {
                          alertPopupLogout.close();

                        }, 2000);
                        $ionicLoading.hide();

                      })
                        .error(function (error) {
                          $ionicLoading.hide();
                          $cordovaToast.showLongBottom('Internet Connection is slow ! Please Connect to Internet and retry later')
                          // alert('error ==' + res.message);
                        }).finally(function ($ionicLoading) {
                          // On both cases hide the loading
                          $scope.hide($ionicLoading);
                        });

                      // logout service end


                    } else {

                    }
                  });
                };
                $scope.showConfirm();
              }else if ($scope.status == true) {
                var username = fbUserEmail;
                if (isRemember == true) {
                  //        alert('true');
                  $window.localStorage['username'] = '';
                  $window.localStorage['password'] = '';
                  $window.localStorage['isRemember'] = isRemember;
                  $window.localStorage['logout'] = true;
                } else {
                  //                                          alert('false');
                  $window.localStorage['username'] = '';
                  $window.localStorage['password'] = '';
                  $window.localStorage['isRemember'] = false;
                }

                var alertPopup = $ionicPopup.alert({
                  title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                  template: res.message,
                  cssClass: 'loginErrorPopUp hideOk'
                });

                $timeout(function () {
//                       $ionicLoading.hide();
                  alertPopup.close();

                }, 2000);

                var userData = JSON.stringify(res.user_data);
                $window.localStorage['userData'] = userData;

                // $timeout(function() {
                $state.go('app.home');

                // }, 5000);


              } else if ($scope.status == false) {
                var alertPopup = $ionicPopup.alert({
                  title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                  template: res.message,
                  cssClass: 'loginErrorPopUp'
                });


                $timeout(function () {
                  alertPopup.close();

                }, 5000);


              }

            }, 3000);


            //success on service
          }).error(function (res) {

          //error on service
          errorMsg = "No Internet Connection. Please check your network and try again!";
        })

      }, function onError(err) {
        //error from FB
        errorMsg = "Something wrong with your facebook Application. Please choose another method.";
        console.error("Failed: " + err);
      }
    );
  };
  $scope.$watch('fbEmailGot', function() {

    $timeout(function(){
      if($scope.fbEmailGot == 'stuck' && $scope.fbEmailGot !== true && $scope.fbEmailGot !== false){

        $scope.doFacebookLogin();
      }
    }, 2000);

  });
  $scope.doFacebookLogin = function () {
    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner>'

    });
    var fbLoginSuccess = function (userDataFB) {


      var fbuserid = userDataFB.authResponse.userID;
      if(fbuserid!="") {
        $scope.FBUserId = fbuserid;
      }


      if(userDataFB.status = 'connected'){
        $timeout(function(){



          $timeout(function(){

            $scope.getFBemail();

          }, 1000);
        }, 1000);


      }
      var errorMsg = "";

      if (errorMsg != "") {
        var alertPopup = $ionicPopup.alert({
          title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
          template: errorMsg,
          cssClass: 'loginErrorPopUp'
        });


        $timeout(function () {
          alertPopup.close();

        }, 5000);
      }

    }


    facebookConnectPlugin.login(["public_profile",'email'], fbLoginSuccess,
      function loginError(err) {
        $ionicLoading.hide();
      }
    );
  }


});
/**
 * @name selectPackageController
 * @todo Main Purpose of this controller is to Select Packages On Sign Up
 * @return void function
 *
 **/
angular.module('starter').controller('selectPackageController', function ($ionicHistory, ionicMaterialMotion, ionicMaterialInk, $state, $scope, $stateParams, $http, $timeout, $ionicLoading, $ionicPlatform) {


  $scope.opinions = [];
  $scope.fbEmail = $stateParams.cid;

  var link = baseUrl + '/membership/all_packages';
  $ionicLoading.show({
    template: '<ion-spinner icon="ios"></ion-spinner>'
  });
  return $http({
    method: 'POST',
    url: link,
    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

  }).success(function (res) {
    $ionicLoading.hide();
    $scope.checked = true;
    $scope.opinions = res.packages_monthly;
    $scope.opinions2 = res.packages_yearly;

    $scope.toggleOpinion = function (opinion) {
      if ($scope.isOpinionShown(opinion)) {
        $scope.shownOpinion = null;
      } else {
        $scope.shownOpinion = opinion;
      }
    };
    $scope.isOpinionShown = function (opinion) {

      return $scope.shownOpinion === opinion;
    };

    $scope.setTest = function (memberID) {
      selectPackage = memberID
      $state.go('app.signup', {cid: $scope.fbEmail});
    }
    var deregisterFirst = $ionicPlatform.registerBackButtonAction(function (e) {

      $ionicHistory.goBack();
      e.preventDefault();
      return false;
    }, 101);

    $scope.$on('$destroy', deregisterFirst);

    $scope.myGoBack = function () {
      $ionicHistory.goBack();
    };
  })


    .error(function (error) {
      // alert('error ==' + res.message);
    });


});
/**
 * @name forgotPassword
 * @todo Main Purpose of this controller is to Manage Forgot Password Action
 * @return void function
 *
 **/
angular.module('starter').controller("forgotPassword", function ($state, $ionicPlatform, $ionicHistory, $scope, $stateParams, $timeout, $ionicLoading, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $http, $ionicPopup, $ionicLoading) {
  $ionicSideMenuDelegate.canDragContent(false);
  $scope.forgotEmail = {};
  $scope.forgotPAssword = function (username) {
    var link = baseUrl + '/login/forget_password';
    var formData = {email: username};
    var postData = 'myData=' + JSON.stringify(formData);
    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner>'

    });
    return $http({
      method: 'POST',
      url: link,
      data: postData,
      headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

    }).success(function (res) {
      $ionicLoading.hide();
      var alertPopup = $ionicPopup.alert({
        title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
        template: res.message,
        cssClass: 'loginErrorPopUp'
      });

      $timeout(function () {
        alertPopup.close();
        $state.go('app.login');
      }, 5000);
    })
      .error(function (error) {
        //  alert('error ==' + res.message);
      });
  }

  var deregisterFirst = $ionicPlatform.registerBackButtonAction(function (e) {

    $state.go('app.login');
    e.preventDefault();
    return false;
  }, 101);

  $scope.$on('$destroy', deregisterFirst);


});
/**
 * @name signupController
 * @todo Main Purpose of this controller is to Sign Up When New User come
 * @return void function
 *
 **/
angular.module('starter').controller('signupController', function ($ionicModal, termsConditions, $ionicHistory, $ionicPopup, $scope, $stateParams, $http, $ionicLoading, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, ionicDatePicker, $state) {


  //terms and conditions

  termsConditions.get().success(function (res) {
    $scope.terms = res.message;
   $ionicLoading.hide();
  }).error(function(error){
    $ionicLoading.hide();
    // console.log('request not completed')
  });


  $ionicSideMenuDelegate.canDragContent(false);
  $scope.signup = {};
  $scope.fbEmail = $stateParams.cid;
  $scope.signup.username = $scope.fbEmail;

  // $scope.phonenumber = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
  // $scope.phonenumber = /^\+(?:[0-9] ?){6,14}[0-9]$/;
  $scope.phonenumber = /^[0-9]{1,22}$/;


// datepicker start

  var ipObj1 = {
    callback: function (val) {
      var date = (new Date(val));

      var day = date.getDate();
      var year = date.getFullYear();
      var month = date.getMonth() + 1;
      $scope.signup.dob = day + ' ' + month + ' ' + year;
    },
    from: new Date(1930, 1, 1),
    to: new Date(2018, 12, 30),
    closeOnSelect: true
  }
  $scope.openDatePicker = function () {
    ionicDatePicker.openDatePicker(ipObj1);
  };
// datepicker end

  $scope.signup = [];
  $scope.status = [];
  $scope.signup.packageID = selectPackage;

  $scope.doSignup = function () {
    var first_name = $scope.signup.enterFName;
    var last_name = $scope.signup.enterLName;
    var email = $scope.signup.username;
    var password = $scope.signup.password;
    var confirm_password = $scope.signup.Cpassword;
    var phone = $scope.signup.phone;
    var gender = $scope.signup.gender;
    var dob = $scope.signup.dob;
    var measurement_type = $scope.signup.enterMeasure;
    var package_id = selectPackage;
    var link = baseUrl + '/login/signup';

    var formData = {
      'first_name': first_name,
      'last_name': last_name,
      'email': email,
      'password': password,
      'confirm_password': confirm_password,
      'phone': phone,
      'gender': gender,
      'dob': dob,
      'measurement_type': measurement_type,
      'package_id': package_id
    }


    var postData = 'myData=' + JSON.stringify(formData);
    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner>'

    });
    $http({
      method: 'POST',
      url: link,
      data: postData,
      headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

    }).success(function (res) {
      $ionicLoading.hide();
      if (res.code == '200') {
        var alertPopup = $ionicPopup.alert({
          title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
          template: res.message,
          cssClass: 'loginErrorPopUp'
        });

        $timeout(function () {
          alertPopup.close();

        }, 5000);
        $state.go('app.login');
      } else if (res.code == '400') {
        var alertPopup = $ionicPopup.alert({
          title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-close"></span></div></div>',
          template: res.message,
          cssClass: 'loginErrorPopUp'
        });

        $timeout(function () {
          alertPopup.close();

        }, 10000);
      }

    }).error(function (res) {

    })

  }

// Set Motion
  $timeout(function () {
    ionicMaterialMotion.fadeSlideInRight({
      startVelocity: 3000
    });
  }, 700);

  // Set Ink
  ionicMaterialInk.displayEffect();
  $timeout(function() {
    $scope.$apply(function() {
      $scope.signup = {
        username : $scope.fbEmail
      };
    });
  });

  $ionicModal.fromTemplateUrl('templates/modal.html', {
    scope: $scope
  }).then(function(modal) {
    //Fix this line, changed the variable name to different name.
    $scope.modal = modal;
  });

  $scope.closeModal = function() {
    $scope.modal.hide();
    $scope.$on('$destroy', function() {
      $scope.modal.remove();
    });
  };


  $scope.myGoBack = function () {
    $ionicHistory.goBack();
  };
});
/**
 * @name termsConditions
 * @todo Main Purpose of this controller is to Show Terms And Conditions of Website
 * @return void function
 *
 **/
angular.module('starter').controller('termsConditions', function ($ionicHistory, $ionicPopup, $scope, $stateParams, $http, $ionicLoading, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, ionicDatePicker, $state) {
  $scope.terms = "";
  var postData = "";
  $ionicLoading.show({
    template: '<ion-spinner icon="ios"></ion-spinner>'
  });

  $http({
    method: 'POST',
    url: baseUrl + '/termsConditions/termsAndConditions',
    data: postData,
    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}


  }).success(function (data) {

    $ionicLoading.hide();

    // $scope.terms = data;
    $scope.terms = data.message;

  });


  $scope.myGoBack = function () {
    $ionicHistory.goBack();
  };

});




