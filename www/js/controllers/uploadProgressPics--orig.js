/**
 * Purpose:
 *@This file is used to upload progress photos.
 *
 **/

angular.module('starter').controller('uploadProgressPics', function(quote, $rootScope,  $cordovaNetwork, $cordovaToast, $cordovaNetwork, $rootScope, $cordovaPreferences, $ionicBackdrop, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading, $cordovaFileTransfer,$q) {


  /**
   *
   * Initializing Variables, Arrays, and Objects
   *
   **/


$scope.upload = [];
$scope.upload.pic = [];
$scope.upload.imagename1="";
$scope.upload.imagename2="";
$scope.upload.imagename3="";
$scope.upload.imagename4="";

$scope.isOnline = [];
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();



        /**
         *
         * Listen for Online event
         *
         **/



        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })


        /**
         *
         * Listen for Offline event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            // console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);



           var userData = $window.localStorage['userData']
           $scope.userData = angular.fromJson(userData);
           // console.log($scope.userData);




  /**
   * @name selectUploadMethod
   * @todo To show popup for adding photo from camera or gallery
   * @return void function
   *
   **/

                              $scope.selectUploadMethod = function(id){
                                var id = id;
                                // console.log(id);
                                var myPopup = $ionicPopup.show({
                                    template: '<div class="uploadMethodSack"><ul><li><a class="ink" ng-click="takePicture('+id+');"><span class="ion-camera"></span><span>Upload from Camera</span></a></li><li><a class=ink"" ng-click="selectPicture('+id+');"><span class="ion-images"></span><span>Upload from Gallery</span></a></li></ul></div>',
                                    title: 'Choose Upload Method',
                                    subTitle: 'Select from Camera or Gallery',
                                    scope: $scope
                                  });

                                  myPopup.then(function(res) {
                                    // console.log('Tapped!', res);
                                  });

                                  $timeout(function() {
                                     myPopup.close(); //close the popup after 3 seconds for some reason
                                  }, 8000);
                              }



  /**
   * @name takePicture
   * @todo To add photo from camera
   * @return void function
   *
   **/


                                $scope.takePicture = function(id){
                                // console.log(id);
                                   document.addEventListener("deviceready", function () {

                                      var options = {

                                        destinationType: Camera.DestinationType.FILE_URI,
                                        sourceType: Camera.PictureSourceType.CAMERA,
                                        allowEdit: true
                                      };

                                      $cordovaCamera.getPicture(options).then(function(imageURI) {
                                      // console.log(imageURI)
                                        $scope.uploadPicturex(imageURI,id);
                                        window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {
                                        // console.log(fileEntry.nativeURL);
                                          //$scope.upload.pic[id] = imageURI;
                                          // console.log($scope.upload.pic[id] + id);
                                          $scope.ftLoad = true;
          //                                var image = document.getElementById('myImage');
          //                                image.src = fileEntry.nativeURL;
                                          });
                                        $ionicLoading.show({template: 'Uploading Photo...', duration:500});
                                      },
                                      function(err){
                                        $ionicLoading.show({template: 'Error While Uploading Image...', duration:500});
                                      })

                                    }, false);
                                }

  /**
   * @name selectPicture
   * @todo To add photo from gallery
   * @return void function
   *
   **/
                                  $scope.selectPicture = function(id) {
                                  // console.log(id);
                                   document.addEventListener("deviceready", function () {
                                      var options = {
                                        quality: 100,
                                        destinationType: Camera.DestinationType.FILE_URI,
                                        sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
                                        encodingType: Camera.EncodingType.JPEG,
                                        targetWidth: 346,
                                        targetHeight: 508,
                                        allowEdit: true
                                      };

                                      $cordovaCamera.getPicture(options).then(
                                      function(imageURI) {
                                     // console.log("Mani");
                                      $scope.uploadPicturex(imageURI,id);

                                       // console.log( $scope.upload.pic[id] );
                                        $scope.ftLoad = true;
                                        // console.log(imageURI);
//                                        window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {
//
//
//                                          });
                                      //  $ionicLoading.show({template: 'Uploading Photo...', duration:500});
                                      },
                                      function(err){
                                        $ionicLoading.show({template: 'Error While Uploading Image...', duration:500});
                                      })
                                       }, false);
                                    };


  /**
   * @name uploadPicturex
   * @todo To upload series of photos to remote server
   * @return void function
   *
   **/
 $scope.uploadPicturex = function(imageURI,id) {

// console.log("In Upload Picture Function");
console.log(imageURI);
  var options = {

  fileKey: "picture",

  filename: "p1.png",

  chunkedMode: false,

  mimeType: "image/png",
  trustAllHosts:true,
  headers:{Connection:"close"}

  };
 $ionicLoading.show({
                                             template: '<ion-spinner icon="ios"></ion-spinner><p>Uploading Image!</p>'

                                         });
  $cordovaFileTransfer.upload(encodeURI("http://www.fitnessbasecamp.com/services/dashboard/upload_progress_images"), imageURI, options)

  .then(function(result) {
  console.log(result.response);
  $scope.upload.pic[id] = imageURI;;
 var  resk  = angular.fromJson(result.response);
  if(id == 0)
  {
    $scope.upload.imagename1 = resk.image_name;
  }
  if(id == 1)
    {
      $scope.upload.imagename2 = resk.image_name;
    }
    if(id == 2)
      {
        $scope.upload.imagename3 = resk.image_name;
      }
      if(id == 3)
        {
          $scope.upload.imagename4 = resk.image_name;
        }
// console.log($scope.upload);
$ionicLoading.hide();


  }, function(err) {

  // console.log(err);
  $ionicLoading.hide();
  $ionicLoading.show({template: 'An error occured while uploading your picture.<br>Please try again.', duration:500});

  }, function (progress) {

            });



  }



  /**
   * @name AddProgressPhoto
   * @todo To add progress pictures to database
   * @return void function
   *
   **/



$scope.AddProgressPhoto = function() {

 var userData = $window.localStorage['userData'];
                    $scope.userData = angular.fromJson(userData);

              // after getting data from sqlite getting user id and then all news feed service

              var user_id = $scope.userData.user_id;
              var workout_id = $stateParams.cid;

              var link = baseUrl+'/dashboard/add_progress_photos';
              var formData = {uu_id : uuid, user_id : user_id, workout_id : workout_id,p1:$scope.upload.imagename1,p2:$scope.upload.imagename2,p3:$scope.upload.imagename3,p4:$scope.upload.imagename4}
              var postData = 'myData='+JSON.stringify(formData);
                  $ionicLoading.show({
                            template: '<ion-spinner icon="ios"></ion-spinner>'

                        });
                   $http({
                          method : 'POST',
                          url : link,
                          data: postData,
                          headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                  }).success(function(res){
                  $ionicLoading.hide();
                  $state.go('app.allProgressPhotos');


                   });



}


});
