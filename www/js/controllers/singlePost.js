/**
 * Purpose:
 *@This file is used to show user single post
 *
 **/

angular.module('starter').controller('singlePostCtrl', function($cordovaInAppBrowser, quote, $cordovaScreenshot, $cordovaSocialSharing, $cordovaToast, $cordovaNetwork, $rootScope, $cordovaPreferences, $ionicBackdrop, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading, $cordovaFileTransfer,$q) {

  /**
   *
   * Initializing Variables, Arrays, and Objects
   *
   **/


    $scope.siteUrl = siteUrl;
    $scope.deviceInfo = deviceInformation.getInfo();
    var uuid = $scope.deviceInfo.uuid
    // console.log(uuid);
    $scope.status = {};
    $scope.status.postImage = {};
    $scope.statusImgs = [];
    $scope.status.commentImage = {};
    $scope.commentImage = [];
    $scope.commentImgs = [];
    $scope.postImage = [];
    $scope.selectedPost="";
    $scope.allFActivity=[];
     $scope.postlimit = 3;
      $scope.FActivity=[];
      $scope.currentrecordsview = 0;
      $scope.morefrndactvtse=true;
$scope.isOnline = [];
  $scope.urlPreviewPosted = [];
  $scope.noStatus = false;
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();


        /**
         *
         * Listen for Online event
         *
         **/
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        /**
         *
         * Listen for ofline event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            // console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);


  /**
   *
   * getting user data from post
   *
   **/



                  var status_id = $stateParams.cid;
                  var noti_id = $stateParams.did;
                 $scope.status_id  = $stateParams.cid;
                 // console.log(noti_id);
                var userData = $window.localStorage['userData'];
                $scope.userData = angular.fromJson(userData);
                $scope.username = $scope.userData.user_fname;
                $scope.userImg = $scope.userData.image;
                var user_id = $scope.userData.user_id;
                // console.log(user_id);

                // $scope.tabChange = function(){
                // alert("here is tab Change");
                // }

  /**
   *
   * for quote
   *
   **/

                      var user_id = $scope.userData.user_id;
                       quote.getQuote(uuid, user_id).success(function (res) {
                                                                          // console.log(res);
                                                                          $scope.quote = res;
                                                                          });


  /**
   *
   * to update notification status in dashobard
   *
   **/

                      var link = baseUrl+'/dashboard/notificationStatus';
                          // console.log('userID---'+ user_id + '--UUID--' + uuid);
                          var formData = {uu_id : uuid, user_id : user_id, status_id : status_id, noti_id : noti_id}
                          var postData = 'myData='+JSON.stringify(formData);


                          $http({
                            method : 'POST',
                            url : link,
                            data: postData,
                            headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                        }).success(function(res){

                          // console.log(res);
                          $scope.post = res.dashboard_data.userStatus;
                            $scope.urlPreviewPosted = res.dashboard_data.userStatus.status_url;
                          if($scope.post === '' || $scope.post === undefined){
                            $scope.noStatus = true;
                          }

                          $ionicLoading.hide();

                      })
                      .error(function(error){
                        // console.log('request not completed')
                      })
                      .finally(function($ionicLoading) {
                        // On both cases hide the loading
                        $scope.hide($ionicLoading);
                      });



  /**
   * @name like
   * @todo To like user post
   * @return void function
   *
   **/
                      ///////// For Liking status //////////////
                      $scope.like = function(id,index){
                          var status_id = id;

                          var link = baseUrl+'/dashboard/like_user_status';
                          // console.log('userID---'+ user_id + '--UUID--' + uuid + '--statusID' + status_id);
                          var formData = {uu_id : uuid, user_id : user_id, status_id : status_id}
                          var postData = 'myData='+JSON.stringify(formData);

                          // $ionicLoading.show({
                          // template: '<ion-spinner icon="ios"></ion-spinner>'
                          //
                          // });
                          $http({
                            method : 'POST',
                            url : link,
                            data: postData,
                            headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                        }).success(function(res){
                        $scope.postLiked = {};
                            // $ionicLoading.hide();

                            // console.log(res);
                            if(res.code == '200'){
                              $scope.actLiked = 'liked';
                              $scope.post.likes = res.likes;

                            } else if(res.code == '402'){
                              $scope.actLiked = 'Disliked';
                              $scope.post.likes = res.likes;

                            } else{
                              // console.log('nothing');
                            }

                      })


                          }

  /**
   * @name postCommentfrnd
   * @todo To comment on post
   * @return void function
   *
   **/
                      $scope.postCommentfrnd = function(id, i,mani){
                      // console.log(mani);
                          var comment_text = mani[id+"_"+id];
                          var status_id = id;
                          // console.log(i);
                          var link = baseUrl+'/dashboard/post_comment';
                          // console.log('userID---'+ user_id + '--UUID--' + uuid + '--statusText' + comment_text + 'status-ID---' + status_id);
                          var formData = {uu_id : uuid, user_id : user_id, comment_text : comment_text, status_id : status_id, comment_images : $scope.commentImgs}
                          var postData = 'myData='+JSON.stringify(formData);

                          $ionicLoading.show({
                          template: '<ion-spinner icon="ios"></ion-spinner>'

                          });
                          $http({
                            method : 'POST',
                            url : link,
                            data: postData,
                            headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                        }).success(function(res){
                        mani[id+"_"+id] = '';
                        $scope.commentImage = [];
                            $scope.commentImgs = [];
                           $ionicLoading.hide();
                            $scope.post.comments = res.comments.comments;


                      })

                        }
  /**
   * @name postCommentfrnd
   * @todo To remove image while posting comment on post
   * @return void function
   *
   **/
$scope.removeCommentIMG = function(index)
{
// console.log("Image Index" + index);
$scope.commentImgs.splice(index, 1);  ;
$scope.commentImage.splice(index, 1);  ;
// console.log($scope.postImage);

}



  /**
   * @name selectUploadMethodComment
   * @todo To show popup for adding photo from camera or gallery
   * @return void function
   *
   **/


$scope.selectUploadMethodComment = function(post_id){
if($scope.selectedPost != post_id)
{
 $scope.commentImage=[];
 $scope.commentImgs=[] ;

}
$scope.selectedPost = post_id;
// console.log("All ACtivities Post ID" + post_id);
// console.log("In Comment");
  var myPopup = $ionicPopup.show({
      template: '<div class="uploadMethodSack"><ul><li><a class="ink" ng-click="takePictureComment();"><span class="ion-camera"></span><span>Upload from Camera</span></a></li><li><a class=ink"" ng-click="selectPictureComment();"><span class="ion-images"></span><span>Upload from Gallery</span></a></li></ul></div>',
      title: 'Choose Upload Method',
      subTitle: 'Select from Camera or Gallery',
      scope: $scope
    });

    myPopup.then(function(res) {
      // console.log('Tapped!', res);
    });

    $timeout(function() {
       myPopup.close(); //close the popup after 3 seconds for some reason
    }, 8000);
}





  /**
   * @name takePictureComment
   * @todo To add photo from camera
   * @return void function
   *
   **/

 $scope.takePictureComment = function(){
  // console.log("In Comment2");
     document.addEventListener("deviceready", function () {

        var options = {

          destinationType: Camera.DestinationType.FILE_URI,
          sourceType: Camera.PictureSourceType.CAMERA,
          allowEdit: true
        };

        $cordovaCamera.getPicture(options).then(function(imageURI) {
        // console.log(imageURI)
        $scope.uploadPicturexComment(imageURI);
          window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {
          //console.log(fileEntry.nativeURL);
           // $scope.status.postImage = imageURI;
           // console.log($scope.status.postImage);
            $scope.ftLoad = true;
//                                var image = document.getElementById('myImage');
//                                image.src = fileEntry.nativeURL;
            });
          $ionicLoading.show({template: 'Uploading Picture...', duration:500});
        },
        function(err){
          $ionicLoading.show({template: 'Error Uploading...', duration:500});
        })

      }, false);
  }

  /**
   * @name selectPictureComment
   * @todo To add photo from gallery
   * @return void function
   *
   **/

    $scope.selectPictureComment = function() {
    // console.log("In Comment3");
     document.addEventListener("deviceready", function () {
        var options = {
          quality: 100,
          destinationType: Camera.DestinationType.FILE_URI,
          sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
          encodingType: Camera.EncodingType.JPEG,
          targetWidth: 250,
          targetHeight: 250,
          allowEdit: true
        };

        $cordovaCamera.getPicture(options).then(
        function(imageURI) {
        $scope.uploadPicturexComment(imageURI);

          window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {
           // $scope.status.postImage = fileEntry.nativeURL;
            $scope.ftLoad = true;
    //				var image = document.getElementById('myImage');
    //				image.src = fileEntry.nativeURL;
            });
          $ionicLoading.show({template: 'Uploading Picture...', duration:500});
        },
        function(err){
          $ionicLoading.show({template: 'Error Uploading...', duration:500});
        })
         }, false);
      };



  /**
   * @name uploadPicturexComment
   * @todo To upload series of photos to remote server
   * @return void function
   *
   **/

  $scope.clicked = function(link){

    var options = {
      location: 'yes',
      clearcache: 'yes',
      toolbar: 'no'
    };
    $cordovaInAppBrowser.open(link, '_system', options)
      .then(function(event) {
        // console.log('its gone to url')

      })
      .catch(function(event) {
        // error
      });
  }

$scope.uploadPicturexComment = function(imageURI) {

// console.log("In Upload Picture Function");
// console.log(imageURI);
  var options = {

  fileKey: "picture",

  filename: "p1.png",

  chunkedMode: false,

  mimeType: "image/png",
  trustAllHosts:true,
  headers:{Connection:"close"}

  };
 $ionicLoading.show({
                                             template: '<ion-spinner icon="ios"></ion-spinner><p>Uploading Image!</p>'

                                         });
  $cordovaFileTransfer.upload(encodeURI(baseUrl+"/dashboard/uploadCommentmge"), imageURI, options)

  .then(function(result) {

  //$scope.status.commentImage = {};
    //    $scope.commentImage = [];
  // console.log(result.response);
   $scope.commentImage.push(imageURI);;
 var  resk  = angular.fromJson(result.response);

          $scope.commentImgs.push(resk.image_name) ;

// console.log($scope.commentImgs);
$ionicLoading.hide();


  }, function(err) {

  // console.log(err);
  $ionicLoading.hide();
  $ionicLoading.show({template: 'An error occured while uploading your picture.<br>Please try again.', duration:500});

  }, function (progress) {

            });



  }

});
