/**
 * Purpose:
 *@This file is used to viw workout detail page
 *
 **/


angular.module('starter').controller('workoutDetailCtrlMain', function($ionicHistory, $cordovaToast, $ionicPlatform, quote, $cordovaNetwork, $window, $log,$q, video, $document, $ionicSlideBoxDelegate, $ionicPopover, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading, $cordovaFileTransfer, $rootScope, $ionicModal) {
 // console.log(GuserData.user_fname);

  /**
   *
   * Initializing Variables, Arrays, and Objects
   *
   **/

$scope.deviceInfo = deviceInformation.getInfo();
var uuid = $scope.deviceInfo.uuid;
$scope.baseUrlForWorkoutImg = 'http://www.fitnessbasecamp.com/uploads/workouts/';
$scope.baseUrlForWorkoutPoster = 'http://www.fitnessbasecamp.com/uploads/category-images/';
$scope.siteUrl = 'http://www.fitnessbasecamp.com/';
$scope.allExercises = {};
$scope.videos = {};
$scope.workoutDetai = {};
$scope.vid_srcc = {};
$scope.reviews = {};
$scope.nct=0;
$scope.disablenext =false;
$scope.workout_Completed = 0;
var mani =[];
$scope.TotalDownloaded = 0;
$scope.progresss=  0;
$scope.vsble  =false;
$scope.isOnline = [];

var userData = $window.localStorage['userData'];
$scope.userData = angular.fromJson(userData);

$scope.addReview_username = $scope.userData.user_fname;
$scope.addReview_email = $scope.userData.user_email;
$scope.addReview_message = '';





 //rating

$scope.rating = {};
$scope.rating.rate = 0;
$scope.rating.max = 5;

$scope.rated = {};
$scope.rated.rate = 3;
$scope.rated.max = 5;
//review



      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();


        /**
         *
         * Listen for Online event
         *
         **/


        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        /**
         *
         * Listen for ofline event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            // console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);





$scope.options = {
  loop: false,
  effect: 'fade',
  speed: 500,
}





  /**
   *
   * get quotes
   *
   **/

              var user_id = $scope.userData.user_id;
              quote.getQuote(uuid, user_id).success(function (res) {
                                  // console.log(res);
                                  $scope.quote = res;
                                  });


  /**
   *
   * get workout detail from service api call
   *
   **/


              var workout_id = $stateParams.cid;
              $scope.workoutID  = $stateParams.cid;

              var link = baseUrl+'/workouts/workout_details';
              var formData = {uu_id : uuid, user_id : user_id, workout_id : workout_id};
              var postData = 'myData='+JSON.stringify(formData);
                  $ionicLoading.show({
                            template: '<ion-spinner icon="ios"></ion-spinner>'

                        });
                   $http({
                          method : 'POST',
                          url : link,
                          data: postData,
                          headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                  }).success(function(res){
                    $ionicLoading.hide();
                    // console.log(res);
                    $scope.workout_name = res.workout[0].name;
                    $scope.workoutDetai = res.workout[0];
                    $scope.reviews = res.workout[0].reviews;
                    $scope.rated.rate =  res.workout[0].ratings;
                    $scope.suggestedWorkouts = res.workout[0].suggestedWorkouts;
                    $ionicSlideBoxDelegate.update();
                    $scope.videoID = res.workout[0].vimeo_id;
                    $scope.poster = res.workout[0].image;
                    $scope.vid_srcc = res.workout[0].embed_video;
                     var vimeo_base_url = 'http://player.vimeo.com/video/';
                     $scope.vimeoVideo = vimeo_base_url + res.workout[0].vimeo_id;


                     $timeout(function() {
                                      ionicMaterialMotion.fadeSlideInRight({
                                          startVelocity: 3000
                                      });
                                  }, 700);

                                ionicMaterialInk.displayEffect();


                  }) .error(function(error){
                    $ionicLoading.hide();
                    // console.log('request not completed');
                  })



  /**
   * @name favorite
   * @todo To add workout to favorite list
   * @return void function
   *
   **/

                  $scope.favorite = function(workout_id){
                      // console.log('favorite');
                      var workout_id = workout_id;
                        var link = baseUrl+'/workouts/Togglefavrtunfavrtworkout';
                       var formData = {uu_id : uuid, user_id : user_id ,  workout_id : workout_id}
                       var postData = 'myData='+JSON.stringify(formData);
                        $ionicLoading.show({
                               template: '<ion-spinner icon="ios"></ion-spinner>'

                           });
                      $http({
                             method : 'POST',
                             url : link,
                             data: postData,
                             headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                     }).success(function(res){

                       // console.log(res);
                        var alertPopup = $ionicPopup.alert({
                        title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                        template: res.message,
                        cssClass: 'loginErrorPopUp'
                      });

                        $ionicLoading.hide();
                     }) .error(function(error){
                       $ionicLoading.hide();
                       // console.log('request not completed');
                     });

                      }

                   $ionicModal.fromTemplateUrl('templates/modal.html', {
                      scope: $scope
                    }).then(function(modal) {
                     $scope.modal = modal;
                        // console.log('modal is opened');
                    });



  /**
   * @name addReview
   * @todo To add review to workout
   * @return void function
   *
   **/


                    $scope.addReview = function(username, email, message){
                    // console.log(username +'-----'+email+'-----'+message)
                      var user_name = username;
                      var email = email;
                      var message = message;
                      var rating = $scope.rating.rate;
                      var user_id = $scope.userData.user_id;
                      var workout_id = $stateParams.cid;

                  var link = baseUrl+'/workouts/add_workout_review';
                  var formData = {uu_id : uuid, user_id : user_id, workout_id : workout_id, message : message, email : email, user_name : user_name, rating : rating}
                  var postData = 'myData='+JSON.stringify(formData);
                      $ionicLoading.show({
                                template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                       $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                      }).success(function(res){
                        $ionicLoading.hide();
                        // console.log(res.message);

                        var alertPopup = $ionicPopup.alert({
                        title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                        template: res.message,
                        cssClass: 'loginErrorPopUp hideOk'
                      });

                          $timeout(function() {
                           alertPopup.close();
                           $scope.modal.hide();

                         }, 3000);
                         $timeout(function() {
                              ionicMaterialMotion.fadeSlideInRight({
                                  startVelocity: 3000
                              });
                          }, 700);

                        ionicMaterialInk.displayEffect();


                      }) .error(function(error){
                        $ionicLoading.hide();
                        // console.log('request not completed');
                      })
                    }



  /**
   * @name deregisterFirst
   * @todo To add back button route
   * @return void function
   *
   **/





  var deregisterFirst = $ionicPlatform.registerBackButtonAction(function(e) {

                       $scope.backView = $ionicHistory.backView();
                       // console.log($scope.backView);
                       if($scope.backView.stateName === 'app.home'){
                         $state.go('app.home');
                       } else if($scope.backView.stateName === "app.allNewsFeed"){
                         $state.go('app.allNewsFeed');
                       } else{
                         $state.go('app.workouts', {cid: workout_id});
                       }



                            // console.log('Back');
                          e.preventDefault();
                          return false;
                         }, 101);

                         $scope.$on('$destroy', deregisterFirst);



});

