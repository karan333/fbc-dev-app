/**
 * Purpose:
 *@This file is used to Show Interval Timer in Timer Section
 *
 **/
angular.module('starter').controller('intervalCtrl', function($window, quote, $rootScope,  $cordovaNetwork, deviceInformation, $scope, $cordovaSQLite, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $ionicLoading, $ionicPopup) {

  var userData = $window.localStorage['userData']
  $scope.userData = angular.fromJson(userData);
  //  console.log("Omar User data consoling "+JSON.stringify($scope.userData));

  // after getting data from sqlite getting user id and then running dashboard service
  $scope.userImg = $scope.userData.image;
  $scope.username = $scope.userData.user_fname;
  $scope.Lusername = $scope.userData.user_lname;

  // console.log("Userimg Source== "+$scope.userImg);

  var user_id = $scope.userData.user_id;

  //for quotes
  /**
   * @name getQuote
   * @todo To Get Random Quotes
   * @return void function
   *
   **/
  quote.getQuote(uuid, user_id).success(function (res) {
    // console.log(res);
    $scope.quote = res;
    $ionicLoading.hide();
  });

  /**
   * @name Timer API
   * @todo Here we are going to use Timer Library
   * @return void function
   *
   **/
  $(document).ready(function(){
    var timer = new STTabataTimerViewControllerNew();
    timer.setValues({
      userId: 0,
      presetId: 0,
      presetName: "",
      prep: 0,
      work: 0,
      rest: 0,
      cycles: 0,
      tabatas: 0,
      soundsOn: 1
    });

    timer.setLabels({
      myPresets: "My Presets",
      newPreset: "Create a New Preset",
      save: "Save",
      workout : "workout",
      tabata : "Tabata",
      prepare : "PREPARE",
      work : "WORK",
      rest : "REST",
      cycles : "Rounds",
      tabatas : "Tabatas",
      cyclesl : "ROUNDS",
      tabatasl : "tabatas",
      start : "START",
      stop : "RESET",
      pause : "STOP",
      resume : "START",
      preset : "Preset",
      sound : "Sound",
      on : "On",
      off : "Off"
    });
    timer.setSounds({
      pausingSession : "end",
      rest : "end-round",
      // sessionComplete : "warning",
      soundOn : "SoundOn",
      startingSession : "start",
      stoppingSession : "end",
      // tabataComplete : "warning",
      work : "start",
      warning : "warning"
    });
  timer.loadSounds(function(){
    timer.drawTimer("#timerContainer");
    console.log(timer.timerHasFired);
  });
  //});


});

});
