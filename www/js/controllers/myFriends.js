

/**
 * Purpose:
 *@This file is used to show friends list
 *
 **/



angular.module('starter').controller('myFriendsCtrl', function(quote, $cordovaToast, $rootScope,  $cordovaNetwork, $window, deviceInformation, $scope, $cordovaSQLite, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $ionicLoading, $ionicPopup) {
   $scope.allfriends = {};
   $scope.pendingFriends = {};
   $scope.siteUrl = siteUrl;
   $scope.deviceInfo = deviceInformation.getInfo();
   var uuid = $scope.deviceInfo.uuid;
   $scope.frndxsearch = {};
$scope.isOnline = [];
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();


        /**
         *
         * Listen for Online event
         *
         **/
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        /**
         *
         * Listen for ofline event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            // console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);
                        var userData = $window.localStorage['userData'];
                        $scope.userData = angular.fromJson(userData);

                        var user_id = $scope.userData.user_id;



  /**
   *
   * for quote
   *
   **/



                        quote.getQuote(uuid, user_id).success(function (res) {
                                            // console.log(res);
                                            $scope.quote = res;
                                            });


  /**
   *
   * to get user friend list
   *
   **/


                        var link = baseUrl+'/friends/myfriends';
                            // console.log('userID---'+ user_id + '--UUID--' + uuid);
                            var formData = {uu_id : uuid, user_id : user_id};
                            var postData = 'myData='+JSON.stringify(formData);

                            $ionicLoading.show({
                            template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                            $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                          }).success(function(res){

                              // console.log(res.friends);
                              $scope.allfriends = res.friends;

                               $ionicLoading.hide();



                        })

  /**
   *
   * to get pending friend request
   *
   **/

                         var user_id = $scope.userData.user_id;
                          var link = baseUrl+'/friends/pendingFriendRequests';
                              // console.log('userID---'+ user_id + '--UUID--' + uuid);
                              var formData = {uu_id : uuid, user_id : user_id}
                              var postData = 'myData='+JSON.stringify(formData);

                              $ionicLoading.show({
                              template: '<ion-spinner icon="ios"></ion-spinner>'

                              });
                              $http({
                                method : 'POST',
                                url : link,
                                data: postData,
                                headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                            }).success(function(res){

                                // console.log(res);
                                $scope.pendingFriends = res.friend_reqs;
                                $ionicLoading.hide();




                          })
  $scope.showSearchIcon = true;


  /**
   * @name change
   * @todo To check value inside search box
   * @return void function
   *
   **/



  $scope.change = function(text) {
    if($scope.frndxsearch.search == ''){
      $scope.showSearchIcon = true;
    }
    $scope.showSearchIcon = false;
  };



  /**
   * @name emptySearch
   * @todo To empty check box
   * @return void function
   *
   **/


  $scope.emptySearch = function(text){
    $scope.showSearchIcon = true;
    $scope.frndxsearch = {};
    $scope.frndxsearch.search = null;
    $scope.frndxsearch.search = '';
    text = '';

  };

  /**
   * @name removeFriend
   * @todo To remove friend from list
   * @return void function
   *
   **/
 $scope.removeFriend = function(id,first,last){
                             $scope.showConfirm = function () {
                               var confirmPopup = $ionicPopup.confirm({
                                 title: 'Warning',
                                 template: 'Are you sure you want to delete ' +first + ' '+last ,
                                 cssClass: 'loginErrorPopUp'

                               });

                               confirmPopup.then(function (res) {
                                 if (res) {
                                   var user_id = $scope.userData.user_id;
                                   var link = baseUrl+'/friends/remove_friend';
                                   var frnd_id = id;
                                   var formData = {uu_id : uuid, user_id : user_id, frnd_id : frnd_id}
                                   var postData = 'myData='+JSON.stringify(formData);

                                   // console.log('frnd_id---' + frnd_id + 'uuid---' + uuid + 'userID----' + user_id)


                                   $ionicLoading.show({
                                     template: '<ion-spinner icon="ios"></ion-spinner>'

                                   });
                                   $http({
                                     method : 'POST',
                                     url : link,
                                     data: postData,
                                     headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                   }).success(function(res){

                                     // console.log(res.message);
                                     // console.log(res);
                                     $scope.allfriends = res.friends;

                                     var alertPopup = $ionicPopup.alert({
                                       title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                       template: res.message,
                                       cssClass: 'loginErrorPopUp hideOk'
                                     });
                                     $timeout(function () {
                                       alertPopup.close();
                                     }, 3000);

                                     $ionicLoading.hide();
                                     $timeout(function() {
                                       ionicMaterialMotion.fadeSlideInRight({
                                         startVelocity: 3000
                                       });
                                     }, 700);
                                     ionicMaterialInk.displayEffect();
                                   });

                                 } else {

                                 }
                               });
                             };
                             $scope.showConfirm();
                          };

  /**
   * @name acceptFriend
   * @todo To accept friend request
   * @return void function
   *
   **/

                          $scope.acceptFriend = function(id,current_user_id){

                            var user_id = $scope.userData.user_id;
                            var frnd_id=0;
                            if(user_id == id)
                            {

                                frnd_id = current_user_id;

                            }
                            else
                            {

                                 frnd_id = id;

                            }
                            // console.log("Friend ID"+frnd_id);
                             var link = baseUrl+'/friends/accept_friend_request';
                             //var frnd_id = id;
                             var formData = {uu_id : uuid, user_id : user_id, frnd_id : frnd_id}
                             var postData = 'myData='+JSON.stringify(formData);

                             // console.log('frnd_id---' + frnd_id + 'uuid---' + uuid + 'userID----' + user_id)


                                 $ionicLoading.show({
                                 template: '<ion-spinner icon="ios"></ion-spinner>'

                                 });
                                 $http({
                                   method : 'POST',
                                   url : link,
                                   data: postData,
                                   headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                               }).success(function(res){

                                   // console.log(res.message);
                                   // console.log(res);
                                   $scope.allfriends = res.myfriends;
                                   $scope.pendingFriends = res.friend_reqs;
                                    var alertPopup = $ionicPopup.alert({
                                     title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                      template: res.message,
                                      cssClass: 'loginErrorPopUp'
                                   });

                               $ionicLoading.hide();
                               $timeout(function() {
                                   ionicMaterialMotion.fadeSlideInRight({
                                       startVelocity: 3000
                                   });
                               }, 700);
                               ionicMaterialInk.displayEffect();
                             });
                          }
  var element = angular.element(document.querySelector('.quoteArea'));
  var height = element[0].offsetHeight;
  // console.log(height);
  var d =  angular.element(document.querySelector('.friendsListScroll'));
  // console.log(d);
  var position = 25 + height+'px';
  // console.log(position);
  d[0].style.top = position;
});
