/**
 * Purpose:
 *@This file is used to Show All News Feeds of a User
 *
 **/

angular.module('starter').controller('notifications', function(notification, $cordovaToast, $ionicPopup, $cordovaInAppBrowser, quote, $state, $rootScope,  $cordovaNetwork, $window, $scope, $cordovaSQLite, $stateParams, $http, $timeout, $ionicSideMenuDelegate, $ionicLoading, ionicMaterialMotion, ionicMaterialInk) {


$scope.morefeeds=false;
$scope.allReturnedNews=[];
$scope.postlimit = 20;
$scope.allNews=[];
$scope.currentrecordsview = 0;
$scope.packageExpired = {};
$scope.isOnline = [];
$scope.notificationFull = [];
$scope.notification = [];
$scope.siteUrl = siteUrl;
  $scope.$on('$ionicView.beforeEnter', function(){
    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner>'

    });
    notification.get(uuid, user_id).success(function (res) {

      var notifications = res;
      $window.localStorage['notifications']  = JSON.stringify(notifications);
      var localNoti = $window.localStorage['notifications'];
      $scope.notification = angular.fromJson(localNoti);
      $scope.notificationFull = $scope.notification.notification;
      if($scope.notificationFull.length > 0){
        if($scope.notificationFull[0].count == 0){
          $scope.green = "green";
        } else if($scope.notificationFull[0].count > 0){
          $scope.green = "red";
        }
      }

      $scope.changeColor = function(){
        $scope.green = "green";

        //;


      }
    });
  });
  $scope.$on('$ionicView.afterEnter', function(){
    $ionicLoading.hide();
  });
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();

        /**
         *
         * Listen for Online event
         *
         **/
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        /**
         *
         * Listen for Offline event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){

            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);





                      var userData = $window.localStorage['userData'];
                      $scope.userData = angular.fromJson(userData);
  var packageExpired = $window.localStorage['expired'];
  $scope.packageExpired = angular.fromJson(packageExpired);
                      // after getting data from sqlite getting user id and then all news feed service


                      var user_id = $scope.userData.user_id;
                      quote.getQuote(uuid, user_id).success(function (res) {
                                                                            // console.log(res);
                                                                            $scope.quote = res;
                                                                            });

  $scope.moveNotification = function(id){

    var bit = $scope.notificationFull[id].bit;
    var workout_id = $scope.notificationFull[id].extra_info;
    var status_id = $scope.notificationFull[id].status_id;
    var noti_id = $scope.notificationFull[id].id;
    var person = $scope.notificationFull[id].sender;
    if(bit === 'status' && (workout_id === null || workout_id === '')){
      $state.go('app.singlePost', {cid: status_id, did : noti_id});
    } else if(bit === 'share_workout' && workout_id !== null){
      $state.go('app.myWorkoutDetailMain', {result: workout_id, cid : bit, did : person, nid : noti_id});
    } else if(bit === 'friend_request' && (workout_id === null || workout_id === '')){
      $state.go('app.friends.myFriends.friendsR', {cid: status_id, did : noti_id});
    }else if(bit === 'accept_friend_request' && (workout_id === null || workout_id === '')){
      $state.go('app.friends.myFriends.myFriendsI', {cid: status_id, did : noti_id});
    }
    var link = baseUrl+'/dashboard/notificationStatus';
    // console.log('userID---'+ user_id + '--UUID--' + uuid);
    var formData = {uu_id : uuid, user_id : user_id, status_id : status_id, noti_id : noti_id};
    var postData = 'myData='+JSON.stringify(formData);


    $http({
      method : 'POST',
      url : link,
      data: postData,
      headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

    }).success(function(res){

      // console.log(res);
      $scope.post = res.dashboard_data.userStatus;
      if($scope.post === '' || $scope.post === undefined){
        $scope.noStatus = true;
      }


    })
      .error(function(error){
        // console.log('request not completed')
      })
      .finally(function($ionicLoading) {
        // On both cases hide the loading
        $scope.hide($ionicLoading);
      });

  };


});
