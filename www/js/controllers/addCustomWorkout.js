	/**
	* Purpose:
	*@This file is used to save and create Custom Workouts.
	*
	**/


angular.module('starter').controller('addCustomWorkout', function ($cordovaToast, quote, $rootScope, $cordovaNetwork, $ionicModal, focus, $ionicScrollDelegate, $ionicPopover, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading, $cordovaFileTransfer) {
	/**
	* @name ionicPopover
	* @todo To Show Popover
	* @return void function
	*
	**/
  $ionicPopover.fromTemplateUrl('templates/popover.html', {
    scope: $scope
  }).then(function (popover) {
    $scope.popover = popover;
  });

  $scope.deviceInfo = deviceInformation.getInfo();
  var uuid = $scope.deviceInfo.uuid;
  $scope.searchStrman = {};
  $scope.layout = 'list';
  $scope.refine = {};
  $scope.baseUrlForWorkoutImg = 'http://www.fitnessbasecamp.com/uploads/workouts/';
//console.log($scope.workouts.search);
  $scope.moreworkouts = false;
  $scope.allWorkoutss = [];
  $scope.myExercises = [];
  $scope.sharedExercises = [];
  $scope.merged_array = [];
  $scope.showReorder = false;
  $scope.refine2 = {};
  $scope.postlimit = 20;
  $scope.workoutss = [];
  $scope.currentrecordsview = 0;
  var user_id = "";
  $scope.total = '';

  $scope.user = {
    merged_array: []
  };
  $scope.user.workoutss = [];
  $scope.user.myworkoutss = [];
  $scope.user.sharedworkoutss = [];

	/**
	* @name uncheckAll
	* @todo To uncheck Exercises
	* @return void function
	*
	**/
  $scope.uncheckAll = function () {
    $scope.previewWorkout = [];
    $scope.total = "";
  };
  /**
  * @name clearData
  * @todo To Clear all custom workout data
  * @return void function
  *
  **/

  $scope.clearData = function(){
    $scope.showConfirm = function() {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Confirm',
        template: 'Are you sure you want to reset all the data in this custom workout?”  ',
        cssClass: 'loginErrorPopUp',
        buttons: [
          { text: 'No' },
          {
            text: 'Yes',
            type: 'button-positive',
            onTap: function(e) {
              $ionicLoading.show({
                template: '<ion-spinner icon="ios"></ion-spinner>'

              });
              $scope.uncheckAll();
              $scope.modal_info.hide();
              $scope.modal.hide();
              $scope.modal.remove();
              $scope.modal_info.remove();
              $scope.refine.Exname = '';
              $scope.refine.Selectedtype = '';
              $scope.refine.Selectedequip = '';
              $scope.refine.Selectedlevel = '';
              $scope.refine.Selectedintensity = '';
              $scope.refine.SelectedPrivacy = '';
              $scope.refine.ExUrl = '';
              $scope.refine.ExDescription = '';
              $scope.refine.pic = '';
              $ionicLoading.hide();
            }
          }
        ]

      });

      confirmPopup.then(function(res) {
        console.log('tapped !' + res);
        if(res)
		{
        }
		else
		{
        }
      });
    };

    $scope.showConfirm();

  };

	/**
	* @name cancelAddingWorkout
	* @todo To Cancel custom workout
	* @return void function
	*
	**/
  $scope.cancelAddingWorkout = function(){
    $scope.showConfirm = function() {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Confirm',
        template: 'Are you sure you want to cancel this custom workout?',
        cssClass: 'loginErrorPopUp',
        buttons: [
          { text: 'No' },
          {
            text: 'Yes',
            type: 'button-positive',
            onTap: function(e) {
              $ionicLoading.show({
                template: '<ion-spinner icon="ios"></ion-spinner>'

              });
              $scope.modal_info.hide();
              $scope.modal.hide();
              $scope.modal.remove();
              $scope.modal_info.remove();
              $state.go('app.workouts');
              $ionicLoading.hide();
            }
          }
        ]

      });

      confirmPopup.then(function(res) {
        if(res)
		{
        }
		else
		{
          console.log('You are not sure');
        }
      });
    };

    $scope.showConfirm();
  };

	/**
	*
	* Initializing Variables, Arrays, and Objects
	*
	**/

  $scope.active = true;
  $scope.active1 = true;
  $scope.refine = {};
  $scope.refine.Exname = "";
  $scope.refine.pic = 'img/no-image.jpg'
  $scope.ex_pic_name = "";
  $scope.previewWorkout = [];
  $scope.siteUrl = siteUrl;
  $scope.isOnline = [];
  $ionicSideMenuDelegate.canDragContent(false);
  document.addEventListener("deviceready", function () {
    $scope.network = $cordovaNetwork.getNetwork();
    $scope.isOnline = $cordovaNetwork.isOnline();


	/**
	*
	* Listen for Online event
	*
	**/
    $rootScope.$on('$cordovaNetwork:online', function (event, networkState) {
      $scope.isOnline = true;
      $scope.network = $cordovaNetwork.getNetwork();
      $cordovaToast.showLongBottom('You are Online !')

    })

	/**
	*
	* Listen for ofline event
	*
	**/
    $rootScope.$on('$cordovaNetwork:offline', function (event, networkState) {
      console.log("got offline");
      $scope.isOnline = false;
      $scope.network = $cordovaNetwork.getNetwork();
      $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
    })

  }, false);
  var userData = $window.localStorage['userData'];
  $scope.userData = angular.fromJson(userData);


  user_id = $scope.userData.user_id;
  quote.getQuote(uuid, user_id).success(function (res) {
    // console.log(res);
    $scope.quote = res;
  });

  /**
   *
   * Get Custom Workout Exercises
   *
   **/

  var link = baseUrl + '/exercise/custom_workoutexercises';
  var formData = {uu_id: uuid, user_id: user_id}
  var postData = 'myData=' + JSON.stringify(formData);
  $ionicLoading.show({
    template: '<ion-spinner icon="ios"></ion-spinner>'

  });
  $http({
    method: 'POST',
    url: link,
    data: postData,
    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

  }).success(function (res) {


    $scope.checked = true;

    $scope.allWorkoutss = res.exercise;
    $scope.myExercises = res.myexercises;
    $scope.sharedExercises = res.shared_exercises;
    res.exercise = angular.fromJson(res.exercise);

    for (var i = 0; i < $scope.postlimit; i++) {
      if (i < $scope.allWorkoutss.length) {
        $scope.workoutss.push(res.exercise[i]);
      }
    }
    $scope.currentrecordsview = $scope.postlimit;
    $scope.moreworkouts = true;


    $ionicLoading.hide();
  }).error(function (error) {
    $ionicLoading.hide();
    // console.log('request not completed');
  });

  /**
   *
   * @name nextForm
   * @purpose Get Next Form
   **/

  $scope.nextForm = function () {

  }


//on swipe right
  /**
   *
   * @name swipeToAdd
   * @purpose On Swipe add Workouts
   **/

  $scope.swipeToAdd = function (workout) {
    var workout = JSON.stringify( workout, function( key, value ) {
      if( key === "$$hashKey" ) {
        return undefined;
      }
      return value;
    });
    workout = angular.fromJson(workout);
    $scope.previewWorkout.push(workout);

    console.log( $scope.previewWorkout);
    var alertPopup = $ionicPopup.alert({
      title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
      template: workout.workout_name + " Added to Workout",
      cssClass: 'loginErrorPopUp notOK'
    });
    $timeout(function () {
      alertPopup.close();

    }, 2000);
  }


  /**
   *
   * @todo To Filter Workouts
   *
   *
   **/

  var link = baseUrl + '/workouts/workoutfilter';
  var formData = {uu_id: uuid, user_id: user_id}
  var postData = 'myData=' + JSON.stringify(formData);
  $ionicLoading.show({
    template: '<ion-spinner icon="ios"></ion-spinner>'

  });
  $http({
    method: 'POST',
    url: link,
    data: postData,
    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

  }).success(function (res) {

    $scope.refine = res.workoutsfilter;



  }).error(function (error) {
    $ionicLoading.hide();

  })


////////////////////////// End Workout Filters ///////////////

  /**
   * @name selectPicture
   * @todo To Select Picture
   * @return void function
   *
   **/
  $scope.selectPicture = function () {
    document.addEventListener("deviceready", function () {
      var options = {
        quality: 100,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 250,
        targetHeight: 250,
        allowEdit: true
      };

      $cordovaCamera.getPicture(options).then(
        function (imageURI) {
          $scope.uploadPicturex(imageURI);

          $ionicLoading.show({template: 'Uploading Photo...', duration: 500});
        },
        function (err) {
          $ionicLoading.show({template: 'Error While Uploading Image...', duration: 500});
        })
    }, false);
  };

  /**
   * @name uploadPicturex
   * @todo To Upload Pictures
   * @return void function
   *
   **/

  $scope.uploadPicturex = function (imageURI) {


    var options = {

      fileKey: "picture",

      filename: "p1.png",

      chunkedMode: false,

      mimeType: "image/png",
      trustAllHosts: true,
      headers: {Connection: "close"}

    };
    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner><p>Uploading Image!</p>'

    });
    $cordovaFileTransfer.upload(encodeURI(baseUrl + "/exercise/upload_exercise_images"), imageURI, options)

      .then(function (result) {
        // console.log(result.response);
        $scope.refine.pic = imageURI;
        ;
        var resk = angular.fromJson(result.response);

        $scope.ex_pic_name = resk.image_name;

        // console.log($scope.upload);
        $ionicLoading.hide();


      }, function (err) {

        // console.log(err);
        $ionicLoading.hide();
        $ionicLoading.show({
          template: 'An error occured while uploading your picture.<br>Please try again.',
          duration: 500
        });

      }, function (progress) {

      });


  }


  /**
   * @name addWorkout
   * @todo To Add Workout
   * @return void function
   *
   **/

  $scope.addWorkout = function (name, type, equip, level, intensity, privacy_status, url, description) {
    var exercises = $scope.previewWorkout;
    exercises.forEach(function(v){
      delete v.download_url;
      delete v.download_url_linl;
      delete v.description;
    });
    user_id = $scope.userData.user_id;
    var link = baseUrl + '/workouts/add_custom_workout';
    var formData = {
      uu_id: uuid,
      user_id: user_id,
      workout_name: name,
      embed_url: url,
      workout_type: type,
      workout_intensity: intensity,
      difficulty_level: level,
      privacy_status: privacy_status,
      equipment_used: equip,
      description: description,
      exercise_image: $scope.ex_pic_name,
      exercises: exercises
    }
    var postData = 'myData=' + JSON.stringify(formData);
    // console.log(formData);
    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner>'

    });
    $http({
      method: 'POST',
      url: link,
      data: postData,
      headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

    }).success(function (res) {
      $ionicLoading.hide();
      // console.log(res);
      if (res.code == 400) {
        $ionicLoading.show({template: 'Something went wrong.', duration: 500});
      }
      else {
        $ionicLoading.show({template: 'Workout Added Successfully', duration: 500});
//
        $timeout(function () {
          $scope.modal_info.hide();
          $scope.modal.hide();
          $scope.modal.remove();
          $scope.modal_info.remove();
          $state.go('app.myWorkouts');

        }, 500);


//

      }

    }).error(function (error) {
      $ionicLoading.hide();
      // console.log('request not completed');
    })
  };


  /**
   * @name previewNext
   * @todo To Preview Next Exercise
   * @return void function
   *
   **/

  $scope.previewNext = function(){
    $ionicModal.fromTemplateUrl('templates/modal.html', {
      id: 'previewModal',
      scope: $scope,
      animation: 'animated slideInRight',
      backdropClickToClose: false

    }).then(function (modal) {
      $scope.modal = modal;
      modal.show();


    });
  };

  /**
   * @name fromTemplateUrl
   * @todo To Preview Added Exercises
   * @return void function
   *
   **/

  $ionicModal.fromTemplateUrl('templates/modal.html', {
    id: 'previewModal',
    scope: $scope,
    animation: 'animated slideInRight',
    backdropClickToClose: false

  }).then(function (modal) {
    $scope.modal = modal;


  });

  /**
   * @name fromTemplateUrl
   * @todo To Provide information about Workout Exercises
   * @return void function
   *
   **/
  $ionicModal.fromTemplateUrl('templates/modal_info.html', {
    id: 'infoModal',
    scope: $scope,
    animation: 'animated slideInRight',
    backdropClickToClose: false

  }).then(function (modal) {
    $scope.modal_info = modal;

  });

  /**
   * @name goToDetailPage
   * @todo To Move Detail Page
   * @return void function
   *
   **/

  $scope.goToDetailPage = function () {
    $ionicModal.fromTemplateUrl('templates/modal_info.html', {
      id: 'infoModal',
      scope: $scope,
      animation: 'animated slideInRight',
      backdropClickToClose: false

    }).then(function (modal) {
      $scope.modal_info = modal;
      modal.show();


    });

  };

  /**
   * @name goBackExList
   * @todo To Go Back on Exercise Preview
   * @return void function
   *
   **/
  $scope.goBackExList = function () {
    $scope.modal.hide();
    $scope.modal_info.hide();
    $ionicModal.fromTemplateUrl('templates/modal.html', {
      id: 'previewModal',
      scope: $scope,
      animation: 'animated slideInRight',
      backdropClickToClose: false

    }).then(function (modal) {
      $scope.modal = modal;
      modal.show();

    });

  };
  /**
   * @name reorderItem
   * @todo To Re order Exercises
   * @return void function
   *
   **/
  $scope.reorderItem = function (item, fromIndex, toIndex) {
    // console.log("in Reorder");
    $scope.previewWorkout.splice(fromIndex, 1);
    $scope.previewWorkout.splice(toIndex, 0, item);
  };

  /**
   * @name onItemDelete
   * @todo To Delete Exercise from list in Add Custom Workout preview modal
   * @return void function
   *
   **/

  $scope.onItemDelete = function (item) {
    // console.log("All PreviewWork Length : " + $scope.previewWorkout.length);
    $scope.previewWorkout.splice($scope.previewWorkout.indexOf(item), 1);
    $scope.sumDuration();
  };

  /**
   * @name focussearch
   * @todo To Focus the input in search
   * @return void function
   *
   **/

  $scope.focussearch = function () {
    // do something awesome
    $ionicScrollDelegate.scrollTop();
    focus('searchStrman');
    $scope.show = "showInput";

  };
  /**
   * @name showSearchIcon
   * @todo To Show Search Icon
   * @return void function
   *
   **/
  $scope.showSearchIcon = true;
  $scope.change = function (text) {
    if ($scope.searchStrman.search == '') {
      $scope.showSearchIcon = true;
    }

    $ionicScrollDelegate.resize();
    $scope.loadmoreworkouts();
    angular.element(document.querySelectorAll('.allWorkoutsStack')).triggerHandler('click');
    $scope.showSearchIcon = false;
  };
  /**
   * @name emptySearch
   * @todo To Show Search Icon
   * @return void function
   *
   **/
  $scope.emptySearch = function (text) {
    $scope.showSearchIcon = true;
    $scope.searchStrman = {};
    $scope.searchStrman.search = null;
    $scope.searchStrman.search = '';
    text = '';

  };

  /**
   * @name loadmoreworkouts
   * @todo To Show More Workouts
   * @return void function
   *
   **/

  $scope.loadmoreworkouts = function () {

    // console.log("In Scroll Workouts");
    var lim = $scope.currentrecordsview + $scope.postlimit;

    for (var i = $scope.currentrecordsview; i < lim; i++) {
      if (i < $scope.allWorkoutss.length) {
        $scope.workoutss.push($scope.allWorkoutss[i]);
      }
      else {
        $scope.moreworkouts = false;
        break;
      }
    }

    $scope.currentrecordsview = lim;
    $scope.$broadcast('scroll.infiniteScrollComplete');
  };

  /**
   * @name sumDuration
   * @todo To Count Duration
   * @return void function
   *
   **/
  $scope.sumDuration = function () {
    var total = 0;
    for (var i = 0; i < $scope.previewWorkout.length; i++) {
      var duration = $scope.previewWorkout[i];
      // console.log(duration);
      if (duration.EXduration !== undefined) {
        total += (duration.EXduration);
      }
    }
    var date = new Date(null);
    date.setSeconds(total); // specify value for SECONDS here
    $scope.total = date.toISOString().substr(11, 8);
  };

  /**
   * @name filterExercise
   * @todo To Filter Exercise
   * @return void function
   *
   **/


  $scope.filterExercise = function () {

    var ex_type = $scope.refine2.Selectedtype;
    var body_part = $scope.refine2.bodyPartid;
    var machines = $scope.refine2.Selectedequip;
    var level = $scope.refine2.Selectedlevel;
    var custom = 'custom';

    ////////////////// Filter Request Start/////////////////

    var link = baseUrl + '/exercise/search_exercises';
    var formData = {
      uu_id: uuid,
      user_id: user_id,
      exerciseType: ex_type,
      bodyPart: body_part,
      wLevels: level,
      exmachines: machines,
      bit: custom
    }
    var postData = 'myData=' + JSON.stringify(formData);
    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner>'

    });
    $http({
      method: 'POST',
      url: link,
      data: postData,
      headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

    }).success(function (res) {

      // console.log(res);
      $scope.NoResultFound = res.code;
      $scope.workoutss = res.exercises;
      $scope.modal_filter.hide();
      $ionicLoading.hide();


    }).error(function (error) {
      $ionicLoading.hide();
      // console.log('request not completed');
    }).finally(function ($ionicLoading) {
      // On both cases hide the loading
      $ionicLoading.hide();
    });


  };

  $scope.modal_filter = function () {
    $scope.modal_filter.hide();
  };

  $ionicModal.fromTemplateUrl('templates/modal_filter.html', {
    scope: $scope
  }).then(function (modal) {
    $scope.modal_filter = modal;
    var user_id = $scope.userData.user_id;
    var link = baseUrl + '/exercise/exercise_filters';
    var formData = {uu_id: uuid, user_id: user_id};
    var postData = 'myData=' + JSON.stringify(formData);
    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner>'

    });
    $http({
      method: 'POST',
      url: link,
      data: postData,
      headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

    }).success(function (res) {

      $scope.refine2 = res.exercise_filters;


    }).error(function (error) {
      $ionicLoading.hide();
      // console.log('request not completed');
    })
  });

  $scope.modal_filter = function () {
    $scope.modal_filter.show();
  };


  ///////////////// Filter Request End ///////////////////


  $scope.$on('$stateChangeSuccess', function () {
    $scope.loadmoreworkouts();
  });


});
