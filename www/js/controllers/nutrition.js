angular.module('starter').controller('nutritionCtrl', function($rootScope,  $cordovaNetwork, deviceInformation, $scope, $cordovaSQLite, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $ionicLoading, $ionicPopup) {
$scope.isOnline = [];
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();


        // listen for Online event
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        // listen for Offline event
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);
 $timeout(function() {
      ionicMaterialMotion.fadeSlideInRight({
          startVelocity: 3000
      });
  }, 700);

  // Set Ink
  ionicMaterialInk.displayEffect();


});
