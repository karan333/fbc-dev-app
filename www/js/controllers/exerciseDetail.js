/**
 * Purpose:
 *@This file is used to Show Exercise Details
 *
 **/

angular.module('starter').controller('exerciseDetailCtrl', function($ionicHistory, $ionicPlatform, $cordovaToast, quote, $log, $document, $ionicSlideBoxDelegate, $ionicPopover, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading, $cordovaFileTransfer) {
 // console.log(GuserData.user_fname);


$scope.deviceInfo = deviceInformation.getInfo();
var uuid = $scope.deviceInfo.uuid;
$scope.siteUrl = siteUrl;
$scope.exercise = {};
$scope.addReview = {};
$scope.baseUrlForWorkoutImg = 'http://www.fitnessbasecamp.com/uploads/workouts/';
$scope.baseUrlForWorkoutPoster = 'http://www.fitnessbasecamp.com/uploads/category-images/';
$scope.reviews = {};
 //rating

$scope.rating = {};
$scope.rating.rate = 0;
$scope.rating.max = 5;

$scope.rated = {};
$scope.rated.rate = 3;
$scope.rated.max = 5;

$scope.readOnly = true;

              var userData = $window.localStorage['userData'];
              $scope.userData = angular.fromJson(userData);

$scope.addReview_username = $scope.userData.user_fname;
$scope.addReview_email = $scope.userData.user_email;
$scope.addReview_message = '';

              // after getting data from sqlite getting user id and then all news feed service

              var user_id = $scope.userData.user_id;
               quote.getQuote(uuid, user_id).success(function (res) {
                                                                  // console.log(res);
                                                                  $scope.quote = res;
                                                                  });
              var exercise_id = $stateParams.cid;
              var prevWorkoutID= $stateParams.did;

              var link = baseUrl+'/exercise/exercise_details';
              var formData = {uu_id : uuid, user_id : user_id, exercise_id : exercise_id};
              var postData = 'myData='+JSON.stringify(formData);
                  $ionicLoading.show({
                            template: '<ion-spinner icon="android"></ion-spinner>'

                        });
                   $http({
                          method : 'POST',
                          url : link,
                          data: postData,
                          headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                  }).success(function(res){
                    $ionicLoading.hide();
                   // console.log(res.exercise);
                   $scope.exercise = res.exercise[0];
                    $scope.reviews = res.exercise[0].reviews;
                    $scope.rated.rate =  res.exercise[0].ratings;
                    $scope.suggestedWorkouts = res.exercise[0].suggestedWorkouts;
                    $ionicSlideBoxDelegate.update();

                     /**
                      *
                      * @todo Checking if Exercise Exist
                      *
                      *
                      **/
                    var filename = res.exercise[0].vimeo_id+".mp4";
                    // console.log(filename);
                      var targetPath = cordova.file.dataDirectory + 'FitnessTest/' + filename;
                    $cordovaFile.checkFile(cordova.file.dataDirectory+'FitnessTest/',filename)
                      .then(function (success) {

                      $scope.vid_srcc=cordova.file.dataDirectory+'FitnessTest/'+res.exercise[0].vimeo_id+".mp4";
                       $timeout(function() {
                              ionicMaterialMotion.fadeSlideInRight({
                                  startVelocity: 3000
                              });
                          }, 700);

                          // Set Ink
                          ionicMaterialInk.displayEffect();

                           $ionicSlideBoxDelegate.update();

                  }, function (error) {

                      $scope.loaderShow = true;
                      ////////////////////////////

                     $cordovaFileTransfer.download(res.exercise[0].download_url_linl, targetPath, {}, true).then(function (result) {
                                   // console.log('Save file on '+targetPath+' success!');
                                   $ionicLoading.hide();
                                   $scope.loaderShow = false;
                                    $scope.vid_srcc=cordova.file.dataDirectory+'FitnessTest/'+res.exercise[0].vimeo_id+".mp4";
                                    $timeout(function() {
                                           ionicMaterialMotion.fadeSlideInRight({
                                               startVelocity: 3000
                                           });
                                       }, 700);

                                       // Set Ink
                                       ionicMaterialInk.displayEffect();

                                        $ionicSlideBoxDelegate.update();

                         }, function (error) {

//                                       console.log('Error Download file' + console.log(error));

                         }, function (progress) {
                         // console.log((progress.loaded / progress.total) * 100);
                         $scope.progresss = 0;
                         $scope.progresss = (progress.loaded / progress.total) * 100;
                         $scope.progresss = Math.floor($scope.progresss)+"%";




                         });

                  });


                   ////////// End Checking ////////////////////////////////////////







                  }) .error(function(error){
                    $ionicLoading.hide();
                    $scope.loaderShow = false;
                    // console.log('request not completed');
                  })


  /**
   * @name favorite
   * @todo To Add Favorite Exercise
   * @return void function
   *
   **/
                  $scope.favorite = function(exercise_id){
                    // console.log('favorite');
                    var exercise_id = exercise_id;
                      var link = baseUrl+'/exercise/Togglefavrtunfavrtexercise';
                     var formData = {uu_id : uuid, user_id : user_id ,  exercise_id : exercise_id}
                     var postData = 'myData='+JSON.stringify(formData);
                      $ionicLoading.show({
                             template: '<ion-spinner icon="ios"></ion-spinner>'

                         });
                    $http({
                           method : 'POST',
                           url : link,
                           data: postData,
                           headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                   }).success(function(res){

                     // console.log(res);
                      var alertPopup = $ionicPopup.alert({
                      title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                      template: res.message,
                      cssClass: 'loginErrorPopUp'
                    });

                      $ionicLoading.hide();
                   }) .error(function(error){
                     $ionicLoading.hide();
                     // console.log('request not completed');
                   });

                    }

  /**
   * @name addReview
   * @todo To Add Review About Exercise
   * @return void function
   *
   **/


                                     $scope.addReview = function(username, email, message){
                                                         // console.log(username +'-----'+email+'-----'+message)
                                                           var user_name = username;
                                                           var email = email;
                                                           var message = message;
                                        var rating = $scope.rating.rate;
                                        var user_id = $scope.userData.user_id;
                                        var workout_id = $stateParams.cid;

                                    var link = baseUrl+'/exercise/add_exercise_review';
                                    var formData = {uu_id : uuid, user_id : user_id, workout_id : workout_id, message : message, email : email, user_name : user_name, rating : rating}
                                    var postData = 'myData='+JSON.stringify(formData);
                                        $ionicLoading.show({
                                                  template: '<ion-spinner icon="ios"></ion-spinner>'

                                              });
                                         $http({
                                                method : 'POST',
                                                url : link,
                                                data: postData,
                                                headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                        }).success(function(res){
                                          $ionicLoading.hide();
                                          // console.log(res.message);

                                          var alertPopup = $ionicPopup.alert({
                                          title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                          template: res.message,
                                          cssClass: 'loginErrorPopUp'
                                        });

                                            $timeout(function() {
                                             alertPopup.close();
//                                             $scope.modal.hide();

                                           }, 5000);
                                           $timeout(function() {
                                                ionicMaterialMotion.fadeSlideInRight({
                                                    startVelocity: 3000
                                                });
                                            }, 700);

                                          ionicMaterialInk.displayEffect();


                                        }) .error(function(error){
                                          $ionicLoading.hide();
                                          // console.log('request not completed');
                                        })
                                      }
  var deregisterFirst = $ionicPlatform.registerBackButtonAction(function(e) {

    $scope.backView = $ionicHistory.backView();
    // console.log($scope.backView);
    if($scope.backView.stateName === 'app.home'){
      $state.go('app.home');
    } else if($scope.backView.stateName === "app.allNewsFeed"){
      $state.go('app.allNewsFeed');
    } else if($scope.backView.stateName === 'app.workoutDetailMain'){
      $state.go('app.workoutDetailMain', {cid: prevWorkoutID});
    } else if($scope.backView.stateName === "app.myWorkoutDetailMain"){
      $state.go('app.myWorkoutDetailMain', {result: prevWorkoutID});
    } else{
      $state.go('app.exercises');
    }



    // console.log('Back');
    e.preventDefault();
    return false;
  }, 101);

  $scope.$on('$destroy', deregisterFirst);


});
