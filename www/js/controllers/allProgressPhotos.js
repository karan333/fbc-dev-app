/**
 * Purpose:
 *@This file is used to Show All Progress Photos
 *
 **/

angular.module('starter').controller('allProgressPhotos', function(notification, $cordovaToast, $cordovaInAppBrowser, quote, $rootScope,  $cordovaNetwork, $window, deviceInformation, $state,$scope, $cordovaSQLite, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $ionicLoading, $ionicPopup) {

    $scope.siteUrl = siteUrl;
  $scope.packageExpired = {};
    $scope.allPhotos = {};
    $scope.toggleDisplay = {};
    $scope.deviceInfo = deviceInformation.getInfo();
    var uuid = $scope.deviceInfo.uuid
    // console.log(uuid);
$scope.isOnline = [];
$scope.permissions= angular.fromJson($window.localStorage['Permissions']);
$scope.inArray = function (needle) {
haystack  = $scope.permissions;
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle){ return true;

        } ;
    }
    return "noPermission";
}
  $scope.notificationFull = [];
  $scope.notification = [];
  $scope.$on('$ionicView.beforeEnter', function(){
    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner>'

    });
    notification.get(uuid, user_id).success(function (res) {

      var notifications = res;
      $window.localStorage['notifications']  = JSON.stringify(notifications);
      var localNoti = $window.localStorage['notifications'];
      $scope.notification = angular.fromJson(localNoti);
      $scope.notificationFull = $scope.notification.notification;
      if($scope.notificationFull.length > 0){
        if($scope.notificationFull[0].count == 0){
          $scope.green = "green";
        } else if($scope.notificationFull[0].count > 0){
          $scope.green = "red";
        }
      }

      $scope.changeColor = function(){
        $scope.green = "green";

        //;


      }
    });
  });
  /**
   * @name Npermission
   * @todo To Check if user have the permission or user have to upgrade the package
   * @return void function
   *
   **/
 $scope.Npermission = function(is_perm)
                                  {

                                      if(is_perm != true)
                                       {

                                          var alertPopup = $ionicPopup.alert({
                                            title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                            template: "You don't have permission to do this.Please upgrade your package.",
                                            cssClass: 'loginErrorPopUp'
                                          });
                                       }

                                  }
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();

        /**
         *
         * Listen for Online event
         *
         **/
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        /**
         *
         * Listen for offline event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            // console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);
                      var userData = $window.localStorage['userData'];
                      $scope.userData = angular.fromJson(userData);
  var packageExpired = $window.localStorage['expired'];
  $scope.packageExpired = angular.fromJson(packageExpired);
                      // after getting data from sqlite getting user id and then all news feed service

                      var user_id = $scope.userData.user_id;
                       quote.getQuote(uuid, user_id).success(function (res) {
                                                                          // console.log(res);
                                                                          $scope.quote = res;
                                                                          });
                      var link = baseUrl+'/friends/progressphotos';
                          // console.log('userID---'+ user_id + '--UUID--' + uuid);
                          var formData = {uu_id : uuid, user_id : user_id}
                          var postData = 'myData='+JSON.stringify(formData);

                          $ionicLoading.show({
                          template: '<ion-spinner icon="ios"></ion-spinner>'

                          });
                          $http({
                            method : 'POST',
                            url : link,
                            data: postData,
                            headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                        }).success(function(res){
                           $ionicLoading.hide();
                            // console.log(res);
                            $scope.allPhotos = res;

                            //for lock unlock class

/**
* @name toggleDashboard
* @todo To Hide and Show dashboard
* @return void function
*
**/


                            $scope.toggleDashboard= function(id){
                                 if(id=="1")
                                    return "hide";
                                 else if(id=="0")
                                     return "show";
                                 else
                                     return "nothing";
                                };
                            $scope.toggleDashboard();

                             $timeout(function() {
                                ionicMaterialMotion.fadeSlideInRight({
                                    startVelocity: 3000
                                });
                            }, 700);

                            // Set Ink
                            ionicMaterialInk.displayEffect();



                      })

                      //delete progress Picture
  /**
   * @name deletePP
   * @todo To Delete Progress Photo
   * @return void function
   *
   **/

                      $scope.deletePP = function(id){

                      $scope.showConfirm = function() {
                         var confirmPopup = $ionicPopup.confirm({
                           title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-close"></span></div></div>',
                           cssClass: 'animated jello progPicDelPop',
                           template: '<div class="PPdelBody"><span class="title">Warning</span><span class="popmessage">Are you sure you want to delete this series of progress photos ? You will not be able to retrieve them once deleted.</span></div>'
                         });

                         confirmPopup.then(function(res) {
                           if(res) {
                              // console.log('delete btn');
                               var progress_id = id;
                               // console.log(progress_id);
                                var user_id = $scope.userData.user_id;
                               var link = baseUrl+'/dashboard/remove_progress_photo';
                                   // console.log('userID---'+ user_id + '--UUID--' + uuid + 'progress_id' + progress_id);
                                   var formData = {uu_id : uuid, user_id : user_id, progress_id : progress_id}
                                   var postData = 'myData='+JSON.stringify(formData);

                                   $ionicLoading.show({
                                   template: '<ion-spinner icon="ios"></ion-spinner>'

                                   });
                                   $http({
                                     method : 'POST',
                                     url : link,
                                     data: postData,
                                     headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                 }).success(function(res){
                                    $ionicLoading.hide();
                                     // console.log(res);
                                    $scope.allPhotos = res;
                                      var alertPopup = $ionicPopup.alert({
                                        title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                        template: res.message,
                                        cssClass: 'loginErrorPopUp hideOk'
                                      });
                                     $timeout(function () {
                                       alertPopup.close();
                                     }, 3000);

                                      $timeout(function() {
                                      ionicMaterialMotion.fadeSlideInRight({
                                          startVelocity: 3000
                                          });
                                      }, 700);

                                      // Set Ink
                                      ionicMaterialInk.displayEffect();
                               })
                           } else {
                             // console.log('You are not sure');
                           }
                         });
                       };
                       $scope.showConfirm();


                      }
  /**
   * @name editProgress
   * @todo To EditProgress Photo
   * @return void function
   *
   **/
    $scope.editProgress = function(result)
    {

      $state.go('app.editprogressphotos', {result: result});




    }
  /**
   * @name toggleDisplay
   * @todo To Hide and Show the Progress Photos from Friends
   * @return void function
   *
   **/
                      $scope.toggleDisplay = function(id, display){
                        if(display === '0'){
                          $scope.display = 'Your friends will be able to see this series of photos';
                        }else if(display === '1'){
                          $scope.display = 'These photos will be hidden from your friends';
                        }
                          $scope.showConfirm = function() {
                            var confirmPopup = $ionicPopup.confirm({
                              title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-close"></span></div></div>',
                              cssClass: 'animated jello progPicDelPop',
                              template: '<div class="PPdelBody"><span class="title">Warning</span><span class="popmessage">'+$scope.display+'</span></div>'
                            });

                            confirmPopup.then(function(res) {
                              if(res) {
                                var progress_id = id;
                                // console.log(progress_id);
                                var user_id = $scope.userData.user_id;
                                var link = baseUrl+'/dashboard/toggle_progress_dsp_db';
                                // console.log('userID---'+ user_id + '--UUID--' + uuid + 'progress_id' + progress_id);
                                var formData = {uu_id : uuid, user_id : user_id, progress_id : progress_id}
                                var postData = 'myData='+JSON.stringify(formData);

                                $ionicLoading.show({
                                  template: '<ion-spinner icon="ios"></ion-spinner>'

                                });
                                $http({
                                  method : 'POST',
                                  url : link,
                                  data: postData,
                                  headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                }).success(function(res){
                                  $ionicLoading.hide();
                                  // console.log(res);
                                  $scope.showAlert = function() {
                                    var alertPopup = $ionicPopup.alert({
                                      title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                      template: res.message,
                                      cssClass: 'loginErrorPopUp hideOk'
                                    });
                                    $timeout(function () {
                                      alertPopup.close();
                                    }, 3000);

                                    alertPopup.then(function(res) {
                                      var user_id = $scope.userData.user_id;
                                      var link = baseUrl+'/friends/progressphotos';
                                      // console.log('userID---'+ user_id + '--UUID--' + uuid);
                                      var formData = {uu_id : uuid, user_id : user_id}
                                      var postData = 'myData='+JSON.stringify(formData);

                                      $ionicLoading.show({
                                        template: '<ion-spinner icon="ios"></ion-spinner>'

                                      });
                                      $http({
                                        method : 'POST',
                                        url : link,
                                        data: postData,
                                        headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                      }).success(function(res){
                                        $ionicLoading.hide();
                                        // console.log(res);
                                        $scope.allPhotos = res;
                                        $scope.toggleDashboard= function(id){
                                          if(id=="1")
                                            return "hide";
                                          else if(id=="0")
                                            return "show";
                                          else
                                            return "nothing";
                                        };
                                        $scope.toggleDashboard();

                                        $timeout(function() {
                                          ionicMaterialMotion.fadeSlideInRight({
                                            startVelocity: 3000
                                          });
                                        }, 700);

                                        // Set Ink
                                        ionicMaterialInk.displayEffect();
                                      });
                                    });
                                  };
                                  $scope.showAlert();





                                })
                              }else {
                                // console.log('You are not sure');
                              }
                            });
                          };
                          $scope.showConfirm();


                       };
  /**
   * @name shareProg
   * @todo To Share Progress Photos
   * @return void function
   *
   **/
  $scope.shareProg = function(index){
    var selectDivForScreenShot = index + 1;
    $ionicLoading.show({template: '<ion-spinner icon="ios"></ion-spinner>'});
    var selector = ".progressMain .list .item:nth-child(" + selectDivForScreenShot + ")";

    html2canvas(document.querySelector(selector), {onclone: function(document) {
      document.querySelector('.progPicsBottom').style.display = 'none';
      document.querySelector(selector).style.padding = "10px";
      document.querySelector(selector).style.background = "#ffffff";
      document.querySelector(selector).style.border = "0";
      document.querySelector(selector).style.margin = "0 0 10px 0";
      document.querySelector('.progressMain .list .item .hideOnApp').style.display = 'block';
      document.querySelector('.progressMain .list .item .progDate span article').style.display = 'none';
    },
      allowTaint : true,
      logging : true,
      useCORS : true,
      proxy : 'http://www.fitnessbasecamp.com'}).then(function (canvas) {
      var context = canvas.getContext("2d");
      context.fillStyle = "#ffffff";
      var imgDataUrl = canvas.toDataURL("image/png");
      window.plugins.socialsharing.share(
        'Fitnessbasecamp',
        'Fitnessbasecamp!',
        imgDataUrl,
        'http://www.fitnessbasecamp.com/',
        function(){
          $ionicLoading.hide();

        },
        function(err){
          //error callback
          $ionicLoading.hide();


        });

    });
  }
  $scope.expired = function(){
    var myPopup = $ionicPopup.show({
      template: '<div class="packageExpiredInner"><div class="selectOption" ng-click="closePopup()"><span>Not Now</span></div><div class="selectOption" ng-click="openBrowser()"><span>Renew Now</span></div></div>',
      subTitle: 'Oops! Your subscription has expired. Please renew your subscription.',
      cssClass: 'packageExpiredPopup',
      scope: $scope
    });

    myPopup.then(function(res) {

    });

    $timeout(function() {
      // myPopup.close(); //close the popup after 3 seconds for some reason
    }, 8000);
    $scope.closePopup = function() {

      myPopup.close();
    };
  };
  /**
   * @name shareProg
   * @todo To Open In App browser
   * @return void function
   *
   **/
  $scope.openBrowser = function(){
    var user_id = $scope.userData.user_id;
    var options = {
      location: 'yes',
      clearcache: 'yes',
      toolbar: 'no'
    };
    $cordovaInAppBrowser.open('http://www.fitnessbasecamp.com/loginUser_admin/'+user_id+'/app', '_system', options)
      .then(function(event) {
        // console.log(event);
      })
      .catch(function(event) {
        // error
      });


    // $cordovaInAppBrowser.close();
  }
});
