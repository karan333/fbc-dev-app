/**
 * Purpose:
 *@This file is used to Show My Exercise Details
 *
 **/
angular.module('starter').controller('myexerciseDetailCtrl', function($ionicHistory, $ionicPlatform, $cordovaToast, quote, $rootScope,  $cordovaNetwork, $window, $log, $document, video, $ionicSlideBoxDelegate, $ionicPopover, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading, $cordovaFileTransfer) {
 // console.log(GuserData.user_fname);


$scope.deviceInfo = deviceInformation.getInfo();
var uuid = $scope.deviceInfo.uuid;
$scope.siteUrl = siteUrl;
$scope.exercise = {};
$scope.isOnline = [];
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();


        /**
         *
         * Listen for Online event
         *
         **/
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        /**
         *
         * Listen for offline event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            // console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);
                              var userData = $window.localStorage['userData'];
                              $scope.userData = angular.fromJson(userData);

              // after getting data from sqlite getting user id and then all news feed service

              var user_id = $scope.userData.user_id;
  /**
   * @name getQuote
   * @todo To Get Random Quotes
   * @return void function
   *
   **/
               quote.getQuote(uuid, user_id).success(function (res) {
                                                                  // console.log(res);
                                                                  $scope.quote = res;
                                                                  });
              var exercise_id = $stateParams.cid;
              var prevWorkoutID= $stateParams.did;

              var link = baseUrl+'/exercise/my_exercise_details';
              var formData = {uu_id : uuid, user_id : user_id, exercise_id : exercise_id}
              var postData = 'myData='+JSON.stringify(formData);
                  $ionicLoading.show({
                            template: '<ion-spinner icon="ios"></ion-spinner>'

                        });
                   $http({
                          method : 'POST',
                          url : link,
                          data: postData,
                          headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                  }).success(function(res){
                    $ionicLoading.hide();
                   // console.log(res);
                   $scope.exercise = res.exercise[0];
                   var vimeo_base_url = 'http://player.vimeo.com/video/';
                   $scope.vimeoVideo = vimeo_base_url + res.exercise[0].vimeo_id;
                   $scope.youtube = $scope.exercise.video + '?rel=0';


                   if( res.exercise[0].vimeo_id === null){
                     // console.log('inside');
                     var filename = res.exercise[0].wid+"1"+".mp4";
                     // console.log(filename);
                     var targetPath = cordova.file.dataDirectory + 'FitnessTest/' + filename;
                     $cordovaFile.checkFile(cordova.file.dataDirectory+'FitnessTest/',filename)
                       .then(function (success) {

                         $scope.vid_srcc=cordova.file.dataDirectory+'FitnessTest/'+res.exercise[0].wid+"1"+".mp4";

                         $ionicSlideBoxDelegate.update();

                       }, function (error) {
//                      $ionicLoading.show({
//                          template: '<ion-spinner icon="android"></ion-spinner><br><span>Downloading Exercise.</span>'
//
//                      });
                         $scope.loaderShow = true;
                         ////////////////////////////
                         var trustHosts = true;
                         var options = {
                           encodeURI : false
                         };
                         var url = $scope.exercise.video;


                         $cordovaFileTransfer.download(url, targetPath, options, trustHosts).then(function (result) {
                           // console.log('Save file on '+targetPath+' success!');
                           $ionicLoading.hide();
                           $scope.loaderShow = false;
                           $scope.vid_srcc=cordova.file.dataDirectory+'FitnessTest/'+res.exercise[0].wid+"1"+".mp4";
                           $timeout(function() {
                             ionicMaterialMotion.fadeSlideInRight({
                               startVelocity: 3000
                             });
                           }, 700);

                           // Set Ink
                           ionicMaterialInk.displayEffect();

                           $ionicSlideBoxDelegate.update();

                         }, function (error) {

//                                       console.log('Error Download file' + console.log(error));

                         }, function (progress) {
                           // console.log((progress.loaded / progress.total) * 100);
                           $scope.progresss = 0;
                           $scope.progresss = (progress.loaded / progress.total) * 100;
                           $scope.progresss = Math.floor($scope.progresss)+"%";




                         });

                       });


                     ////////// End Checking ////////////////////////////////////////

                   }

                       $ionicSlideBoxDelegate.update();


                  }) .error(function(error){
                    $ionicLoading.hide();
                    // console.log('request not completed');
                  })


  var deregisterFirst = $ionicPlatform.registerBackButtonAction(function(e) {

    $scope.backView = $ionicHistory.backView();
    // console.log($scope.backView);
    if($scope.backView.stateName === 'app.home'){
      $state.go('app.home');
    } else if($scope.backView.stateName === "app.allNewsFeed"){
      $state.go('app.allNewsFeed');
    } else if($scope.backView.stateName === 'app.workoutDetailMain'){
      $state.go('app.workoutDetailMain', {cid: prevWorkoutID});
    } else if($scope.backView.stateName === "app.myWorkoutDetailMain"){
      $state.go('app.myWorkoutDetailMain', {result: prevWorkoutID});
    } else{
      $state.go('app.exercises');
    }



    // console.log('Back');
    e.preventDefault();
    return false;
  }, 101);

  $scope.$on('$destroy', deregisterFirst);

});
