/**
 * Purpose:
 *@This file is used to Create details about Meals
 *
 **/

angular.module('starter').controller('mealCtrl', function(ionicDatePicker, $ionicModal, $ionicPopover, $rootScope,  $cordovaNetwork, $window, deviceInformation, $scope, $cordovaSQLite, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $ionicLoading, $ionicPopup) {
$ionicPopover.fromTemplateUrl('templates/popover.html', {
    scope: $scope,
  }).then(function(popover) {
    $scope.popover = popover;
  });



  $scope.siteUrl = siteUrl;
   $scope.deviceInfo = deviceInformation.getInfo();
   var uuid = $scope.deviceInfo.uuid;

   $scope.mealLogger = {};
   $scope.meal = [];
$scope.isOnline = [];
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();


        /**
         *
         * Listen for Online event
         *
         **/
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        /**
         *
         * Listen for Online event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);


var ipObj1 = {
 callback: function (val) {
   var date = (new Date(val));

           var day = date.getDate();
           var year = date.getFullYear();
           var month = date.getMonth()+ 1;
   $scope.meal.date = year+'-'+ month+'-'+day;

 },
 from: new Date(1970, 1, 1),
 to: new Date(2016, 12, 30),
 closeOnSelect: true
}
 $scope.openDatePicker = function(){
   console.log('hi');
 ionicDatePicker.openDatePicker(ipObj1);
//                              $scope.showPopup();
};



                              var userData = $window.localStorage['userData'];
                             $scope.userData = angular.fromJson(userData);

                           var user_id = $scope.userData.user_id;
                            var link = baseUrl+'/nutrition/meal_logger';
                            console.log('userID---'+ user_id + '--UUID--' + uuid);
                            var formData = {uu_id : uuid, user_id : user_id}
                            var postData = 'myData='+JSON.stringify(formData);

                            $ionicLoading.show({
                            template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                            $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                          }).success(function(res){

                              console.log(res);
                               $scope.NoResultFound =  res.code;
                              $scope.mealLogger = res.meal_logger;
                              console.log($scope.mealLogger);


                          $ionicLoading.hide();
                          $timeout(function() {
                              ionicMaterialMotion.fadeSlideInRight({
                                  startVelocity: 3000
                              });
                          }, 700);

                          // Set Ink
                          ionicMaterialInk.displayEffect();


                        })

                        //add meal log

                        $ionicModal.fromTemplateUrl('templates/modal.html', {
                          scope: $scope
                        }).then(function(modal) {
                          $scope.modal = modal;
                          $scope.addMealLog = function(){
                            var user_id = $scope.userData.user_id;
                          var link = baseUrl+'/nutrition/addMealLog';
                          var formData = {uu_id : uuid, user_id : user_id, food_type : $scope.meal.foodtype, quantity : $scope.meal.amount, calories : $scope.meal.calories, fats : $scope.meal.fats, proteins : $scope.meal.proteins, meal_time : $scope.meal.mealtime, c_date : $scope.meal.date}
                          var postData = 'myData='+JSON.stringify(formData);
                              $ionicLoading.show({
                                        template: '<ion-spinner icon="ios"></ion-spinner>'

                                    });
                               $http({
                                      method : 'POST',
                                      url : link,
                                      data: postData,
                                      headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                              }).success(function(res){

                                console.log(res);
                                $scope.mealLogger = res.meal_logger;

                                var alertPopup = $ionicPopup.alert({
                                 title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                 template: res.message,
                                 cssClass: 'loginErrorPopUp'
                               });

                                   $timeout(function() {
                                    alertPopup.close();
                                    $scope.modal.hide();
                                    $scope.mealLogger = res.meal_logger;
                                  }, 5000);

                                  $timeout(function() {
                                      ionicMaterialMotion.fadeSlideInRight({
                                          startVelocity: 3000
                                      });
                                      $ionicLoading.hide();
                                  }, 6500);

                                  // Set Ink
                                  ionicMaterialInk.displayEffect();


                              }) .error(function(error){
                                $ionicLoading.hide();
                                console.log('request not completed');
                              })
                          }

                        });
                        $scope.$on('modal.shown', function() {
                                   $timeout(function() {
                                       ionicMaterialMotion.fadeSlideInRight({
                                           startVelocity: 3000
                                       });
                                       $ionicLoading.hide();
                                   }, 700);

                                   // Set Ink
                                   ionicMaterialInk.displayEffect();
                                   });


});
