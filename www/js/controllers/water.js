angular.module('starter').controller('waterCtrl', function(ionicDatePicker, $ionicModal, $ionicPopover, $rootScope,  $cordovaNetwork,$window, deviceInformation, $scope, $cordovaSQLite, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $ionicLoading, $ionicPopup) {

 $ionicPopover.fromTemplateUrl('templates/popover.html', {
     scope: $scope,
   }).then(function(popover) {
     $scope.popover = popover;
   });


$scope.water = [];

  $scope.siteUrl = siteUrl;
   $scope.deviceInfo = deviceInformation.getInfo();
   var uuid = $scope.deviceInfo.uuid;
$scope.isOnline = [];
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();


        // listen for Online event
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        // listen for Offline event
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);


var ipObj1 = {
 callback: function (val) {
   var date = (new Date(val));

           var day = date.getDate();
           var year = date.getFullYear();
           var month = date.getMonth()+ 1;
   $scope.water.date = year+'-'+ month+'-'+day;

 },
 from: new Date(1970, 1, 1),
 to: new Date(2016, 12, 30),
 closeOnSelect: true
}
 $scope.openDatePicker = function(){
   console.log('hi');
 ionicDatePicker.openDatePicker(ipObj1);
//                              $scope.showPopup();
};



   $scope.waterLogger = {};

                       var userData = $window.localStorage['userData'];
                       $scope.userData = angular.fromJson(userData);

                           var user_id = $scope.userData.user_id;
                            var link = baseUrl+'/nutrition/water_logger';
                            console.log('userID---'+ user_id + '--UUID--' + uuid);
                            var formData = {uu_id : uuid, user_id : user_id}
                            var postData = 'myData='+JSON.stringify(formData);

                            $ionicLoading.show({
                            template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                            $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                          }).success(function(res){

                              console.log(res);
                              $scope.NoResultFound =  res.code;
                              $scope.waterLogger = res.water_logger;


                          $ionicLoading.hide();
                          $timeout(function() {
                              ionicMaterialMotion.fadeSlideInRight({
                                  startVelocity: 3000
                              });
                          }, 700);

                          // Set Ink
                          ionicMaterialInk.displayEffect();


                        })

                        $ionicModal.fromTemplateUrl('templates/modal.html', {
                          scope: $scope
                        }).then(function(modal) {
                          $scope.modal = modal;
                          $scope.addWaterLog = function(){
                            var user_id = $scope.userData.user_id;
                            var link = baseUrl+'/nutrition/addWaterLog';
                            var formData = {uu_id : uuid, user_id : user_id, quantity : $scope.water.quantity, c_date : $scope.water.date, unit : $scope.water.unit}
                            console.log(formData);
                            var postData = 'myData='+JSON.stringify(formData);
                                $ionicLoading.show({
                                          template: '<ion-spinner icon="ios"></ion-spinner>'

                                      });
                                 $http({
                                        method : 'POST',
                                        url : link,
                                        data: postData,
                                        headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                }).success(function(res){

                                  console.log(res);
                                  $scope.waterLogger = res.water_logger;
                                  var alertPopup = $ionicPopup.alert({
                                   title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                   template: res.message,
                                   cssClass: 'loginErrorPopUp'
                                 });

                                     $timeout(function() {
                                      alertPopup.close();
                                    }, 5000);


                                    $timeout(function() {
                                        ionicMaterialMotion.fadeSlideInRight({
                                            startVelocity: 3000
                                        });
                                        $ionicLoading.hide();
                                    }, 700);

                                    // Set Ink
                                    ionicMaterialInk.displayEffect();


                                }) .error(function(error){
                                  $ionicLoading.hide();
                                  console.log('request not completed');
                                })
                          }

                        });
                        $scope.$on('modal.shown', function() {
                                   $timeout(function() {
                                       ionicMaterialMotion.fadeSlideInRight({
                                           startVelocity: 3000
                                       });
                                       $ionicLoading.hide();
                                   }, 700);

                                   // Set Ink
                                   ionicMaterialInk.displayEffect();
                                   });

});
