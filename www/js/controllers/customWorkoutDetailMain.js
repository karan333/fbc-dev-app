/**
 * Purpose:
 *@This file is used to Show Custom Workout Details in Which exercises Lists in a consistent manner and properly formatted by manipulating json Api calls using Lodash.js and Vanilla JavaScript
 *
 **/


angular.module('starter').controller('customWorkoutDetailMainCtrl', function(quote, $rootScope,  $cordovaNetwork, $window, $log,$q, video, $document, $ionicSlideBoxDelegate, $ionicPopover, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading, $cordovaFileTransfer, $rootScope, $ionicModal) {
 // console.log(GuserData.user_fname);


$scope.deviceInfo = deviceInformation.getInfo();
var uuid = $scope.deviceInfo.uuid;
$scope.baseUrlForWorkoutImg = 'http://www.fitnessbasecamp.com/uploads/workouts/';
$scope.baseUrlForWorkoutPoster = 'http://www.fitnessbasecamp.com/uploads/category-images/';
$scope.siteUrl = 'http://www.fitnessbasecamp.com/';
$scope.allExercises = {};
$scope.videos = {};
$scope.workoutDetai = {};
$scope.vid_srcc = {};
$scope.reviews = {};
$scope.nct=0;
$scope.disablenext =false;
$scope.workout_Completed = 0;
var mani =[];
$scope.TotalDownloaded = 0;
$scope.progresss=  0;
$scope.vsble  =false;
$scope.isOnline = [];

 //rating

$scope.rating = {};
$scope.rating.rate = 0;
$scope.rating.max = 5;


//review

$scope.addReview = {};

      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();


        /**
         *
         * Listen for Online event
         *
         **/
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        /**
         *
         * Listen for Offline event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            // console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);

                    var userData = $window.localStorage['userData'];
                    $scope.userData = angular.fromJson(userData);

              // after getting data from sqlite getting user id and then all news feed service

              var user_id = $scope.userData.user_id;
               quote.getQuote(uuid, user_id).success(function (res) {
                                                                  // console.log(res);
                                                                  $scope.quote = res;
                                                                  });
              var workout_id = $stateParams.cid;
              $scope.workoutID  = $stateParams.cid;

              var link = baseUrl+'/workouts/workout_details';
              var formData = {uu_id : uuid, user_id : user_id, workout_id : workout_id}
              var postData = 'myData='+JSON.stringify(formData);
                  $ionicLoading.show({
                            template: '<ion-spinner icon="ios"></ion-spinner>'

                        });
                   $http({
                          method : 'POST',
                          url : link,
                          data: postData,
                          headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                  }).success(function(res){
                    $ionicLoading.hide();
                    // console.log(res);
                    $scope.workout_name = res.workout[0].name;
                    $scope.workoutDetai = res.workout[0];
                    $scope.reviews = res.workout[0].reviews;

                    $scope.vid_srcc = res.workout[0].embed_video;



                     $timeout(function() {
                                      ionicMaterialMotion.fadeSlideInRight({
                                          startVelocity: 3000
                                      });
                                  }, 700);

                                ionicMaterialInk.displayEffect();


                  }) .error(function(error){
                    $ionicLoading.hide();
                    // console.log('request not completed');
                  })


                   $ionicModal.fromTemplateUrl('templates/modal.html', {
                      scope: $scope
                    }).then(function(modal) {
                     $scope.modal = modal;
                        // console.log('modal is opened');
                    });



                  //add review section

  /**
   * @name addReview
   * @todo To add review to workout
   * @return void function
   *
   **/
                    $scope.addReview = function(){
                      var user_name = $scope.addReview.username;
                      var email = $scope.addReview.email;
                      var message = $scope.addReview.message;
                      var rating = $scope.rating.rate;
                      var user_id = $scope.userData.user_id;
                      var workout_id = $stateParams.cid;

                  var link = baseUrl+'/workouts/add_workout_review';
                  var formData = {uu_id : uuid, user_id : user_id, workout_id : workout_id, message : message, email : email, user_name : user_name, rating : rating}
                  var postData = 'myData='+JSON.stringify(formData);
                      $ionicLoading.show({
                                template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                       $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                      }).success(function(res){
                        $ionicLoading.hide();
                        // console.log(res.message);

                        var alertPopup = $ionicPopup.alert({
                        title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                        template: res.message,
                        cssClass: 'loginErrorPopUp'
                      });

                          $timeout(function() {
                           alertPopup.close();
                           $scope.modal.hide();

                         }, 5000);
                         $timeout(function() {
                              ionicMaterialMotion.fadeSlideInRight({
                                  startVelocity: 3000
                              });
                          }, 700);

                        ionicMaterialInk.displayEffect();


                      }) .error(function(error){
                        $ionicLoading.hide();
                        // console.log('request not completed');
                      })
                    }

  var deregisterFirst = $ionicPlatform.registerBackButtonAction(function(e) {

    $state.go('app.myWorkouts', {cid: workout_id});
    // console.log('Back');
    e.preventDefault();
    return false;
  }, 101);

  $scope.$on('$destroy', deregisterFirst);

});

