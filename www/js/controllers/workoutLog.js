/**
 * Purpose:
 *@This file is used to view Workout log.
 *
 **/


angular.module('starter').controller('workoutLogCtrl', function($cordovaInAppBrowser, ionicDatePicker ,$cordovaToast, quote, $rootScope,  $cordovaNetwork, $window, $ionicModal,focus,$ionicScrollDelegate, $ionicPopover, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading) {
  // console.log("here in workout log controller");


  /**
   *
   * Initializing Variables, Arrays, and Objects
   *
   **/

$scope.deviceInfo = deviceInformation.getInfo();
var uuid = $scope.deviceInfo.uuid;
$scope.layout = 'list';
  $scope.packageExpired = {};
$scope.refine = {};
$scope.baseUrlForWorkoutImg = 'http://www.fitnessbasecamp.com/uploads/category-images/';
  $scope.baseUrlForExerciseImg = 'http://www.fitnessbasecamp.com/uploads/workouts/';
//console.log($scope.workouts.search);
$scope.moreworkouts=false;
$scope.logHistory = [];
$scope.postlimit = 6;
$scope.workoutss=[];
$scope.currentrecordsview = 0;
var user_id="";
$scope.isOnline = [];
$scope.workoutLog = {};
  $scope.filter = {};

  /**
   *
   * date picker object
   *
   **/

  var ipObj1 = {
    callback: function (val) {
      var date = (new Date(val));

      var day = date.getDate();
      var year = date.getFullYear();
      var month = date.getMonth()+ 1;
      $scope.filter.from = year + '-' + month + '-' + day;

    },
    from: new Date(1970, 1, 1),
    to: new Date(2017, 12, 30),
    inputDate: new Date(),
    titleLabel: 'Select a Date',
    setLabel: 'Set',
    todayLabel: 'Today',
    closeLabel: 'Close',
    mondayFirst: false,
    weeksList: ["S", "M", "T", "W", "T", "F", "S"],
    monthsList: ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"],
    templateType: 'popup',
    showTodayButton: true,
    dateFormat: 'dd MMMM yyyy',
    closeOnSelect: false,
    disableWeekdays: []
  }


  /**
   * @name openDatePicker
   * @todo To open datepicker
   * @return void function
   *
   **/




  $scope.openDatePicker = function(){
    // console.log('hi');
    ionicDatePicker.openDatePicker(ipObj1);
//                              $scope.showPopup();
  };


  /**
   *
   * date picker object
   *
   **/


  var ipObj2 = {
    callback: function (val) {
      var date = (new Date(val));

      var day = date.getDate();
      var year = date.getFullYear();
      var month = date.getMonth()+ 1;
      $scope.filter.to = year + '-' + month + '-' + day;
    },
    from: new Date(1970, 1, 1),
    to: new Date(2017, 12, 30),
    inputDate: new Date(),
    titleLabel: 'Select a Date',
    setLabel: 'Set',
    todayLabel: 'Today',
    closeLabel: 'Close',
    mondayFirst: false,
    weeksList: ["S", "M", "T", "W", "T", "F", "S"],
    monthsList: ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"],
    templateType: 'popup',
    showTodayButton: true,
    dateFormat: 'dd MMMM yyyy',
    closeOnSelect: false,
    disableWeekdays: []
  }


  /**
   * @name openDatePicker
   * @todo To open datepicker
   * @return void function
   *
   **/


  $scope.openDatePicker2 = function(){
    // console.log('hi');
    ionicDatePicker.openDatePicker(ipObj2);
//                                   $scope.showPopup();
  };
             document.addEventListener("deviceready", function () {
               $scope.network = $cordovaNetwork.getNetwork();
               $scope.isOnline = $cordovaNetwork.isOnline();


               /**
                *
                * Listen for Online event
                *
                **/


               // listen for Online event
               $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
                   $scope.isOnline = true;
                   $scope.network = $cordovaNetwork.getNetwork();
                   $cordovaToast.showLongBottom('You are Online !')

               });

               /**
                *
                * Listen for ofline event
                *
                **/

               // listen for Offline event
               $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
                   // console.log("got offline");
                   $scope.isOnline = false;
                   $scope.network = $cordovaNetwork.getNetwork();
                   $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
               });

             }, false);




  /**
   *
   * get workout log from service api call
   *
   **/


                  $scope.siteUrl = siteUrl;

                  var userData = $window.localStorage['userData'];
                   $scope.userData = angular.fromJson(userData);

                  var packageExpired = $window.localStorage['expired'];
                  $scope.packageExpired = angular.fromJson(packageExpired);


                  user_id = $scope.userData.user_id;
                  quote.getQuote(uuid, user_id).success(function (res) {
                                                    // console.log(res);
                                                    $scope.quote = res;
                                                    });
                  var link = baseUrl+'/workouts/getWorkoutLog';
                  var formData = {uu_id : uuid, user_id : user_id};
                  var postData = 'myData='+JSON.stringify(formData);
                      $ionicLoading.show({
                                template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                       $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                      }).success(function(res){


                      var arr = Object.keys(res.workoutslog).map(function(k) { return res.workoutslog[k] });
                      $scope.workoutLog = arr.reverse();
                         // console.log($scope.workoutLog);
                         $ionicLoading.hide();

                      }) .error(function(error){
                        $ionicLoading.hide();
                        // console.log('request not completed');
                      });

  /**
   * @name toggleGroup
   * @todo To show hide workout exercises
   * @return void function
   *
   **/




  $scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };



  /**
   * @name showPopup
   * @todo To filter workout log
   * @return void function
   *
   **/



  $scope.showPopup = function() {
    $scope.data = {};

    // An elaborate, custom popup
    var myPopup = $ionicPopup.show({
      template: '<div class="filterStat"><span>From</span><input type="text" placeholder="" ng-click="openDatePicker();" ng-model="filter.from" name="" ng-readonly="true"><span>To</span><input type="text" placeholder="" ng-click="openDatePicker2();" ng-model="filter.to" name="" ng-readonly="true"></div>',
      title: 'Filter Workout Log',
      scope: $scope,
      cssClass : 'statFilterPopup',
      buttons: [
//                            { text: 'Cancel' },
        {
          text: '<b>Filter</b>',
          type: 'button-positive',
          onTap: function(e) {

            ////////////////////////////////////////

            var user_id = $scope.userData.user_id;
            // console.log('userID---' + user_id + 'uuid----' + uuid);
            var link = baseUrl+'/workouts/getWorkoutLog';
            var from = $scope.filter.from;
            var to = $scope.filter.to;
            var formData = {uu_id : uuid, user_id : user_id, from : from, to : to};
            // console.log(formData);
            var postData = 'myData='+JSON.stringify(formData);
            $ionicLoading.show({
              template: '<ion-spinner icon="ios"></ion-spinner>'

            });
            $http({
              method : 'POST',
              url : link,
              data: postData,
              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

            }).success(function(res){

              // console.log(res);
              $scope.workoutLog = res.workoutslog;
              $ionicLoading.hide();



              $timeout(function() {
                ionicMaterialMotion.fadeSlideInRight({
                  startVelocity: 3000
                });
              }, 700);

              // Set Ink
              ionicMaterialInk.displayEffect();


            }) .error(function(error){
              $ionicLoading.hide();
              // console.log('request not completed');
            });



            ///////////////////////////////////////////


          }
        }
      ]
    });

    myPopup.then(function(res) {

    });
//                           $timeout(function() {

//                              myPopup.close(); //close the popup after 3 seconds for some reason
//                           }, 3000);
  };





  /**
   * @name expired
   * @todo To show expiry message
   * @return void function
   *
   **/





  $scope.expired = function(){
    var myPopup = $ionicPopup.show({
      template: '<div class="packageExpiredInner"><div class="selectOption" ng-click="closePopup()"><span>Not Now</span></div><div class="selectOption" ng-click="openBrowser()"><span>Renew Now</span></div></div>',
      subTitle: 'Oops! Your subscription has expired. Please renew your subscription.',
      cssClass: 'packageExpiredPopup',
      scope: $scope
    });

    myPopup.then(function(res) {

    });

    $timeout(function() {
      // myPopup.close(); //close the popup after 3 seconds for some reason
    }, 8000);
    $scope.closePopup = function() {

      myPopup.close();
    };
  };


  /**
   * @name openBrowser
   * @todo To open InAppBrowser
   * @return void function
   *
   **/



  $scope.openBrowser = function(){
    var user_id = $scope.userData.user_id;
    var options = {
      location: 'yes',
      clearcache: 'yes',
      toolbar: 'no'
    };
    $cordovaInAppBrowser.open('http://www.fitnessbasecamp.com/loginUser_admin/'+user_id+'/app', '_system', options)
      .then(function(event) {

      })
      .catch(function(event) {
        // error
      });


    // $cordovaInAppBrowser.close();
  }
});
