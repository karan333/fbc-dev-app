/**
 * Purpose:
 *@This file is used to view friend's dashobard.
 *
 **/

angular.module('starter').controller('userdashboard', function(quote, $cordovaToast, $cordovaNetwork, $rootScope, $cordovaPreferences, $ionicBackdrop, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading, $cordovaFileTransfer,$q) {


  /**
   *
   * Initializing Variables, Arrays, and Objects
   *
   **/


    var mani =[];

    $scope.Ccalories = {};
      $scope.Cfats = {};
      $scope.Cproteins = {};
      $scope.level = {};
      $scope.points = {};
      $scope.countFriends = {};
      $scope.weight ={};
      $scope.waist ={};
      $scope.consistency = {};
      $scope.userData = {};
      $scope.userStatus = {};
      $scope.friendsList = {};
      $scope.newsFeed = {};
      $scope.userProgressPhotos = {};
      $scope.getFriendsActivity = {};
      $scope.my_graph = {};
      $scope.status = {};
      $scope.comment = {};
      $scope.status.postImage = {};
      $scope.postImage = [];
      $scope.status.commentImage = {};
      $scope.commentImage = [];

      $scope.siteUrl = siteUrl;
      $scope.TotalDownloaded = 0;
      $scope.statusImgs = [];
       $scope.commentImgs = [];
      $scope.friend_id  = $stateParams.result;
      $scope.FriendsData = {};
      $scope.userExercises = {};
      $scope.userWorkouts = {};
      $scope.FriendCoverImage = {};
//      $scope.FriendImage = '';
//       console.log($scope.friend_id);
      $scope.isOnline = [];
            document.addEventListener("deviceready", function () {
              $scope.network = $cordovaNetwork.getNetwork();
              $scope.isOnline = $cordovaNetwork.isOnline();


              /**
               *
               * Listen for Online event
               *
               **/


              $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
                  $scope.isOnline = true;
                  $scope.network = $cordovaNetwork.getNetwork();
                  $cordovaToast.showLongBottom('You are Online !')

              })



              /**
               *
               * Listen for ofline event
               *
               **/


              $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
                  // console.log("got offline");
                  $scope.isOnline = false;
                  $scope.network = $cordovaNetwork.getNetwork();
                  $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
              })

            }, false);




  /**
   *
   * get user information
   *
   **/


               var userData = $window.localStorage['userData']
               $scope.userData = angular.fromJson(userData);
               // console.log($scope.userData);


                                // after getting data from sqlite getting user id and then running dashboard service
                                $scope.userImg = $scope.userData.image;
                                $scope.username = $scope.userData.user_fname;
                                $scope.Lusername = $scope.userData.user_lname;
  $scope.userCover = $scope.userData.cover_image;

                                var user_id = $scope.userData.user_id;



  /**
   *
   * get quote
   *
   **/


quote.getQuote(uuid, user_id).success(function (res) {
                        // console.log(res);
                        $scope.quote = res;
                      });




  /**
   *
   * get friend's dashobard data from service api call
   *
   **/


                                var link = baseUrl+'/dashboard/friendsDashboard';
                                var formData = {uu_id : uuid, user_id : user_id,friend_id:$scope.friend_id}
                                var postData = 'myData='+JSON.stringify(formData);
                                    $ionicLoading.show({
                                              template: '<ion-spinner icon="ios"></ion-spinner>'

                                          });
                                     $http({
                                            method : 'POST',
                                            url : link,
                                            data: postData,
                                            headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                    }).success(function(res){
                                      $scope.checked = true;
                                      $ionicLoading.hide();
                                      // console.log(res);
                                      // console.log(res.dashboard_data.userStatus);
                                       // Friends Information Object
                                       $scope.FriendsData = res.dashboard_data.userData[0];
                                       $scope.FriendImage = res.dashboard_data.userData[0].image;
                                       $scope.FriendCoverImage = res.dashboard_data.userData[0].cover_image;
                                       // console.log($scope.FriendsData);
                                       $timeout(function() {
                                                         ionicMaterialMotion.slideUp({
                                                             selector: '.slide-up'
                                                         });
                                                     }, 1000);



                                      // user activity json object

                                      $scope.userStatus = res.dashboard_data.userStatus;
                                      $scope.consistency = res.dashboard_data.consistency;

                                      //  friends activity json object
                                      $scope.getFriendsActivity = res.dashboard_data.getFriendsActivity;

                                      // friends list json object

                                      $scope.friendsList = res.dashboard_data.myFriends;

                                      // news feed json object

                                      $scope.newsFeed = res.dashboard_data.newsFeed;

                                      //progress pictures json object

                                      $scope.userProgressPhotos = res.dashboard_data.userProgressPhotos;

                                      // User Custom Exercises
                                      $scope.userExercises = res.dashboard_data.userExercises;
                                      $scope.userWorkouts = res.dashboard_data.userWorkouts;

                                      //levels

                                      $scope.level = res.dashboard_data.level;

                                      //points

                                      $scope.points = res.dashboard_data.points;

                                      //friends

                                      $scope.countFriends = res.dashboard_data.countFriends;

                                      //weight

                                      $scope.weight = res.dashboard_data.weight;

                                      //waist

                                      $scope.waist = res.dashboard_data.waist;

                                      //calorie graph json object

                                      $scope.calories_graph = parseInt(res.dashboard_data.calories_graph);
                                      $scope.Ccalories = res.dashboard_data.calories_graph.calories;
                                      $scope.Cfats = res.dashboard_data.calories_graph.fats;
                                      $scope.Cproteins = res.dashboard_data.calories_graph.proteins;

                                    // charts

                                      $scope.chartConfig = {
                                              options: {
                                                  chart: {
                                                      type: 'line',
                                                      zoomType: 'x',
                                                      backgroundColor : '#fff'
                                                  }
                                              },
                                              credits: {
                                                enabled : false
                                              },
                                              series: [{
                                                  name: 'Calories',
                                                  data: [$scope.Ccalories]
                                              },{
                                                  name: 'Fats',
                                                  data: [$scope.Cfats]
                                              },{
                                                  name: 'Proteins',
                                                  data: [$scope.Cproteins]
                                              }],
                                              xAxis: {
                                              categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                                              },
                                              title: {
                                                  text: ''
                                              },
                                              yAxis: {
                                                plotLines: [{
                                                  value: 0,
                                                  width: 1,
                                                  color: '#808080'
                                                }]
                                              },
                                              loading: false
                                          }

                                          //my_graph data

                                          $scope.my_graph = res.dashboard_data.my_graph;
                                          $scope.my_graph.x_axis = res.dashboard_data.my_graph.x_axis;
                                          $scope.my_graph.y_axis = res.dashboard_data.my_graph.y_axis;

                                          $scope.chartConfig2 = {
                                            options: {
                                                chart: {
                                                    type: 'line',
                                                    zoomType: 'x',
                                                    backgroundColor : '#fff'
                                                }
                                            },
                                            credits: {
                                              enabled : false
                                            },
                                            series: $scope.my_graph.y_axis,
                                            xAxis: {
                                            categories: $scope.my_graph.x_axis
                                            },
                                            title: {
                                                text: ''
                                            },
                                            yAxis: {
                                              plotLines: [{
                                                value: 0,
                                                width: 1,
                                                color: '#808080'
                                              }]
                                            },
                                            loading: false
                                        }

                                    }) .error(function(error){
                                       $ionicLoading.hide();
                                       // console.log('request is not completed');
                                    }) //dashboard service sucess end









  /**
   * @name viewAllNews
   * @todo To see all news feed
   * @return void function
   *
   **/



                                        $scope.viewAllNews = function(){
                                          $state.go('app.allNewsFeed');
                                        }

  /**
   * @name viewAllNews
   * @todo To see all progress photos
   * @return void function
   *
   **/
                                        $scope.viewAllPP = function(){
                                          $state.go('app.allProgressPhotos');
                                        }



                                        //for user post like dislike


  /**
   * @name like
   * @todo To like user post
   * @return void function
   *
   **/

                                        $scope.like = function(id,viewTypx){
                                        var status_id = id;

                                        var link = baseUrl+'/dashboard/like_user_status';
                                        // console.log('userID---'+ user_id + '--UUID--' + uuid + '--statusID' + status_id);
                                        var formData = {uu_id : uuid, user_id : user_id, status_id : status_id}
                                        var postData = 'myData='+JSON.stringify(formData);

                                        // $ionicLoading.show({
                                        // template: '<ion-spinner icon="ios"></ion-spinner>'
                                        //
                                        // });
                                        $http({
                                          method : 'POST',
                                          url : link,
                                          data: postData,
                                          headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                      }).success(function(res){
                                      $scope.postLiked = {};
                                          // $ionicLoading.hide();

                                          // console.log(res);
                                          if(res.code == '200'){

                                            if(viewTypx == "MyActivity")
                                            {
                                             $scope.postLiked = 'liked';
                                              // console.log("mani");
                                             $scope.userStatus.likes = res.likes;

                                            if($scope.userStatus.id == $scope.getFriendsActivity.id)
                                            {
                                              $scope.postLikedFrnd = 'liked';
                                              $scope.getFriendsActivity.likes = res.likes;
                                              //$scope.getFriendsActivity.comments = res.comments.comments;
                                            }

                                            }
                                            else
                                            {
                                              $scope.postLikedFrnd = 'liked';
                                              $scope.getFriendsActivity.likes = res.likes;
                                              if($scope.userStatus.id == $scope.getFriendsActivity.id)
                                              {
                                              $scope.postLiked = 'liked';
                                              $scope.userStatus.likes = res.likes;
                                              }


                                            }
                                          } else if(res.code == '402'){


                                             if(viewTypx == "MyActivity")
                                              {
                                                $scope.postLiked = 'Disliked';
                                                $scope.userStatus.likes = res.likes;

                                              if($scope.userStatus.id == $scope.getFriendsActivity.id)
                                              {
                                                $scope.postLikedFrnd = 'Disliked';
                                                $scope.getFriendsActivity.likes = res.likes;
                                                //$scope.getFriendsActivity.comments = res.comments.comments;
                                              }

                                              }
                                              else
                                              {
                                                  $scope.postLikedFrnd = 'Disliked';
                                                $scope.getFriendsActivity.likes = res.likes;
                                                if($scope.userStatus.id == $scope.getFriendsActivity.id)
                                                {
                                                $scope.postLiked = 'Disliked';
                                                $scope.userStatus.likes = res.likes;
                                                }


                                              }

                                          } else{
                                            // console.log('nothing');
                                          }

                                    })


                                        }

  /**
   * @name removePost
   * @todo To remove post
   * @return void function
   *
   **/







                                         $scope.removePost = function(id){
                                          var status_id = id;

                                          var link = baseUrl+'/dashboard/remove_status';
                                          // console.log('userID---'+ user_id + '--UUID--' + uuid + '--statusID' + status_id);
                                          var formData = {uu_id : uuid, user_id : user_id, status_id : status_id}
                                          var postData = 'myData='+JSON.stringify(formData);

                                          $ionicLoading.show({
                                          template: '<ion-spinner icon="ios"></ion-spinner>'

                                          });
                                          $http({
                                            method : 'POST',
                                            url : link,
                                            data: postData,
                                            headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                        }).success(function(res){

                                            $ionicLoading.hide();
                                            // console.log(res);
                                            $scope.userStatus = res.new_status;
                                            $scope.getFriendsActivity = res.friends_activity;
                                      })

                                      //remove


                                          }

  /**
   * @name removePost
   * @todo To comment on post
   * @return void function
   *
   **/

                                    $scope.postComment = function(id,viewTyp){
                                      var comment_text = $scope.comment.text;
                                      var status_id = id;
                                      var link = baseUrl+'/dashboard/post_comment';
                                      // console.log('userID---'+ user_id + '--UUID--' + uuid + '--statusText' + comment_text + 'status-ID---' + status_id);
                                      var formData = {uu_id : uuid, user_id : user_id, comment_text : comment_text, status_id : status_id,comment_images:$scope.commentImgs}
                                      var postData = 'myData='+JSON.stringify(formData);

                                      $ionicLoading.show({
                                      template: '<ion-spinner icon="ios"></ion-spinner>'

                                      });
                                      $http({
                                        method : 'POST',
                                        url : link,
                                        data: postData,
                                        headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                    }).success(function(res){
                                    $scope.comment.text = ''
                                     $scope.commentImgs = [];
                                     $scope.commentImage = [];
                                       $ionicLoading.hide();
                //                             $window.location.reload();
              //                          console.log(res.comments.comments);
              //                           console.log(res);
                                        if(viewTyp == "MyActivity")
                                        {
                                          // console.log("Status ID" + $scope.userStatus.id + " User Status ID" + $scope.getFriendsActivity.id);
                                          $scope.userStatus.comments = res.comments.comments;

                                        if($scope.userStatus.id == $scope.getFriendsActivity.id)
                                        {
                                          $scope.getFriendsActivity.comments = res.comments.comments;
                                        }

                                        }
                                        else
                                        {

                                         $scope.getFriendsActivity.comments = res.comments.comments;
                                        if($scope.userStatus.id == $scope.getFriendsActivity.id)
                                        {
                                          $scope.userStatus.comments = res.comments.comments;
                                        }


                                        }

                                        var alertPopup = $ionicPopup.alert({
                                          title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                          template: res.message + ' You can view it on Friends Activity Section.',
                                          cssClass: 'loginErrorPopUp hideOk'
                                        });
                                        $timeout(function () {
                                          alertPopup.close();

                                        }, 3000);

                                  })

                                    };

  /**
   * @name removePost
   * @todo To post status on user's wall
   * @return void function
   *
   **/



                                    $scope.postStatus = function(){
                                        var status_text = $scope.status.text;
                                        //$scope.friend_id
                                        // console.log(status_text + ' ' + $scope.statusText);
                                        var link = baseUrl+'/dashboard/postOnFriendsWall';
                                        // console.log('userID---'+ user_id + '--UUID--' + uuid + '--statusText' + status_text);
                                        var formData = {uu_id : uuid, user_id : user_id, status_text : status_text,friend_id:$scope.friend_id,status_images:$scope.statusImgs}
                                        var postData = 'myData='+JSON.stringify(formData);

                                        $ionicLoading.show({
                                        template: '<ion-spinner icon="ios"></ion-spinner>'

                                        });
                                        $http({
                                          method : 'POST',
                                          url : link,
                                          data: postData,
                                          headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                      }).success(function(res){
                                         $ionicLoading.hide();
                                         $scope.statusImgs = [];
                                         $scope.postImage = [];
                                          // console.log(res.userstatus);
                                          $scope.userStatus = res.userstatus;
                                          $scope.getFriendsActivity = res.userstatus;
                                          $scope.status.text="";
                                          var alertPopup = $ionicPopup.alert({
                                            title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                            template: res.message + ' You can view it on Friends Activity Section.',
                                            cssClass: 'loginErrorPopUp hideOk'
                                          });

                                          $timeout(function () {
                                            alertPopup.close();

                                          }, 3000);

                                    })

                                  };

  /**
   * @name selectUploadMethod
   * @todo To show popup for adding photo from camera or gallery
   * @return void function
   *
   **/

                                  $scope.selectUploadMethod = function(){

                                    var myPopup = $ionicPopup.show({
                                        template: '<div class="uploadMethodSack"><ul><li><a class="ink" ng-click="takePicture();"><span class="ion-camera"></span><span>Upload from Camera</span></a></li><li><a class=ink"" ng-click="selectPicture();"><span class="ion-images"></span><span>Upload from Gallery</span></a></li></ul></div>',
                                        title: 'Choose Upload Method',
                                        subTitle: 'Select from Camera or Gallery',
                                        scope: $scope
                                      });

                                      myPopup.then(function(res) {
                                        // console.log('Tapped!', res);
                                      });

                                      $timeout(function() {
                                         myPopup.close(); //close the popup after 3 seconds for some reason
                                      }, 8000);
                                  }
  /**
   * @name takePicture
   * @todo To add photo from camera
   * @return void function
   *
   **/
                                    $scope.takePicture = function(){
                                       document.addEventListener("deviceready", function () {

                                          var options = {

                                            destinationType: Camera.DestinationType.FILE_URI,
                                            sourceType: Camera.PictureSourceType.CAMERA,
                                            allowEdit: true
                                          };

                                          $cordovaCamera.getPicture(options).then(function(imageURI) {
                                          // console.log(imageURI)
                                          $scope.uploadPicturex(imageURI);
                                            window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {
                                            //console.log(fileEntry.nativeURL);
                                             // $scope.status.postImage = imageURI;
                                             // console.log($scope.status.postImage);
                                              $scope.ftLoad = true;
              //                                var image = document.getElementById('myImage');
              //                                image.src = fileEntry.nativeURL;
                                              });
                                            $ionicLoading.show({template: 'Uploading Picture...', duration:500});
                                          },
                                          function(err){
                                            $ionicLoading.show({template: 'Errore di caricamento...', duration:500});
                                          })

                                        }, false);
                                    }


  /**
   * @name selectPicture
   * @todo To add photo from gallery
   * @return void function
   *
   **/
                                      $scope.selectPicture = function() {
                                       document.addEventListener("deviceready", function () {
                                          var options = {
                                            quality: 100,
                                            destinationType: Camera.DestinationType.FILE_URI,
                                            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                                            encodingType: Camera.EncodingType.JPEG,
                                            targetWidth: 250,
                                            targetHeight: 250,
                                            allowEdit: true
                                          };

                                          $cordovaCamera.getPicture(options).then(
                                          function(imageURI) {
                                          $scope.uploadPicturex(imageURI);

                                            window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {
                                             // $scope.status.postImage = fileEntry.nativeURL;
                                              $scope.ftLoad = true;
                                      //				var image = document.getElementById('myImage');
                                      //				image.src = fileEntry.nativeURL;
                                              });
                                            $ionicLoading.show({template: 'Uploading Picture...', duration:500});
                                          },
                                          function(err){
                                            $ionicLoading.show({template: 'Errore di caricamento...', duration:500});
                                          })
                                           }, false);
                                        };



  /**
   * @name removeStatusIMG
   * @todo To remove selected photo from post
   * @return void function
   *
   **/




$scope.removeStatusIMG = function(index)
{
// console.log("Image Index" + index);
$scope.statusImgs.splice(index, 1);  ;
$scope.postImage.splice(index, 1);  ;
// console.log($scope.postImage);

}



  /**
   * @name removeCommentIMG
   * @todo To remove selected photo from comment
   * @return void function
   *
   **/




  $scope.removeCommentIMG = function(index)
{
// console.log("Image Index" + index);
$scope.commentImgs.splice(index, 1);  ;
$scope.commentImage.splice(index, 1);  ;
// console.log($scope.postImage);

}


  /**
   * @name uploadPicturex
   * @todo To upload photo to remote server
   * @return void function
   *
   **/

$scope.uploadPicturex = function(imageURI) {

// console.log("In Upload Picture Function");
// console.log(imageURI);
  var options = {

  fileKey: "picture",

  filename: "p1.png",

  chunkedMode: false,

  mimeType: "image/png",
  trustAllHosts:true,
  headers:{Connection:"close"}

  };
 $ionicLoading.show({
                                             template: '<ion-spinner icon="ios"></ion-spinner><p>Uploading Image!</p>'

                                         });
  $cordovaFileTransfer.upload(encodeURI(baseUrl+"/dashboard/uploadStatusImge"), imageURI, options)

  .then(function(result) {
  // console.log(result.response);
   $scope.postImage.push(imageURI);;
 var  resk  = angular.fromJson(result.response);

          $scope.statusImgs.push(resk.image_name) ;

// console.log($scope.statusImgs);
$ionicLoading.hide();


  }, function(err) {

  // console.log(err);
  $ionicLoading.hide();
  $ionicLoading.show({template: 'An error occured while uploading your picture.<br>Please try again.', duration:500});

  }, function (progress) {

            });



  }


  /**
   * @name doRefresh
   * @todo To add pull to refresh
   * @return void function
   *
   **/



                                    $scope.doRefresh = function() {
                                       var link = baseUrl+'/dashboard/userprofile';
                                       var formData = {uu_id : uuid, user_id : user_id}
                                       var postData = 'myData='+JSON.stringify(formData);
                                           $ionicLoading.show({
                                                     template: '<ion-spinner icon="ios"></ion-spinner>'

                                                 });
                                            $http({
                                                   method : 'POST',
                                                   url : link,
                                                   data: postData,
                                                   headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                           }).success(function(res){
                                             $ionicLoading.hide();
                                             // console.log(res);
                                             // console.log(res.dashboard_data.userStatus);

                                             // user activity json object

                                            $scope.userStatus = res.dashboard_data.userStatus;

                                            //  friends activity json object
                                            $scope.getFriendsActivity = res.dashboard_data.getFriendsActivity;

                                            // friends list json object

                                            $scope.friendsList = res.dashboard_data.myFriends;

                                            // news feed json object

                                            //$scope.newsFeed = res.dashboard_data.newsFeed;

                                            //progress pictures json object

                                            $scope.userProgressPhotos = res.dashboard_data.userProgressPhotos;

                                            // User Custom Exercises


                                            $scope.calories_graph = parseInt(res.dashboard_data.calories_graph);

                                            $scope.my_graph = res.dashboard_data.my_graph;

                                           }).finally(function() {
                                           // Stop the ion-refresher from spinning
                                           $scope.$broadcast('scroll.refreshComplete');
                                       });

                                    }

  /**
   * @name selectUploadMethodComment
   * @todo To show popup for adding photo from camera or gallery in comment section
   * @return void function
   *
   **/


$scope.selectUploadMethodComment = function(){
// console.log("In Comment");
                                    var myPopup = $ionicPopup.show({
                                        template: '<div class="uploadMethodSack"><ul><li><a class="ink" ng-click="takePictureComment();"><span class="ion-camera"></span><span>Upload from Camera</span></a></li><li><a class=ink"" ng-click="selectPictureComment();"><span class="ion-images"></span><span>Upload from Gallery</span></a></li></ul></div>',
                                        title: 'Choose Upload Method',
                                        subTitle: 'Select from Camera or Gallery',
                                        scope: $scope
                                      });

                                      myPopup.then(function(res) {
                                        // console.log('Tapped!', res);
                                      });

                                      $timeout(function() {
                                         myPopup.close(); //close the popup after 3 seconds for some reason
                                      }, 8000);
                                  }


  /**
   * @name takePictureComment
   * @todo To take photo from camera in comment section
   * @return void function
   *
   **/
                                    $scope.takePictureComment = function(){
                                    // console.log("In Comment2");
                                       document.addEventListener("deviceready", function () {

                                          var options = {

                                            destinationType: Camera.DestinationType.FILE_URI,
                                            sourceType: Camera.PictureSourceType.CAMERA,
                                            allowEdit: true
                                          };

                                          $cordovaCamera.getPicture(options).then(function(imageURI) {
                                          // console.log(imageURI)
                                          $scope.uploadPicturexComment(imageURI);
                                            window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {
                                            //console.log(fileEntry.nativeURL);
                                             // $scope.status.postImage = imageURI;
                                             // console.log($scope.status.postImage);
                                              $scope.ftLoad = true;
              //                                var image = document.getElementById('myImage');
              //                                image.src = fileEntry.nativeURL;
                                              });
                                            $ionicLoading.show({template: 'Uploading Picture...', duration:500});
                                          },
                                          function(err){
                                            $ionicLoading.show({template: 'Errore di caricamento...', duration:500});
                                          })

                                        }, false);
                                    }


  /**
   * @name selectPictureComment
   * @todo To take photo from gallery in comment section
   * @return void function
   *
   **/

                                      $scope.selectPictureComment = function() {
                                      // console.log("In Comment3");
                                       document.addEventListener("deviceready", function () {
                                          var options = {
                                            quality: 100,
                                            destinationType: Camera.DestinationType.FILE_URI,
                                            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                                            encodingType: Camera.EncodingType.JPEG,
                                            targetWidth: 250,
                                            targetHeight: 250,
                                            allowEdit: true
                                          };

                                          $cordovaCamera.getPicture(options).then(
                                          function(imageURI) {
                                          $scope.uploadPicturexComment(imageURI);

                                            window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {
                                             // $scope.status.postImage = fileEntry.nativeURL;
                                              $scope.ftLoad = true;
                                      //				var image = document.getElementById('myImage');
                                      //				image.src = fileEntry.nativeURL;
                                              });
                                            $ionicLoading.show({template: 'Uploading Picture...', duration:500});
                                          },
                                          function(err){
                                            $ionicLoading.show({template: 'Errore di caricamento...', duration:500});
                                          })
                                           }, false);
                                        };


  /**
   * @name uploadPicturexComment
   * @todo To upload comment image to remote server
   * @return void function
   *
   **/




$scope.uploadPicturexComment = function(imageURI) {

// console.log("In Upload Picture Function");
// console.log(imageURI);
  var options = {

  fileKey: "picture",

  filename: "p1.png",

  chunkedMode: false,

  mimeType: "image/png",
  trustAllHosts:true,
  headers:{Connection:"close"}

  };
 $ionicLoading.show({
                                             template: '<ion-spinner icon="ios"></ion-spinner><p>Uploading Image!</p>'

                                         });
  $cordovaFileTransfer.upload(encodeURI(baseUrl+"/dashboard/uploadCommentmge"), imageURI, options)

  .then(function(result) {

  //$scope.status.commentImage = {};
    //    $scope.commentImage = [];
  // console.log(result.response);
   $scope.commentImage.push(imageURI);;
 var  resk  = angular.fromJson(result.response);

          $scope.commentImgs.push(resk.image_name) ;

// console.log($scope.commentImgs);
$ionicLoading.hide();


  }, function(err) {

  // console.log(err);
  $ionicLoading.hide();
  $ionicLoading.show({template: 'An error occured while uploading your picture.<br>Please try again.', duration:500});

  }, function (progress) {

            });



  };


  /**
   * @name showSwipeTutorial
   * @todo To check if user is self
   * @return void function
   *
   **/

$scope.userdashboard = function(result){
  if(result == user_id){
    $state.go('app.home');
  } else{
    $state.go('app.userdashboard', {result: result});
  }

    };


  /**
   * @name FriendsAllActivity
   * @todo To show user all activites
   * @return void function
   *
   **/



$scope.FriendsAllActivity = function(result)
    {
      // console.log(result);
      // console.log("Above");
      $state.go('app.allFriendsActivities', {result: result});




    }

});
