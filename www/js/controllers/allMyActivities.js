/**
 * Purpose:
 *@This file is used to Show All My Activities
 *
 **/

angular.module('starter').controller('allMyActivitiesCtrl', function(quote, $cordovaScreenshot, $cordovaSocialSharing, $cordovaToast, $cordovaNetwork, $rootScope, $cordovaPreferences, $ionicBackdrop, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading, $cordovaFileTransfer,$q) {

$scope.deviceInfo = deviceInformation.getInfo();
var uuid = $scope.deviceInfo.uuid;
$scope.selectedPost="";
 $scope.comment = {};
 $scope.commentImage = [];
 $scope.commentImgs = [];
 $scope.myStatus = {};
 $scope.myStatus.postImage = {};
  $scope.siteUrl = 'http://www.fitnessbasecamp.com/';
$scope.allActivity=[];
     $scope.postlimit = 20;
      $scope.Activity=[];
      $scope.currentrecordsview = 0;
      $scope.moreactvtse=true;

$scope.isOnline = [];
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();


        /**
         *
         * Listen for Online event
         *
         **/
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        /**
         *
         * Listen for Offline event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            // console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);


                  var userData = $window.localStorage['userData'];
                  $scope.userData = angular.fromJson(userData);


                  var user_id = $scope.userData.user_id;
                   quote.getQuote(uuid, user_id).success(function (res) {
                                                  // console.log(res);
                                                  $scope.quote = res;
                                                  });
                  var link = baseUrl+'/dashboard/all_mystatus';
                  var formData = {uu_id : uuid, user_id : user_id}
                  var postData = 'myData='+JSON.stringify(formData);
                      $ionicLoading.show({
                                template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                       $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                      }).success(function(res){


                       $scope.allActivity = res.mystatus;
                        res.mystatus  =  angular.fromJson(res.mystatus);


                        for(var i=0; i< $scope.postlimit ; i++) {
                        if(res.mystatus.length > i)
                        {
                         $scope.Activity.push(res.mystatus[i]);
                        }
                        }
                        $scope.currentrecordsview=$scope.postlimit;

                        $ionicLoading.hide();


                            $timeout(function() {
                                ionicMaterialMotion.fadeSlideInRight({
                                    startVelocity: 3000
                                });
                            }, 2000);

                            // Set Ink
                            ionicMaterialInk.displayEffect();


                      }) .error(function(error){
                        $ionicLoading.hide();
                        // console.log('request not completed');
                      })

                       $scope.like = function(id, i){
                        var status_id = id;
                        // console.log(i);

                        var link = baseUrl+'/dashboard/like_user_status';
                        // console.log('userID---'+ user_id + '--UUID--' + uuid + '--statusID' + status_id);
                        var formData = {uu_id : uuid, user_id : user_id, status_id : status_id}
                        var postData = 'myData='+JSON.stringify(formData);

                        // $ionicLoading.show({
                        // template: '<ion-spinner icon="ios"></ion-spinner>'
                        //
                        // });
                        $http({
                          method : 'POST',
                          url : link,
                          data: postData,
                          headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                      }).success(function(res){
                      $scope.postLiked = {};
                          // $ionicLoading.hide();

                          // console.log(res);
                          if(res.code == '200'){
                            $scope.postLiked = 'liked';
                            $scope.myStatus[i].likes = res.likes;
                            // console.log($scope.myStatus[i].likes);
                          } else if(res.code == '402'){
                            $scope.postLiked = 'Disliked';
                            $scope.myStatus.likes = res.likes;
                          } else{
                            // console.log('nothing');
                          }

                           $timeout(function() {
                              ionicMaterialMotion.fadeSlideInRight({
                                  startVelocity: 3000
                              });
                          }, 700);

                          // Set Ink
                          ionicMaterialInk.displayEffect();

                    })


                        }

  /**
   * @name postComment
   * @todo To Post Comment
   * @return void function
   *
   **/

                          $scope.postComment = function(id, i,mani){
                            var comment_text = mani[id+"_"+id];
                            var status_id = id;
                            var link = baseUrl+'/dashboard/post_comment';
                            var formData = {uu_id : uuid, user_id : user_id, comment_text : comment_text, status_id : status_id, comment_images : $scope.commentImgs}
                            var postData = 'myData='+JSON.stringify(formData);

                            $ionicLoading.show({
                            template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                            $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                          }).success(function(res){
                          mani[id+"_"+id] = '';
                             $ionicLoading.hide();
                             $scope.commentImage = [];
                                 $scope.commentImgs = [];
                              $scope.myStatus[i].comments = res.comments.comment;


                        })

                          }

  /**
   * @name doRefresh
   * @todo To Refresh Data
   * @return void function
   *
   **/

                      $scope.doRefresh = function() {
                      var user_id = $scope.userData.user_id;
                        var link = baseUrl+'/dashboard/all_mystatus';
                         // console.log('userID---'+ user_id + '--UUID--' + uuid);
                         var formData = {uu_id : uuid, user_id : user_id}
                         var postData = 'myData='+JSON.stringify(formData);

                         $ionicLoading.show({
                         template: '<ion-spinner icon="ios"></ion-spinner>'

                         });
                         $http({
                           method : 'POST',
                           url : link,
                           data: postData,
                           headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                       }).success(function(res){
                          $ionicLoading.hide();
                          $scope.myStatus = res.mystatus;
                          // console.log($scope.myStatus);


                     }).finally(function() {
                         $scope.$broadcast('scroll.refreshComplete');
                          $timeout(function() {
                              ionicMaterialMotion.fadeSlideInRight({
                                  startVelocity: 3000
                              });
                          }, 700);

                          // Set Ink
                          ionicMaterialInk.displayEffect();
                     });

                  }


  /**
   * @name removeCommentIMG
   * @todo To Remove Comment Image
   * @return void function
   *
   **/
//comment image uploading

$scope.removeCommentIMG = function(index)
{
// console.log("Image Index" + index);
$scope.commentImgs.splice(index, 1);  ;
$scope.commentImage.splice(index, 1);  ;
// console.log($scope.postImage);

}


  /**
   * @name selectUploadMethodComment
   * @todo To Select Upload Method Comment
   * @return void function
   *
   **/

$scope.selectUploadMethodComment = function(post_id){
if($scope.selectedPost != post_id)
{
 $scope.commentImage=[];
 $scope.commentImgs=[] ;

}

$scope.selectedPost = post_id;
// console.log("In Comment");
  var myPopup = $ionicPopup.show({
      template: '<div class="uploadMethodSack"><ul><li><a class="ink" ng-click="takePictureComment();"><span class="ion-camera"></span><span>Upload from Camera</span></a></li><li><a class=ink"" ng-click="selectPictureComment();"><span class="ion-images"></span><span>Upload from Gallery</span></a></li></ul></div>',
      title: 'Choose Upload Method',
      subTitle: 'Select from Camera or Gallery',
      scope: $scope
    });

    myPopup.then(function(res) {
      // console.log('Tapped!', res);
    });

    $timeout(function() {
       myPopup.close(); //close the popup after 3 seconds for some reason
    }, 8000);
}
  /**
   * @name takePictureComment
   * @todo To Take Picture in Comment
   * @return void function
   *
   **/
 $scope.takePictureComment = function(){
  // console.log("In Comment2");
     document.addEventListener("deviceready", function () {

        var options = {

          destinationType: Camera.DestinationType.FILE_URI,
          sourceType: Camera.PictureSourceType.CAMERA,
          allowEdit: true
        };

        $cordovaCamera.getPicture(options).then(function(imageURI) {
        // console.log(imageURI)
        $scope.uploadPicturexComment(imageURI);
          window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {

            $scope.ftLoad = true;
            });
          $ionicLoading.show({template: 'Uploading Picture...', duration:500});
        },
        function(err){
          $ionicLoading.show({template: 'Error Uploading...', duration:500});
        })

      }, false);
  }

  /**
   * @name selectPictureComment
   * @todo To Select Picture For Comment
   * @return void function
   *
   **/

    $scope.selectPictureComment = function() {
    // console.log("In Comment3");
     document.addEventListener("deviceready", function () {
        var options = {
          quality: 100,
          destinationType: Camera.DestinationType.FILE_URI,
          sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
          encodingType: Camera.EncodingType.JPEG,
          targetWidth: 250,
          targetHeight: 250,
          allowEdit: true
        };

        $cordovaCamera.getPicture(options).then(
        function(imageURI) {
        $scope.uploadPicturexComment(imageURI);

          window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {
           // $scope.status.postImage = fileEntry.nativeURL;
            $scope.ftLoad = true;

            });
          $ionicLoading.show({template: 'Uploading Picture...', duration:500});
        },
        function(err){
          $ionicLoading.show({template: 'Error Uploading...', duration:500});
        })
         }, false);
      };

  /**
   * @name uploadPicturexComment
   * @todo To Upload  Pictures For Comment
   * @return void function
   *
   **/

$scope.uploadPicturexComment = function(imageURI) {


  var options = {

  fileKey: "picture",

  filename: "p1.png",

  chunkedMode: false,

  mimeType: "image/png",
  trustAllHosts:true,
  headers:{Connection:"close"}

  };
 $ionicLoading.show({
                                             template: '<ion-spinner icon="ios"></ion-spinner><p>Uploading Image!</p>'

                                         });
  $cordovaFileTransfer.upload(encodeURI(baseUrl+"/dashboard/uploadCommentmge"), imageURI, options)

  .then(function(result) {

  //$scope.status.commentImage = {};
    //    $scope.commentImage = [];
  // console.log(result.response);
   $scope.commentImage.push(imageURI);;
 var  resk  = angular.fromJson(result.response);

          $scope.commentImgs.push(resk.image_name) ;

// console.log($scope.commentImgs);
$ionicLoading.hide();


  }, function(err) {

  // console.log(err);
  $ionicLoading.hide();
  $ionicLoading.show({template: 'An error occured while uploading your picture.<br>Please try again.', duration:500});

  }, function (progress) {

            });



  }

                  ///////////// For Loading More Data //////////////

  /**
   * @name loadmoreact
   * @todo To Load More Actions
   * @return void function
   *
   **/
 $scope.loadmoreact = function() {
   var lim = $scope.currentrecordsview + $scope.postlimit;

                                        for(var i=$scope.currentrecordsview;i< lim;i++)
                                        {
                                          if(i < $scope.allActivity.length)
                                          {
                                            $scope.Activity.push($scope.allActivity[i]);
                                          }
                                          else
                                          {
                                            $scope.moreactvtse=false;
                                            break;
                                          }
                                        }
                                        // console.log($scope.Fctivity);
                                      $scope.currentrecordsview=lim;
                                      $timeout(function() {
                                                                      ionicMaterialMotion.fadeSlideInRight({
                                                                          startVelocity: 3000
                                                                      });
                                                                  }, 1000);

                                                                  // Set Ink
                                                                  ionicMaterialInk.displayEffect();
                                        $scope.$broadcast('scroll.infiniteScrollComplete');
                                    };
                                    $scope.$on('$stateChangeSuccess', function() {
                                      $scope.loadmoreact();
                                    });

                    //////////////// End For Loading More data ////////////////








});
