/**
 * Purpose:
 *@This file is used to Add Feedback
 *
 **/

angular.module('starter').controller('FeedbackCtrl', function(notification, $cordovaInAppBrowser, $cordovaToast, quote, $rootScope,  $cordovaNetwork, $window, ionicDatePicker,$ionicModal, $ionicPopover, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading) {
 // console.log(GuserData.user_fname);

$ionicPopover.fromTemplateUrl('templates/popover.html', {
    scope: $scope,
  }).then(function(popover) {
    $scope.popover = popover;
  });
  document.addEventListener("deviceready", function () {
    $scope.network = $cordovaNetwork.getNetwork();
    $scope.isOnline = $cordovaNetwork.isOnline();


    /**
     *
     * Listen for Online event
     *
     **/
    $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
      $scope.isOnline = true;
      $scope.network = $cordovaNetwork.getNetwork();
      // $scope.syncLocData();
//            $cordovaToast.showLongBottom('You are Online !')

    })

    /**
     *
     * Listen for Offline event
     *
     **/
    $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){

      $scope.isOnline = false;
      $scope.network = $cordovaNetwork.getNetwork();
      $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
    })

  }, false);


  var userData = $window.localStorage['userData'];
  $scope.userData = angular.fromJson(userData);
  // console.log($scope.userData);

  //  console.log("Omar User data consoling "+JSON.stringify($scope.userData));

  // after getting data from sqlite getting user id and then running dashboard service
  $scope.userImg = $scope.userData.image;
  $scope.username = $scope.userData.user_fname;
  $scope.Lusername = $scope.userData.user_lname;

  // console.log("Userimg Source== "+$scope.userImg);

  var user_id = $scope.userData.user_id;

  /**
   * @name getQuote
   * @todo To Get Random Quotes
   * @return void function
   *
   **/

  quote.getQuote(uuid, user_id).success(function (res) {
    $ionicLoading.hide();
    $scope.quote = res;
    $window.localStorage['quotes']  = JSON.stringify($scope.quote);

  });
  $scope.notificationFull = [];
  $scope.notification = [];
  $scope.$on('$ionicView.beforeEnter', function(){
    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner>'

    });
    notification.get(uuid, user_id).success(function (res) {

      var notifications = res;
      $window.localStorage['notifications']  = JSON.stringify(notifications);
      var localNoti = $window.localStorage['notifications'];
      $scope.notification = angular.fromJson(localNoti);
      $scope.notificationFull = $scope.notification.notification;
      if($scope.notificationFull.length > 0){
        if($scope.notificationFull[0].count == 0){
          $scope.green = "green";
        } else if($scope.notificationFull[0].count > 0){
          $scope.green = "red";
        }
      }

      $scope.changeColor = function(){
        $scope.green = "green";

        //;


      }
    });
  });
  $scope.sugg = {
    fname : $scope.userData.user_fname,
    lname : $scope.userData.user_lname,
    email : $scope.userData.user_email,
    area : 'Dashboard',
    suggText : ''
  };
  /**
   * @name doSuggest
   * @todo To Get Information of user for feedback
   * @return void function
   *
   **/
  $scope.doSuggest = function(fname, lname, email, area, text){

    var user_id = $scope.userData.user_id;

    var link = baseUrl+'/dashboard/suggestionsMade';
    var fName = fname;
    var lName = lname;
    var email = email;
    var area = area;
    var suggestion = text;
    var formData = {uu_id : uuid, user_id : user_id, fName : fName, lName : lName, email : email, area : area, suggestion : suggestion}

    var postData = 'myData='+JSON.stringify(formData);

    if(suggestion == undefined || suggestion == ''){
      var alertPopup = $ionicPopup.alert({
        title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
        template: 'All the Fields Must be Filled',
        cssClass: 'loginErrorPopUp hideOk'
      });

      $timeout(function () {
//                       $ionicLoading.hide();
        alertPopup.close();

      }, 6000);
    } else{
      $ionicLoading.show({
        template: '<ion-spinner icon="ios"></ion-spinner>'

      });
      $http({
        method : 'POST',
        url : link,
        data: postData,
        headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

      }).success(function(res){

        $ionicLoading.hide();
        var alertPopup = $ionicPopup.alert({
          title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
          template: res.message,
          cssClass: 'loginErrorPopUp hideOk'
        });
        $timeout(function () {
          alertPopup.close();
          $state.go('app.home');
        }, 3000);


      }) .error(function(error){
        $ionicLoading.hide();

      })
    }



  };
});
