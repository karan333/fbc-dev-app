/**
 * Purpose:
 *@This file is used to show user profile
 *
 **/





angular.module('starter').controller('profileController', function($cordovaInAppBrowser, quote, ionicDatePicker , $ionicModal, $ionicPopover, $cordovaToast, $cordovaNetwork, $rootScope, $cordovaPreferences, $ionicBackdrop, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading, $cordovaFileTransfer,$q) {
//popover
$ionicPopover.fromTemplateUrl('templates/popover.html', {
    scope: $scope,
  }).then(function(popover) {
    $scope.popover = popover;
  });

  /**
   *
   * Initializing Variables, Arrays, and Objects
   *
   **/



$scope.deviceInfo = deviceInformation.getInfo();

var uuid = $scope.deviceInfo.uuid;
$scope.siteUrl = 'http://www.fitnessbasecamp.com/';
var userData = $window.localStorage['userData']
$scope.userData = angular.fromJson(userData);
// console.log($scope.userData);

$scope.userImg = $scope.userData.image;
$scope.userCover = $scope.siteUrl + $scope.userData.cover_image;
$scope.username = $scope.userData.user_fname;
$scope.Lusername = $scope.userData.user_lname;
$scope.email = $scope.userData.user_email;
$scope.measurement = $scope.userData.measurement;
$scope.joining = $scope.userData.user_subscription_date;
$scope.phone = $scope.userData.phone;
$scope.share = [];


$scope.username_edit = $scope.username;
$scope.Lusername_edit = $scope.userData.user_lname;
$scope.email_edit = $scope.userData.user_email;
$scope.measurement_edit = $scope.userData.measurement;
$scope.joining_edit = $scope.userData.user_subscription_date;
$scope.phone_edit = $scope.userData.phone;
$scope.share_edit = $scope.userData.auto_social_sharing;



var user_id = $scope.userData.user_id;
$scope.isExpanded = false;


$scope.password = [];
$scope.address = [];
$scope.UadditionalData = [];
$scope.isOnline = [];





      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();


        /**
         *
         * Listen for Online event
         *
         **/
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        /**
         *
         * Listen for ofline event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            // console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);

  /**
   *
   * date picker initialize
   *
   **/
var ipObj1 = {
 callback: function (val) {
   var date = (new Date(val));

           var day = date.getDate();
           var year = date.getFullYear();
           var month = date.getMonth()+ 1;
   $scope.UadditionalData.user_dob = day+'/'+ month+'/'+year;

 },
 from: new Date(1970, 1, 1),
 to: new Date(2016, 12, 30),
 closeOnSelect: true
}
 $scope.openDatePicker = function(){
   // console.log('hi');
 ionicDatePicker.openDatePicker(ipObj1);
//                              $scope.showPopup();
};


      $scope.siteUrl = siteUrl;

  /**
   *
   * for quotes
   *
   **/

      quote.getQuote(uuid, user_id).success(function (res) {
        // console.log(res);
        $scope.quote = res;
      });


  /**
   *
   * to get user address information
   *
   **/

      var link = baseUrl+'/dashboard/get_useraddress_info';
      var formData = {uu_id : uuid, user_id : user_id}
      var postData = 'myData='+JSON.stringify(formData);
      if ($scope.isOnline == true){
      $ionicLoading.show({
                          template: '<ion-spinner icon="android"></ion-spinner>'

                      });
                 $http({
                        method : 'POST',
                        url : link,
                        data: postData,
                        headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                }).success(function(res){
                // console.log(res);
                $scope.address = res.address_info[0];
                $scope.UadditionalData = res.user_data[0];
                    // Set Motion
                    $timeout(function() {
                        ionicMaterialMotion.slideUp({
                            selector: '.slide-up'
                        });
                    }, 1000);
                    $timeout(function() {
                        ionicMaterialMotion.fadeSlideInRight({
                            startVelocity: 3000
                        });
                    }, 1500);
                    ionicMaterialInk.displayEffect();
                $ionicLoading.hide();

                }) .error(function(error){
                   $ionicLoading.hide();
                   // console.log('request is not completed');
                })
      } else{
      $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
      }










  /**
   *
   * change password popup
   *
   **/



          $scope.showPopup = function() {
           if ($scope.isOnline == true){
           $scope.data = {};

                     // An elaborate, custom popup
                     var myPopup = $ionicPopup.show({
                       template: '<div class="changePassword"><span>Old Password *</span><input type="password" placeholder="Old Password" ng-model="password.old" name="oldPassword"><span>New Password *</span><input type="password" placeholder="New Password"  ng-model="password.new" name="newPassword"><span>Confirm New Password * </span><input type="password" placeholder="Confirm New Password"  ng-model="password.Cnew" name="CnewPassword"></div>',
                       title: 'Change Password',
                       scope: $scope,
                       cssClass : 'statFilterPopup updatePassword',
                       buttons: [
           //                            { text: 'Cancel' },
                         {
                           text: '<b>Update</b>',
                           type: 'button-positive',
                           onTap: function(e) {

                           ////////////////////////////////////////

                            var user_id = $scope.userData.user_id;
                            // console.log('userID---' + user_id + 'uuid----' + uuid);
                            var link = baseUrl+'/dashboard/update_password';
                            var old_password = $scope.password.old;
                            var old_password_encode = $window.btoa(old_password);
                            var new_password = $scope.password.new;
                            var new_password_encode = $window.btoa(new_password);
                            // console.log('encode Old' + old_password_encode);
                            // console.log('decode Old' + $window.atob(old_password_encode));
                            var formData = {uu_id : uuid, user_id : user_id, old_password : old_password_encode, new_password : new_password_encode}
                            // console.log(formData);
                            var postData = 'myData='+JSON.stringify(formData);
                                $ionicLoading.show({
                                          template: '<ion-spinner icon="android"></ion-spinner>'

                                      });
                                 $http({
                                        method : 'POST',
                                        url : link,
                                        data: postData,
                                        headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                }).success(function(res){
                                // console.log(res);
                                if(res.status == true){
                                $window.localStorage['password'] = null;
                                $window.localStorage['username'] = null;
                                }

                                 $ionicLoading.hide();
                                 var alertPopup = $ionicPopup.alert({
                                   title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                   template: res.message,
                                   cssClass: 'loginErrorPopUp'
                                 });

                                     $timeout(function() {
                                      alertPopup.close();
                                    }, 5000);


                                    $timeout(function() {
                                        ionicMaterialMotion.fadeSlideInRight({
                                            startVelocity: 3000
                                        });
                                    }, 700);

                                    // Set Ink
                                    ionicMaterialInk.displayEffect();


                                }) .error(function(error){
                                  $ionicLoading.hide();
                                  // console.log('request not completed');
                                })


                           }
                         }
                       ]
                     });

                     myPopup.then(function(res) {

                      });
           } else{
           $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
           }

         };



  /**
   *
   * edit profile section
   *
   **/


         $ionicModal.fromTemplateUrl('templates/modal.html', {
             scope: $scope
           }).then(function(modal) {
            if ($scope.isOnline == true){
            $scope.modal = modal;
                         $scope.editProfile = function(first_name,last_name,dob,phone,email,address,city,zip,state,country,measurements,share_completed_workout_sm){


                         var user_id = $scope.userData.user_id;
                         var link = baseUrl+'/dashboard/edit_profile';
                         var formData = {uu_id : uuid, user_id : user_id, first_name : first_name, last_name : last_name, dob : dob, phone : phone, email : email, address : address, city : city, zip : zip, state : state, country : country, measurements : measurements, share_completed_workout_sm : share_completed_workout_sm}
                         // console.log(formData);
                         var postData = 'myData='+JSON.stringify(formData);
                             $ionicLoading.show({
                                       template: '<ion-spinner icon="android"></ion-spinner>'

                                   });
                              $http({
                                     method : 'POST',
                                     url : link,
                                     data: postData,
                                     headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                             }).success(function(res){
                             if(res.code == 200)
                             {

                                var existing_email= $scope.userData.user_email;
                                 if(existing_email != res.user_data.user_email)
                                 {

                                          $window.localStorage['password'] = null;
                                          $window.localStorage['username'] = null;

                                 }
                              var userData = JSON.stringify(res.user_data);
                               $window.localStorage['userData'] = userData;
                               $scope.userData = angular.fromJson(userData);
                               $scope.username = $scope.userData.user_fname;
                               $scope.Lusername = $scope.userData.user_lname;
                               $scope.email = $scope.userData.user_email;
                               $scope.measurement = $scope.userData.measurement;
                               $scope.joining = $scope.userData.user_subscription_date;
                               $scope.phone = $scope.userData.phone;
                               $scope.share_edit = $scope.userData.auto_social_sharing;
                               // console.log($window.localStorage['userData']);
                               $ionicLoading.hide();
                               // console.log(res);
                              $timeout(function() {
                                  ionicMaterialMotion.fadeSlideInRight({
                                      startVelocity: 3000
                                  });
                                  $ionicLoading.hide();
                              }, 700);

                              // Set Ink
                              ionicMaterialInk.displayEffect();
                              //$scope.popover.remove();
                               //$state.go($state.current, {}, {reload: true});
                               $scope.modal.hide();


                              }
                              else {
                                $ionicLoading.hide();
                               // console.log(res);
                                 var alertPopup = $ionicPopup.alert({
                                    title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                    template: res.message,
                                    cssClass: 'loginErrorPopUp'
                                  });


                                      $timeout(function() {
                                       alertPopup.close();

                                     }, 5000);


                                 $timeout(function() {
                                     ionicMaterialMotion.fadeSlideInRight({
                                         startVelocity: 3000
                                     });
                                     $ionicLoading.hide();
                                 }, 700);

                                 // Set Ink
                                 ionicMaterialInk.displayEffect();
                                }

                             }) .error(function(error){
                               $ionicLoading.hide();
                               // console.log('request not completed');
                             })
                             }
            }else {
             $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
            }

           });



  /**
   *
   * show edit modal
   *
   **/




           $scope.$on('modal.shown', function() {
           $timeout(function() {
               ionicMaterialMotion.fadeSlideInRight({
                   startVelocity: 3000
               });
               $ionicLoading.hide();
           }, 700);

           // Set Ink
           ionicMaterialInk.displayEffect();
           });
  /**
   * @name selectPicture
   * @todo To show popup for adding photo from camera or gallery
   * @return void function
   *
   **/

$scope.selectPicture = function() {
// console.log("In Seldct Picture");
                       document.addEventListener("deviceready", function () {
                          var options = {
                            quality: 100,
                            destinationType: Camera.DestinationType.FILE_URI,
                            sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
                            encodingType: Camera.EncodingType.JPEG,
                            targetWidth: 500,
                            targetHeight: 500,
                            allowEdit: true
                          };

                          $cordovaCamera.getPicture(options).then(
                          function(imageURI) {
                           $scope.uploadPicturex(imageURI);
//                            window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {
//                             $scope.refine.pic = fileEntry.nativeURL;
//                             console.log( $scope.upload.pic[id] );
//                              $scope.ftLoad = true;
//                      //
//                              });
                            $ionicLoading.show({template: 'Uploading Photo...', duration:500});
                          },
                          function(err){
                            $ionicLoading.show({template: 'Error While Uploading Image...', duration:500});
                          })
                           }, false);
                        };




  /**
   * @name uploadPicturex
   * @todo To upload series of photos to remote server
   * @return void function
   *
   **/
$scope.uploadPicturex = function(imageURI) {

// console.log("In Upload Picture Function");
// console.log(imageURI);
  var options = {

  fileKey: "picture",

  filename: "p1.png",

  chunkedMode: false,

  mimeType: "image/png",
  trustAllHosts:true,
  headers:{Connection:"close"}

  };
 $ionicLoading.show({
                                             template: '<ion-spinner icon="android"></ion-spinner><p>Uploading Image!</p>'

                                         });
                                          var user_id = $scope.userData.user_id;
  $cordovaFileTransfer.upload(encodeURI(baseUrl+"/dashboard/upload_profile_image/"+user_id), imageURI, options)

  .then(function(result) {
  // console.log(result);

  if(result.responseCode == 200)
                               {
res=angular.fromJson(result.response);
// console.log(res.image_path);
                                 $scope.existingData  = $window.localStorage['userData'];
                                 $scope.ExistingDObject = angular.fromJson($scope.existingData);
                                 $scope.ExistingDObject.image=  res.image_path;
                                  $window.localStorage['userData'] = JSON.stringify($scope.ExistingDObject);
                                 $scope.userImg = res.image_path;
                                 $state.go($state.current, {}, {reload: true});

                                 $ionicLoading.hide();

                                $timeout(function() {
                                    ionicMaterialMotion.fadeSlideInRight({
                                        startVelocity: 3000
                                    });
                                    $ionicLoading.hide();
                                }, 700);

                                // Set Ink
                                ionicMaterialInk.displayEffect();
                                //$scope.popover.remove();
                                 //$state.go($state.current, {}, {reload: true});



                                }
                                else {
                                  $ionicLoading.hide();
                                 // console.log(result);
                                   var alertPopup = $ionicPopup.alert({
                                      title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                      template: result.response.message,
                                      cssClass: 'loginErrorPopUp'
                                    });


                                        $timeout(function() {
                                         alertPopup.close();

                                       }, 5000);


                                  }
$ionicLoading.hide();


  }, function(err) {

  // console.log(err);
  $ionicLoading.hide();
  $ionicLoading.show({template: 'An error occured while uploading your picture.<br>Please try again.', duration:500});

  }, function (progress) {

            });



  }




  /**
   * @name selectPicture2
   * @todo To show popup for adding photo from camera or gallery for cover photo
   * @return void function
   *
   **/



$scope.selectPicture2 = function() {
// console.log("In Seldct Picture");
                       document.addEventListener("deviceready", function () {
                          var options = {
                            quality: 100,
                            destinationType: Camera.DestinationType.FILE_URI,
                            sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
                            encodingType: Camera.EncodingType.JPEG,
                            targetWidth: 1024,
                            targetHeight: 768,
                            allowEdit: true
                          };

                          $cordovaCamera.getPicture(options).then(
                          function(imageURI) {
                           $scope.uploadPicturex2(imageURI);
//                            window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {
//                             $scope.refine.pic = fileEntry.nativeURL;
//                             console.log( $scope.upload.pic[id] );
//                              $scope.ftLoad = true;
//                      //
//                              });
                            $ionicLoading.show({template: 'Uploading Photo...', duration:500});
                          },
                          function(err){
                            $ionicLoading.show({template: 'Error While Uploading Image...', duration:500});
                          })
                           }, false);
                        };



  /**
   * @name uploadPicturex2
   * @todo To upload series of photos to remote server for cover photo
   * @return void function
   *
   **/



$scope.uploadPicturex2 = function(imageURI) {

// console.log("In Upload Picture Function");
// console.log(imageURI);
  var options = {

  fileKey: "picture",

  filename: "p1.png",

  chunkedMode: false,

  mimeType: "image/png",
  trustAllHosts:true,
  headers:{Connection:"close"}

  };
 $ionicLoading.show({
                                             template: '<ion-spinner icon="android"></ion-spinner><p>Uploading Image!</p>'

                                         });
                                          var user_id = $scope.userData.user_id;
  $cordovaFileTransfer.upload(encodeURI(baseUrl+"/dashboard/upload_cover_image/"+user_id), imageURI, options)

  .then(function(result) {
  // console.log(result);

  if(result.responseCode == 200)
                               {
res=angular.fromJson(result.response);
// console.log(res.image_path);
                                 $scope.existingData  = $window.localStorage['userData'];
                                 $scope.ExistingDObject = angular.fromJson($scope.existingData);
                                 $scope.ExistingDObject.cover_image=  res.image_path;
                                  $window.localStorage['userData'] = JSON.stringify($scope.ExistingDObject);
                                 $scope.userCover = res.image_path;
                                 $state.go($state.current, {}, {reload: true});

                                 $ionicLoading.hide();

                                $timeout(function() {
                                    ionicMaterialMotion.fadeSlideInRight({
                                        startVelocity: 3000
                                    });
                                    $ionicLoading.hide();
                                }, 700);

                                // Set Ink
                                ionicMaterialInk.displayEffect();
                                //$scope.popover.remove();
                                 //$state.go($state.current, {}, {reload: true});



                                }
                                else {
                                  $ionicLoading.hide();
                                 // console.log(result);
                                   var alertPopup = $ionicPopup.alert({
                                      title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                      template: result.response.message,
                                      cssClass: 'loginErrorPopUp'
                                    });


                                        $timeout(function() {
                                         alertPopup.close();

                                       }, 5000);


                                  }
$ionicLoading.hide();


  }, function(err) {

  // console.log(err);
  $ionicLoading.hide();
  $ionicLoading.show({template: 'An error occured while uploading your picture.<br>Please try again.', duration:500});

  }, function (progress) {

            });



  };


  /**
   * @name openBrowser
   * @todo To open InAppBrowser
   * @return void function
   *
   **/



  $scope.openBrowser = function(){
    var user_id = $scope.userData.user_id;
    var options = {
      location: 'yes',
      clearcache: 'yes',
      toolbar: 'no'
    };
    $cordovaInAppBrowser.open('http://www.fitnessbasecamp.com/loginUser_admin/'+user_id+'/app', '_system', options)
      .then(function(event) {
        // console.log(event);
      })
      .catch(function(event) {
        // error
      });


    // $cordovaInAppBrowser.close();
  }


});
