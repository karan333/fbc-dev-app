/**
 * Purpose:
 *@This file is used to Find Friends
 *
 **/

angular.module('starter').controller('fFriendsCtrl', function($ionicModal, quote, $cordovaToast, $rootScope,  $cordovaNetwork, $window, deviceInformation, $scope, $cordovaSQLite, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $ionicLoading, $ionicPopup) {
  $scope.siteUrl = siteUrl;
   $scope.deviceInfo = deviceInformation.getInfo();
   var uuid = $scope.deviceInfo.uuid;

   $scope.friendSearch = {};
   $scope.friend = {};
$scope.isOnline = [];
$scope.noResultFound = 0;
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();


        /**
         *
         * Listen for Online event
         *
         **/
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        /**
         *
         * Listen for Offline event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            // console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);
                             var userData = $window.localStorage['userData'];
                            $scope.userData = angular.fromJson(userData);
  var user_id = $scope.userData.user_id;
  quote.getQuote(uuid, user_id).success(function (res) {
    // console.log(res);
    $ionicLoading.hide();
    $scope.quote = res;
  });
                           // after getting data from sqlite getting user id and then all news feed service
                           $scope.searchFriend= function(que){


                            var link = baseUrl+'/friends/searchFriends';
                            var que = que;
                            // console.log('userID---'+ user_id + '--UUID--' + uuid + 'que----' + que);
                            var formData = {uu_id : uuid, user_id : user_id, que : que}
                            var postData = 'myData='+JSON.stringify(formData);

                            $ionicLoading.show({
                            template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                            $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                          }).success(function(res){

                              // console.log(res);
                              if(res.code == '400'){
                                $scope.noResultFound = true;
                              }else{
                                $scope.friend = res.friends.search_res;
                                $scope.noResultFound = false;
                              }

                             // console.log("friends object = "+$scope.friend);



                          $ionicLoading.hide();
                          $timeout(function() {
                              ionicMaterialMotion.fadeSlideInRight({
                                  startVelocity: 3000
                              });
                          }, 700);

                          // Set Ink
                          ionicMaterialInk.displayEffect();


                        })
                      }
  /**
   * @name addFriend
   * @todo To Send Friend Request
   * @return void function
   *
   **/
                         $scope.addFriend = function(id, qd){
                             var user_id = $scope.userData.user_id;
                              var link = baseUrl+'/friends/send_friend_request';
                              var recepient_id = id;
                              // console.log("mani");
                              // console.log('userID---'+ user_id + '--UUID--' + uuid + 'recepient_id----' + recepient_id);
                              var formData = {uu_id : uuid, user_id : user_id, recepient_id : recepient_id, que : qd}
                              var postData = 'myData='+JSON.stringify(formData);

                              $ionicLoading.show({
                              template: '<ion-spinner icon="ios"></ion-spinner>'

                              });
                              $http({
                                method : 'POST',
                                url : link,
                                data: postData,
                                headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                            }).success(function(res){

                                // console.log(res);

                              $scope.friend = res.friends.search_res;
                              // console.log("again in friends object=== "+ $scope.friend);
                                var alertPopup = $ionicPopup.alert({
                                 title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                 template: res.message,
                                 cssClass: 'loginErrorPopUp hideOk'
                               });
                                $timeout(function () {
                                  alertPopup.close();
                                }, 3000);



                            $ionicLoading.hide();
                            $timeout(function() {
                                ionicMaterialMotion.fadeSlideInRight({
                                    startVelocity: 3000
                                });
                            }, 700);

                            // Set Ink
                            ionicMaterialInk.displayEffect();


                          })
                         }

  $scope.showSearchIcon = true;
  $scope.change = function(text) {
    if($scope.friendSearch.text == ''){
      $scope.showSearchIcon = true;
    }
    // console.log(text);
    $scope.showSearchIcon = false;
  };
  $scope.emptySearch = function(text){
    $scope.friend = {};
    $scope.showSearchIcon = true;
    $scope.friendSearch.text ="";
    // console.log($scope.friendSearch.text);
    $scope.noResultFound = 0;
  }


  //invite friend

  $ionicModal.fromTemplateUrl('templates/modal.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;



    // Set Ink
    ionicMaterialInk.displayEffect();
  });
  $scope.closeModal = function() {
    $scope.modal.hide();
  };

  $scope.inviteFriend = function(email){
    var link = baseUrl+'/friends/inviteFriend';
    // console.log('userID---'+ user_id + '--UUID--' + uuid + 'email----' + email);
    var formData = {uu_id : uuid, user_id : user_id, email : email};
    var postData = 'myData='+JSON.stringify(formData);

    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner>'
    });
    $http({
      method : 'POST',
      url : link,
      data: postData,
      headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

    }).success(function(res){
      // console.log(res);
      var alertPopup = $ionicPopup.alert({
        title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
        template: res.message,
        cssClass: 'loginErrorPopUp hideOk'
      });
      $timeout(function () {
        alertPopup.close();
      }, 3000);
      $scope.modal.hide();
      $ionicLoading.hide();
    });

  }

});
