/**
 * Purpose:
 *@This file is used as to show pre workout timer
 *
 **/




angular.module('starter').controller('MyPreWorkoutTimerCtrl', function($ionicPlatform, quote, $cordovaToast,  $cordovaNetwork, $log,$q, video, $document, $ionicSlideBoxDelegate, $ionicPopover, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading, $cordovaFileTransfer, $rootScope, $ionicModal) {



  /**
   *
   * Initializing Variables, Arrays, and Objects
   *
   **/



  window.plugins.insomnia.keepAwake();
  $scope.deviceInfo = deviceInformation.getInfo();
  var uuid = $scope.deviceInfo.uuid;
  $scope.siteUrl = siteUrl;
  $scope.allExercises = {};
  $scope.videos = {};
  $scope.nct = 0;
  $scope.disablenext = false;
  $scope.workout_Completed = 0;
  var mani = [];
  $scope.TotalDownloaded = 0;
  $scope.progresss = 0;
  $scope.vsble = false;
  $scope.backhit = false;
  $scope.isOnline = [];
  $scope.isPlayed = false;
  $scope.timerCompleted = false;
  $scope.firstVideoDownloaded = false;


  /**
   *
   * to initialize text to speech
   *
   **/

  document.addEventListener("deviceready", function () {
    $scope.network = $cordovaNetwork.getNetwork();
    $scope.isOnline = $cordovaNetwork.isOnline();
    TTS.speak({
      text: '',
      locale: 'en-US',
      rate: 1.5
    }, function () {
      // alert('success');
    }, function (reason) {
      // alert(reason);
    });


    /**
     *
     * Listen for Online event
     *
     **/
    $rootScope.$on('$cordovaNetwork:online', function (event, networkState) {
      $scope.isOnline = true;
      $scope.network = $cordovaNetwork.getNetwork();
      $cordovaToast.showLongBottom('You are Online !')

    })

    /**
     *
     * Listen for Offline event
     *
     **/
    $rootScope.$on('$cordovaNetwork:offline', function (event, networkState) {
      // console.log("got offline");
      $scope.isOnline = false;
      $scope.network = $cordovaNetwork.getNetwork();
      $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
    })

  }, false);


  /**
   * @name lockSlide
   * @todo To disabe slider swipe
   * @return void function
   *
   **/


  $scope.lockSlide = function () {
    $ionicSlideBoxDelegate.enableSlide(false);
  };



  /**
   *
   * getting user data
   *
   **/


  $scope.videos = {};
  var userData = $window.localStorage['userData'];
  $scope.userData = angular.fromJson(userData);

  // after getting data from sqlite getting user id and then all news feed service

  var user_id = $scope.userData.user_id;



  /**
   *
   * for quote
   *
   **/


  quote.getQuote(uuid, user_id).success(function (res) {
    // console.log(res);
    $scope.quote = res;
  });
  var workout_id = $stateParams.result;



  /**
   *
   * getting workout details
   *
   **/


  var link = baseUrl + '/workouts/my_workout_details';
  var formData = {uu_id: uuid, user_id: user_id, workout_id: workout_id}
  var postData = 'myData=' + JSON.stringify(formData);
  $ionicLoading.show({
    template: '<ion-spinner icon="ios"></ion-spinner>'

  });
  $http({
    method: 'POST',
    url: link,
    data: postData,
    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

  }).success(function (res) {
    $ionicLoading.hide();
    // console.log(res);
    $scope.workout_name = res.workout[0].name;
    $scope.allExercises = res.workout[0].workout_exercises;
    $scope.totalExercises = $scope.allExercises.length;
    // console.log("workout name= " + $scope.workout_name);

    res.exercise = angular.fromJson($scope.allExercises);

    /**
     * @name recursiFnc
     * @to to check for exercises to if already exist
     * @return void function
     *
     **/


    $scope.recursiFnc = function (indexx) {
      if (indexx < $scope.totalExercises) {
        $scope.disablenext = true;
        var url = res.exercise[indexx].download_url_linl;
        // console.log("url======== " + url);
        var filename =res.exercise[indexx].workout_id+res.exercise[indexx].custom + ".mp4";
        // console.log('filename ===== ' + filename);
        if (url != "") {

          var targetPath = cordova.file.dataDirectory + 'FitnessTest/' + filename;
          $cordovaFile.checkFile(cordova.file.dataDirectory + 'FitnessTest/', filename)
            .then(function (success) {
              $scope.isFirstExerDownloaded = true;


              var cidw = indexx + 1;
              $scope.recursiFnc(cidw);
              if (indexx == 0) {

                if($scope.isPlayed==false) {
                  $scope.disablenext = true;
                  // $scope.playfirstvideo();
                  $scope.isPlayed = true;
                  $scope.firstVideoDownloaded = true;
                }
              }
              else if (indexx == $scope.totalExercises) {

                $scope.disablenext = false;
                $scope.firstVideoDownloaded = true;

              }

            }, function (error) {
              // console.log("Downloading " + indexx + " Exercise");
              if (indexx == 0) {

//                            $ionicLoading.show({
//                              template: '<ion-spinner icon="ios"></ion-spinner><div class="card" ><div class="item item-text-wrap"><div class="loader" style="width: {{$scope.progresss}};"><p class="percent">{{$scope.progresss}}</p></div></div></div>'
//
//                          });
                $scope.loaderShow = true;
              }
              saveVideoToPhone(url, filename);
            });

        }
        else {
          var cidw = indexx + 1;
          $scope.recursiFnc(cidw);

        }
      }
      else {

        $ionicLoading.hide();
        $scope.loaderShow = false;
        $scope.disablenext = false;
        $scope.firstVideoDownloaded = true;

      }

    }



    $scope.recursiFnc(0);


    /**
     * @name saveVideoToPhone
     * @to to download videos if not found in phone
     * @return void function
     *
     **/


    function saveVideoToPhone(url, file) {
      if ($scope.backhit == true) {
        return false;
      }


//                         console.log("In Save Video Function "+mani.length);

      // console.log("FileName In Function " + file);
      var deferred = $q.defer();
      var url = url;

      var targetPath = cordova.file.dataDirectory + 'FitnessTest/' + file;

      // console.log("In Save Video Function "+mani.length);

      $cordovaFile.checkFile(cordova.file.dataDirectory + 'FitnessTest/', file)
        .then(function (success) {


        }, function (error) {


//                         console.log("mani");
          ////////////////////////////
          $cordovaFileTransfer.download(url, targetPath, {}, true).then(function (result) {
//                                      console.log('Save file on '+targetPath+' success!');
            $scope.TotalDownloaded++;


            if ($scope.TotalDownloaded == $scope.totalExercises) {


              $ionicLoading.hide();
              $scope.loaderShow = false;
              $scope.disablenext = false;
              $scope.firstVideoDownloaded = true;

            }
            else {
              $scope.recursiFnc($scope.TotalDownloaded);

            }
          }, function (error) {
            $scope.TotalDownloaded++;
//                                       console.log('Error Download file' + console.log(error));

          }, function (progress) {
           // console.log((progress.loaded / progress.total) * 100);
            $scope.progresss = 0;
            $scope.progresss = (progress.loaded / progress.total) * 100;
            $scope.progresss = Math.floor($scope.progresss) + "%";


            if (((progress.loaded / progress.total) * 100) == 100) {
              if ($scope.TotalDownloaded == 0) {
                if($scope.isPlayed==false) {
                  $scope.firstVideoDownloaded = true;
                  $ionicLoading.hide();
                  $scope.loaderShow = false;
                  $scope.isPlayed=true;
                  // console.log('video downloaded');
                  if($scope.timerCompleted == true) {
                    $scope.moveToDetail();
                  }

                }

              }

            }
          });


        });


    }


    /**
     * @name moveToDetail
     * @to to route to workout detail page
     * @return void function
     *
     **/



    $scope.moveToDetail = function(){
      // console.log($scope.progresss);
      // console.log( $scope.timerCompleted);
      $scope.done = true;
      var ply = document.getElementById('audio');
      ply.pause();
      $scope.backhit = true;
      $timeout(function() {
        // console.log('now move to workout detail');
        $state.go('app.customWorkoutDetail', {result: workout_id});
      }, 1500);
    };

    /**
     * @name timer
     * @todo To Start Timer
     * @return void function
     *
     **/


    var mytimeout = null; // the current timeoutID
    // actual timer method, counts down every second, stops on zero
    $scope.onTimeout = function() {
      if ($scope.timer === 0) {
        $scope.$broadcast('timer-stopped', 0);
        $timeout.cancel(mytimeout);
        return;
      }
      $scope.timer--;
      mytimeout = $timeout($scope.onTimeout, 1000);
    };
    /**
     * @name startTimer
     * @todo To Start Timer
     * @return void function
     *
     **/
    $scope.startTimer = function() {
      mytimeout = $timeout($scope.onTimeout, 1000);
      $scope.started = true;
      // var ply = document.getElementById('audio');
      // ply.play();

    };


    /**
     * @name stopTimer
     * @todo To stops and resets the current timer
     * @return void function
     *
     **/

    $scope.stopTimer = function(closingModal) {
      if (closingModal != true) {
        $scope.$broadcast('timer-stopped', $scope.timer);
      }
      $scope.timer = $scope.timeForTimer;
      $scope.started = false;
      $scope.paused = false;
      $timeout.cancel(mytimeout);
    };
    /**
     * @name pauseTimer
     * @todo To pauses the timer
     * @return void function
     *
     **/
    $scope.pauseTimer = function() {
      $scope.$broadcast('timer-stopped', $scope.timer);
      $scope.started = false;
      $scope.paused = true;
      $timeout.cancel(mytimeout);
    };


    /**
     * @name timer-stopped
     * @todo  triggered, when the timer stops, you can do something here, maybe show a visual indicator or vibrate the device
     * @return void function
     *
     **/


    $scope.$on('timer-stopped', function(event, remaining) {
      if (remaining === 0) {
        $scope.timerCompleted = true;
        if($scope.firstVideoDownloaded == true){
          $scope.moveToDetail();
        }


      }
    });
    /**
     * @name selectTimer
     * @todo When you press a timer button this function is called
     * @return void function
     *
     **/
    $scope.selectTimer = function(val) {
      $scope.timeForTimer = val;
      $scope.timer = val;
      $scope.started = false;
      $scope.paused = false;
      $scope.done = false;
    };

    /**
     * @name humanizeDurationTimer
     * @todo to display the time in a correct way in the center of the timer
     * @return void function
     *
     **/
    $scope.humanizeDurationTimer = function(input, units) {
      // units is a string with possible values of y, M, w, d, h, m, s, ms
      if (input == 0) {
        return 0;
      } else {
        var duration = moment().startOf('day').add(input, units);
        var format = "";
        if (duration.hour() > 0) {
          format += "H[h] ";
        }
        if (duration.minute() > 0) {
          format += "m[m] ";
        }
        if (duration.second() > 0) {
          format += "s[s] ";
        }
        return duration.format(format);
      }
    };
    $scope.selectTimer(10);
    $scope.startTimer();


    /**
     * @name $watch
     * @todo playing counter when timer is 9
     * @return void function
     *
     **/




    $scope.$watch('timer', function() {


      if($scope.timer < 11){
        TTS.speak({
          text: $scope.timer + '',
          locale: 'en-US',
          rate: 1.5
        }, function () {
          // alert('success');
        }, function (reason) {
          // alert(reason);
        });
        $scope.playingCounter = true;
      }



    }, true);


    /**
     * @name deregisterFirst
     * @todo To add back button route
     * @return void function
     *
     **/




    var deregisterFirst = $ionicPlatform.registerBackButtonAction(function (e) {


      $scope.showConfirm = function () {
        var confirmPopup = $ionicPopup.confirm({
          title: 'Warning',
          template: 'Are you Sure You Want To Exit the Workout ?',
          cssClass: 'loginErrorPopUp logoutCpop'

        });

        confirmPopup.then(function (res) {
          if (res) {
            $scope.backhit = true;


            $cordovaFileTransfer.abort();
            // var cnrrentSlidex = $ionicSlideBoxDelegate.currentIndex();

            $scope.started = false;
            $scope.paused = true;
            // // $rootScope.$broadcast("Thisisit",{ any: {cnrrentSlidex+1} });
            // var vid_index = cnrrentSlidex + 1;
            // $rootScope.$broadcast('stopVideo', {any: cnrrentSlidex});


            $cordovaFileTransfer.abort();

            // video.emptyPlaylist();
            $state.go('app.myWorkoutDetailMain', {result: workout_id});


            return false;
          } else {

          }
        });
      };
      $scope.showConfirm();


    }, 101);

    $scope.$on('$destroy', deregisterFirst);


  });
});
/* angular-svg-round-progressbar@0.3.8 2015-10-21 */
"use strict";
! function() {
  for (var a = 0, b = ["webkit", "moz"], c = 0; c < b.length && !window.requestAnimationFrame; ++c) window.requestAnimationFrame = window[b[c] + "RequestAnimationFrame"], window.cancelAnimationFrame = window[b[c] + "CancelAnimationFrame"] || window[b[c] + "CancelRequestAnimationFrame"];
  window.requestAnimationFrame || (window.requestAnimationFrame = function(b) {
    var c = (new Date).getTime(),
      d = Math.max(0, 16 - (c - a)),
      e = window.setTimeout(function() {
        b(c + d)
      }, d);
    return a = c + d, e
  }), window.cancelAnimationFrame || (window.cancelAnimationFrame = function(a) {
    window.clearTimeout(a)
  })
}(), angular.module("angular-svg-round-progress", []), angular.module("angular-svg-round-progress").constant("roundProgressConfig", {
  max: 50,
  semi: !1,
  rounded: !1,
  responsive: !1,
  clockwise: !0,
  radius: 100,
  color: "#45ccce",
  bgcolor: "#eaeaea",
  stroke: 15,
  duration: 800,
  animation: "easeOutCubic",
  offset: 0
}), angular.module("angular-svg-round-progress").service("roundProgressService", [function() {
  var a = {},
    b = angular.isNumber,
    c = document.head.querySelector("base");
  a.resolveColor = c && c.href ? function(a) {
    var b = a.indexOf("#");
    return b > -1 && a.indexOf("url") > -1 ? a.slice(0, b) + window.location.href + a.slice(b) : a
  } : function(a) {
    return a
  }, a.isSupported = !(!document.createElementNS || !document.createElementNS("http://www.w3.org/2000/svg", "svg").createSVGRect);
  var d = function(a, b, c, d) {
    var e = (d - 90) * Math.PI / 180;
    return {
      x: a + c * Math.cos(e),
      y: b + c * Math.sin(e)
    }
  };
  return a.toNumber = function(a) {
    return b(a) ? a : parseFloat((a + "").replace(",", "."))
  }, a.getOffset = function(b, c) {
    var d = +c.offset || 0;
    if ("inherit" === c.offset)
      for (var e, f = b; !f.hasClass("round-progress-wrapper");) a.isDirective(f) && (e = f.scope().$parent.getOptions(), d += (+e.offset || 0) + (+e.stroke || 0)), f = f.parent();
    return d
  }, a.updateState = function(a, b, c, e, f, g) {
    if (!f) return e;
    var h = a > 0 ? Math.min(a, b) : 0,
      i = g ? 180 : 359.9999,
      j = 0 === b ? 0 : h / b * i,
      k = f / 2,
      l = d(k, k, c, j),
      m = d(k, k, c, 0),
      n = 180 >= j ? "0" : "1",
      o = ["M", l.x, l.y, "A", c, c, 0, n, 0, m.x, m.y].join(" ");
    return e.attr("d", o)
  }, a.isDirective = function(a) {
    return a && a.length ? "undefined" != typeof a.attr("round-progress") || "round-progress" === a[0].nodeName.toLowerCase() : !1
  }, a.animations = {
    linearEase: function(a, b, c, d) {
      return c * a / d + b
    },
    easeInQuad: function(a, b, c, d) {
      return c * (a /= d) * a + b
    },
    easeOutQuad: function(a, b, c, d) {
      return -c * (a /= d) * (a - 2) + b
    },
    easeInOutQuad: function(a, b, c, d) {
      return (a /= d / 2) < 1 ? c / 2 * a * a + b : -c / 2 * (--a * (a - 2) - 1) + b
    },
    easeInCubic: function(a, b, c, d) {
      return c * (a /= d) * a * a + b
    },
    easeOutCubic: function(a, b, c, d) {
      return c * ((a = a / d - 1) * a * a + 1) + b
    },
    easeInOutCubic: function(a, b, c, d) {
      return (a /= d / 2) < 1 ? c / 2 * a * a * a + b : c / 2 * ((a -= 2) * a * a + 2) + b
    },
    easeInQuart: function(a, b, c, d) {
      return c * (a /= d) * a * a * a + b
    },
    easeOutQuart: function(a, b, c, d) {
      return -c * ((a = a / d - 1) * a * a * a - 1) + b
    },
    easeInOutQuart: function(a, b, c, d) {
      return (a /= d / 2) < 1 ? c / 2 * a * a * a * a + b : -c / 2 * ((a -= 2) * a * a * a - 2) + b
    },
    easeInQuint: function(a, b, c, d) {
      return c * (a /= d) * a * a * a * a + b
    },
    easeOutQuint: function(a, b, c, d) {
      return c * ((a = a / d - 1) * a * a * a * a + 1) + b
    },
    easeInOutQuint: function(a, b, c, d) {
      return (a /= d / 2) < 1 ? c / 2 * a * a * a * a * a + b : c / 2 * ((a -= 2) * a * a * a * a + 2) + b
    },
    easeInSine: function(a, b, c, d) {
      return -c * Math.cos(a / d * (Math.PI / 2)) + c + b
    },
    easeOutSine: function(a, b, c, d) {
      return c * Math.sin(a / d * (Math.PI / 2)) + b
    },
    easeInOutSine: function(a, b, c, d) {
      return -c / 2 * (Math.cos(Math.PI * a / d) - 1) + b
    },
    easeInExpo: function(a, b, c, d) {
      return 0 == a ? b : c * Math.pow(2, 10 * (a / d - 1)) + b
    },
    easeOutExpo: function(a, b, c, d) {
      return a == d ? b + c : c * (-Math.pow(2, -10 * a / d) + 1) + b
    },
    easeInOutExpo: function(a, b, c, d) {
      return 0 == a ? b : a == d ? b + c : (a /= d / 2) < 1 ? c / 2 * Math.pow(2, 10 * (a - 1)) + b : c / 2 * (-Math.pow(2, -10 * --a) + 2) + b
    },
    easeInCirc: function(a, b, c, d) {
      return -c * (Math.sqrt(1 - (a /= d) * a) - 1) + b
    },
    easeOutCirc: function(a, b, c, d) {
      return c * Math.sqrt(1 - (a = a / d - 1) * a) + b
    },
    easeInOutCirc: function(a, b, c, d) {
      return (a /= d / 2) < 1 ? -c / 2 * (Math.sqrt(1 - a * a) - 1) + b : c / 2 * (Math.sqrt(1 - (a -= 2) * a) + 1) + b
    },
    easeInElastic: function(a, b, c, d) {
      var e = 1.70158,
        f = 0,
        g = c;
      return 0 == a ? b : 1 == (a /= d) ? b + c : (f || (f = .3 * d), g < Math.abs(c) ? (g = c, e = f / 4) : e = f / (2 * Math.PI) * Math.asin(c / g), -(g * Math.pow(2, 10 * (a -= 1)) * Math.sin((a * d - e) * (2 * Math.PI) / f)) + b)
    },
    easeOutElastic: function(a, b, c, d) {
      var e = 1.70158,
        f = 0,
        g = c;
      return 0 == a ? b : 1 == (a /= d) ? b + c : (f || (f = .3 * d), g < Math.abs(c) ? (g = c, e = f / 4) : e = f / (2 * Math.PI) * Math.asin(c / g), g * Math.pow(2, -10 * a) * Math.sin((a * d - e) * (2 * Math.PI) / f) + c + b)
    },
    easeInOutElastic: function(a, b, c, d) {
      var e = 1.70158,
        f = 0,
        g = c;
      return 0 == a ? b : 2 == (a /= d / 2) ? b + c : (f || (f = d * (.3 * 1.5)), g < Math.abs(c) ? (g = c, e = f / 4) : e = f / (2 * Math.PI) * Math.asin(c / g), 1 > a ? -.5 * (g * Math.pow(2, 10 * (a -= 1)) * Math.sin((a * d - e) * (2 * Math.PI) / f)) + b : g * Math.pow(2, -10 * (a -= 1)) * Math.sin((a * d - e) * (2 * Math.PI) / f) * .5 + c + b)
    },
    easeInBack: function(a, b, c, d, e) {
      return void 0 == e && (e = 1.70158), c * (a /= d) * a * ((e + 1) * a - e) + b
    },
    easeOutBack: function(a, b, c, d, e) {
      return void 0 == e && (e = 1.70158), c * ((a = a / d - 1) * a * ((e + 1) * a + e) + 1) + b
    },
    easeInOutBack: function(a, b, c, d, e) {
      return void 0 == e && (e = 1.70158), (a /= d / 2) < 1 ? c / 2 * (a * a * (((e *= 1.525) + 1) * a - e)) + b : c / 2 * ((a -= 2) * a * (((e *= 1.525) + 1) * a + e) + 2) + b
    },
    easeInBounce: function(b, c, d, e) {
      return d - a.animations.easeOutBounce(e - b, 0, d, e) + c
    },
    easeOutBounce: function(a, b, c, d) {
      return (a /= d) < 1 / 2.75 ? c * (7.5625 * a * a) + b : 2 / 2.75 > a ? c * (7.5625 * (a -= 1.5 / 2.75) * a + .75) + b : 2.5 / 2.75 > a ? c * (7.5625 * (a -= 2.25 / 2.75) * a + .9375) + b : c * (7.5625 * (a -= 2.625 / 2.75) * a + .984375) + b
    },
    easeInOutBounce: function(b, c, d, e) {
      return e / 2 > b ? .5 * a.animations.easeInBounce(2 * b, 0, d, e) + c : .5 * a.animations.easeOutBounce(2 * b - e, 0, d, e) + .5 * d + c
    }
  }, a
}]), angular.module("angular-svg-round-progress").directive("roundProgress", ["$window", "roundProgressService", "roundProgressConfig", function(a, b, c) {
  var d = {
    restrict: "EA",
    replace: !0,
    transclude: !0,
    scope: {
      current: "=",
      max: "=",
      semi: "=",
      rounded: "=",
      clockwise: "=",
      responsive: "=",
      radius: "@",
      color: "@",
      bgcolor: "@",
      stroke: "@",
      duration: "@",
      animation: "@",
      offset: "@"
    }
  };
  return b.isSupported ? angular.extend(d, {
    link: function(e, f) {
      var g, h, i = !f.hasClass("round-progress-wrapper"),
        j = i ? f : f.find("svg").eq(0),
        k = j.find("path").eq(0),
        l = j.find("circle").eq(0),
        m = angular.copy(c);
      e.getOptions = function() {
        return m
      };
      var n = function() {
          var a = m.semi,
            c = m.responsive,
            d = +m.radius || 0,
            e = +m.stroke,
            g = 2 * d,
            h = d - e / 2 - b.getOffset(f, m);
          j.css({
            top: 0,
            left: 0,
            position: c ? "absolute" : "static",
            width: c ? "100%" : g + "px",
            height: c ? "100%" : (a ? d : g) + "px",
            overflow: "hidden"
          }), i || (j[0].setAttribute("viewBox", "0 0 " + g + " " + (a ? d : g)), f.css({
            width: c ? "100%" : "auto",
            position: "relative",
            "padding-bottom": c ? a ? "50%" : "100%" : 0
          })), f.css({
            width: c ? "100%" : "auto",
            position: "relative",
            "padding-bottom": c ? a ? "50%" : "100%" : 0
          }), k.css({
            stroke: b.resolveColor(m.color),
            "stroke-width": e,
            "stroke-linecap": m.rounded ? "round" : "butt"
          }), a ? k.attr("transform", m.clockwise ? "translate(0," + g + ") rotate(-90)" : "translate(" + g + ", " + g + ") rotate(90) scale(-1, 1)") : k.attr("transform", m.clockwise ? "" : "scale(-1, 1) translate(" + -g + " 0)"), l.attr({
            cx: d,
            cy: d,
            r: h >= 0 ? h : 0
          }).css({
            stroke: b.resolveColor(m.bgcolor),
            "stroke-width": e
          })
        },
        o = function(c, d, e) {
          var h = b.toNumber(m.max || 0),
            i = c > 0 ? a.Math.min(c, h) : 0,
            j = d === i || 0 > d ? 0 : d || 0,
            l = i - j,
            n = b.animations[m.animation],
            o = new a.Date,
            p = +m.duration || 0,
            q = e || c > h && d > h || 0 > c && 0 > d || 25 > p,
            r = m.radius,
            s = r - m.stroke / 2 - b.getOffset(f, m),
            t = 2 * r,
            u = m.semi;
          q ? b.updateState(i, h, s, k, t, u) : (a.cancelAnimationFrame(g), function v() {
            var c = a.Math.min(new Date - o, p);
            b.updateState(n(c, j, l, p), h, s, k, t, u), p > c && (g = a.requestAnimationFrame(v))
          }())
        },
        p = Object.keys(d.scope).filter(function(a) {
          return "current" !== a
        });
      e.$watchGroup(p, function(a) {
        for (var b = 0; b < a.length; b++) "undefined" != typeof a[b] && (m[p[b]] = a[b]);
        n(), e.$broadcast("$parentOffsetChanged"), "inherit" !== m.offset || h ? "inherit" !== m.offset && h && h() : h = e.$on("$parentOffsetChanged", function() {
          o(e.current, e.current, !0), n()
        })
      }), e.$watchGroup(["current", "max", "animation", "duration", "radius", "stroke", "semi", "offset"], function(a, c) {
        o(b.toNumber(a[0]), b.toNumber(c[0]))
      })
    },
    template: function(a) {
      for (var c = a.parent(), d = "round-progress", e = ['<svg class="' + d + '" xmlns="http://www.w3.org/2000/svg">', '<circle fill="none"/>', '<path fill="none"/>', "<g ng-transclude></g>", "</svg>"]; c.length && !b.isDirective(c);) c = c.parent();
      return c && c.length || (e.unshift('<div class="round-progress-wrapper">'), e.push("</div>")), e.join("\n")
    }
  }) : angular.extend(d, {
    template: '<div class="round-progress" ng-transclude></div>'
  })
}]);
