/**
 * Purpose:
 *@This file is used as start a workout select level
 *
 **/


angular.module('starter').controller('startWorkout2Ctrl', function($cordovaToast, $rootScope,  $cordovaNetwork, ionicDatePicker,$ionicModal, $ionicPopover, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading) {
 // console.log(GuserData.user_fname);


  /**
   *
   * Initializing Variables, Arrays, and Objects
   *
   **/
$scope.deviceInfo = deviceInformation.getInfo();
var uuid = $scope.deviceInfo.uuid;
$scope.siteUrl = siteUrl;
$scope.selectType = {};
$scope.isOnline = [];
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();


        /**
         *
         * Listen for Online event
         *
         **/
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        /**
         *
         * Listen for ofline event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            // console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);

  /**
   * @name selectLevel
   * @todo To select level
   * @return void function
   *
   **/

              $scope.selectLevel = function(value){
                $window.localStorage['selectLevel'] = value;
                $state.go('app.startWorkout3');
              }


});
