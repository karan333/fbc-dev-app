/**
 * Purpose:
 *@This file is used Show Friend's Activity
 *
 **/
angular.module('starter').controller('FriendsActivityCtrl', function($cordovaInAppBrowser, friendsActivity, quote, $cordovaScreenshot, $cordovaSocialSharing, $cordovaToast, $cordovaNetwork, $rootScope, $cordovaPreferences, $ionicBackdrop, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading, $cordovaFileTransfer,$q) {

    $scope.siteUrl = siteUrl;
    $scope.deviceInfo = deviceInformation.getInfo();
    var uuid = $scope.deviceInfo.uuid;
    // console.log(uuid);
    var userData = $window.localStorage['userData'];
    $scope.userData = angular.fromJson(userData);
    $scope.username = $scope.userData.user_fname;
    $scope.userImg = $scope.userData.image;
    user_id = $scope.userData.user_id;
    // console.log(user_id);
    $scope.status = {};
    $scope.status.postImage = {};
    $scope.statusImgs = [];
    $scope.status.commentImage = {};
    $scope.commentImage = [];
    $scope.commentImgs = [];
    $scope.comment = {};
    $scope.postImage = [];
    $scope.selectedPost="";
    $scope.allFActivity=[];
    $scope.postlimit = 20;
    $scope.FActivity=[];
    $scope.LoadMoreData = [];
    $scope.currentrecordsview = 0;
    $scope.morefrndactvtse=true;
    $scope.edit_status = [];
    $scope.edit_status_images = [];
    $scope.edit_post_images = [];
    $scope.edit_upload_post_images = [];
    $scope.editcomment = {};
    $scope.edit_comment_images = [];
    $scope.commentImgs = [];
    $scope.edit_upload_comment_images = [];
    $scope.isOnline = [];
    $scope.completed = '';
    $scope.start = 0;
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();

        /**
         *
         * Listen for Online event
         *
         **/
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        /**
         *
         * Listen for Offline event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            // console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);


      //life cycle events

  $scope.$on('$ionicView.beforeEnter', function(){
    friendsActivity.get(uuid, user_id, $scope.start).success(function (res) {
      $scope.allFActivity = res.friends_activities;

      if(res.friends_activities.length>0){

        $scope.FActivity  =  angular.fromJson(res.friends_activities);

        for(activity  in $scope.FActivity){


          $scope.content = $scope.FActivity[activity].status;
          $scope.urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
          $scope.url = $scope.content.match($scope.urlRegex);

          $scope.FActivity[activity].status = $scope.FActivity[activity].status.replace($scope.url, '');

          for(var x in $scope.FActivity[activity].comments){
            console.log($scope.FActivity[activity].comments[x].comment);
            $scope.content = $scope.FActivity[activity].comments[x].comment;
            $scope.urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
            $scope.url = $scope.content.match($scope.urlRegex);
            // console.log($scope.url);
            $scope.FActivity[activity].comments[x].comment = $scope.FActivity[activity].comments[x].comment.replace($scope.url, '');
          }

        }


      } else{
        $timeout(function(){
          $ionicLoading.hide();
        }, 5000);
        $scope.NoResultFound = '402';
      }

      $ionicLoading.hide();
    }).error(function(error){
      $ionicLoading.hide();
      // console.log('request not completed')
    });

  });
  /**
   * @name postStatus
   * @todo To Post Status
   * @return void function
   *
   **/
                       $scope.postStatus = function(){
                      if ($scope.isOnline == true){
                        var status_text = $scope.status.text;
                        var not_dashboard = 1;
                          // console.log(status_text + ' ' + $scope.statusText)
                          var link = baseUrl+'/dashboard/post_status';
                          // console.log('userID---'+ user_id + '--UUID--' + uuid + '--statusText' + status_text);
                          var formData = {uu_id : uuid, user_id : user_id, status_text : status_text,status_images:$scope.statusImgs,not_dashboard : not_dashboard, status_url : $scope.urlPreview}
                          var postData = 'myData='+JSON.stringify(formData);

                          $ionicLoading.show({
                          template: '<ion-spinner icon="ios"></ion-spinner>'

                          });
                          $http({
                            method : 'POST',
                            url : link,
                            data: postData,
                            headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                        }).success(function(res){


                           $scope.status.text = '';
                           $scope.statusImgs = [];
                           $scope.postImage = [];
                           $scope.allFActivity = res.friends_activities;
                           // console.log($scope.allFActivity);

                            $scope.urlPreview = {};
                            $scope.urlPreviewClass = '';
                            $scope.showUrlPreview = false;




                            $scope.content = $scope.allFActivity.userStatus.status;
                            $scope.urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
                            $scope.url = $scope.content.match($scope.urlRegex);
                            // console.log($scope.url);
                            $scope.allFActivity.userStatus.status = $scope.allFActivity.userStatus.status.replace($scope.url, '');
                            $scope.urlPreviewPosted = res.friends_activities.userStatus.status_url;



                            $scope.FActivity.unshift($scope.allFActivity.userStatus);
                            $ionicLoading.hide()

                      }).finally(function($ionicLoading) {
                            $ionicLoading.hide()
                          });
                          }else{
                           $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
                          }


                    };

  /**
   * @name editPost
   * @todo To Edit User's Status
   * @return void function
   *
   **/
  $scope.editPost = function(index,id,text,image){
    if ($scope.isOnline == true){
      var status_id = id;
      var status_text = text;
      var status_images = image;

      var link = baseUrl+'/dashboard/update_post_status_friends';
      var formData = {uu_id : uuid, user_id : user_id, status_text : status_text, status_id : status_id, status_images : $scope.edit_upload_post_images ,  status_url : $scope.editurlPreview }
      // console.log(formData);
      var postData = 'myData='+JSON.stringify(formData);

      $ionicLoading.show({
        template: '<ion-spinner icon="ios"></ion-spinner>'

      });
      $http({
        method : 'POST',
        url : link,
        data: postData,
        headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

      }).success(function(res){
        $scope.comment.text = ''
        // console.log(res);
        $scope.edit_status_images = [];
        $scope.edit_post_images = [];
        $scope.edit_upload_post_images = [];
        $scope.FActivity[index] = res.friends_activities[0];


        $scope.content = $scope.FActivity[index].status;
        $scope.urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        $scope.url = $scope.content.match($scope.urlRegex);
        // console.log($scope.url);
        $scope.FActivity[index].status =$scope.FActivity[index].status.replace($scope.url, '');

        $ionicLoading.hide();


      })
    }else{
      $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
    }


  };

  //edit post scene

  /**
   * @name selectUploadMethodPostEdit
   * @todo To Show popup of Upload Method for Editing Post
   * @return void function
   *
   **/
  $scope.selectUploadMethodPostEdit = function(){
    // console.log("In Comment");
    var myPopup = $ionicPopup.show({
      template: '<div class="uploadMethodSack"><ul><li><a class="ink" ng-click="takePicturePostEdit();"><span class="ion-camera"></span><span>Upload from Camera</span></a></li><li><a class=ink"" ng-click="selectPicturePostEdit();"><span class="ion-images"></span><span>Upload from Gallery</span></a></li></ul></div>',
      title: 'Choose Upload Method',
      subTitle: 'Select from Camera or Gallery',
      scope: $scope
    });

    myPopup.then(function(res) {
      console.log('Tapped!', res);
    });

    $timeout(function() {
      myPopup.close(); //close the popup after 8 seconds for some reason
    }, 8000);
  }
  /**
   * @name takePicturePostEdit
   * @todo To Edit Post containing Images
   * @return void function
   *
   **/
  $scope.takePicturePostEdit = function(){
    // console.log("In Comment2");
    document.addEventListener("deviceready", function () {

      var options = {

        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.CAMERA,
        allowEdit: true
      };
      /**
       * @name getPicture
       * @todo To Take Pictures from Gallery
       * @return void function
       *
       **/
      $cordovaCamera.getPicture(options).then(function(imageURI) {
          // console.log(imageURI)
          $scope.uploadPicturexPostedit(imageURI);
          window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {

            $scope.ftLoad = true;

          });
          $ionicLoading.show({template: 'Uploading Picture...', duration:500});
        },
        function(err){
          $ionicLoading.show({template: 'Error Uploading...', duration:500});
        })

    }, false);
  }

  /**
   * @name selectPicturePostEdit
   * @todo To Select Picture Post Edit
   * @return void function
   *
   **/

  $scope.selectPicturePostEdit = function() {
    // console.log("In Comment3");
    document.addEventListener("deviceready", function () {
      var options = {
        quality: 50,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 250,
        targetHeight: 250,
        allowEdit: true
      };
      /**
       * @name getPicture
       * @todo To Take Pictures from Gallery
       * @return void function
       *
       **/
      $cordovaCamera.getPicture(options).then(
        function(imageURI) {
          $scope.uploadPicturexPostedit(imageURI);

          window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {

            $scope.ftLoad = true;

          });
          $ionicLoading.show({template: 'Uploading Picture...', duration:500});
        },
        function(err){
          $ionicLoading.show({template: 'Error Uploading...', duration:500});
        })
    }, false);
  };
  /**
   * @name uploadPicturexPostedit
   * @todo To Upload Picture Post Edit
   * @return void function
   *
   **/
  $scope.uploadPicturexPostedit = function(imageURI) {

    // console.log("In Upload Picture Function");
    // console.log(imageURI);
    var options = {

      fileKey: "picture",

      filename: "p1.png",

      chunkedMode: false,

      mimeType: "image/png",
      trustAllHosts:true,
      headers:{Connection:"close"}

    };
    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner><p>Uploading Image!</p>'

    });
    $cordovaFileTransfer.upload(encodeURI(baseUrl+"/dashboard/uploadStatusImge"), imageURI, options)

      .then(function(result) {


        // console.log(result.response);
        $scope.edit_status_images.push(imageURI);
        var  resk  = angular.fromJson(result.response);

        $scope.edit_upload_post_images.push(resk.image_name) ;

        // console.log($scope.edit_status_images);
        // console.log($scope.edit_upload_post_images);
        $ionicLoading.hide();


      }, function(err) {

        // console.log(err);
        $ionicLoading.hide();
        $ionicLoading.show({template: 'An error occured while uploading your picture.<br>Please try again.', duration:500});

      }, function (progress) {

      });



  };


// end post edit scene





  ///////// For Liking status //////////////
  /**
   * @name like
   * @todo To Like Posts
   * @return void function
   *
   **/
                      $scope.like = function(id,index){
                                                var status_id = id;

                                                var link = baseUrl+'/dashboard/like_user_status';
                                                // console.log('userID---'+ user_id + '--UUID--' + uuid + '--statusID' + status_id);
                                                var formData = {uu_id : uuid, user_id : user_id, status_id : status_id}
                                                var postData = 'myData='+JSON.stringify(formData);

                                                // $ionicLoading.show({
                                                // template: '<ion-spinner icon="ios"></ion-spinner>'
                                                //
                                                // });
                                                $http({
                                                  method : 'POST',
                                                  url : link,
                                                  data: postData,
                                                  headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                              }).success(function(res){
                                              $scope.postLiked = {};
                                                  // $ionicLoading.hide();

                                                  // console.log(res);
                                                  if(res.code == '200'){
                                                    // $scope.actLiked[index] = 'liked';
                                                    $scope.FActivity[index].likes = res.likes;

                                                  } else if(res.code == '402'){
                                                    // $scope.actLiked[index] = 'Disliked';
                                                    $scope.FActivity[index].likes = res.likes;

                                                  } else{
                                                    // console.log('nothing');
                                                  }

                                            })


                                                };

                      ///////////End For Liking ///////////////

                      //////delete post/////
  /**
   * @name removePost
   * @todo To Remove Post
   * @return void function
   *
   **/
  $scope.removePost = function(post, id){
    var status_id = id;
    if ($scope.isOnline == true){
      $scope.showConfirm = function () {
        var confirmPopup = $ionicPopup.confirm({
          title: 'Warning',
          template: 'Are you Sure You Want To Delete your Post ?',
          cssClass: 'loginErrorPopUp logoutCpop'

        });

        confirmPopup.then(function (res) {
          if (res) {
            var link = baseUrl+'/dashboard/remove_status_friends';
            // console.log('userID---'+ user_id + '--UUID--' + uuid + '--statusID' + status_id);
            var formData = {uu_id : uuid, user_id : user_id, status_id : status_id};
            var postData = 'myData='+JSON.stringify(formData);
            // console.log(formData);
            $ionicLoading.show({
              template: '<ion-spinner icon="ios"></ion-spinner>'

            });
            $http({
              method : 'POST',
              url : link,
              data: postData,
              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

            }).success(function(res){

              // console.log(res);
              $timeout(function(){

                var index=$scope.FActivity.indexOf(post)
                $scope.FActivity.splice(index,1);
              });
              // $state.reload();
              $ionicLoading.hide();
            })
          } else {
            // console.log('You are not sure');
          }
        });
      };
      $scope.showConfirm();

    }else{
      $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
    }


    //remove


  };

                      ////////////// For Posting Comments /////
  /**
   * @name postCommentfrnd
   * @todo To Post Comment on Friend's Wall
   * @return void function
   *
   **/
                      $scope.postCommentfrnd = function(id, i,mani){
                      // console.log(mani);

                      if(mani === '' || mani === undefined){
                        var comment_text = null;
                      } else{
                        var comment_text = mani[id+"_"+id];
                      }

                      // console.log( mani[id+"_"+id]);
                      var status_id = id;
                      // console.log(i);
                      var link = baseUrl+'/dashboard/post_comment';
                      // console.log('userID---'+ user_id + '--UUID--' + uuid + '--statusText' + comment_text + 'status-ID---' + status_id);
                      var formData = {uu_id : uuid, user_id : user_id, comment_text : comment_text, status_id : status_id, comment_images : $scope.commentImgs, status_url : $scope.urlPreview};
                      var postData = 'myData='+JSON.stringify(formData);

                      $ionicLoading.show({
                      template: '<ion-spinner icon="ios"></ion-spinner>'

                      });
                      $http({
                        method : 'POST',
                        url : link,
                        data: postData,
                        headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                      }).success(function(res){

                        if(res.status === false){
                          $ionicLoading.hide();
                          return false
                        }

                        $scope.urlPreview = {};
                        $scope.urlPreviewClass = '';
                        $scope.showUrlPreview = false;
                        $scope.showCommentUrlPreview = false;
                        $scope.urlCommentPreviewClass = '';


                        if(mani === '' || mani === undefined){
                            // console.log('do nothing');
                        } else{
                          mani[id+"_"+id] = '';
                        }

                        $scope.commentImage = [];
                        $scope.commentImgs = [];
                        $scope.FActivity[i].comments = res.comments.comments;

                        for(var x in $scope.FActivity[i].comments){
                          console.log($scope.FActivity[i].comments[x].comment);
                          $scope.content = $scope.FActivity[i].comments[x].comment;
                          $scope.urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
                          $scope.url = $scope.content.match($scope.urlRegex);
                          // console.log($scope.url);
                          $scope.FActivity[i].comments[x].comment = $scope.FActivity[i].comments[x].comment.replace($scope.url, '');
                        }


                        // $state.reload();
                        $ionicLoading.hide()
                      }).finally(function($ionicLoading) {
                        $ionicLoading.hide()
                      });

                      };

  //edit comment
  /**
   * @name editComment
   * @todo To Edit Comment
   * @return void function
   *
   **/
  $scope.editComment = function(index, id,status_id, text){
    if ($scope.isOnline == true){
      var comment_text = text;
      var status_id = status_id;
      var comment_id = id;
      var link = baseUrl+'/dashboard/edit_comment_friends';
      // console.log('userID---'+ user_id + '--UUID--' + uuid + '--statusText' + comment_text + 'status-ID---' + status_id);
      var formData = {uu_id : uuid, user_id : user_id, comment_text : comment_text, status_id : status_id, comment_id : comment_id, comment_images : $scope.edit_upload_comment_images, status_url : $scope.editurlPreview }
      var postData = 'myData='+JSON.stringify(formData);
      // console.log(formData);
      $ionicLoading.show({
        template: '<ion-spinner icon="ios"></ion-spinner>'

      });
      $http({
        method : 'POST',
        url : link,
        data: postData,
        headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

      }).success(function(res){
        $scope.comment.text = '';

        // console.log(res);

        $scope.commentImage = [];
        $scope.edit_comment_images = [];
        $scope.commentImgs = [];
        $scope.FActivity[index].comments = res.comments.comments;

        for(var x in $scope.FActivity[index].comments){
          console.log($scope.FActivity[index].comments[x].comment);
          $scope.content = $scope.FActivity[index].comments[x].comment;
          $scope.urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
          $scope.url = $scope.content.match($scope.urlRegex);
          // console.log($scope.url);
          $scope.FActivity[index].comments[x].comment = $scope.FActivity[index].comments[x].comment.replace($scope.url, '');
        }

        $ionicLoading.hide();


      })
    }else{
      $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
    }


  };

  //remove comment
  /**
   * @name removeComment
   * @todo To Delete Comment
   * @return void function
   *
   **/
  $scope.removeComment = function(index, comment, commentID, statusID){

    if ($scope.isOnline == true){
      $scope.showConfirm = function () {
        var confirmPopup = $ionicPopup.confirm({
          title: 'Warning',
          template: 'Are you Sure You Want To Delete your Comment ?',
          cssClass: 'loginErrorPopUp logoutCpop'

        });

        confirmPopup.then(function (res) {
          if (res) {
            var comment_id = commentID;
            var status_id = statusID;

            var link = baseUrl+'/dashboard/remove_comment_friends';

            var formData = {uu_id : uuid, user_id : user_id, comment_id : comment_id, status_id : status_id}
            var postData = 'myData='+JSON.stringify(formData);
            // console.log(formData);
            $ionicLoading.show({
              template: '<ion-spinner icon="ios"></ion-spinner>'

            });
            $http({
              method : 'POST',
              url : link,
              data: postData,
              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

            }).success(function(res){
              $scope.FActivity[index].comments = res.comments.comments;
              $ionicLoading.hide();


              for(var x in $scope.FActivity[index].comments){
                console.log($scope.FActivity[index].comments[x].comment);
                $scope.content = $scope.FActivity[index].comments[x].comment;
                $scope.urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
                $scope.url = $scope.content.match($scope.urlRegex);
                // console.log($scope.url);
                $scope.FActivity[index].comments[x].comment = $scope.FActivity[index].comments[x].comment.replace($scope.url, '');
              }


            })
          } else {
            // console.log('You are not sure');
          }
        });
      };
      $scope.showConfirm();

    }else{
      $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
    }


  };

  //Edit Comment
  /**
   * @name selectUploadMethodCommentEdit
   * @todo To Select Upload Method Comment Edit
   * @return void function
   *
   **/
  $scope.selectUploadMethodCommentEdit = function(){
    // console.log("In Comment");
    var myPopup = $ionicPopup.show({
      template: '<div class="uploadMethodSack"><ul><li><a class="ink" ng-click="takePictureCommentEdit();"><span class="ion-camera"></span><span>Upload from Camera</span></a></li><li><a class=ink"" ng-click="selectPictureCommentEdit();"><span class="ion-images"></span><span>Upload from Gallery</span></a></li></ul></div>',
      title: 'Choose Upload Method',
      subTitle: 'Select from Camera or Gallery',
      scope: $scope
    });

    myPopup.then(function(res) {
      // console.log('Tapped!', res);
    });

    $timeout(function() {
      myPopup.close(); //close the popup after 8 seconds for some reason
    }, 8000);
  }
  /**
   * @name takePictureCommentEdit
   * @todo To Take Picture Comment Edit
   * @return void function
   *
   **/
  $scope.takePictureCommentEdit = function(){
    // console.log("In Comment2");
    document.addEventListener("deviceready", function () {

      var options = {

        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.CAMERA,
        allowEdit: true
      };
      /**
       * @name getPicture
       * @todo To Take Picture From Camera
       * @return void function
       *
       **/
      $cordovaCamera.getPicture(options).then(function(imageURI) {
          // console.log(imageURI)
          $scope.uploadPicturexCommentedit(imageURI);
          window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {

            $scope.ftLoad = true;

          });
          $ionicLoading.show({template: 'Uploading Picture...', duration:500});
        },
        function(err){
          $ionicLoading.show({template: 'Error Uploading...', duration:500});
        })

    }, false);
  }


  /**
   * @name selectPictureCommentEdit
   * @todo To Take Picture From Camera
   * @return void function
   *
   **/
  $scope.selectPictureCommentEdit = function() {
    // console.log("In Comment3");
    document.addEventListener("deviceready", function () {
      var options = {
        quality: 50,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 250,
        targetHeight: 250,
        allowEdit: true
      };
      /**
       * @name getPicture
       * @todo To Get Picture from Gallery
       * @return void function
       *
       **/
      $cordovaCamera.getPicture(options).then(
        function(imageURI) {
          $scope.uploadPicturexCommentedit(imageURI);

          window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {

            $scope.ftLoad = true;

          });
          $ionicLoading.show({template: 'Uploading Picture...', duration:500});
        },
        function(err){
          $ionicLoading.show({template: 'Error Uploading...', duration:500});
        })
    }, false);
  };

  /**
   * @name uploadPicturexCommentedit
   * @todo To Upload Pictures Commment Edit
   * @return void function
   *
   **/
  $scope.uploadPicturexCommentedit = function(imageURI) {

    // console.log("In Upload Picture Function");
    // console.log(imageURI);
    var options = {

      fileKey: "picture",

      filename: "p1.png",

      chunkedMode: false,

      mimeType: "image/png",
      trustAllHosts:true,
      headers:{Connection:"close"}

    };
    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner><p>Uploading Image!</p>'

    });
    $cordovaFileTransfer.upload(encodeURI(baseUrl+"/dashboard/uploadCommentmge"), imageURI, options)

      .then(function(result) {


        // console.log(result.response);
        $scope.edit_comment_images.push(imageURI);
        var  resk  = angular.fromJson(result.response);

        $scope.edit_upload_comment_images.push(resk.image_name) ;

        // console.log($scope.edit_comment_images);
        // console.log($scope.edit_upload_comment_images);
        $ionicLoading.hide();


      }, function(err) {

        // console.log(err);
        $ionicLoading.hide();
        $ionicLoading.show({template: 'An error occured while uploading your picture.<br>Please try again.', duration:500});

      }, function (progress) {

      });



  };


//end edit comment image



  /**
   * @name removeCommentIMG
   * @todo To Delete Comment Image
   * @return void function
   *
   **/

$scope.removeCommentIMG = function(index)
{
// console.log("Image Index" + index);
$scope.commentImgs.splice(index, 1);  ;
$scope.commentImage.splice(index, 1);  ;
console.log($scope.postImage);

}

  /**
   * @name selectUploadMethodComment
   * @todo To Comment Image Upload
   * @return void function
   *
   **/
$scope.selectUploadMethodComment = function(post_id){
if($scope.selectedPost != post_id)
{
 $scope.commentImage=[];
 $scope.commentImgs=[] ;

}
$scope.selectedPost = post_id;
// console.log("All ACtivities Post ID" + post_id);
// console.log("In Comment");
  var myPopup = $ionicPopup.show({
      template: '<div class="uploadMethodSack"><ul><li><a class="ink" ng-click="takePictureComment();"><span class="ion-camera"></span><span>Upload from Camera</span></a></li><li><a class=ink"" ng-click="selectPictureComment();"><span class="ion-images"></span><span>Upload from Gallery</span></a></li></ul></div>',
      title: 'Choose Upload Method',
      subTitle: 'Select from Camera or Gallery',
      scope: $scope
    });

    myPopup.then(function(res) {
      console.log('Tapped!', res);
    });

    $timeout(function() {
       myPopup.close(); //close the popup after 3 seconds for some reason
    }, 8000);
}
  /**
   * @name takePictureComment
   * @todo To Take Picture for Comment
   * @return void function
   *
   **/
 $scope.takePictureComment = function(){
  // console.log("In Comment2");
     document.addEventListener("deviceready", function () {

        var options = {

          destinationType: Camera.DestinationType.FILE_URI,
          sourceType: Camera.PictureSourceType.CAMERA,
          allowEdit: true
        };

        $cordovaCamera.getPicture(options).then(function(imageURI) {
        // console.log(imageURI)
        $scope.uploadPicturexComment(imageURI);
          window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {
          //console.log(fileEntry.nativeURL);
           // $scope.status.postImage = imageURI;
           // console.log($scope.status.postImage);
            $scope.ftLoad = true;
//                                var image = document.getElementById('myImage');
//                                image.src = fileEntry.nativeURL;
            });
          $ionicLoading.show({template: 'Uploading Picture...', duration:500});
        },
        function(err){
          $ionicLoading.show({template: 'Error Uploading...', duration:500});
        })

      }, false);
  }


  /**
   * @name selectPictureComment
   * @todo To Take Picture for Comment
   * @return void function
   *
   **/
    $scope.selectPictureComment = function() {
    // console.log("In Comment3");
     document.addEventListener("deviceready", function () {
        var options = {
          quality: 50,
          destinationType: Camera.DestinationType.FILE_URI,
          sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
          encodingType: Camera.EncodingType.JPEG,
          targetWidth: 250,
          targetHeight: 250,
          allowEdit: true
        };

        $cordovaCamera.getPicture(options).then(
        function(imageURI) {
        $scope.uploadPicturexComment(imageURI);

          window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {
           // $scope.status.postImage = fileEntry.nativeURL;
            $scope.ftLoad = true;
    //				var image = document.getElementById('myImage');
    //				image.src = fileEntry.nativeURL;
            });
          $ionicLoading.show({template: 'Uploading Picture...', duration:500});
        },
        function(err){
          $ionicLoading.show({template: 'Error Uploading...', duration:500});
        })
         }, false);
      };
  /**
   * @name uploadPicturexComment
   * @todo To Take Picture for Comment
   * @return void function
   *
   **/
$scope.uploadPicturexComment = function(imageURI) {

// console.log("In Upload Picture Function");
// console.log(imageURI);
  var options = {

  fileKey: "picture",

  filename: "p1.png",

  chunkedMode: false,

  mimeType: "image/png",
  trustAllHosts:true,
  headers:{Connection:"close"}

  };
 $ionicLoading.show({
                                             template: '<ion-spinner icon="ios"></ion-spinner><p>Uploading Image!</p>'

                                         });
  $cordovaFileTransfer.upload(encodeURI(baseUrl+"/dashboard/uploadCommentmge"), imageURI, options)

  .then(function(result) {

  //$scope.status.commentImage = {};
    //    $scope.commentImage = [];
  // console.log(result.response);
   $scope.commentImage.push(imageURI);
 var  resk  = angular.fromJson(result.response);

          $scope.commentImgs.push(resk.image_name) ;

// console.log($scope.commentImgs);
$ionicLoading.hide();


  }, function(err) {

  // console.log(err);
  $ionicLoading.hide();
  $ionicLoading.show({template: 'An error occured while uploading your picture.<br>Please try again.', duration:500});

  }, function (progress) {

            });



  }


                      /////////////// End For Posting Comments ////////////
  /**
   * @name removeStatusIMG
   * @todo To Take Picture for Comment
   * @return void function
   *
   **/
$scope.removeStatusIMG = function(index)
{
// console.log("Image Index" + index);
$scope.statusImgs.splice(index, 1);  ;
$scope.postImage.splice(index, 1);  ;
// console.log($scope.postImage);

}
  /**
   * @name selectUploadMethod
   * @todo To Select Upload Mehtod
   * @return void function
   *
   **/
 $scope.selectUploadMethod = function(){


      var myPopup = $ionicPopup.show({
          template: '<div class="uploadMethodSack"><ul><li><a class="ink" ng-click="takePicture();"><span class="ion-camera"></span><span>Upload from Camera</span></a></li><li><a class=ink"" ng-click="selectPicture();"><span class="ion-images"></span><span>Upload from Gallery</span></a></li></ul></div>',
          title: 'Choose Upload Method',
          subTitle: 'Select from Camera or Gallery',
          scope: $scope
        });

        myPopup.then(function(res) {
          // console.log('Tapped!', res);
        });

        $timeout(function() {
           myPopup.close(); //close the popup after 3 seconds for some reason
        }, 8000);


    }
  /**
   * @name takePicture
   * @todo To Take Picture
   * @return void function
   *
   **/
                                    $scope.takePicture = function(){
                                       document.addEventListener("deviceready", function () {

                                          var options = {

                                            destinationType: Camera.DestinationType.FILE_URI,
                                            sourceType: Camera.PictureSourceType.CAMERA,
                                            allowEdit: true
                                          };

                                          $cordovaCamera.getPicture(options).then(function(imageURI) {
                                          // console.log(imageURI)
                                          $scope.uploadPicturex(imageURI);
                                            window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {
                                            //console.log(fileEntry.nativeURL);
                                             // $scope.status.postImage = imageURI;
                                             // console.log($scope.status.postImage);
                                              $scope.ftLoad = true;
              //                                var image = document.getElementById('myImage');
              //                                image.src = fileEntry.nativeURL;
                                              });
                                            $ionicLoading.show({template: 'Uploading Picture', duration:500});
                                          },
                                          function(err){
                                            $ionicLoading.show({template: 'Errore di caricamento...', duration:500});
                                          })

                                        }, false);
                                    }


  /**
   * @name selectPicture
   * @todo To Select Picture from Gallery
   * @return void function
   *
   **/

  $scope.selectPicture = function() {
                                       document.addEventListener("deviceready", function () {
                                          var options = {
                                            quality: 50,
                                            destinationType: Camera.DestinationType.FILE_URI,
                                            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                                            encodingType: Camera.EncodingType.JPEG,
                                            targetWidth: 250,
                                            targetHeight: 250,
                                            allowEdit: true
                                          };

                                          $cordovaCamera.getPicture(options).then(
                                          function(imageURI) {
                                          $scope.uploadPicturex(imageURI);

                                            window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {
                                             // $scope.status.postImage = fileEntry.nativeURL;
                                              $scope.ftLoad = true;
                                      //				var image = document.getElementById('myImage');
                                      //				image.src = fileEntry.nativeURL;
                                              });
                                            $ionicLoading.show({template: 'Uploading Picture...', duration:500});
                                          },
                                          function(err){
                                            $ionicLoading.show({template: 'Errore di caricamento...', duration:500});
                                          })
                                           }, false);
                                        };


  /**
   * @name uploadPicturex
   * @todo To Upload Pictures
   * @return void function
   *
   **/
$scope.uploadPicturex = function(imageURI) {

// console.log("In Upload Picture Function");
// console.log(imageURI);
  var options = {

  fileKey: "picture",

  filename: "p1.png",

  chunkedMode: false,

  mimeType: "image/png",
  trustAllHosts:true,
  headers:{Connection:"close"}

  };
 $ionicLoading.show({
                                             template: '<ion-spinner icon="ios"></ion-spinner><p>Uploading Image!</p>'

                                         });
  $cordovaFileTransfer.upload(encodeURI(baseUrl+"/dashboard/uploadStatusImge"), imageURI, options)

  .then(function(result) {
  // console.log(result.response);
   $scope.postImage.push(imageURI);;
 var  resk  = angular.fromJson(result.response);

          $scope.statusImgs.push(resk.image_name) ;

// console.log($scope.statusImgs);
$ionicLoading.hide();


  }, function(err) {

  // console.log(err);
  $ionicLoading.hide();
  $ionicLoading.show({template: 'An error occured while uploading your picture.<br>Please try again.', duration:500});

  }, function (progress) {

            });



  };


//post status url preview


  $scope.url = {};
  $scope.urlPreview = {};
  $scope.editurlPreview = {};
  $scope.urlPreviewClass = '';
  $scope.showUrlPreview = false;
  $scope.editurlPreviewClass = '';
  $scope.editshowUrlPreview = false;


  $scope.postEntering = function (value, edit, isComment, index) {
    // console.log(value);
    // console.log(edit);


    var edit = edit;
    var isComment = isComment;
    console.log(index);

    $scope.urlPreview = {};
    $scope.editurlPreview = {};
    $scope.urlPreviewClass = '';
    $scope.urlCommentPreviewClass = '';
    $scope.showUrlPreview = false;
    $scope.showCommentUrlPreview = {};

    $scope.editurlPreviewClass = '';
    $scope.editCommenturlPreviewClass = '';
    $scope.editshowUrlPreview = false;
    $scope.editshowCommentUrlPreview = false;

    $scope.content = value;

    // console.log($scope.content);
    if($scope.content !== undefined){
      // console.log('not undefined');

      $scope.urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
      $scope.url = $scope.content.match($scope.urlRegex);
      // console.log($scope.url);

      if($scope.url !== null){
        // console.log('not null');
        if($scope.url.length > 0){




          var link = baseUrl+'/dashboard/url_preview';

          var formData = {url : $scope.url};
          var postData = 'myData='+JSON.stringify(formData);

          /*  var formData = {url : $scope.url};
           var postData = JSON.stringify(formData);*/
          $http({
            method : 'POST',
            url : link,
            data: postData,
            headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

          }).success(function(res){
            // console.log(res);


            if(edit !== 1){
              console.log('not edit');
              $scope.urlPreview = res;
              if($scope.urlPreview.site_url !== ''){
                if(isComment === true){
                  console.log('is comment');
                  $scope.showCommentUrlPreview[index] = true
                  console.log($scope.showCommentUrlPreview);
                  $scope.urlCommentPreviewClass = 'urlAddTweak';
                } else {
                  $scope.showUrlPreview = true;
                  $scope.urlPreviewClass = 'urlAddTweak';
                }

              }
            } else if(edit === 1){
              $scope.editurlPreview = res;
              if($scope.editurlPreview.site_url !== ''){

                if(isComment === true){
                  $scope.editshowCommentUrlPreview = true;
                  $scope.editCommenturlPreviewClass = 'urlAddTweak';
                }else {
                  $scope.editshowUrlPreview = true;
                  $scope.editurlPreviewClass = 'urlAddTweak';
                }

              }
            }
          }) .error(function(error){
            // console.log(error);

            if(edit !== 1){
              $scope.urlPreview = {};
              $scope.urlPreviewClass = '';
              $scope.showUrlPreview = false;
              $scope.showCommentUrlPreview[index].show = false;

            } else if(edit === 1){
              $scope.editurlPreview = {};
              $scope.editurlPreviewClass = '';
              $scope.editshowUrlPreview = false;
              $scope.editshowCommentUrlPreview = false;
            }


          })


        }
      }
    }


    return false;
  };

  /**
   * @name clicked
   * @todo To Open In App Browser
   * @return void function
   *
   **/
  $scope.clicked = function(link){

    var options = {
      location: 'yes',
      clearcache: 'yes',
      toolbar: 'no'
    };
    $cordovaInAppBrowser.open(link, '_system', options)
      .then(function(event) {
        console.log('its gone to url')

      })
      .catch(function(event) {
        // error
      });
  }
  /**
   * @name loadmorefriendsact
   * @todo To Load More Friends Activity
   * @return void function
   *
   **/

$scope.loadmorefriendsact = function() {
                        $scope.start++;
                        // console.log($scope.start);
                        friendsActivity.get(uuid, user_id, $scope.start).success(function (res) {

                          // console.log(res);
                          if(res.friends_activities.length>0){
                            //res.friends_activities  =  angular.fromJson(res.friends_activities);
                            $scope.LoadMoreData  =  angular.fromJson(res.friends_activities);
                            // console.log($scope.LoadMoreData);


                            for(activity  in $scope.LoadMoreData){
                              // console.log(activity);


                              $scope.content = $scope.LoadMoreData[activity].status;
                              $scope.urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
                              $scope.url = $scope.content.match($scope.urlRegex);
                              // console.log($scope.url);
                              $scope.LoadMoreData[activity].status = $scope.LoadMoreData[activity].status.replace($scope.url, '');
                              // $scope.urlPreviewPosted = res.friends_activities.userStatus.status_url;


                            }

                            $scope.FActivity.push.apply($scope.FActivity, $scope.LoadMoreData);
                            // console.log($scope.FActivity);
                            $scope.$broadcast('scroll.infiniteScrollComplete');
                            //$ionicLoading.hide();
                            $rootScope.showLoader = 'hideLoader';
                          } else{
                            //$ionicLoading.hide();
                            $rootScope.showLoader = 'hideLoader';
                            return false;
                          }

                        }).error(function(error){
                          $rootScope.showLoader = 'hideLoader';
                          console.log('request not completed')
                        });

                  };

});
