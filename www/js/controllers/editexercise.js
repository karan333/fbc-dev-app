/**
 * Purpose:
 *@This file is used to Edit Exercise
 *
 **/


angular.module('starter').controller('editexercise', function(quote, $rootScope,  $cordovaNetwork, $window, $ionicModal,focus,$ionicScrollDelegate, $ionicPopover, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading,$cordovaFileTransfer) {

$scope.deviceInfo = deviceInformation.getInfo();
var uuid = $scope.deviceInfo.uuid
$scope.siteUrl = siteUrl;
$scope.refine = {};
$scope.refine.Exname="";
$scope.refine.pic = 'img/no-image.jpg'
$scope.ex_pic_name="";

$scope.exerciseName="";
$scope.exerciseTypes="";
$scope.bodyPart="";
$scope.SelectedEquipments="";
$scope.Selectedfitness_level="";
$scope.description="";
$scope.exercise_url="";

$scope.baseUrlForWorkoutImg = 'http://www.fitnessbasecamp.com/uploads/workouts/'
$scope.exercise_id  = $stateParams.result;
//popover
$ionicPopover.fromTemplateUrl('templates/popover.html', {
    scope: $scope,
  }).then(function(popover) {
    $scope.popover = popover;
  });
$scope.isOnline = [];
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();


        // listen for Online event
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        // listen for Offline event
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            // console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);
var userData = $window.localStorage['userData'];
                   $scope.userData = angular.fromJson(userData);


                  user_id = $scope.userData.user_id;

                   quote.getQuote(uuid, user_id).success(function (res) {
                                                                      // console.log(res);
                                                                      $scope.quote = res;
                                                                      });

                  var link = baseUrl+'/exercise/exercise_filters';
                  var formData = {uu_id : uuid, user_id : user_id}
                  var postData = 'myData='+JSON.stringify(formData);
                      $ionicLoading.show({
                                template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                       $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                      }).success(function(res){

                        $scope.refine = res.exercise_filters;
                        // console.log($scope.refine);
                          $timeout(function() {
                              ionicMaterialMotion.fadeSlideInRight({
                                  startVelocity: 3000
                              });
                              $ionicLoading.hide();
                          }, 700);

                          // Set Ink
                          ionicMaterialInk.displayEffect();


                      }) .error(function(error){
                        $ionicLoading.hide();
                        // console.log('request not completed');
                      })
////////////////// For Getting Exercise Current Information //////////////

              var link = baseUrl+'/exercise/my_exercise_details_for_edit';
                            var formData = {uu_id : uuid, user_id : user_id, exercise_id : $scope.exercise_id}
                            var postData = 'myData='+JSON.stringify(formData);
                                $ionicLoading.show({
                                          template: '<ion-spinner icon="ios"></ion-spinner>'

                                      });
                                 $http({
                                        method : 'POST',
                                        url : link,
                                        data: postData,
                                        headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                }).success(function(res){
                                  $ionicLoading.hide();

                                  $scope.exerciseName = res.exercise[0].c_name;
                                  $scope.exerciseTypes  = res.exercise[0].exerciseTypeID;
                                  $scope.bodyPart  = res.exercise[0].bodyPartID;
                                  $scope.SelectedEquipments  = res.exercise[0].equipment_id;
                                  $scope.Selectedfitness_level = res.exercise[0].diff_level;
                                  $scope.description=res.exercise[0].c_description;;
                                  $scope.exercise_url=res.exercise[0].video;
                                  $scope.refine.pic = res.exercise[0].video_thumb;




                                }) .error(function(error){
                                  $ionicLoading.hide();
                                  // console.log('request not completed');
                                })


              //////////////////////////////////////////////////////////////////////////

                      $scope.selectPicture = function() {
                       document.addEventListener("deviceready", function () {
                          var options = {
                            quality: 100,
                            destinationType: Camera.DestinationType.FILE_URI,
                            sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
                            encodingType: Camera.EncodingType.JPEG,
                            targetWidth: 346,
                            targetHeight: 508,
                            allowEdit: true
                          };

                          $cordovaCamera.getPicture(options).then(
                          function(imageURI) {
                           $scope.uploadPicturex(imageURI);

                            $ionicLoading.show({template: 'Uploading Photo...', duration:500});
                          },
                          function(err){
                            $ionicLoading.show({template: 'Error While Uploading Image...', duration:500});
                          })
                           }, false);
                        };


                     $scope.addExercise = function(name, type, part, equip, level, url, description){


                     if(name == "" || type == "" || part == "" || equip == "" || level == "" || url == "" || description == "" || name == "undefined" || type == "undefined" || part == "undefined" || equip == "undefined" || level == "undefined" || url == "undefined" || description == "undefined")
                     {
                        $ionicLoading.show({template: 'All the fields must be filled.', duration:500});
                     }
                     else
                     {
                        user_id = $scope.userData.user_id;
                         var link = baseUrl+'/exercise/editCustomExercise';
                         var formData = {uu_id : uuid, user_id : user_id, exercise_name : name, exercise_type : type, embed_link : url, equipment_id : equip, difficulty_level : level, body_parts : part, description : description,exercise_image:$scope.ex_pic_name,exercise_id:$scope.exercise_id}
                         var postData = 'myData='+JSON.stringify(formData);
                             $ionicLoading.show({
                                       template: '<ion-spinner icon="ios"></ion-spinner>'

                                   });
                              $http({
                                     method : 'POST',
                                     url : link,
                                     data: postData,
                                     headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                             }).success(function(res){
                             // console.log(res);
                             if(res.code == 400)
                             {
                                $ionicLoading.show({template: 'All the fields must be filled.', duration:500});
                             }
                             else
                             {

                               $state.go('app.myexercises');
                              }

                             }) .error(function(error){
                               $ionicLoading.hide();
                               // console.log('request not completed');
                             })
                         }
                     }

  /**
   * @name uploadPicturex
   * @todo To Upload Pictures
   * @return void function
   *
   **/
 $scope.uploadPicturex = function(imageURI) {

// console.log("In Upload Picture Function");
// console.log(imageURI);
  var options = {

  fileKey: "picture",

  filename: "p1.png",

  chunkedMode: false,

  mimeType: "image/png",
  trustAllHosts:true,
  headers:{Connection:"close"}

  };
 $ionicLoading.show({
                                             template: '<ion-spinner icon="ios"></ion-spinner><p>Uploading Image!</p>'

                                         });
  $cordovaFileTransfer.upload(encodeURI(baseUrl+"/exercise/upload_exercise_images"), imageURI, options)

  .then(function(result) {
  // console.log(result.response);
   $scope.refine.pic= imageURI;;
 var  resk  = angular.fromJson(result.response);

          $scope.ex_pic_name = resk.image_name;

// console.log($scope.upload);
$ionicLoading.hide();


  }, function(err) {

  // console.log(err);
  $ionicLoading.hide();
  $ionicLoading.show({template: 'An error occured while uploading your picture.<br>Please try again.', duration:500});

  }, function (progress) {

            });



  }




});
