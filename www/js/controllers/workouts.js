/**
 * Purpose:
 *@This file is used to view Workouts.
 *
 **/


angular.module('starter').controller('workoutsCtrl', function(notification, $sce, $filter, $cordovaInAppBrowser, $cordovaToast, quote, $rootScope,  $cordovaNetwork, $window, $ionicModal,focus,$ionicScrollDelegate, $ionicPopover, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading) {
 // console.log(GuserData.user_fname);

  /**
   *
   * Initializing Variables, Arrays, and Objects
   *
   **/


$scope.deviceInfo = deviceInformation.getInfo();
var uuid = $scope.deviceInfo.uuid
$scope.layout = 'list';
$scope.packageExpired = {};
$scope.refine = {};
$scope.searchStrman = {};
$scope.baseUrlForWorkoutImg = 'http://www.fitnessbasecamp.com/uploads/category-images/'
//console.log($scope.workouts.search);
$scope.moreworkouts=false;
$scope.allWorkoutss=[];
$scope.postlimit = 30;
$scope.workoutss=[];
$scope.currentrecordsview = 0;
var user_id="";
$scope.isOnline = [];
$scope.dontShow = {};
$scope.ifNoWorkout = 0;
$scope.noWorkoutFound = false;
$scope.notificationFull = [];
$scope.notification = [];
  // $scope.$on('$ionicView.loaded', function(){
  //   console.log('view loaded');
  // });
  // $scope.$on('$ionicView.enter', function(){
  //   console.log('view enter');
  // });
  // $scope.$on('$ionicView.leave', function(){
  //   console.log('view leave');
  // });
  $scope.$on('$ionicView.beforeEnter', function(){
    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner>'

    });
    notification.get(uuid, user_id).success(function (res) {

      var notifications = res;
      $window.localStorage['notifications']  = JSON.stringify(notifications);
      var localNoti = $window.localStorage['notifications'];
      $scope.notification = angular.fromJson(localNoti);
      $scope.notificationFull = $scope.notification.notification;
      if($scope.notificationFull.length > 0){
        if($scope.notificationFull[0].count == 0){
          $scope.green = "green";
        } else if($scope.notificationFull[0].count > 0){
          $scope.green = "red";
        }
      }

      $scope.changeColor = function(){
        $scope.green = "green";

        //;


      }
    });
  });
  // $scope.$on('$ionicView.beforeLeave', function(){
  //   console.log('view before leave');
  // });
  // $scope.$on('$ionicView.afterEnter', function(){
  //   console.log('view after enter');
  // });
  // $scope.$on('$ionicView.afterLeave', function(){
  //   console.log('view after leave');
  // });
  // $scope.$on('$ionicView.unloaded', function(){
  //   console.log('view unloaded');
  // });

             document.addEventListener("deviceready", function () {
               $scope.network = $cordovaNetwork.getNetwork();
               $scope.isOnline = $cordovaNetwork.isOnline();


               /**
                *
                * Listen for Online event
                *
                **/

               $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
                   $scope.isOnline = true;
                   $scope.network = $cordovaNetwork.getNetwork();
                   $cordovaToast.showLongBottom('You are Online !')

               })



               /**
                *
                * Listen for ofline event
                *
                **/

               $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
                   // console.log("got offline");
                   $scope.isOnline = false;
                   $scope.network = $cordovaNetwork.getNetwork();
                   $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
               })

             }, false);

  /**
   * @name showSwipeTutorial
   * @todo To view swipe anmation to user on first time login
   * @return void function
   *
   **/

  $scope.showSwipeTutorial = function(){
  var alertPopup = $ionicPopup.alert({
    title: '<div class="swipeItemText">Swipe over the items to reveal more options</div>',
    template: '<div class="swipeHand"><img ng-src="img/hand.gif" class="img-responsive"></div><div class="dontShowAgain"></div>',
    cssClass: 'loginErrorPopUp hideOk swipeGuide'
  });
$scope.dontShowFunc = function(value){
  // console.log(value);
};
  $timeout(function() {
    alertPopup.close();
    $window.localStorage['showSwipeTutorial'] = false;
  }, 8000);
};

  //
  $scope.$watch('dontShow', function() {
    // console.log($scope.dontShow.value);
  }, true);

if ($stateParams.result) {

            //////////// My Coding Starts /////////
                var userData = $window.localStorage['userData'];
                $scope.userData = angular.fromJson(userData);
                user_id = $scope.userData.user_id;

  /**
   *
   * get quotes
   *
   **/
                quote.getQuote(uuid, user_id).success(function (res) {
                  // console.log(res);
                  $ionicLoading.hide();
                  $scope.quote = res;
                });
  /**
   *
   * get workouts list when using start a workout from dashboard
   *
   **/
            $stateParams.result =angular.fromJson($stateParams.result);
            $scope.allWorkoutss = $stateParams.result.workouts;
            if($scope.allWorkoutss.length < 1){
              $scope.noWorkoutFound = true
            }


            // console.log("Mani");
            // console.log($stateParams.result);
            if($stateParams.result.workouts.length < 1){
              $scope.NoResultFound =  '402';
            }



            for(var i=0; i< $scope.postlimit ; i++) {
             if(i < $stateParams.result.workouts.length)
              {
                $scope.workoutss.push($stateParams.result.workouts[i]);
              }
            }
            $scope.currentrecordsview=$scope.postlimit;
            $scope.moreworkouts=true;
            $ionicLoading.hide();



  /**
   * @name loadmoreworkouts
   * @todo To load workouts on scroll bottom in offline mode
   * @return void function
   *
   **/


  $scope.loadmoreworkouts = function() {

    // console.log("In Scroll Workouts");
    var lim = $scope.currentrecordsview + $scope.postlimit;

    for(var i=$scope.currentrecordsview;i< lim;i++)
    {
      if(i < $scope.allWorkoutss.length)
      {
        $scope.workoutss.push($scope.allWorkoutss[i]);
      }
      else
      {
        $scope.moreworkouts=false;
        break;
      }
    }
    // console.log($scope.workouts);
    $scope.currentrecordsview=lim;
    // $timeout(function() {
    //                                 ionicMaterialMotion.fadeSlideInRight({
    //                                     startVelocity: 3000
    //                                 });
    //                             }, 1000);
    //
    //                             // Set Ink
    //                             ionicMaterialInk.displayEffect();
    $scope.$broadcast('scroll.infiniteScrollComplete');
  };


            //////////// End ///////////////////
        } else{
  if ($scope.isOnline == true){



    /**
     * @name ionicPopover
     * @todo To Show Popover
     * @return void function
     *
     **/


    $ionicPopover.fromTemplateUrl('templates/popover.html', {
      scope: $scope,
    }).then(function(popover) {
      $scope.popover = popover;
    });


    $scope.siteUrl = siteUrl;

    var packageExpired = $window.localStorage['expired'];
    $scope.packageExpired = angular.fromJson(packageExpired);

    var userData = $window.localStorage['userData'];
    $scope.userData = angular.fromJson(userData);


    user_id = $scope.userData.user_id;
    quote.getQuote(uuid, user_id).success(function (res) {
      // console.log(res);
      $scope.quote = res;
    });


    /**
     *
     * Get workouts from service api call
     *
     **/



    var link = baseUrl+'/workouts/allworkouts';
    var formData = {uu_id : uuid, user_id : user_id};
    var postData = 'myData='+JSON.stringify(formData);

    $http({
      method : 'POST',
      url : link,
      data: postData,
      headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

    }).success(function(res){

      // console.log(res);
      var showTutorial = $window.localStorage['showSwipeTutorial'];
      // console.log("showTutorial : "+showTutorial);
      if(showTutorial === undefined||showTutorial === 'undefined'){
        $scope.showSwipeTutorial();
      }

      $scope.allWorkoutss = res.allworkouts;
      if($scope.allWorkoutss.length < 1){
        $scope.noWorkoutFound = true
      }
      $window.localStorage['allWorkouts'] =  JSON.stringify(res.allworkouts);
      res.allworkouts  =  angular.fromJson(res.allworkouts);

      // console.log("Mani");
      // console.log(res.allworkouts[0]);
      for(var i=0; i< $scope.postlimit ; i++) {
        if(i < $scope.allWorkoutss.length)
        {
          $scope.workoutss.push(res.allworkouts[i]);
        }
      }
      $scope.currentrecordsview=$scope.postlimit;
      $scope.moreworkouts=true;
      $ionicLoading.hide();

    }) .error(function(error){
      $ionicLoading.hide();
      // console.log('request not completed');
    });
  } else{
    var allWorkouts = $window.localStorage['allWorkouts'];
    $scope.allWorkoutss = angular.fromJson(allWorkouts);
    if($scope.allWorkoutss.length < 1){
      $scope.noWorkoutFound = true
    }

    // console.log("Mani");

    for(var i=0; i< $scope.postlimit ; i++) {
      if(i < $scope.allWorkoutss.length)
      {
        $scope.workoutss.push($scope.allWorkoutss[i]);
      }
    }
    $scope.currentrecordsview=$scope.postlimit;
    $scope.moreworkouts=true;
  }

}


  /**
   * @name content
   * @todo To render service data as html and applying chracter limit
   * @return void function
   *
   **/



$scope.content = function(id){
  var dataWithHtml = $scope.allWorkoutss[id].description;
  $scope.dotdot = '...';
  $scope.workDescription = $filter('limitTo')(dataWithHtml, 60, 0);
  if($scope.workDescription.length > 58){
    $scope.workDescription = $scope.workDescription + $scope.dotdot;
  }
  return $scope.workDescription = $sce.trustAsHtml($scope.workDescription);
};

  /**
   * @name favorite
   * @todo To add workout to favorite list
   * @return void function
   *
   **/

 $scope.favorite = function(workout_id){
                      // console.log('favorite');
                      var workout_id = workout_id;
                        var link = baseUrl+'/workouts/Togglefavrtunfavrtworkout';
                       var formData = {uu_id : uuid, user_id : user_id ,  workout_id : workout_id}
                       var postData = 'myData='+JSON.stringify(formData);
                        $ionicLoading.show({
                               template: '<ion-spinner icon="ios"></ion-spinner>'

                           });
                      $http({
                             method : 'POST',
                             url : link,
                             data: postData,
                             headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                     }).success(function(res){

                       // console.log(res);
                        var alertPopup = $ionicPopup.alert({
                        title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                        template: res.message,
                        cssClass: 'loginErrorPopUp hideOk'
                      });
                        $timeout(function() {
                          alertPopup.close();

                        }, 2000);
                        $ionicLoading.hide();
                     }) .error(function(error){
                       $ionicLoading.hide();
                       // console.log('request not completed');
                     });

                      }


  /**
   * @name focussearch
   * @todo To show search box
   * @return void function
   *
   **/


 $scope.focussearch = function() {
      // do something awesome
      $ionicScrollDelegate.scrollTop();
      focus('searchStrman');
       $scope.show = "showInput";

    };


 $scope.showSearchIcon = true;


  /**
   * @name change
   * @todo To check value inside search box
   * @return void function
   *
   **/

  $scope.change = function(text) {
  if($scope.searchStrman.search == ''){
    $scope.showSearchIcon = true;
  }
  $ionicScrollDelegate.resize();
  $scope.loadmoreworkouts();
  angular.element(document.querySelectorAll('.allWorkoutsStack')).triggerHandler('click');
  $scope.showSearchIcon = false;
};



  /**
   * @name emptySearch
   * @todo To empty check box
   * @return void function
   *
   **/




  $scope.emptySearch = function(text){
    $scope.showSearchIcon = true;
    $scope.searchStrman = {};
    $scope.searchStrman.search = null;
    $scope.searchStrman.search = '';
    text = '';

  };

  /**
   *
   * to check package expiry
   *
   **/

$scope.permissions= angular.fromJson($window.localStorage['Permissions']);
$scope.inArray = function (needle) {
haystack  = $scope.permissions;
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle){ return true;

        } ;
    }
    return "noPermission";
}
 $scope.Npermission = function(is_perm)
                                  {

                                      if(is_perm != true)
                                       {

                                          var alertPopup = $ionicPopup.alert({
                                            title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                            template: "You don't have permission to do this.Please upgrade your package.",
                                            cssClass: 'loginErrorPopUp'
                                          });
                                       }

                                  }
$scope.addWorkoutx = function()
 {
if($scope.inArray('Add Custom Workout')==true)
{

 $state.go('app.addCustomWorkout');
}
else
                                      {

 var alertPopup = $ionicPopup.alert({
                                        title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                        template: "You don't have permission to do this.Please upgrade your package.",
                                        cssClass: 'loginErrorPopUp'
                                      });
                                      }
 }


  /**
   * @name loadmoreworkouts
   * @todo To load workouts on scroll bottom
   * @return void function
   *
   **/



$scope.loadmoreworkouts = function() {


              var lim = $scope.currentrecordsview + $scope.postlimit;

              for(var i=$scope.currentrecordsview;i< lim;i++)
              {
                if(i < $scope.allWorkoutss.length)
                {
                  $scope.workoutss.push($scope.allWorkoutss[i]);
                }
                else
                {
                  $scope.moreworkouts=false;
                  break;
                }
              }
             // console.log($scope.workouts);
            $scope.currentrecordsview=lim;
            // $timeout(function() {
            //                                 ionicMaterialMotion.fadeSlideInRight({
            //                                     startVelocity: 3000
            //                                 });
            //                             }, 1000);
            //
            //                             // Set Ink
            //                             ionicMaterialInk.displayEffect();
              $scope.$broadcast('scroll.infiniteScrollComplete');
        };
        $scope.$on('$stateChangeSuccess', function() {
          $scope.loadmoreworkouts();
        });




  /**
   * @name filterWorkouts
   * @todo To filter workout
   * @return void function
   *
   **/




                      $scope.filterWorkouts = function() {

                        var w_type = $scope.refine.Selectedtype;
                        var machines = $scope.refine.Selectedequip;
                        var level = $scope.refine.Selectedlevel;
                        var Selectedintensity = $scope.refine.Selectedintensity;
                        var Selectedduration = $scope.refine.Selectedduration;
                        ////////////////// Filter Request Start/////////////////

                        var link = baseUrl+'/workouts/search_workouts';
                        var formData = {uu_id : uuid, user_id : user_id ,  workout_type : w_type,  wLevels : level, exmachines : machines,duration:Selectedduration,wGoals:Selectedintensity}
                        var postData = 'myData='+JSON.stringify(formData);
                      $ionicLoading.show({
                                template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                       $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                      }).success(function(res){

                        // console.log(res);
                        $scope.NoResultFound =  res.code;
                        $scope.workoutss = [];
                        $scope.allWorkoutss=[];
                      $scope.allWorkoutss = res.workouts;
                         if($scope.allWorkoutss.length < 1){
                           $scope.noWorkoutFound = true
                         }
                      for(var i=0; i< $scope.postlimit ; i++) {

                      if(i < $scope.allWorkoutss.length )
                        {
                          $scope.workoutss.push(res.workouts[i]);
                        }


                      }
                      if(res.workouts.length > 6)
                      {
                        $scope.moreworkouts=true;

                      }
                      else
                      {

                      $scope.moreworkouts=false;

                      }
                      $scope.currentrecordsview=$scope.postlimit;
                      $scope.modal.hide();


                        // console.log($scope.workoutss);
                         $ionicLoading.hide();



                          // $timeout(function() {
                          //     ionicMaterialMotion.fadeSlideInRight({
                          //         startVelocity: 3000
                          //     });
                          //     $ionicLoading.hide();
                          // }, 700);
                          //
                          // // Set Ink
                          // ionicMaterialInk.displayEffect();


                      }) .error(function(error){
                        $ionicLoading.hide();
                        // console.log('request not completed');
                      }).finally(function ($ionicLoading) {
                         // On both cases hide the loading
                         $ionicLoading.hide();
                       });


                      //favorite workout



                        ///////////////// Filter Request End ///////////////////



                       };



  /**
   * @name fromTemplateUrl
   * @todo To load workout filters from service api call
   * @return void function
   *
   **/




                        //refine modal
                      $ionicModal.fromTemplateUrl('templates/modal.html', {
                          scope: $scope
                        }).then(function(modal) {
                          $scope.modal = modal;
                          var user_id = $scope.userData.user_id;
                          var link = baseUrl+'/workouts/workoutfilter';
                          var formData = {uu_id : uuid, user_id : user_id}
                          var postData = 'myData='+JSON.stringify(formData);
                              $ionicLoading.show({
                                        template: '<ion-spinner icon="ios"></ion-spinner>'

                                    });
                               $http({
                                      method : 'POST',
                                      url : link,
                                      data: postData,
                                      headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                              }).success(function(res){

                                $scope.refine = res.workoutsfilter;
                                // console.log($scope.refine);
                                  // $timeout(function() {
                                  //     ionicMaterialMotion.fadeSlideInRight({
                                  //         startVelocity: 3000
                                  //     });
                                  //     $ionicLoading.hide();
                                  // }, 700);
                                  //
                                  // // Set Ink
                                  // ionicMaterialInk.displayEffect();


                              }) .error(function(error){
                                $ionicLoading.hide();
                                // console.log('request not completed');
                              })
                        });
                        $scope.$on('modal.shown', function() {
                                   // $timeout(function() {
                                   //     ionicMaterialMotion.fadeSlideInRight({
                                   //         startVelocity: 3000
                                   //     });
                                   //     $ionicLoading.hide();
                                   // }, 700);
                                   //
                                   // // Set Ink
                                   // ionicMaterialInk.displayEffect();
                                   });


  /**
   * @name toggleGroup
   * @todo To show hide workout labels
   * @return void function
   *
   **/


                      $scope.toggleGroup = function(id) {
                          if ($scope.isGroupShown(id)) {
                            $scope.shownGroup = null;
                          } else {
                            $scope.shownGroup = id;
                          }
                        };
                        $scope.isGroupShown = function(id) {
                          return $scope.shownGroup === id;
                        };




  /**
   * @name expired
   * @todo To show package expiry package
   * @return void function
   *
   **/


  $scope.expired = function(){
    var myPopup = $ionicPopup.show({
      template: '<div class="packageExpiredInner"><div class="selectOption" ng-click="closePopup()"><span>Not Now</span></div><div class="selectOption" ng-click="openBrowser()"><span>Renew Now</span></div></div>',
      subTitle: 'Oops! Your subscription has expired. Please renew your subscription.',
      cssClass: 'packageExpiredPopup',
      scope: $scope
    });

    myPopup.then(function(res) {

    });

    $timeout(function() {
      // myPopup.close(); //close the popup after 3 seconds for some reason
    }, 8000);
    $scope.closePopup = function() {

      myPopup.close();
    };
  };


  /**
   * @name openBrowser
   * @todo To open InAppBrowser
   * @return void function
   *
   **/





  $scope.openBrowser = function(){
    var user_id = $scope.userData.user_id;
    var options = {
      location: 'yes',
      clearcache: 'yes',
      toolbar: 'no'
    };
    $cordovaInAppBrowser.open('http://www.fitnessbasecamp.com/loginUser_admin/'+user_id+'/app', '_system', options)
      .then(function(event) {
        // console.log(event);
      })
      .catch(function(event) {
        // error
      });


    // $cordovaInAppBrowser.close();
  }
});
