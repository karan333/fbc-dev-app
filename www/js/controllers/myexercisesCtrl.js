/**
 * Purpose:
 *@This file is used to Show My Exercises
 *
 **/
angular.module('starter').controller('myexercisesCtrl', function($ionicPlatform, $cordovaToast, $ionicModal, quote, $rootScope,  $cordovaNetwork, deviceInformation, focus,$ionicScrollDelegate,$ionicPopover , $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading) {
 // console.log(GuserData.user_fname);


$scope.deviceInfo = deviceInformation.getInfo();
var uuid = $scope.deviceInfo.uuid
$scope.layout = 'list';
$scope.moremyexercise=true;
  $scope.searchStrman = {};
$scope.baseUrlForWorkoutImg = 'http://www.fitnessbasecamp.com/uploads/workouts/'
//console.log($scope.myexercises.search);
$scope.allmyexercises=[];
     $scope.postlimit = 6;
      $scope.myexercises=[];
      $scope.currentrecordsview = 0;

//popover
$ionicPopover.fromTemplateUrl('templates/popover.html', {
    scope: $scope,
  }).then(function(popover) {
    $scope.popover = popover;
  });

  $scope.siteUrl = siteUrl;
$scope.isOnline = [];
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();

        /**
         *
         * Listen for Online event
         *
         **/
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        /**
         *
         * Listen for Offline event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            // console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);
                              var userData = $window.localStorage['userData'];
                              $scope.userData = angular.fromJson(userData);


                  var user_id = $scope.userData.user_id;
                   quote.getQuote(uuid, user_id).success(function (res) {
                    // console.log(res);
                    $scope.quote = res;
                    });
                  var link = baseUrl+'/exercise/myexercises';
                  var formData = {uu_id : uuid, user_id : user_id}
                  var postData = 'myData='+JSON.stringify(formData);
                      $ionicLoading.show({
                                template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                       $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                      }).success(function(res){

                        // console.log(res);
                         $scope.NoResultFound =  res.code;
                         // console.log($scope.NoResultFound );
                        $scope.allmyexercises = res.myexercises;
                        // console.log("Usman");
                        // console.log($scope.allmyexercises);
                        if(res.myexercises.length > 0){
                        res.myexercises  =  angular.fromJson(res.myexercises);
                         // $scope.workouts = res.myworkouts;
                         if(res.myexercises.length > 6)
                         {
                          $scope.moremyexercise=true;

                         }
                          for(var i=0; i< $scope.postlimit ; i++) {
                          if(res.myexercises.length > i){
                           $scope.myexercises.push(res.myexercises[i]);
                          }


                          }
                          $scope.currentrecordsview=$scope.postlimit;
                          $ionicLoading.hide();
                        } else{
                        // console.log('else');
                        $ionicLoading.hide();
                        $scope.NoResultFound = '402';
                        }



                      }) .error(function(error){

                        // console.log('request not completed');
                      });

  /**
   * @name filterExercise
   * @todo To Get Filter Exercises
   * @return void function
   *
   **/

              $scope.filterExercise = function() {

                var ex_type = $scope.refine.Selectedtype;
                var body_part = $scope.refine.bodyPartid;
                var machines = $scope.refine.Selectedequip;
                var level = $scope.refine.Selectedlevel;

                ////////////////// Filter Request Start/////////////////

                var link = baseUrl+'/exercise/search_custom_exercises';
                var formData = {uu_id : uuid, user_id : user_id ,  exerciseType : ex_type, bodyPart : body_part  ,  wLevels : level, exmachines : machines}
                var postData = 'myData='+JSON.stringify(formData);
                $ionicLoading.show({
                  template: '<ion-spinner icon="ios"></ion-spinner>'

                });
                $http({
                  method : 'POST',
                  url : link,
                  data: postData,
                  headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                }).success(function(res){
                  // console.log(res);
                  $scope.NoResultFound =  res.code;
                  // console.log($scope.NoResultFound );
                  $scope.myexercises = res.exercises;
                    $ionicLoading.hide();
                    $scope.modal.hide();



                }) .error(function(error){
                  $ionicLoading.hide();
                  // console.log('request not completed');
                }).finally(function ($ionicLoading) {
                  // On both cases hide the loading
                  $ionicLoading.hide();
                });
 ///////////////// Filter Request End ///////////////////



  };

  /**
   * @name loadmoremyexercise
   * @todo To Load More Exercises
   * @return void function
   *
   **/
  $scope.loadmoremyexercise = function() {



                          var lim = $scope.currentrecordsview + $scope.postlimit;

                          for(var i=$scope.currentrecordsview;i< lim;i++)
                          {
                            if(i < $scope.allmyexercises.length )
                            {
                              $scope.myexercises.push($scope.allmyexercises[i]);
                            }
                            else
                            {
                              $scope.moremyexercise=false;
                              break;
                            }
                          }

                        $scope.currentrecordsview=lim;

                          $scope.$broadcast('scroll.infiniteScrollComplete');
                      };





  //refine modal
  $ionicModal.fromTemplateUrl('templates/modal.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
    var user_id = $scope.userData.user_id;
    var link = baseUrl+'/exercise/exercise_filters';
    var formData = {uu_id : uuid, user_id : user_id}
    var postData = 'myData='+JSON.stringify(formData);
    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner>'

    });
    $http({
      method : 'POST',
      url : link,
      data: postData,
      headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

    }).success(function(res){

      $scope.refine = res.exercise_filters;
      // console.log($scope.refine);
      // $timeout(function() {
      //     ionicMaterialMotion.fadeSlideInRight({
      //         startVelocity: 3000
      //     });
      //     $ionicLoading.hide();
      // }, 700);
      //
      // // Set Ink
      // ionicMaterialInk.displayEffect();


    }) .error(function(error){
      $ionicLoading.hide();
      // console.log('request not completed');
    })
  });

  /**
   * @name focussearch
   * @todo To Focus on Search Input
   * @return void function
   *
   **/
  $scope.focussearch = function() {
    // do something awesome
    $ionicScrollDelegate.scrollTop();
    focus('searchStrman');
    $scope.show = "showInput";

  };
  $scope.showSearchIcon = true;
  $scope.change = function(text) {
    if($scope.searchStrman.search == ''){
      $scope.showSearchIcon = true;
    }
    // console.log(text);
    $ionicScrollDelegate.resize();
    $scope.loadmoremyexercise();
    angular.element(document.querySelectorAll('.allWorkoutsStack')).triggerHandler('click');
    $scope.showSearchIcon = false;
  };
  /**
   * @name emptySearch
   * @todo To Empty Search input box
   * @return void function
   *
   **/
  $scope.emptySearch = function(text){
    $scope.showSearchIcon = true;
    $scope.searchStrman = {};
    $scope.searchStrman.search = null;
    $scope.searchStrman.search = '';
    text = '';

  }

  $scope.$on('$stateChangeSuccess', function() {
                        $scope.loadmoremyexercise();
                      });

                      $scope.toggleGroup = function(id) {
                          if ($scope.isGroupShown(id)) {
                            $scope.shownGroup = null;
                          } else {
                            $scope.shownGroup = id;
                          }
                        };
                        $scope.isGroupShown = function(id) {
                          return $scope.shownGroup === id;
                        };


  /**
   * @name editexercise
   * @todo To Edit Exercise
   * @return void function
   *
   **/
 $scope.editexercise = function(result)
    {

      $state.go('app.editexercise', {result: result});




    }


    $scope.removeexercise = function(id)
        {
        // console.log("mani");
$scope.showConfirm = function() {
                         var confirmPopup = $ionicPopup.confirm({
                           title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-close"></span></div></div>',
                           cssClass: 'animated jello progPicDelPop',
                           template: '<div class="PPdelBody"><span class="title">Warning</span><span class="popmessage">Are you sure you want to delete this exercise ? You will not be able to retrieve this once deleted.</span></div>'
                         });

                         confirmPopup.then(function(res) {
                           if(res) {
         /////// Code For Removing Exercise ///////////

var link = baseUrl+'/exercise/remove_exercise';
                  var formData = {uu_id : uuid, user_id : user_id,exercise_id:id}
                  var postData = 'myData='+JSON.stringify(formData);
                      $ionicLoading.show({
                                template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                       $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                      }).success(function(res){
                      // console.log(res);
                       $state.go($state.current, {}, {reload: true});

                        $ionicLoading.hide();

                      });
                      }
                      });


        };
         $scope.showConfirm();
        };




});
