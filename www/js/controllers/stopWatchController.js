/**
 * Purpose:
 *@This file is for stop watch
 *
 **/



angular.module('starter').controller('stopWatchController', ['$window', 'quote', '$scope', '$interval', '$ionicLoading', function ($window, quote, $scope, $interval, $ionicLoading) {


  /**
   *
   * Initializing Variables, Arrays, and Objects
   *
   **/



  var userData = $window.localStorage['userData'];
  $scope.userData = angular.fromJson(userData);
  $scope.userImg = $scope.userData.image;
  $scope.username = $scope.userData.user_fname;
  $scope.Lusername = $scope.userData.user_lname;

  // console.log("Userimg Source== "+$scope.userImg);

  var user_id = $scope.userData.user_id;

  /**
   *
   * for quotes
   *
   **/

  quote.getQuote(uuid, user_id).success(function (res) {
    // console.log(res);
    $ionicLoading.hide();
    $scope.quote = res;
  });
  /*

JQUERY: STOPWATCH & COUNTDOWN

This is a basic stopwatch & countdown plugin to run with jquery. Start timer, pause it, stop it or reset it. Same behaviour with the countdown besides you need to input the countdown value in seconds first. At the end of the countdown a callback function is invoked.

Any questions, suggestions? marc.fuehnen(at)gmail.com

*/

$(document).ready(function() {

    (function($){

        $.extend({

            APP : {

                formatTimer : function(a) {
                    if (a < 10) {
                        a = '0' + a;
                    }
                    return a;
                },

                startTimer : function(dir) {

                    var a;

                    // save type
                    $.APP.dir = dir;

                    // get current date
                    $.APP.d1 = new Date();

                    switch($.APP.state) {

                        case 'pause' :

                            // resume timer
                            // get current timestamp (for calculations) and
                            // substract time difference between pause and now
                            $.APP.t1 = $.APP.d1.getTime() - $.APP.td;

                        break;

                        default :

                            // get current timestamp (for calculations)
                            $.APP.t1 = $.APP.d1.getTime();

                            // if countdown add ms based on seconds in textfield
                            if ($.APP.dir === 'cd') {
                                $.APP.t1 += parseInt($('#cd_seconds').val())*1000;
                            }

                        break;

                    }

                    // reset state
                    $.APP.state = 'alive';
                    $('#' + $.APP.dir + '_status').html('Running');

                    // start loop
                    $.APP.loopTimer();

                },

                pauseTimer : function() {

                    // save timestamp of pause
                    $.APP.dp = new Date();
                    $.APP.tp = $.APP.dp.getTime();

                    // save elapsed time (until pause)
                    $.APP.td = $.APP.tp - $.APP.t1;

                    // change button value
                    $('#' + $.APP.dir + '_start').val('Resume');

                    // set state
                    $.APP.state = 'pause';
                    $('#' + $.APP.dir + '_status').html('Paused');

                },

                stopTimer : function() {

                    // change button value
                    $('#' + $.APP.dir + '_start').val('Restart');

                    // set state
                    $.APP.state = 'stop';
                    $('#' + $.APP.dir + '_status').html('Stopped');

                },

                resetTimer : function() {

                    // reset display
                    $('#' + $.APP.dir + '_ms,#' + $.APP.dir + '_s,#' + $.APP.dir + '_m,#' + $.APP.dir + '_h').html('00');

                    // change button value
                    $('#' + $.APP.dir + '_start').val('Start');

                    // set state
                    $.APP.state = 'reset';
                    $('#' + $.APP.dir + '_status').html('Reset & Idle again');

                },

                endTimer : function(callback) {

                    // change button value
                    $('#' + $.APP.dir + '_start').val('Restart');

                    // set state
                    $.APP.state = 'end';

                    // invoke callback
                    if (typeof callback === 'function') {
                        callback();
                      // console.log('timer ended');
                      var ply = document.getElementById('audio');
                      ply.play();
                    }

                },

                loopTimer : function() {

                    var td;
                    var d2,t2;

                    var ms = 0;
                    var s  = 0;
                    var m  = 0;
                    var h  = 0;

                    if ($.APP.state === 'alive') {

                        // get current date and convert it into
                        // timestamp for calculations
                        d2 = new Date();
                        t2 = d2.getTime();

                        // calculate time difference between
                        // initial and current timestamp
                        if ($.APP.dir === 'sw') {
                            td = t2 - $.APP.t1;
                        // reversed if countdown
                        } else {
                            td = $.APP.t1 - t2;
                            if (td <= 0) {
                                // if time difference is 0 end countdown
                                $.APP.endTimer(function(){
                                    $.APP.resetTimer();
                                    $('#' + $.APP.dir + '_status').html('Ended & Reset');
                                });
                            }
                        }

                        // calculate milliseconds
                        ms = td%1000;
                        if (ms < 1) {
                            ms = 0;
                        } else {
                            // calculate seconds
                            s = (td-ms)/1000;
                            if (s < 1) {
                                s = 0;
                            } else {
                                // calculate minutes
                                var m = (s-(s%60))/60;
                                if (m < 1) {
                                    m = 0;
                                } else {
                                    // calculate hours
                                    var h = (m-(m%60))/60;
                                    if (h < 1) {
                                        h = 0;
                                    }
                                }
                            }
                        }

                        // substract elapsed minutes & hours
                        ms = Math.round(ms/100);
                        s  = s-(m*60);
                        m  = m-(h*60);

                        // update display
                        $('#' + $.APP.dir + '_ms').html($.APP.formatTimer(ms));
                        $('#' + $.APP.dir + '_s').html($.APP.formatTimer(s));
                        $('#' + $.APP.dir + '_m').html($.APP.formatTimer(m));
                        $('#' + $.APP.dir + '_h').html($.APP.formatTimer(h));

                        // loop
                        $.APP.t = setTimeout($.APP.loopTimer,1);

                    } else {

                        // kill loop
                        clearTimeout($.APP.t);
                        return true;

                    }

                }

            }

        });

        $('#sw_start').on('click', function() {

            $.APP.startTimer('sw');
			$('#sw_pause').removeClass('hide');
			 $('#sw_start').addClass('hide');
        });

        $('#cd_start').on('click', function() {
			$('.inpbc').removeClass('hide');
			$('.inpf').addClass('hide');
			$('#cd_pause').removeClass('hide');
			 $('#cd_start').addClass('hide');
			var tot_secs = 0;
			var hours	=	parseInt($('#cd_h_i').val());
			var minutes	=	parseInt($('#cd_m_i').val());
			var seconds	=	parseInt($('#cd_s_i').val());
			if(hours > 0)
			{
				var hours_to_sec	=	hours * 3600;
				tot_secs = tot_secs	+ hours_to_sec	;

			}
			if(minutes > 0)
			{
				var min_to_sec	=	minutes * 60;
				tot_secs = tot_secs	+ min_to_sec	;

			}
			if(seconds > 0)
			{

				tot_secs = tot_secs	+ seconds	;

			}

			$('#cd_seconds').val(tot_secs);
            $.APP.startTimer('cd');
        });

        $('#sw_stop,#cd_stop').on('click', function() {
            $.APP.stopTimer();
        });

        $('#sw_reset').on('click', function() {
            $.APP.resetTimer();
			$('#sw_start').removeClass('hide');
			 $('#sw_pause').addClass('hide');
        });

		$('#cd_reset').on('click', function() {
            $.APP.resetTimer();
			$('#cd_start').removeClass('hide');
			 $('#cd_pause').addClass('hide');
			 $('.inpbc').addClass('hide');
			$('.inpf').removeClass('hide');
        });

        $('#sw_pause').on('click', function() {
            $.APP.pauseTimer();
			$('#sw_start').removeClass('hide');
			 $('#sw_pause').addClass('hide');

        });
		$('#cd_pause').on('click', function() {
            $.APP.pauseTimer();
			$('#cd_start').removeClass('hide');
			 $('#cd_pause').addClass('hide');

        });

    })(jQuery);

});
                 }]);
