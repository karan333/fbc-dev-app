/**
 * Purpose:
 *@This file is used as start a workout select intensity
 *
 **/


angular.module('starter').controller('startWorkout3Ctrl', function($cordovaToast, $rootScope,  $cordovaNetwork, ionicDatePicker,$ionicModal, $ionicPopover, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading) {
 // console.log(GuserData.user_fname);


  /**
   *
   * Initializing Variables, Arrays, and Objects
   *
   **/
$scope.deviceInfo = deviceInformation.getInfo();
var uuid = $scope.deviceInfo.uuid;
$scope.siteUrl = siteUrl;
$scope.selectType = {};
$scope.isOnline = [];
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();


        /**
         *
         * Listen for Online event
         *
         **/
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        /**
         *
         * Listen for ofline event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            // console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);


  /**
   * @name selectIntensity
   * @todo To select intensity
   * @return void function
   *
   **/




              $scope.selectIntensity = function(value){
                $window.localStorage['selectIntensity'] = value;
                // console.log('type---' + $window.localStorage['selectType'] + '--level----' + $window.localStorage['selectLevel'] + '--intensity--' + $window.localStorage['selectIntensity']);
                $state.go('app.startWorkout4');
              }


});
