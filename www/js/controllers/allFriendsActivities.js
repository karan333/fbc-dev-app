/**
 * Purpose:
 *@This file is used Show Friend's Activities.
 *
 **/

angular.module('starter').controller('allFriendsActivities', function(quote, $cordovaScreenshot, $cordovaSocialSharing, $cordovaToast, $cordovaNetwork, $rootScope, $cordovaPreferences, $ionicBackdrop, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading, $cordovaFileTransfer,$q) {


    $scope.siteUrl = siteUrl;
    $scope.deviceInfo = deviceInformation.getInfo();
    var uuid = $scope.deviceInfo.uuid
    $scope.selectedPost="";

    $scope.commentImage = [];
    $scope.commentImgs = [];
    $scope.allFActivity=[];
     $scope.postlimit = 3;
      $scope.FActivity=[];
      $scope.currentrecordsview = 20;
      $scope.morefrndactvtse=true;
$scope.friend_id  = $stateParams.result;

$scope.isOnline = [];
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();


        /**
         *
         * Listen for Online event
         *
         **/
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        /**
         *
         * Listen for Ofline event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            // console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);
                var userData = $window.localStorage['userData'];
                $scope.userData = angular.fromJson(userData);

                      // after getting data from sqlite getting user id and then all news feed service

                      var user_id = $scope.userData.user_id;
                      quote.getQuote(uuid, user_id).success(function (res) {
                                                                      // console.log(res);
                                                                      $scope.quote = res;
                                                                      });
                      var link = baseUrl+'/dashboard/all_specificfriendsactivities';
                          // console.log('userID---'+ user_id + '--UUID--' + uuid);
                          var formData = {uu_id : uuid, user_id : user_id,friend_id:$scope.friend_id}
                          var postData = 'myData='+JSON.stringify(formData);

                          $ionicLoading.show({
                          template: '<ion-spinner icon="ios"></ion-spinner>'

                          });
                          $http({
                            method : 'POST',
                            url : link,
                            data: postData,
                            headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                        }).success(function(res){
                          $scope.allFActivity = res.friends_activities;
                          if(res.friends_activities.length>0){
                           res.friends_activities  =  angular.fromJson(res.friends_activities);

                            // console.log("Mani");
                            // console.log(res.friends_activities[0]);

                            for(var i=0; i< $scope.postlimit ; i++) {
                             $scope.FActivity.push(res.friends_activities[i]);

                            }
                            $scope.currentrecordsview=$scope.postlimit;
                           //$scope.FActivity = res.friends_activities;

                           $ionicLoading.hide();
                             $timeout(function() {
                                ionicMaterialMotion.fadeSlideInRight({
                                    startVelocity: 3000
                                });
                            }, 1000);

                            // Set Ink
                            ionicMaterialInk.displayEffect();
                          } else{
                           $ionicLoading.hide();
                          $scope.NoResultFound = '402';
                          }

                      })
                      .error(function(error){
                      $ionicLoading.hide();
                        // console.log('request not completed')
                      });

  /**
   * @name like
   * @todo To Like Status
   * @return void function
   *
   **/
                      $scope.like = function(id,index){
                                                var status_id = id;

                                                var link = baseUrl+'/dashboard/like_user_status';

                                                var formData = {uu_id : uuid, user_id : user_id, status_id : status_id,}
                                                var postData = 'myData='+JSON.stringify(formData);

                                                // $ionicLoading.show({
                                                // template: '<ion-spinner icon="ios"></ion-spinner>'
                                                //
                                                // });
                                                $http({
                                                  method : 'POST',
                                                  url : link,
                                                  data: postData,
                                                  headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                              }).success(function(res){

                                              $scope.postLiked = {};
                                                  // $ionicLoading.hide();


                                                  if(res.code == '200'){
                                                    $scope.actLiked = 'liked';
                                                    $scope.FActivity[index].likes = res.likes;

                                                  } else if(res.code == '402'){
                                                    $scope.actLiked = 'Disliked';
                                                    $scope.FActivity[index].likes = res.likes;

                                                  } else{

                                                  }

                                            })


                                                }

                      ///////////End For Liking ///////////////
  /**
   * @name postCommentfrnd
   * @todo To Comment on Post
   * @return void function
   *
   **/

                      $scope.postCommentfrnd = function(id, i,mani){
                      console.log(mani);
                                                  var comment_text = mani[id+"_"+id];
                                                  var status_id = id;

                                                  var link = baseUrl+'/dashboard/post_comment';
                                                  var formData = {uu_id : uuid, user_id : user_id, comment_text : comment_text, status_id : status_id, comment_images : $scope.commentImgs}
                                                  var postData = 'myData='+JSON.stringify(formData);

                                                  $ionicLoading.show({
                                                  template: '<ion-spinner icon="ios"></ion-spinner>'

                                                  });
                                                  $http({
                                                    method : 'POST',
                                                    url : link,
                                                    data: postData,
                                                    headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                                }).success(function(res){
                                                mani[id+"_"+id] = '';
                                                $scope.commentImage = [];
                                                $scope.commentImgs = [];

                                                   $ionicLoading.hide();
                                                    $scope.FActivity[i].comments = res.comments.comments;


                                              })

                                                }


  /**
   * @name removeCommentIMG
   * @todo To Delete Comment Image
   * @return void function
   *
   **/

$scope.removeCommentIMG = function(index)
{
// console.log("Image Index" + index);
$scope.commentImgs.splice(index, 1);  ;
$scope.commentImage.splice(index, 1);  ;
// console.log($scope.postImage);

}

  /**
   * @name selectUploadMethodComment
   * @todo To Upload Images in comment
   * @return void function
   *
   **/

$scope.selectUploadMethodComment = function(post_id){
if($scope.selectedPost != post_id)
{
 $scope.commentImage=[];
 $scope.commentImgs=[] ;

}
$scope.selectedPost = post_id;
// console.log($scope.selectedPost+ "Post _id");
  var myPopup = $ionicPopup.show({
      template: '<div class="uploadMethodSack"><ul><li><a class="ink" ng-click="takePictureComment();"><span class="ion-camera"></span><span>Upload from Camera</span></a></li><li><a class=ink"" ng-click="selectPictureComment();"><span class="ion-images"></span><span>Upload from Gallery</span></a></li></ul></div>',
      title: 'Choose Upload Method',
      subTitle: 'Select from Camera or Gallery',
      scope: $scope
    });

    myPopup.then(function(res) {

    });

    $timeout(function() {
       myPopup.close();
    }, 8000);
}

  /**
   * @name takePictureComment
   * @todo To Take Picture in comment via Camera
   * @return void function
   *
   **/
 $scope.takePictureComment = function(){
  // console.log("In Comment2");
     document.addEventListener("deviceready", function () {

        var options = {

          destinationType: Camera.DestinationType.FILE_URI,
          sourceType: Camera.PictureSourceType.CAMERA,
          allowEdit: true
        };

        $cordovaCamera.getPicture(options).then(function(imageURI) {
        // console.log(imageURI)
        $scope.uploadPicturexComment(imageURI);
          window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {
          //console.log(fileEntry.nativeURL);
           // $scope.status.postImage = imageURI;
           // console.log($scope.status.postImage);
            $scope.ftLoad = true;
//                                var image = document.getElementById('myImage');
//                                image.src = fileEntry.nativeURL;
            });
          $ionicLoading.show({template: 'Uploading Picture...', duration:500});
        },
        function(err){
          $ionicLoading.show({template: 'Error Uploading...', duration:500});
        })

      }, false);
  }

  /**
   * @name selectPictureComment
   * @todo To Select Pictures from Gallery
   * @return void function
   *
   **/

    $scope.selectPictureComment = function() {
     document.addEventListener("deviceready", function () {
        var options = {
          quality: 100,
          destinationType: Camera.DestinationType.FILE_URI,
          sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
          encodingType: Camera.EncodingType.JPEG,
          targetWidth: 250,
          targetHeight: 250,
          allowEdit: true
        };

        $cordovaCamera.getPicture(options).then(
        function(imageURI) {
        $scope.uploadPicturexComment(imageURI);

          window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {

            $scope.ftLoad = true;

            });
          $ionicLoading.show({template: 'Uploading Picture...', duration:500});
        },
        function(err){
          $ionicLoading.show({template: 'Error Uploading...', duration:500});
        })
         }, false);
      };
  /**
   * @name uploadPicturexComment
   * @todo To Upload Comment Pictures
   * @return void function
   *
   **/

$scope.uploadPicturexComment = function(imageURI) {

// console.log("In Upload Picture Function");
// console.log(imageURI);
  var options = {

  fileKey: "picture",

  filename: "p1.png",

  chunkedMode: false,

  mimeType: "image/png",
  trustAllHosts:true,
  headers:{Connection:"close"}

  };
 $ionicLoading.show({
                                             template: '<ion-spinner icon="ios"></ion-spinner><p>Uploading Image!</p>'

                                         });
  $cordovaFileTransfer.upload(encodeURI(baseUrl+"/dashboard/uploadCommentmge"), imageURI, options)

  .then(function(result) {


   $scope.commentImage.push(imageURI);;
 var  resk  = angular.fromJson(result.response);

          $scope.commentImgs.push(resk.image_name) ;

$ionicLoading.hide();


  }, function(err) {


  $ionicLoading.hide();
  $ionicLoading.show({template: 'An error occured while uploading your picture.<br>Please try again.', duration:500});

  }, function (progress) {

            });



  }





                      /////////////// End For Posting Comments ////////////
  /**
   * @name loadmorefriendsact
   * @todo To Load More Friends Actions
   * @return void function
   *
   **/
                      $scope.loadmorefriendsact = function() {

                      // console.log("In Scroll");
                      var lim = $scope.currentrecordsview + $scope.postlimit;

                      for(var i=$scope.currentrecordsview;i< lim;i++)
                      {
                        if(i < $scope.allFActivity.length)
                        {
                          $scope.FActivity.push($scope.allFActivity[i]);
                        }
                        else
                        {
                          $scope.morefrndactvtse=false;
                          break;
                        }
                      }
                      // console.log($scope.FActivity);
                    $scope.currentrecordsview=lim;
                    $timeout(function() {
                                                    ionicMaterialMotion.fadeSlideInRight({
                                                        startVelocity: 3000
                                                    });
                                                }, 1000);

                                                // Set Ink
                                                ionicMaterialInk.displayEffect();
                      $scope.$broadcast('scroll.infiniteScrollComplete');
                  };
                  $scope.$on('$stateChangeSuccess', function() {
                    $scope.loadmorefriendsact();
                  });

});
