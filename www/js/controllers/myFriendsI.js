
/**
 * Purpose:
 *@This file is used to show friends list wrapper
 *
 **/


angular.module('starter').controller('myFriendsICtrl', function(quote, $cordovaToast, $rootScope,  $cordovaNetwork, deviceInformation, $scope, $cordovaSQLite, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $ionicLoading, $ionicPopup) {

 $scope.isOnline = [];




       document.addEventListener("deviceready", function () {
         $scope.network = $cordovaNetwork.getNetwork();
         $scope.isOnline = $cordovaNetwork.isOnline();

         /**
          *
          * Listen for Online event
          *
          **/
         $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
             $scope.isOnline = true;
             $scope.network = $cordovaNetwork.getNetwork();
             $cordovaToast.showLongBottom('You are Online !')

         })


         /**
          *
          * Listen for ofline event
          *
          **/
         $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
             // console.log("got offline");
             $scope.isOnline = false;
             $scope.network = $cordovaNetwork.getNetwork();
             $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
         })

       }, false);




  /**
   *
   * get device info
   *
   **/


   $scope.deviceInfo = deviceInformation.getInfo();
   var uuid = $scope.deviceInfo.uuid
});
