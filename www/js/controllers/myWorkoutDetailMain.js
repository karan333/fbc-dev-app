/**
 * Purpose:
 *@This file is used to view user workout detail page
 *
 **/



angular.module('starter').controller('myWorkoutDetailMainCtrl', function($ionicHistory, $cordovaToast, $ionicPlatform, quote, $rootScope,  $cordovaNetwork, $window, $log,$q, video, $document, $ionicSlideBoxDelegate, $ionicPopover, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading, $cordovaFileTransfer, $rootScope, $ionicModal) {




  /**
   *
   * Initializing Variables, Arrays, and Objects
   *
   **/


$scope.deviceInfo = deviceInformation.getInfo();
var uuid = $scope.deviceInfo.uuid;
$scope.baseUrlForWorkoutImg = 'http://www.fitnessbasecamp.com/uploads/workouts/';
$scope.baseUrlForWorkoutPoster = 'http://www.fitnessbasecamp.com/uploads/category-images/';
$scope.siteUrl = 'http://www.fitnessbasecamp.com/';
$scope.allExercises = {};
$scope.videos = {};
$scope.workoutDetai = {};
$scope.vid_srcc = {};
$scope.reviews = {};
$scope.nct=0;
$scope.disablenext =false;
$scope.workout_Completed = 0;
var mani =[];
$scope.TotalDownloaded = 0;
$scope.progresss=  0;
$scope.vsble  =false;
$scope.isOnline = [];
$scope.bit = {};

var userData = $window.localStorage['userData'];
$scope.userData = angular.fromJson(userData);

$scope.addReview_username = $scope.userData.user_fname;
$scope.addReview_email = $scope.userData.user_email;
$scope.addReview_message = '';





 //rating

$scope.rating = {};
$scope.rating.rate = 0;
$scope.rating.max = 5;


//review



      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();


        /**
         *
         * Listen for Online event
         *
         **/


        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        /**
         *
         * Listen for ofline event
         *
         **/


        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            // console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);





$scope.options = {
  loop: false,
  effect: 'fade',
  speed: 500
};




  /**
   *
   * get quotes
   *
   **/

              var user_id = $scope.userData.user_id;
              quote.getQuote(uuid, user_id).success(function (res) {
                                  // console.log(res);
                                  $scope.quote = res;
                                  });
              var workout_id = $stateParams.result;
              $scope.bit = $stateParams.cid;
              $scope.sender = $stateParams.did;
              $scope.noti_id = $stateParams.nid;
              $scope.workoutID  = $stateParams.result;


  /**
   *
   * get user workout detail from service api call
   *
   **/


              var link = baseUrl+'/workouts/my_workout_details';
              var formData = {uu_id : uuid, user_id : user_id, workout_id : workout_id}
              var postData = 'myData='+JSON.stringify(formData);
                  $ionicLoading.show({
                            template: '<ion-spinner icon="ios"></ion-spinner>'

                        });
                   $http({
                          method : 'POST',
                          url : link,
                          data: postData,
                          headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                  }).success(function(res){
                    $ionicLoading.hide();
                    // console.log(res);
                    $scope.workout_name = res.workout[0].name;
                    $scope.workoutDetai = res.workout[0];
                    $scope.reviews = res.workout[0].reviews;
                    $scope.suggestedWorkouts = res.workout[0].suggestedWorkouts;
                    $ionicSlideBoxDelegate.update();
                    $scope.videoID = res.workout[0].embed_code;
                    $scope.poster = res.workout[0].custom_image;
                    $scope.vid_srcc = res.workout[0].embed_video;
                     // $scope.youtube = $scope.vid_srcc + '?rel=0';
                     $scope.youtube = $scope.vid_srcc;
                     var vimeo_base_url = 'http://player.vimeo.com/video/';
                     $scope.vimeoVideo = vimeo_base_url + res.workout[0].embed_code;
                     $scope.myWorkoutPoster = res.workout[0].custom_image;
                     if($scope.myWorkoutPoster == ''){
                       $scope.myWorkoutPoster = "../../frontend/img/no-img-available-1.jpg";
                     }


                     // $timeout(function() {
                     //                  ionicMaterialMotion.fadeSlideInRight({
                     //                      startVelocity: 3000
                     //                  });
                     //              }, 700);
                     //
                     //            ionicMaterialInk.displayEffect();


                  }) .error(function(error){
                    $ionicLoading.hide();
                    // console.log('request not completed');
                  })


  /**
   * @name favorite
   * @todo To add workout to favorite list
   * @return void function
   *
   **/
                  $scope.favorite = function(workout_id){
                      // console.log('favorite');
                      var workout_id = workout_id;
                        var link = baseUrl+'/workouts/Togglefavrtunfavrtworkout';
                       var formData = {uu_id : uuid, user_id : user_id ,  workout_id : workout_id}
                       var postData = 'myData='+JSON.stringify(formData);
                        $ionicLoading.show({
                               template: '<ion-spinner icon="ios"></ion-spinner>'

                           });
                      $http({
                             method : 'POST',
                             url : link,
                             data: postData,
                             headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                     }).success(function(res){

                       // console.log(res);
                        var alertPopup = $ionicPopup.alert({
                        title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                        template: res.message,
                        cssClass: 'loginErrorPopUp'
                      });

                        $ionicLoading.hide();
                     }) .error(function(error){
                       $ionicLoading.hide();
                       // console.log('request not completed');
                     });

                      }

                   $ionicModal.fromTemplateUrl('templates/modal.html', {
                      scope: $scope
                    }).then(function(modal) {
                     $scope.modal = modal;
                        // console.log('modal is opened');
                    });



  /**
   * @name addReview
   * @todo To add review to workout
   * @return void function
   *
   **/


                    $scope.addReview = function(username, email, message){
                    // console.log(username +'-----'+email+'-----'+message)
                      var user_name = username;
                      var email = email;
                      var message = message;
                      var rating = $scope.rating.rate;
                      var user_id = $scope.userData.user_id;
                      var workout_id = $stateParams.result;
                      var is_custom = 1;

                  var link = baseUrl+'/workouts/add_workout_review';
                  var formData = {uu_id : uuid, user_id : user_id, workout_id : workout_id, message : message, email : email, user_name : user_name, rating : rating, is_custom : is_custom};
                  var postData = 'myData='+JSON.stringify(formData);
                      $ionicLoading.show({
                                template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                       $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                      }).success(function(res){
                        $ionicLoading.hide();
                        // console.log(res.message);

                        var alertPopup = $ionicPopup.alert({
                        title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                        template: res.message,
                        cssClass: 'loginErrorPopUp hideOk'
                      });

                          $timeout(function() {
                           alertPopup.close();
                           $scope.modal.hide();

                         }, 3000);
                        //  $timeout(function() {
                        //       ionicMaterialMotion.fadeSlideInRight({
                        //           startVelocity: 3000
                        //       });
                        //   }, 700);
                        //
                        // ionicMaterialInk.displayEffect();


                      }) .error(function(error){
                        $ionicLoading.hide();
                        // console.log('request not completed');
                      })
                    };

  /**
   *
   * to initialize modals
   *
   **/


  $ionicModal.fromTemplateUrl('templates/modal3.html', {
    id: 'modal3',
    scope: $scope,
    animation: 'animated bounceIn',
    backdropClickToClose: false
  }).then(function(modal) {
    $scope.modal3 = modal;



    // Set Ink
    ionicMaterialInk.displayEffect();
  });


  /**
   *
   * to check if shred workout has some message sent by user
   *
   **/



  $scope.$on('modal.shown', function(event, modal) {
   if(modal.id == 'modal3'){

     var link = baseUrl+'/workouts/getMessage';
     var formData = {uu_id : uuid, user_id : user_id, noti_id : $scope.noti_id};
     // console.log(formData);
     var postData = 'myData='+JSON.stringify(formData);
     $ionicLoading.show({
       template: '<ion-spinner icon="ios"></ion-spinner>'

     });
     $http({
       method : 'POST',
       url : link,
       data: postData,
       headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

     }).success(function(res){
       $ionicLoading.hide();
       // console.log(res.notification);
       $scope.notiUserImg = res.notification[0].posted_by;
       $scope.message = res.notification[0].custom_message;


     }) .error(function(error){
       $ionicLoading.hide();
       // console.log('request not completed');
     })

   }
  });
  $scope.closeModal3 = function() {
    $scope.modal3.hide();
  };



  $scope.showMEssage = function () {

    $scope.modal3.show();

  };



  /**
   * @name deregisterFirst
   * @todo To add back button route
   * @return void function
   *
   **/


  var deregisterFirst = $ionicPlatform.registerBackButtonAction(function(e) {
    $scope.backView = $ionicHistory.backView();
    // console.log($scope.backView);
    if($scope.backView.stateName === 'app.home'){
      $state.go('app.home');
    } else if($scope.backView.stateName === "app.allNewsFeed"){
      $state.go('app.allNewsFeed');
    } else{
      $state.go('app.myWorkouts', {cid: workout_id});
    }

    // console.log('Back');
    e.preventDefault();
    return false;
  }, 101);

  $scope.$on('$destroy', deregisterFirst);



});

