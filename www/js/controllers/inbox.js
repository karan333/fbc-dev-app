/**
 * Purpose:
 *@This file is used to Show Inbox of user's Messages
 *
 **/
angular.module('starter').controller('inboxCtrl', function($rootScope,  $cordovaNetwork, $window, deviceInformation, $scope, $cordovaSQLite, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $ionicLoading, $ionicPopup) {
 // console.log('inbox');
 $scope.isOnline = [];
       document.addEventListener("deviceready", function () {
         $scope.network = $cordovaNetwork.getNetwork();
         $scope.isOnline = $cordovaNetwork.isOnline();


         // listen for Online event
         $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
             $scope.isOnline = true;
             $scope.network = $cordovaNetwork.getNetwork();
             $cordovaToast.showLongBottom('You are Online !')

         })

         // listen for Offline event
         $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
             // console.log("got offline");
             $scope.isOnline = false;
             $scope.network = $cordovaNetwork.getNetwork();
             $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
         })

       }, false);
 $scope.siteUrl = siteUrl;
                          var userData = $window.localStorage['userData'];
                           $scope.userData = angular.fromJson(userData);


                  var user_id = $scope.userData.user_id;
                  $scope.ifr_url = siteUrl+"arrowchat/public/popout/index_app.php?user_id="+user_id;


});
