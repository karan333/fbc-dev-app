/**
 * Purpose:
 *@This file is used to show stats
 *
 **/

angular.module('starter').controller('statsCtrl', function(notification, $cordovaInAppBrowser, $cordovaToast, quote, $rootScope,  $cordovaNetwork, $window, ionicDatePicker,$ionicModal, $ionicPopover, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading) {
 // console.log(GuserData.user_fname);

$ionicPopover.fromTemplateUrl('templates/popover.html', {
    scope: $scope,
  }).then(function(popover) {
    $scope.popover = popover;
  });


  /**
   *
   * Initializing Variables, Arrays, and Objects
   *
   **/



$scope.noStatFound = false;
$scope.deviceInfo = deviceInformation.getInfo();
var uuid = $scope.deviceInfo.uuid;
  $scope.packageExpired = {};
$scope.items = {};
$scope.addstat = {};
  $scope.newuser = {};
  $scope.singleValueType = {};
  $scope.statWeight = '';
  $scope.statLength = '';
  $scope.statHeight = '';
  $scope.measurement = $window.localStorage['measurement'];
  // console.log($scope.measurement);

  if($scope.measurement === 'Metric'){
    // console.log('in metric');
    $scope.statWeight = 'kg';
    $scope.statLength = 'cm';
    $scope.statHeight = 'm';
  } else{
    $scope.statWeight = 'lbs';
    $scope.statLength = 'inch';
    $scope.statHeight = 'ft';
  }




$scope.editstat = {
  value : 'Inches'
};
$scope.filter = {};
$scope.single_stats = {};
$scope.editItem = {};
$scope.editDate = {};
$scope.addM = {
  fieldName : '',
  fieldValue : '',
  statDate : ''
};

  $scope.notificationFull = [];
  $scope.notification = [];
  $scope.$on('$ionicView.beforeEnter', function(){
    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner>'

    });
    notification.get(uuid, user_id).success(function (res) {

      var notifications = res;
      $window.localStorage['notifications']  = JSON.stringify(notifications);
      var localNoti = $window.localStorage['notifications'];
      $scope.notification = angular.fromJson(localNoti);
      $scope.notificationFull = $scope.notification.notification;
      if($scope.notificationFull.length > 0){
        if($scope.notificationFull[0].count == 0){
          $scope.green = "green";
        } else if($scope.notificationFull[0].count > 0){
          $scope.green = "red";
        }
      }

      $scope.changeColor = function(){
        $scope.green = "green";

        //;


      }
    });
  });
  /**
   *
   * Initializing datepicker
   *
   **/


  var dateToday = new Date();
  var dayAdd = dateToday.getDate();
  var yearAdd = dateToday.getFullYear();
  var monthAdd = dateToday.getMonth()+ 1;
  $scope.addM.statDate = yearAdd + '-' + monthAdd + '-' + dayAdd;

  $scope.siteUrl = siteUrl;
 var ipObj1 = {
 callback: function (val) {
   var date = (new Date(val));

           var day = date.getDate();
           var year = date.getFullYear();
           var month = date.getMonth()+ 1;
   $scope.filter.from = year + '-' + month + '-' + day;

 },
 from: new Date(1970, 1, 1),
 to: new Date(2017, 12, 30),
 inputDate: new Date(),
 titleLabel: 'Select a Date',
 setLabel: 'Set',
 todayLabel: 'Today',
 closeLabel: 'Close',
 mondayFirst: false,
 weeksList: ["S", "M", "T", "W", "T", "F", "S"],
 monthsList: ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"],
 templateType: 'popup',
 showTodayButton: true,
 dateFormat: 'dd MMMM yyyy',
 closeOnSelect: false,
 disableWeekdays: []
};
 $scope.openDatePicker = function(){
 ionicDatePicker.openDatePicker(ipObj1);
};


  /**
   *
   * Initializing datepicker
   *
   **/



var ipObj2 = {
     callback: function (val) {
       var date = (new Date(val));

        var day = date.getDate();
        var year = date.getFullYear();
        var month = date.getMonth()+ 1;
       $scope.filter.to = year + '-' + month + '-' + day;
     },
  from: new Date(1970, 1, 1),
  to: new Date(2017, 12, 30),
  inputDate: new Date(),
  titleLabel: 'Select a Date',
  setLabel: 'Set',
  todayLabel: 'Today',
  closeLabel: 'Close',
  mondayFirst: false,
  weeksList: ["S", "M", "T", "W", "T", "F", "S"],
  monthsList: ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"],
  templateType: 'popup',
  showTodayButton: true,
  dateFormat: 'dd MMMM yyyy',
  closeOnSelect: false,
  disableWeekdays: []
   }
     $scope.openDatePicker2 = function(){
     ionicDatePicker.openDatePicker(ipObj2);
   };



  /**
   *
   * Initializing datepicker
   *
   **/



  var arrayIndex;
  var ipObj3 = {
    callback: function (val) {
      var date = (new Date(val));

      var day = date.getDate();
      var year = date.getFullYear();
      var month = date.getMonth()+ 1;
      $scope.single_stats[arrayIndex].date_created = year + '-' + month + '-' + day;
      // console.log($scope.single_stats[arrayIndex].date_created);
      // console.log('arrayIndex = '+arrayIndex);
    },
    from: new Date(1970, 1, 1),
    to: new Date(2017, 12, 30),
    inputDate: new Date(),
    titleLabel: 'Select a Date',
    setLabel: 'Set',
    todayLabel: 'Today',
    closeLabel: 'Close',
    mondayFirst: false,
    weeksList: ["S", "M", "T", "W", "T", "F", "S"],
    monthsList: ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"],
    templateType: 'popup',
    showTodayButton: true,
    dateFormat: 'dd MMMM yyyy',
    closeOnSelect: false,
    disableWeekdays: []
  }
  $scope.openDatePicker3 = function(id){
    // console.log(id);
    arrayIndex = id;
    ionicDatePicker.openDatePicker(ipObj3);
//                                   $scope.showPopup();
  };


  /**
   *
   * Initializing datepicker
   *
   **/




  var ipObj4 = {
    callback: function (val) {
      var date = (new Date(val));

      var day = date.getDate();
      var year = date.getFullYear();
      var month = date.getMonth()+ 1;
      $scope.addM.statDate = year + '-' + month + '-' + day;
      // console.log($scope.addM.date);
    },
    from: new Date(1970, 1, 1),
    to: new Date(2017, 12, 30),
    closeOnSelect: true
  }
  $scope.openDatePicker4 = function(id){

    ionicDatePicker.openDatePicker(ipObj4);
//                                   $scope.showPopup();
  };






  /**
   * @name freshStats
   * @todo to get stats if user has login for first time
   * @return void function
   *
   **/

  $scope.freshStats = function(heightf, heighti, weight, waist, chest, shoulder, arms, hips, thighs, calves, neck){

    // $window.localStorage['newUser'] = 1;
    var user_id = $scope.userData.user_id;

    var link = baseUrl+'/dashboard/newUserStat';

    var formData = {uu_id : uuid, user_id : user_id, heightf : heightf, heighti : heighti, weight : weight, chest : chest, shoulder : shoulder, arms : arms, hips : hips, thighs : thighs, calves : calves, neck : neck, waist : waist, from : 'stat'};
    // console.log(formData);

    var postData = 'myData='+JSON.stringify(formData);

    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner>'

    });
    $http({
      method : 'POST',
      url : link,
      data: postData,
      headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

    }).success(function(res){

      // console.log(res);

      $scope.items = res.stats;
      if($scope.items.length < 1){
        $scope.noStatFound = true;
        $ionicModal.fromTemplateUrl('templates/newUser.html', {
          scope: $scope
        }).then(function(modal) {

          $scope.newUserModal = modal;
          modal.show();

        });
      } else{
        $scope.noStatFound = false;
      }


      var alertPopup = $ionicPopup.alert({
        title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
        template: res.message,
        cssClass: 'loginErrorPopUp hideOk'
      });
      $timeout(function(){
        alertPopup.close();
      }, 3000);
      $scope.newUserModal.hide();
      $scope.newUserModal.remove();
      $ionicLoading.hide();


      $timeout(function() {
        ionicMaterialMotion.fadeSlideInRight({
          startVelocity: 3000
        });
      }, 700);

      // Set Ink
      ionicMaterialInk.displayEffect();






    }) .error(function(error){
      $ionicLoading.hide();

    })




  };


  /**
   * @name skipStat
   * @todo to get skip add body stat form while first signup
   * @return void function
   *
   **/




  $scope.skipStat = function(){
    $scope.newUserModal.hide();
    $scope.newUserModal.remove();
    // $window.localStorage['newUser'] = 1;
  };













$scope.isOnline = [];
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();


        /**
         *
         * Listen for Online event
         *
         **/
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        /**
         *
         * Listen for ofline event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            // console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);

            var userData = $window.localStorage['userData'];
             $scope.userData = angular.fromJson(userData);

  var packageExpired = $window.localStorage['expired'];
  $scope.packageExpired = angular.fromJson(packageExpired);





  /**
   *
   * for quotes
   *
   **/


                  var user_id = $scope.userData.user_id;
                  quote.getQuote(uuid, user_id).success(function (res) {
                                      // console.log(res);
                                      $scope.quote = res;
                                      });
                  // console.log('userID---' + user_id + 'uuid----' + uuid);




  /**
   *
   * for getting stat charts
   *
   **/



                  var link = baseUrl+'/stats/getstats';
                  var formData = {uu_id : uuid, user_id : user_id}
                  var postData = 'myData='+JSON.stringify(formData);
                      $ionicLoading.show({
                                template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                       $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                      }).success(function(res){

                        // console.log(res);

                       $scope.items = res.stats;
                        if($scope.items.length < 1){
                          $scope.noStatFound = true;
                          $ionicModal.fromTemplateUrl('templates/newUser.html', {
                            scope: $scope
                          }).then(function(modal) {

                            $scope.newUserModal = modal;
                              modal.show();

                          });
                        }
                         $ionicLoading.hide();



                          $timeout(function() {
                              ionicMaterialMotion.fadeSlideInRight({
                                  startVelocity: 3000
                              });
                          }, 700);

                          // Set Ink
                          ionicMaterialInk.displayEffect();


                      }) .error(function(error){
                        $ionicLoading.hide();
                        // console.log('request not completed');
                      })

                      //update (add) body stats
  /**
   * @name updateStats
   * @todo To add stat to a chart
   * @return void function
   *
   **/

                      $scope.updateStats = function(){
                        var user_id = $scope.userData.user_id;
                        // console.log('userID---' + user_id + 'uuid----' + uuid);
                        var link = baseUrl+'/stats/add_body_stats';
                        var field_name = $scope.addstat.name;
                        var field_value = $scope.addstat.value
                        var value_type = $scope.addstat.type;
                        var formData = {uu_id : uuid, user_id : user_id, field_name : field_name, field_value : field_value, value_type : value_type}
                        // console.log(formData);
                        var postData = 'myData='+JSON.stringify(formData);
                            $ionicLoading.show({
                                      template: '<ion-spinner icon="ios"></ion-spinner>'

                                  });
                             $http({
                                    method : 'POST',
                                    url : link,
                                    data: postData,
                                    headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                            }).success(function(res){

                              // console.log(res)

                               $ionicLoading.hide();
                               $scope.showAlert = function() {
                                 var alertPopup = $ionicPopup.alert({
                                   title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                   template: res.message,
                                   cssClass: 'loginErrorPopUp hideOk'
                                 });
                                 $timeout(function () {
                                   alertPopup.close();
                                 }, 3000);
                                 alertPopup.then(function(res) {
                                 $scope.modal.hide();
                                  var user_id = $scope.userData.user_id;
                                 // console.log('userID---' + user_id + 'uuid----' + uuid);
                                 var link = baseUrl+'/stats/getstats';
                                 var formData = {uu_id : uuid, user_id : user_id}
                                 var postData = 'myData='+JSON.stringify(formData);
                                     $ionicLoading.show({
                                               template: '<ion-spinner icon="ios"></ion-spinner>'

                                           });
                                      $http({
                                             method : 'POST',
                                             url : link,
                                             data: postData,
                                             headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                     }).success(function(res){

                                       // console.log(res)
                                      $scope.items = res.stats;
                                        if($scope.items.length < 1){
                                          $scope.noStatFound = true;
                                        }
                                        $ionicLoading.hide();



                                         $timeout(function() {
                                             ionicMaterialMotion.fadeSlideInRight({
                                                 startVelocity: 3000
                                             });
                                         }, 700);

                                         // Set Ink
                                         ionicMaterialInk.displayEffect();


                                     }) .error(function(error){
                                       $ionicLoading.hide();
                                       // console.log('request not completed');
                                     })
                                 });
                               };
                               $scope.showAlert();



                                $timeout(function() {
                                    ionicMaterialMotion.fadeSlideInRight({
                                        startVelocity: 3000
                                    });
                                }, 700);

                                // Set Ink
                                ionicMaterialInk.displayEffect();


                            }) .error(function(error){
                              $ionicLoading.hide();
                              // console.log('request not completed');
                            })
                      }



  /**
   * @name deleteStat
   * @todo To delete stat from a chart
   * @return void function
   *
   **/

                        $scope.deleteStat = function(id, field){

                          $scope.showConfirm = function() {
                            var confirmPopup = $ionicPopup.confirm({
                              title: 'Confirm',
                              template: 'Are you sure you want to delete?',
                              cssClass: 'loginErrorPopUp'

                            });

                            confirmPopup.then(function(res) {
                              if(res) {
                                var user_id = $scope.userData.user_id;
                                // console.log('userID---' + user_id + 'uuid----' + uuid);
                                var link = baseUrl+'/stats/delete_stats';
                                var stat_id = id;
                                var stats_field = field;
                                var formData = {uu_id : uuid, user_id : user_id, stat_id : stat_id, stats_field : stats_field}
                                // console.log(formData);
                                var postData = 'myData='+JSON.stringify(formData);
                                $ionicLoading.show({
                                  template: '<ion-spinner icon="ios"></ion-spinner>'

                                });
                                $http({
                                  method : 'POST',
                                  url : link,
                                  data: postData,
                                  headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                }).success(function(res){

                                  // console.log(res);
                                  var alertPopup = $ionicPopup.alert({
                                    title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                    template: res.message,
                                    cssClass: 'loginErrorPopUp hideOk'
                                  });
                                  $timeout(function () {
                                    alertPopup.close();
                                  }, 3000);
                                  $scope.single_stats = res.single_stats;
                                  $ionicLoading.hide();

                                }) .error(function(error){
                                  $ionicLoading.hide();
                                  // console.log('request not completed');
                                })


                              } else {
                                // console.log('You are not sure');
                              }
                            });
                          };

                          $scope.showConfirm();

                        }








  /**
   * @name editChart
   * @todo To get single chart values
   * @return void function
   *
   **/

                      $scope.editChart = function(id, statfield){

                        // console.log(id);
                        // console.log(statfield);
                        var stat_id = id;
                        var stats_field = statfield;
                        var user_id = $scope.userData.user_id;
                        // console.log('userID---' + user_id + 'uuid----' + uuid);
                        var link = baseUrl+'/stats/get_single_chart';
                        var formData = {uu_id : uuid, user_id : user_id, stat_id : stat_id, stats_field : stats_field}
                        // console.log(formData);
                        var postData = 'myData='+JSON.stringify(formData);
                        $ionicLoading.show({
                          template: '<ion-spinner icon="ios"></ion-spinner>'

                        });
                        $http({
                          method : 'POST',
                          url : link,
                          data: postData,
                          headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                        }).success(function(res){

                          // console.log(res);
                          $scope.single_stats = res.single_stats;
                          $scope.singleValueType = res.single_stats.valueType;
                          // console.log($scope.singleValueType);
                          $timeout(function() {
                            $ionicLoading.hide();
                            $scope.model2();
                          }, 700);


                        }) .error(function(error){
                          $ionicLoading.hide();
                          // console.log('request not completed');
                        })

                      };

  /**
   * @name editStats
   * @todo To edit stats inside chart
   * @return void function
   *
   **/

                      $scope.editStats = function(single_stats, value){
                        var user_id = $scope.userData.user_id;
                        // console.log('userID---' + user_id + 'uuid----' + uuid);
                        var link = baseUrl+'/stats/edit_body_stats';
                        var single_stats = $scope.single_stats;
                        var value_type = $scope.singleValueType;
                        var formData = {uu_id : uuid, user_id : user_id, single_stats : single_stats, value_type : value_type}
                        // console.log(formData);
                        var postData = 'myData='+JSON.stringify(formData);
                        $ionicLoading.show({
                          template: '<ion-spinner icon="ios"></ion-spinner>'

                        });
                        $http({
                          method : 'POST',
                          url : link,
                          data: postData,
                          headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                        }).success(function(res){

                          // console.log(res);
                          var alertPopup = $ionicPopup.alert({
                            title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                            template: res.message,
                            cssClass: 'loginErrorPopUp hideOk'
                          });
                          $timeout(function () {
                            alertPopup.close();
                          }, 3000);
                          $scope.single_stats = res.single_stats;
                          $scope.closeModal2();
                          $ionicLoading.hide();

                        }) .error(function(error){
                          $ionicLoading.hide();
                          // console.log('request not completed');
                        }).finally(function(){
                          var user_id = $scope.userData.user_id;
                          quote.getQuote(uuid, user_id).success(function (res) {
                            // console.log(res);
                            $scope.quote = res;
                          });
                          // console.log('userID---' + user_id + 'uuid----' + uuid);
                          var link = baseUrl+'/stats/getstats';
                          var formData = {uu_id : uuid, user_id : user_id}
                          var postData = 'myData='+JSON.stringify(formData);
                          $ionicLoading.show({
                            template: '<ion-spinner icon="ios"></ion-spinner>'

                          });
                          $http({
                            method : 'POST',
                            url : link,
                            data: postData,
                            headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                          }).success(function(res){

                            // console.log(res)
                            $scope.items = res.stats;
                            if($scope.items.length < 1){
                              $scope.noStatFound = true;
                            }
                            $ionicLoading.hide();



                            $timeout(function() {
                              ionicMaterialMotion.fadeSlideInRight({
                                startVelocity: 3000
                              });
                            }, 700);

                            // Set Ink
                            ionicMaterialInk.displayEffect();


                          }) .error(function(error){
                            $ionicLoading.hide();
                            // console.log('request not completed');
                          })
                        });
                      }

  /**
   * @name addMeasurement
   * @todo To show add measurement modal
   * @return void function
   *
   **/
                      //add measurement parameters

                          $scope.addMeasurement = function(id, statfield){

                            // console.log(id);
                            // console.log(statfield);
                            $scope.addM.fieldName = statfield;
                            $scope.model3();

                          }


  /**
   * @name addMeasurement
   * @todo To add measurement inside specific chart
   * @return void function
   *
   **/

                          $scope.submitAddM = function(fieldname, statdate, fieldvalue){

                            var fieldName = fieldname;
                            var fieldValue = fieldvalue;
                            var statDate = statdate;
                            var user_id = $scope.userData.user_id;
                            // console.log('userID---' + user_id + 'uuid----' + uuid);
                            var link = baseUrl+'/stats/updateBodyStats';
                            var formData = {uu_id : uuid, user_id : user_id, fieldName : fieldName, fieldValue : fieldValue, statDate : statDate}
                            // console.log(formData);
                            var postData = 'myData='+JSON.stringify(formData);
                            $ionicLoading.show({
                              template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                            $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                            }).success(function(res){

                              // console.log(res);
                              var alertPopup = $ionicPopup.alert({
                                title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                template: res.message,
                                cssClass: 'loginErrorPopUp hideOk'
                              });
                              $timeout(function () {
                                alertPopup.close();
                              }, 3000);
                              $ionicLoading.hide();
                              $scope.closeModal3();

                            }) .error(function(error){
                              $ionicLoading.hide();
                              // console.log('request not completed');
                            }).finally(function(){
                              var user_id = $scope.userData.user_id;
                              quote.getQuote(uuid, user_id).success(function (res) {
                                // console.log(res);
                                $scope.quote = res;
                              });
                              // console.log('userID---' + user_id + 'uuid----' + uuid);
                              var link = baseUrl+'/stats/getstats';
                              var formData = {uu_id : uuid, user_id : user_id}
                              var postData = 'myData='+JSON.stringify(formData);
                              $ionicLoading.show({
                                template: '<ion-spinner icon="ios"></ion-spinner>'

                              });
                              $http({
                                method : 'POST',
                                url : link,
                                data: postData,
                                headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                              }).success(function(res){

                                // console.log(res)
                                $scope.items = res.stats;
                                if($scope.items.length < 1){
                                  $scope.noStatFound = true;
                                }
                                $ionicLoading.hide();



                                $timeout(function() {
                                  ionicMaterialMotion.fadeSlideInRight({
                                    startVelocity: 3000
                                  });
                                }, 700);

                                // Set Ink
                                ionicMaterialInk.displayEffect();


                              }) .error(function(error){
                                $ionicLoading.hide();
                                // console.log('request not completed');
                              })
                            });

                          }





  /**
   * @name showPopup
   * @todo To show filter popup
   * @return void function
   *
   **/

                      $scope.showPopup = function() {
                        $scope.data = {};

                        // An elaborate, custom popup
                        var myPopup = $ionicPopup.show({
                          template: '<div class="filterStat"><span>From</span><input type="text" placeholder="" ng-click="openDatePicker();" ng-model="filter.from" name="" ng-readonly="true"><span>To</span><input type="text" placeholder="" ng-click="openDatePicker2();" ng-model="filter.to" name="" ng-readonly="true"></div>',
                          title: 'Filter Stats',
                          scope: $scope,
                          cssClass : 'statFilterPopup',
                          buttons: [
//                            { text: 'Cancel' },
                            {
                              text: '<b>Filter</b>',
                              type: 'button-positive',
                              onTap: function(e) {

                                /**
                                 *
                                 * to filter stats
                                 *
                                 **/

                               var user_id = $scope.userData.user_id;
                               // console.log('userID---' + user_id + 'uuid----' + uuid);
                               var link = baseUrl+'/stats/filter_mystats';
                               var from = $scope.filter.from;
                               var to = $scope.filter.to;
                               var formData = {uu_id : uuid, user_id : user_id, from : from, to : to}
                               // console.log(formData);
                               var postData = 'myData='+JSON.stringify(formData);
                                   $ionicLoading.show({
                                             template: '<ion-spinner icon="ios"></ion-spinner>'

                                         });
                                    $http({
                                           method : 'POST',
                                           url : link,
                                           data: postData,
                                           headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                   }).success(function(res){

                                     // console.log(res);
                                      $scope.items = res.stats;
                                      if($scope.items.length < 1){
                                        $scope.noStatFound = true;
                                      }
                                      $scope.NoResultFound =  res.code;
                                      $ionicLoading.hide();



                                       $timeout(function() {
                                           ionicMaterialMotion.fadeSlideInRight({
                                               startVelocity: 3000
                                           });
                                       }, 700);

                                       // Set Ink
                                       ionicMaterialInk.displayEffect();


                                   }) .error(function(error){
                                     $ionicLoading.hide();
                                     // console.log('request not completed');
                                   })



                              ///////////////////////////////////////////


                              }
                            }
                          ]
                        });

                        myPopup.then(function(res) {

                         });
//                           $timeout(function() {
//                              myPopup.close(); //close the popup after 3 seconds for some reason
//                           }, 3000);
                       };

  /**
   * @name toggleChart
   * @todo To show/hide a chart
   * @return void function
   *
   **/


  $scope.toggleChart = function(id, statfield, status){
    var status = status;
    // console.log(status);
    if(status === '0'){
      status = 1;
      $scope.display = 'show';
    }else if(status === '1'){
      $scope.display = 'hide';
      status = 0;
    }

    $scope.showConfirm = function() {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Confirm',
        template: 'Are you sure you want to '+$scope.display+' this chart on your dashboard',
        cssClass: 'loginErrorPopUp'

      });

      confirmPopup.then(function(res) {
        if(res) {
          var user_id = $scope.userData.user_id;
          var link = baseUrl+'/stats/changeStat';
          var stat_id = id;
          var stats_field = statfield;
          // console.log(status);

          var formData = {uu_id : uuid, user_id : user_id, stat_id : stat_id, stats_field : stats_field, status : status};
          // console.log(formData);
          var postData = 'myData='+JSON.stringify(formData);
          $ionicLoading.show({
            template: '<ion-spinner icon="ios"></ion-spinner>'

          });
          $http({
            method : 'POST',
            url : link,
            data: postData,
            headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

          }).success(function(res){
            // console.log(res);

            var alertPopup = $ionicPopup.alert({
              title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
              template: res.message,
              cssClass: 'loginErrorPopUp hideOk'
            });
            $timeout(function () {
              alertPopup.close();
            }, 4000);
            $scope.items = res.stats;
            if($scope.items.length < 1){
              $scope.noStatFound = true;
            }
            $ionicLoading.hide();



            $timeout(function() {
              ionicMaterialMotion.fadeSlideInRight({
                startVelocity: 3000
              });
            }, 700);

            // Set Ink
            ionicMaterialInk.displayEffect();


          }) .error(function(error){
            $ionicLoading.hide();
            // console.log('request not completed');
          })


        } else {
          // console.log('You are not sure');
        }
      });
    };

    $scope.showConfirm();

  };


  /**
   * @name deleteChart
   * @todo To delete a chart
   * @return void function
   *
   **/



                    $scope.deleteChart = function(id, statfield){

                      $scope.showConfirm = function() {
                        var confirmPopup = $ionicPopup.confirm({
                          title: 'Confirm',
                          template: 'Are you sure you want to delete this Chart?',
                          cssClass: 'loginErrorPopUp'

                        });

                        confirmPopup.then(function(res) {
                          if(res) {
                            var user_id = $scope.userData.user_id;
                            var link = baseUrl+'/stats/delete_stats_chart';
                            var stat_id = id;
                            var stats_field = statfield;
                            var formData = {uu_id : uuid, user_id : user_id, stat_id : stat_id, stats_field : stats_field}
                            // console.log(formData);
                            var postData = 'myData='+JSON.stringify(formData);
                            $ionicLoading.show({
                              template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                            $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                            }).success(function(res){

                              // console.log(res);

                              var alertPopup = $ionicPopup.alert({
                                title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                template: 'Your chart is deleted !',
                                cssClass: 'loginErrorPopUp hideOk'
                              });
                              $timeout(function () {
                                alertPopup.close();
                              }, 3000);
                              $scope.items = res.stats;
                              if($scope.items.length < 1){
                                $scope.noStatFound = true;
                                $ionicModal.fromTemplateUrl('templates/newUser.html', {
                                  scope: $scope
                                }).then(function(modal) {

                                  $scope.newUserModal = modal;
                                  modal.show();

                                });
                              }
                              $ionicLoading.hide();



                              $timeout(function() {
                                ionicMaterialMotion.fadeSlideInRight({
                                  startVelocity: 3000
                                });
                              }, 700);

                              // Set Ink
                              ionicMaterialInk.displayEffect();

                            }) .error(function(error){
                              $ionicLoading.hide();
                              // console.log('request not completed');
                            })


                          } else {
                            // console.log('You are not sure');
                          }
                        });
                      };

                      $scope.showConfirm();

                    };
  /**
   *
   * Initializing modals
   *
   **/
$ionicModal.fromTemplateUrl('templates/modal.html', {
  scope: $scope
}).then(function(modal) {
 $scope.modal = modal;
    // console.log('modal is opened');
});

  /**
   *
   * Initializing modals
   *
   **/
  $scope.modal2Data = {};
  $ionicModal.fromTemplateUrl('templates/modal2.html', {
    scope: $scope
  }).then(function(modal) {
    //Fix this line, changed the variable name to different name.
    $scope.modal2 = modal;
  });

  $scope.closeModal2 = function() {
    $scope.modal2.hide();
  };

  $scope.model2 = function() {
    $scope.modal2.show();
  };

  $scope.doModal2 = function() {
    // console.log('Doing Modal2', $scope.modal2Data);

    $timeout(function() {
      $scope.closeUseful();
    }, 1000);
  };



  /**
   *
   * Initializing modals
   *
   **/
  $scope.modal3Data = {};
  $ionicModal.fromTemplateUrl('templates/modal3.html', {
    scope: $scope
  }).then(function(modal) {
    //Fix this line, changed the variable name to different name.
    $scope.modal3 = modal;
  });

  $scope.closeModal3 = function() {
    $scope.modal3.hide();
  };

  $scope.model3 = function() {
    $scope.modal3.show();
  };

  $scope.doModal3 = function() {
    // console.log('Doing Modal3', $scope.modal3Data);

    $timeout(function() {
      $scope.closeUseful();
    }, 1000);
  };






  /**
   * @name expired
   * @todo To check if package expired
   * @return void function
   *
   **/
  $scope.expired = function(){
    var myPopup = $ionicPopup.show({
      template: '<div class="packageExpiredInner"><div class="selectOption" ng-click="closePopup()"><span>Not Now</span></div><div class="selectOption" ng-click="openBrowser()"><span>Renew Now</span></div></div>',
      subTitle: 'Oops! Your subscription has expired. Please renew your subscription.',
      cssClass: 'packageExpiredPopup',
      scope: $scope
    });

    myPopup.then(function(res) {

    });

    $timeout(function() {
      // myPopup.close(); //close the popup after 3 seconds for some reason
    }, 8000);
    $scope.closePopup = function() {

      myPopup.close();
    };
  };


  /**
   * @name openBrowser
   * @todo To open InAppBrowser
   * @return void function
   *
   **/



  $scope.openBrowser = function(){
    var user_id = $scope.userData.user_id;
    var options = {
      location: 'yes',
      clearcache: 'yes',
      toolbar: 'no'
    };
    $cordovaInAppBrowser.open('http://www.fitnessbasecamp.com/loginUser_admin/'+user_id+'/app', '_system', options)
      .then(function(event) {
        // console.log(event);
      })
      .catch(function(event) {
        // error
      });


    // $cordovaInAppBrowser.close();
  }


});
