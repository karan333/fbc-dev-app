/**
 * Purpose:
 *@This file is used to Show All News Feeds of a User
 *
 **/

angular.module('starter').controller('allNewsFeed', function(notification, $cordovaToast, $ionicPopup, $cordovaInAppBrowser, quote, $state, $rootScope,  $cordovaNetwork, $window, $scope, $cordovaSQLite, $stateParams, $http, $timeout, $ionicSideMenuDelegate, $ionicLoading, ionicMaterialMotion, ionicMaterialInk) {


$scope.morefeeds=false;
$scope.allReturnedNews=[];
     $scope.postlimit = 20;
      $scope.allNews=[];
      $scope.currentrecordsview = 0;
  $scope.packageExpired = {};
$scope.isOnline = [];
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();

        /**
         *
         * Listen for Online event
         *
         **/
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        /**
         *
         * Listen for Offline event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){

            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);





                      var userData = $window.localStorage['userData'];
                      $scope.userData = angular.fromJson(userData);
  var packageExpired = $window.localStorage['expired'];
  $scope.packageExpired = angular.fromJson(packageExpired);
                      // after getting data from sqlite getting user id and then all news feed service


                      var user_id = $scope.userData.user_id;
                      quote.getQuote(uuid, user_id).success(function (res) {
                                                                            // console.log(res);
                                                                            $scope.quote = res;
                                                                            });

  $scope.notificationFull = [];
  $scope.notification = [];
  $scope.$on('$ionicView.beforeEnter', function(){
    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner>'

    });
    notification.get(uuid, user_id).success(function (res) {

      var notifications = res;
      $window.localStorage['notifications']  = JSON.stringify(notifications);
      var localNoti = $window.localStorage['notifications'];
      $scope.notification = angular.fromJson(localNoti);
      $scope.notificationFull = $scope.notification.notification;
      if($scope.notificationFull.length > 0){
        if($scope.notificationFull[0].count == 0){
          $scope.green = "green";
        } else if($scope.notificationFull[0].count > 0){
          $scope.green = "red";
        }
      }

      $scope.changeColor = function(){
        $scope.green = "green";

        //;


      }
    });
  });
                      var link = baseUrl+'/dashboard/all_newsfeeds';
                          // console.log('userID---'+ user_id + '--UUID--' + uuid);
                          var formData = {uu_id : uuid, user_id : user_id}
                          var postData = 'myData='+JSON.stringify(formData);

                          $ionicLoading.show({
                          template: '<ion-spinner icon="ios"></ion-spinner>'

                          });
                          $http({
                            method : 'POST',
                            url : link,
                            data: postData,
                            headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                        }).success(function(res){

                        $scope.allReturnedNews = res.newsfeed;
                        res.newsfeed  =  angular.fromJson(res.newsfeed);

                        // console.log("Mani");
                        // console.log(res.newsfeed[0]);
                        for(var i=0; i< $scope.postlimit ; i++) {
                         $scope.allNews.push(res.newsfeed[i]);

                        }
                        $scope.currentrecordsview=$scope.postlimit;
                        $scope.morefeeds  = true;


                           $ionicLoading.hide();
                            // console.log(res);
                           // $scope.allNews = res.newsfeed;

                             $timeout(function() {
                                          ionicMaterialMotion.fadeSlideInRight({
                                              startVelocity: 1000
                                          });
                                      }, 700);

                                      // Set Ink
                                      ionicMaterialInk.displayEffect();


                      })


  /**
   * @name newsDetail
   * @todo To Get All News Details
   * @return void function
   *
   **/

$scope.newsDetail = function(index){

  var result = $scope.allNews[index].item_ID;

  if($scope.allNews[index].bit == "workout"){
    $state.go('app.workoutDetailMain', {cid: result});
    $state.go('app.workoutDetailMain', {cid: result});
  } else if($scope.allNews[index].bit == "exercise"){
    $state.go('app.exerciseDetail', {cid: result});
  } else if($scope.allNews[index].bit == "news") {

    var link = $scope.allNews[index].link;
    var options = {
      location: 'yes',
      clearcache: 'yes',
      toolbar: 'no'
    };
    $cordovaInAppBrowser.open(link, '_system', options)
      .then(function(event) {

      })
      .catch(function(event) {
        // error
      });


    // $cordovaInAppBrowser.close();

  }
}



///////////// For Loading More Data //////////////




  /**
   * @name loadmorefeeds
   * @todo To Load More Feeds
   * @return void function
   *
   **/


                   $scope.loadmorefeeds = function() {

                                        // console.log("In Scroll");
                                        var lim = $scope.currentrecordsview + $scope.postlimit;

                                        for(var i=$scope.currentrecordsview;i< lim;i++)
                                        {
                                          if(i < $scope.allReturnedNews.length)
                                          {
                                            $scope.allNews.push($scope.allReturnedNews[i]);
                                          }
                                          else
                                          {
                                            $scope.morefeeds=false;
                                            break;
                                          }
                                        }
                                        // console.log($scope.allNews);
                                      $scope.currentrecordsview=lim;
                                      $timeout(function() {
                                                                      ionicMaterialMotion.fadeSlideInRight({
                                                                          startVelocity: 3000
                                                                      });
                                                                  }, 1000);

                                                                  // Set Ink
                                                                  ionicMaterialInk.displayEffect();
                                        $scope.$broadcast('scroll.infiniteScrollComplete');
                                    };
                                    $scope.$on('$stateChangeSuccess', function() {
                                      $scope.loadmorefeeds();
                                    });

                    //////////////// End For Loading More data ////////////////

  /**
   * @name doRefresh
   * @todo To Refresh all data
   * @return void function
   *
   **/
                      $scope.doRefresh = function() {
                          var user_id = $scope.userData.user_id;
                         var link = baseUrl+'/dashboard/all_newsfeeds';
                             // console.log('userID---'+ user_id + '--UUID--' + uuid);
                             var formData = {uu_id : uuid, user_id : user_id}
                             var postData = 'myData='+JSON.stringify(formData);

                             $ionicLoading.show({
                             template: '<ion-spinner icon="ios"></ion-spinner>'

                             });
                             $http({
                               method : 'POST',
                               url : link,
                               data: postData,
                               headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                           }).success(function(res){
                              $ionicLoading.hide();
                               // console.log(res);
                               $scope.allNews = res.newsfeed;


                         }).finally(function() {
                             $scope.$broadcast('scroll.refreshComplete');
                              $timeout(function() {
                                  ionicMaterialMotion.fadeSlideInRight({
                                      startVelocity: 3000
                                  });
                              }, 700);

                              // Set Ink
                              ionicMaterialInk.displayEffect();
                         });

                      }



  /**
   * @name expired
   * @todo To Check if user's Subscription is expired or not
   * @return void function
   *
   **/

  $scope.expired = function(){
    var myPopup = $ionicPopup.show({
      template: '<div class="packageExpiredInner"><div class="selectOption" ng-click="closePopup()"><span>Not Now</span></div><div class="selectOption" ng-click="openBrowser()"><span>Renew Now</span></div></div>',
      subTitle: 'Oops! Your subscription has expired. Please renew your subscription.',
      cssClass: 'packageExpiredPopup',
      scope: $scope
    });

    myPopup.then(function(res) {

    });

    $timeout(function() {
      // myPopup.close(); //close the popup after 3 seconds for some reason
    }, 8000);
    $scope.closePopup = function() {

      myPopup.close();
    };
  };
  /**
   * @name openBrowser
   * @todo To open In app Browser
   * @return void function
   *
   **/
  $scope.openBrowser = function(){
    var user_id = $scope.userData.user_id;
    var options = {
      location: 'yes',
      clearcache: 'yes',
      toolbar: 'no'
    };
    $cordovaInAppBrowser.open('http://www.fitnessbasecamp.com/loginUser_admin/'+user_id+'/app', '_system', options)
      .then(function(event) {
        // console.log(event);
      })
      .catch(function(event) {
        // error
      });


    // $cordovaInAppBrowser.close();
  }

      // Set Motion



});
