/**
 * Purpose:
 *@This file is used to Show Main Dashboard of User
 *
 **/

angular.module('starter').controller('HomeCtrl', function($cordovaInAppBrowser, $ionicSlideBoxDelegate, notification, quote, $ionicModal, $cordovaScreenshot, $cordovaSocialSharing, $cordovaToast,$ionicPlatform, $cordovaNetwork, $rootScope, $cordovaPreferences, $ionicBackdrop, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading, $cordovaFileTransfer,$q) {

var mani =[];
      $scope.activity_distance=0;
      $scope.activity_duration=0;
      $scope.calories_burned=0;
      $scope.permissions=[];
      $scope.Ccalories = {};
      $scope.Cfats = {};
      $scope.Cproteins = {};
      $scope.level = {};
      $scope.points = {};
      $scope.countFriends = {};
      $scope.weight ={};
      $scope.waist ={};

      $scope.userData = {};
      $scope.packageExpired = {};
      $scope.userStatus = {};
      $scope.friendsList = {};
      $scope.newsFeed = {};
      $scope.userProgressPhotos = {};
      $scope.getFriendsActivity = {};
      $scope.my_graph = {};
      $scope.status = {};
      $scope.comment = {};
      $scope.status.postImage = {};
      $scope.postImage = [];

      $scope.siteUrl = siteUrl;
      $scope.TotalDownloaded = 0;
      $scope.statusImgs = [];
      $scope.status.commentImage = {};
      $scope.commentImage = [];
      $scope.edit_comment_images = [];
      $scope.commentImgs = [];
      $scope.isOnline = [];
      $scope.screenshots = [];
      $scope.stepCount = [];
      $scope.editcomment = {};
      $scope.showEdit = {};
      $scope.edit_upload_comment_images = [];
      $scope.items = [];
      $scope.notificationFull = [];
      $scope.edit_status = [];
      $scope.edit_status_images = [];
      $scope.edit_post_images = [];
      $scope.edit_upload_post_images = [];
      $scope.completed = '';
      $scope.userProfile = {};
      $scope.urlPreviewPosted = [];
      $scope.imageURI = null;
      $scope.newuser = {
        // heightf : 0,
        // heighti : 0,
        // weight : 0,
        // waist : 0,
        // chest : 0,
        // shoulder : 0,
        // arms : 0,
        // hips : 0,
        // thighs : 0,
        // calves : 0,
        // neck : 0
      };
      $scope.statWeight = '';
      $scope.statLength = '';
      $scope.statHeight = '';
      $scope.measurement = '';



      //for smileys

  var embed = {
    fontSmiley  :true,    // toggle converting ascii smileys into font smileys
    sanitizeHtml: true,   // toggle converting html code into text
    emoji       :true,    // toggle converting emojis short names into images
    link        :false,    // toggle converting urls into anchor tags
    linkTarget  :'cordova'  //_blank|_self|_parent|_top|framename|cordova
  };




       $scope.green = "";

  /**
   * @name inArray
   * @todo To Check if Array and then get the length of array
   * @return void function
   *
   **/

$scope.inArray = function (needle) {
haystack  = $scope.permissions;
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle){ return true;

        } ;
    }
    return "noPermission";
};



  /**
   * @name freshStats
   * @todo Get New user's Stats
   * @return void function
   *
   **/
  $scope.freshStats = function(heightf, heighti, weight, waist, chest, shoulder, arms, hips, thighs, calves, neck){

    // $window.localStorage['newUser'] = 1;
    var user_id = $scope.userData.user_id;

    var link = baseUrl+'/dashboard/newUserStat';

    var formData = {uu_id : uuid, user_id : user_id, heightf : heightf, heighti : heighti, weight : weight, chest : chest, shoulder : shoulder, arms : arms, hips : hips, thighs : thighs, calves : calves, neck : neck, waist : waist};
    // console.log(formData);

    var postData = 'myData='+JSON.stringify(formData);

    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner>'

    });
    $http({
      method : 'POST',
      url : link,
      data: postData,
      headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

    }).success(function(res){


      var alertPopup = $ionicPopup.alert({
        title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
        template: res.message,
        cssClass: 'loginErrorPopUp hideOk'
      });
      $timeout(function(){
          alertPopup.close();
      }, 3000);
      $scope.newUserModal.hide();
      $scope.newUserModal.remove();
      $ionicLoading.hide();



    }) .error(function(error){
      $ionicLoading.hide();

    })




  };
  /**
   * @name skipStat
   * @todo To Skip Stats for new users
   * @return void function
   *
   **/
  $scope.skipStat = function(){
    $scope.newUserModal.hide();
    $scope.newUserModal.remove();
    // $window.localStorage['newUser'] = 1;
  };




//END NEW USER STAT SECTION


      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();
        TTS.speak({
          text: '',
          locale: 'en-GB',
          rate: 1
        }, function () {
          // alert('success');
        }, function (reason) {
          // alert(reason);
        });


        // listen for Online event
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
             // $scope.syncLocData();
//            $cordovaToast.showLongBottom('You are Online !')

        })

        // listen for Offline event
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){

            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);


               var userData = $window.localStorage['userData'];
               $scope.userData = angular.fromJson(userData);
               // console.log($scope.userData);


             //  console.log("Omar User data consoling "+JSON.stringify($scope.userData));

                                // after getting data from sqlite getting user id and then running dashboard service
                                $scope.userImg = $scope.userData.image;
                                $scope.username = $scope.userData.user_fname;
                                $scope.Lusername = $scope.userData.user_lname;

                               // console.log("Userimg Source== "+$scope.userImg);

                                var user_id = $scope.userData.user_id;

                                //for quotes

                          if($scope.isOnline === true){
                            quote.getQuote(uuid, user_id).success(function (res) {

                              $scope.quote = res;
                              $window.localStorage['quotes']  = JSON.stringify($scope.quote);

                            });

                            notification.get(uuid, user_id).success(function (res) {

                              var notifications = res;
                              $window.localStorage['notifications']  = JSON.stringify(notifications);
                              var localNoti = $window.localStorage['notifications'];
                              $scope.notification = angular.fromJson(localNoti);
                              $scope.notificationFull = $scope.notification.notification;
                              if($scope.notificationFull.length > 0){
                                if($scope.notificationFull[0].count == 0){
                                  $scope.green = "green";
                                } else if($scope.notificationFull[0].count > 0){
                                  $scope.green = "red";
                                }
                              }

                              $scope.changeColor = function(){
                                $scope.green = "green";

                                //;


                              }
                            });
                          }




                              //userprofile service

                                var link = baseUrl+'/dashboard/userprofile';
                                var formData = {uu_id : uuid, user_id : user_id};
                                var postData = 'myData='+JSON.stringify(formData);


                                 if ($scope.isOnline == true){
                                 $ionicLoading.show({
                                     template: '<ion-spinner icon="ios"></ion-spinner>'

                                 });
                                   $http({
                                      method : 'POST',
                                      url : link,
                                      data: postData,
                                      headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                              }).success(function(res){
                             // alert("in success");
                                $scope.checked = true;
                                $ionicLoading.hide();

                               // console.log("consoling user status "+res.dashboard_data.userStatus);


                                 $window.localStorage['Permissions']  = JSON.stringify(res.dashboard_data.permissions);

                                $window.localStorage['expired']  = JSON.stringify(res.dashboard_data.IS_Expired);
                                $scope.packageExpired = res.dashboard_data.IS_Expired;

                                //userProfile data in local storage
                                $window.localStorage['userProfile']  = JSON.stringify(res.dashboard_data);







                                //is new user

                                 $scope.newUserBit = res.dashboard_data.isNewUser;

                                 $scope.measurement = res.dashboard_data.measurement;

                                 $window.localStorage['measurement'] = $scope.measurement;
                                 // console.log($window.localStorage['measurement']);

                                 if($scope.measurement === 'Metric'){
                                   // console.log('in metric');
                                   $scope.statWeight = 'kg';
                                   $scope.statLength = 'cm';
                                   $scope.statHeight = 'm';
                                 } else{
                                   $scope.statWeight = 'lbs';
                                   $scope.statLength = 'inch';
                                   $scope.statHeight = 'ft';
                                 }


                                // user activity json object
                                $scope.userStatus = res.dashboard_data.userStatus;
                                $scope.activityImage = siteUrl + res.dashboard_data.userStatus.image;
                                $scope.postingImage = res.dashboard_data.userData[0].image;


                                //adding fresh userData in local storage
                                     var userData = JSON.stringify(res.dashboard_data.userData[0]);
                                     $window.localStorage['userData'] = userData;
                                 $rootScope.mainImage = res.dashboard_data.userData[0].image;

                                //  friends activity json object
                                $scope.getFriendsActivity = res.dashboard_data.getFriendsActivity;




                                //for url preview

                                 $scope.content = $scope.getFriendsActivity.status;
                                 $scope.urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
                                 $scope.url = $scope.content.match($scope.urlRegex);
                             //    console.log($scope.url);
                                 $scope.getFriendsActivity.status = $scope.getFriendsActivity.status.replace($scope.url, '');

                                 $scope.urlPreviewPosted = res.dashboard_data.getFriendsActivity.status_url;
                                 $scope.urlCommentPreviewPosted = res.dashboard_data.getFriendsActivity.status_url;



                                     for(var x in $scope.getFriendsActivity.comments){
                                       console.log($scope.getFriendsActivity.comments[x].comment);
                                       $scope.content = $scope.getFriendsActivity.comments[x].comment;
                                       $scope.urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
                                       $scope.url = $scope.content.match($scope.urlRegex);
                                       // console.log($scope.url);
                                       $scope.getFriendsActivity.comments[x].comment = $scope.getFriendsActivity.comments[x].comment.replace($scope.url, '');
                                     }






                                // friends list json object

                                $scope.friendsList = res.dashboard_data.myFriends;

                                // news feed json object

                                $scope.newsFeed = res.dashboard_data.newsFeed;

                                //progress pictures json object

                                $scope.userProgressPhotos = res.dashboard_data.userProgressPhotos;

                                //levels

                                $scope.level = res.dashboard_data.level;

                                //points

                                $scope.points = res.dashboard_data.points;

                                //friends

                                $scope.countFriends = res.dashboard_data.countFriends;

                                //weight

                                $scope.weight = res.dashboard_data.weight;

                                //waist

                                $scope.waist = res.dashboard_data.waist;

                                //calorie graph json object

                                $scope.calories_graph = parseInt(res.dashboard_data.calories_graph);
                                $scope.Ccalories = res.dashboard_data.calories_graph.calories;
                                $scope.Cfats = res.dashboard_data.calories_graph.fats;
                                $scope.Cproteins = res.dashboard_data.calories_graph.proteins;

                              // charts

                                $scope.chartConfig = {
                                        options: {
                                            chart: {
                                                type: 'line',
                                                zoomType: 'x',
                                                backgroundColor : '#fff',
                                                height: '200'
                                            }
                                        },
                                        credits: {
                                          enabled : false
                                        },
                                        series: [{
                                            name: 'Calories',
                                            data: [$scope.Ccalories]
                                        },{
                                            name: 'Fats',
                                            data: [$scope.Cfats]
                                        },{
                                            name: 'Proteins',
                                            data: [$scope.Cproteins]
                                        }],
                                        xAxis: {
                                        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                                        },
                                        title: {
                                            text: ''
                                        },
                                        yAxis: {
                                          plotLines: [{
                                            value: 0,
                                            width: 1,
                                            color: '#808080'
                                          }]
                                        },
                                        loading: false
                                    }

                                    //my_graph data

                                    $scope.my_graph = res.dashboard_data.my_graph;
                                    $scope.my_graph.x_axis = res.dashboard_data.my_graph.x_axis;
                                    $scope.my_graph.y_axis = res.dashboard_data.my_graph.y_axis;

                                    $scope.chartConfig2 = {
                                      options: {
                                          chart: {
                                              type: 'line',
                                              zoomType: 'x',
                                              backgroundColor : '#fff',
                                              height : '200'
                                          }
                                      },
                                      credits: {
                                        enabled : false
                                      },
                                      series: $scope.my_graph.y_axis,
                                      xAxis: {
                                      categories: $scope.my_graph.x_axis
                                      },
                                      title: {
                                          text: ''
                                      },
                                      yAxis: {
                                        plotLines: [{
                                          value: 0,
                                          width: 1,
                                          color: '#808080'
                                        }]
                                      },
                                      loading: false
                                  };

                               $scope.permissions = res.dashboard_data.permissions;

                               // console.log($scope.getFriendsActivity.status.length);
                                     if($scope.getFriendsActivity !== ''){
                                       if($scope.getFriendsActivity.status.length > 0){
                                         // if($scope.userStatus.status.match("Exercises In This Workout")) {
                                         //   $scope.completed = 'WorkoutPost';
                                         // } else {
                                         //   $scope.completed = '';
                                         // }

                                         if($scope.getFriendsActivity.status.match("Exercises In This Workout")) {
                                           $scope.completedF = 'WorkoutPost';
                                         } else {
                                           $scope.completedF = 'simplePost';
                                         }
                                       }
                                     }

                                     //first time new user signin Popup


                                     $ionicModal.fromTemplateUrl('templates/newUser.html', {
                                       scope: $scope
                                     }).then(function(modal) {

                                       $scope.newUserModal = modal;

                                       // var newUser = $window.localStorage['newUser'];
                                       // console.log("newUser : "+newUser);
                                       if($scope.newUserBit === 1){
                                         modal.show();
                                       };


                                     });





                                   }) .error(function(error){
                                 $ionicLoading.hide();

                              }); //dashboard service sucess end
                                }else{
                                   $scope.checked = true;
                                   $ionicLoading.hide();
                                 $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet');


                                   var quotes = $window.localStorage['quotes'];
                                   $scope.quote = angular.fromJson(quotes);


                                   // var notifications = $window.localStorage['notifications'];
                                   // $scope.notification = angular.fromJson(notifications);
                                   // $scope.notificationFull = $scope.notification.notification;

                                   $scope.userProfile = angular.fromJson(userProfile);

                                   var userProfile = $window.localStorage['userProfile'];
                                   $scope.userProfile = angular.fromJson(userProfile);

                                   $scope.userStatus = $scope.userProfile.userStatus;

                                   //  friends activity json object
                                   $scope.getFriendsActivity = $scope.userProfile.getFriendsActivity;
                                   // console.log($scope.getFriendsActivity.status);
                                   // console.log($scope.userStatus.status);
                                   //for url preview

                                   // $scope.content = $scope.getFriendsActivity.status;
                                   // $scope.urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
                                   // $scope.url = $scope.content.match($scope.urlRegex);
                                   // // console.log($scope.url);
                                   // $scope.getFriendsActivity.status = $scope.getFriendsActivity.status.replace($scope.url, '');
                                   // $scope.urlPreviewPosted = res.dashboard_data.getFriendsActivity.status_url;

                                   for(var x in $scope.getFriendsActivity.comments){
                                     console.log($scope.getFriendsActivity.comments[x].comment);
                                     $scope.content = $scope.getFriendsActivity.comments[x].comment;
                                     $scope.urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
                                     $scope.url = $scope.content.match($scope.urlRegex);
                                     // console.log($scope.url);
                                     $scope.getFriendsActivity.comments[x].comment = $scope.getFriendsActivity.comments[x].comment.replace($scope.url, '');
                                   }
                                   if($scope.userStatus.status.match("completed")) {
                                     $scope.completed = 'WorkoutPost';
                                   } else {
                                     $scope.completed = '';
                                   }

                                   // friends list json object

                                   $scope.friendsList = $scope.userProfile.myFriends;

                                   // news feed json object

                                   $scope.newsFeed = $scope.userProfile.newsFeed;

                                   //progress pictures json object

                                   $scope.userProgressPhotos = $scope.userProfile.userProgressPhotos;

                                   //levels

                                   $scope.level = $scope.userProfile.level;

                                   //points

                                   $scope.points = $scope.userProfile.points;

                                   //friends

                                   $scope.countFriends = $scope.userProfile.countFriends;

                                   //weight

                                   $scope.weight = $scope.userProfile.weight;

                                   //waist

                                   $scope.waist = $scope.userProfile.waist;

                                   //calorie graph json object

                                   $scope.calories_graph = parseInt($scope.userProfile.calories_graph);
                                   $scope.Ccalories = $scope.userProfile.calories_graph.calories;
                                   $scope.Cfats = $scope.userProfile.calories_graph.fats;
                                   $scope.Cproteins = $scope.userProfile.calories_graph.proteins;

                                   // charts

                                   $scope.chartConfig = {
                                     options: {
                                       chart: {
                                         type: 'line',
                                         zoomType: 'x',
                                         backgroundColor : '#fff',
                                         height: '200'
                                       }
                                     },
                                     credits: {
                                       enabled : false
                                     },
                                     series: [{
                                       name: 'Calories',
                                       data: [$scope.Ccalories]
                                     },{
                                       name: 'Fats',
                                       data: [$scope.Cfats]
                                     },{
                                       name: 'Proteins',
                                       data: [$scope.Cproteins]
                                     }],
                                     xAxis: {
                                       categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                                     },
                                     title: {
                                       text: ''
                                     },
                                     yAxis: {
                                       plotLines: [{
                                         value: 0,
                                         width: 1,
                                         color: '#808080'
                                       }]
                                     },
                                     loading: false
                                   }

                                   //my_graph data

                                   $scope.my_graph = $scope.userProfile.my_graph;
                                   $scope.my_graph.x_axis = $scope.userProfile.my_graph.x_axis;
                                   $scope.my_graph.y_axis = $scope.userProfile.my_graph.y_axis;

                                   $scope.chartConfig2 = {
                                     options: {
                                       chart: {
                                         type: 'line',
                                         zoomType: 'x',
                                         backgroundColor : '#fff',
                                         height : '200'
                                       }
                                     },
                                     credits: {
                                       enabled : false
                                     },
                                     series: $scope.my_graph.y_axis,
                                     xAxis: {
                                       categories: $scope.my_graph.x_axis
                                     },
                                     title: {
                                       text: ''
                                     },
                                     yAxis: {
                                       plotLines: [{
                                         value: 0,
                                         width: 1,
                                         color: '#808080'
                                       }]
                                     },
                                     loading: false
                                   };

                                }




//news feed detail

  var packageExpired = $window.localStorage['expired'];
  $scope.packageExpired = angular.fromJson(packageExpired);

  /**
   * @name newsDetail
   * @todo To View All News Details
   * @return void function
   *
   **/


  $scope.newsDetail = function(index){

  var result = $scope.newsFeed[index].item_ID;
  if($scope.newsFeed[index].bit == "workout"){
    $state.go('app.workoutDetailMain', {cid: result});
  } else if($scope.newsFeed[index].bit == "exercise") {
    $state.go('app.exerciseDetail', {cid: result});
  } else if($scope.newsFeed[index].bit == "news") {

      var link = $scope.newsFeed[index].link;
      var options = {
        location: 'yes',
        clearcache: 'yes',
        toolbar: 'no'
      };
      $cordovaInAppBrowser.open(link, '_system', options)
        .then(function(event) {

        })
        .catch(function(event) {
          // error
        });


      // $cordovaInAppBrowser.close();

  }
};

//post status url preview

  $scope.url = {};
  $scope.urlPreview = {};
  $scope.editurlPreview = {};
  $scope.urlPreviewClass = '';
  $scope.showUrlPreview = false;
  $scope.editurlPreviewClass = '';
  $scope.editshowUrlPreview = false;


  $scope.postEntering = function (value, edit, isComment) {
    // console.log(value);
    // console.log(edit);

    var edit = edit;
    var isComment = isComment;

    $scope.urlPreview = {};
    $scope.editurlPreview = {};
    $scope.urlPreviewClass = '';
    $scope.urlCommentPreviewClass = '';
    $scope.showUrlPreview = false;
    $scope.showCommentUrlPreview = false;

    $scope.editurlPreviewClass = '';
    $scope.editCommenturlPreviewClass = '';
    $scope.editshowUrlPreview = false;
    $scope.editshowCommentUrlPreview = false;

    $scope.content = value;

    // console.log($scope.content);
    if($scope.content !== undefined){
      // console.log('not undefined');

      $scope.urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
      $scope.url = $scope.content.match($scope.urlRegex);
      // console.log($scope.url);

      if($scope.url !== null){
        // console.log('not null');
        if($scope.url.length > 0){




          var link = baseUrl+'/dashboard/url_preview';

          var formData = {url : $scope.url};
          var postData = 'myData='+JSON.stringify(formData);

          /*  var formData = {url : $scope.url};
           var postData = JSON.stringify(formData);*/
          $http({
            method : 'POST',
            url : link,
            data: postData,
            headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

          }).success(function(res){
            // console.log(res);


            if(edit !== 1){
              $scope.urlPreview = res;


              $scope.titlecontent = $scope.urlPreview.title;
              $scope.titleRegex = /[?!\w]/gi;
              $scope.title = $scope.titlecontent.match($scope.titleRegex);
              // console.log($scope.url);
              $scope.urlPreview.title = $scope.urlPreview.title.replace($scope.title, '');


              $scope.descriptioncontent = $scope.urlPreview.description;
              $scope.descriptionRegex = /[?!\w]/gi;
              $scope.description = $scope.descriptioncontent.match($scope.descriptionRegex);
              $scope.urlPreview.description = $scope.urlPreview.description.replace($scope.description, '');

              // ([^a-z0-9])\w+
              console.log($scope.urlPreview.title);
              console.log($scope.urlPreview.description);
              console.log($scope.urlPreview);



              if($scope.urlPreview.site_url !== ''){
                if(isComment === true){
                  $scope.showCommentUrlPreview = true;
                  $scope.urlCommentPreviewClass = 'urlAddTweak';
                } else {
                  $scope.showUrlPreview = true;
                  $scope.urlPreviewClass = 'urlAddTweak';
                }

              }
            } else if(edit === 1){
              $scope.editurlPreview = res;
              if($scope.editurlPreview.site_url !== ''){

                if(isComment === true){
                  $scope.editshowCommentUrlPreview = true;
                  $scope.editCommenturlPreviewClass = 'urlAddTweak';
                }else {
                  $scope.editshowUrlPreview = true;
                  $scope.editurlPreviewClass = 'urlAddTweak';
                }

              }
            }
  }) .error(function(error){
            // console.log(error);

            if(edit !== 1){
              $scope.urlPreview = {};
              $scope.urlPreviewClass = '';
              $scope.showUrlPreview = false;
              $scope.showCommentUrlPreview = false;

            } else if(edit === 1){
              $scope.editurlPreview = {};
              $scope.editurlPreviewClass = '';
              $scope.editshowUrlPreview = false;
              $scope.editshowCommentUrlPreview = false;
            }


          })


        }
      }
    }


    return false;
  };

  $scope.clicked = function(link){

    var options = {
      location: 'yes',
      clearcache: 'yes',
      toolbar: 'no'
    };
    $cordovaInAppBrowser.open(link, '_system', options)
      .then(function(event) {
        // console.log('its gone to url')

      })
      .catch(function(event) {
        // error
      });
  }


  /**
   * @name viewAllNews
   * @todo To View All News
   * @return void function
   *
   **/
                                     $scope.viewAllNews = function(){
                                        if ($scope.isOnline == true){
                                          $state.go('app.allNewsFeed');
                                            }else{
                                             $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
                                            }

                                        };


  /**
   * @name viewAllPP
   * @todo To View All Progress Photos
   * @return void function
   *
   **/
                                        $scope.viewAllPP = function(){
                                        if ($scope.isOnline == true){
                                          $state.go('app.allProgressPhotos');
                                            }else{
                                             $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
                                            }

                                        };
  /**
   * @name viewAllFriends
   * @todo To View All Friends
   * @return void function
   *
   **/

                                        $scope.viewAllFriends = function(){
                                        if ($scope.isOnline == true){
                                        $state.go('app.friends.myFriends');
                                        }else{
                                         $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
                                        }
                                        };

                                        //for user post like dislike

                                        $scope.like = function(id,viewTypx){
                                        if ($scope.isOnline == true){
                                        var status_id = id;

                                        var link = baseUrl+'/dashboard/like_user_status';
                                        //console.log('userID---'+ user_id + '--UUID--' + uuid + '--statusID' + status_id);
                                        var formData = {uu_id : uuid, user_id : user_id, status_id : status_id}
                                        var postData = 'myData='+JSON.stringify(formData);

                                        // $ionicLoading.show({
                                        // template: '<ion-spinner icon="ios"></ion-spinner>'
                                        //
                                        // });
                                        $http({
                                          method : 'POST',
                                          url : link,
                                          data: postData,
                                          headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                      }).success(function(res){
                                      $scope.postLiked = {};
                                          // $ionicLoading.hide();
                                          if($scope.userStatus.status.match("completed")) {
                                            $scope.completed = 'WorkoutPost';
                                          } else {
                                            $scope.completed = '';
                                          }

                                          if(res.code == '200'){

                                            if(viewTypx == "MyActivity")
                                            {
                                             $scope.postLiked = 'liked';

                                             $scope.userStatus.likes = res.likes;

                                            if($scope.userStatus.id == $scope.getFriendsActivity.id)
                                            {
                                              $scope.postLikedFrnd = 'liked';
                                              $scope.getFriendsActivity.likes = res.likes;
                                              //$scope.getFriendsActivity.comments = res.comments.comments;
                                            }

                                            }
                                            else
                                            {
                                              $scope.postLikedFrnd = 'liked';
                                              $scope.getFriendsActivity.likes = res.likes;
                                              if($scope.userStatus.id == $scope.getFriendsActivity.id)
                                              {
                                              $scope.postLiked = 'liked';
                                              $scope.userStatus.likes = res.likes;
                                              }


                                            }
                                          } else if(res.code == '402'){


                                             if(viewTypx == "MyActivity")
                                              {
                                                $scope.postLiked = 'Disliked';
                                                $scope.userStatus.likes = res.likes;

                                              if($scope.userStatus.id == $scope.getFriendsActivity.id)
                                              {
                                                $scope.postLikedFrnd = 'Disliked';
                                                $scope.getFriendsActivity.likes = res.likes;
                                                //$scope.getFriendsActivity.comments = res.comments.comments;
                                              }

                                              }
                                              else
                                              {
                                                  $scope.postLikedFrnd = 'Disliked';
                                                $scope.getFriendsActivity.likes = res.likes;
                                                if($scope.userStatus.id == $scope.getFriendsActivity.id)
                                                {
                                                $scope.postLiked = 'Disliked';
                                                $scope.userStatus.likes = res.likes;
                                                }


                                              }

                                          } else{

                                          }

                                    })


                                        }else{
                                        $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
                                        }

                                        };


  /**
   * @name removePost
   * @todo To Delete Post
   * @return void function
   *
   **/
                                         $scope.removePost = function(id){
                                          var status_id = id;
                                          if ($scope.isOnline == true){
                                            $scope.showConfirm = function () {
                                              var confirmPopup = $ionicPopup.confirm({
                                                title: 'Warning',
                                                template: 'Are you Sure You Want To Delete your Post ?',
                                                cssClass: 'loginErrorPopUp logoutCpop'

                                              });

                                              confirmPopup.then(function (res) {
                                                if (res) {
                                                  var link = baseUrl+'/dashboard/remove_status';
                                                  //console.log('userID---'+ user_id + '--UUID--' + uuid + '--statusID' + status_id);
                                                  var formData = {uu_id : uuid, user_id : user_id, status_id : status_id}
                                                  var postData = 'myData='+JSON.stringify(formData);

                                                  $ionicLoading.show({
                                                    template: '<ion-spinner icon="ios"></ion-spinner>'

                                                  });
                                                  $http({
                                                    method : 'POST',
                                                    url : link,
                                                    data: postData,
                                                    headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                                  }).success(function(res){
                                                    // $scope.urlPreviewPosted = [];

                                                    $ionicLoading.hide();

                                                    $scope.userStatus = res.new_status;
                                                    $scope.getFriendsActivity = res.friends_activity;
                                                    $scope.urlPreview = {};
                                                    $scope.urlPreviewClass = '';
                                                    $scope.showUrlPreview = false;


                                                    $scope.content = $scope.getFriendsActivity.status;
                                                    $scope.urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
                                                    $scope.url = $scope.content.match($scope.urlRegex);
                                                    // console.log($scope.url);
                                                    $scope.getFriendsActivity.status = $scope.getFriendsActivity.status.replace($scope.url, '');
                                                    $scope.urlPreviewPosted = res.friends_activity.status_url;

                                                    if($scope.userStatus.status.match("completed")) {
                                                      $scope.completed = 'WorkoutPost';
                                                      $state.reload();
                                                    } else {
                                                      $scope.completed = '';
                                                    }
                                                  })
                                                } else {

                                                }
                                              });
                                            };
                                            $scope.showConfirm();

                                              }else{
                                               $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
                                              }


                                      //remove


                                          };

                                     //user comments on post
  /**
   * @name postComment
   * @todo To Post Comment
   * @return void function
   *
   **/
                                    $scope.postComment = function(id,viewTyp){
                                    if ($scope.isOnline == true){
                                    var comment_text = $scope.comment.text;
                                    var status_id = id;
                                    var link = baseUrl+'/dashboard/post_comment';
                                    //console.log('userID---'+ user_id + '--UUID--' + uuid + '--statusText' + comment_text + 'status-ID---' + status_id);
                                     var formData = {uu_id : uuid, user_id : user_id, comment_text : comment_text, status_id : status_id, comment_images : $scope.commentImgs, status_url : $scope.urlPreview}
                                    var postData = 'myData='+JSON.stringify(formData);

                                    $ionicLoading.show({
                                    template: '<ion-spinner icon="ios"></ion-spinner>'

                                    });
                                    $http({
                                      method : 'POST',
                                      url : link,
                                      data: postData,
                                      headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                  }).success(function(res){


                                    if(res.status === false){
                                      $ionicLoading.hide();
                                      return false
                                    }

                                      $scope.urlPreview = {};
                                      $scope.urlPreviewClass = '';
                                      $scope.showUrlPreview = false;
                                      $scope.showCommentUrlPreview = false;
                                      $scope.urlCommentPreviewClass = '';


                                  $scope.comment.text = '';
                                     $ionicLoading.hide();
                                      if($scope.userStatus.status.match("completed")) {
                                        $scope.completed = 'WorkoutPost';
                                      } else {
                                        $scope.completed = '';
                                      }

                                      if(viewTyp == "MyActivity")
                                      {
                               //         console.log("Status ID" + $scope.userStatus.id + " User Status ID" + $scope.getFriendsActivity.id);
                                        $scope.userStatus.comments = res.comments.comments;

                                      if($scope.userStatus.id == $scope.getFriendsActivity.id)
                                      {
                                        $scope.getFriendsActivity.comments = res.comments.comments;
                                      }

                                      }
                                      else
                                      {

                                       $scope.getFriendsActivity.comments = res.comments.comments;
                                      if($scope.userStatus.id == $scope.getFriendsActivity.id)
                                      {
                                        $scope.userStatus.comments = res.comments.comments;
                                      }


                                      }


                                      for(var x in $scope.getFriendsActivity.comments){
                                        console.log($scope.getFriendsActivity.comments[x].comment);
                                        $scope.content = $scope.getFriendsActivity.comments[x].comment;
                                        $scope.urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
                                        $scope.url = $scope.content.match($scope.urlRegex);
                                        // console.log($scope.url);
                                        $scope.getFriendsActivity.comments[x].comment = $scope.getFriendsActivity.comments[x].comment.replace($scope.url, '');
                                      }




                                      $scope.commentImage = [];
                                      $scope.edit_comment_images = [];
                                      $scope.commentImgs = [];




                                      // $scope.urlCommentPreviewPosted = res.comments.comments[0].comment_status_url;

                                })
                                        }else{
                                        $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
                                        }


                                    }

  /**
   * @name editComment
   * @todo To Edit Comment
   * @return void function
   *
   **/
                                    //edit comment
                                   $scope.editComment = function(id,status_id, text){
                                    if ($scope.isOnline == true){
                                    var comment_text = text;
                                    var status_id = status_id;
                                    var comment_id = id;
                                    var link = baseUrl+'/dashboard/edit_comment';
                                    console.log($scope.editurlPreview);
                                    //console.log('userID---'+ user_id + '--UUID--' + uuid + '--statusText' + comment_text + 'status-ID---' + status_id);
                                     var formData = {uu_id : uuid, user_id : user_id, comment_text : comment_text, status_id : status_id, comment_id : comment_id, comment_images : $scope.edit_upload_comment_images, status_url : $scope.editurlPreview}
                                    var postData = 'myData='+JSON.stringify(formData);

                                    $ionicLoading.show({
                                    template: '<ion-spinner icon="ios"></ion-spinner>'

                                    });
                                    $http({
                                      method : 'POST',
                                      url : link,
                                      data: postData,
                                      headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                  }).success(function(res){


                                      $scope.urlPreview = {};
                                      $scope.urlPreviewClass = '';
                                      $scope.showUrlPreview = false;
                                      $scope.showCommentUrlPreview = false;
                                      $scope.urlCommentPreviewClass = '';



                                  $scope.comment.text = '';
                                     $ionicLoading.hide();
                                      if($scope.userStatus.status.match("completed")) {
                                        $scope.completed = 'WorkoutPost';
                                      } else {
                                        $scope.completed = '';
                                      }
                                      $scope.getFriendsActivity.comments = res.status_comments.comments;
                                      for(var x in $scope.getFriendsActivity.comments){
                                        console.log($scope.getFriendsActivity.comments[x].comment);
                                        $scope.content = $scope.getFriendsActivity.comments[x].comment;
                                        $scope.urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
                                        $scope.url = $scope.content.match($scope.urlRegex);
                                        // console.log($scope.url);
                                        $scope.getFriendsActivity.comments[x].comment = $scope.getFriendsActivity.comments[x].comment.replace($scope.url, '');
                                      }




                                      $scope.commentImage = [];
                                      $scope.edit_comment_images = [];
                                      $scope.commentImgs = [];

                                      $scope.userStatus.comments = res.status_comments.comments;


                                })
                                        }else{
                                        $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
                                        }


                                    };
  /**
   * @name postStatus
   * @todo To Post Status
   * @return void function
   *
   **/
                                     // user status post
                                    $scope.postStatus = function(){


                                    if ($scope.isOnline == true){
                                      var status_text = $scope.status.text;

                                        var link = baseUrl+'/dashboard/post_status';
                                       // console.log('userID---'+ user_id + '--UUID--' + uuid + '--statusText' + status_text);
                                        var formData = {uu_id : uuid, user_id : user_id, status_text : status_text,status_images:$scope.statusImgs,status_url : $scope.urlPreview};
                                        var postData = 'myData='+JSON.stringify(formData);

                                        $ionicLoading.show({
                                        template: '<ion-spinner icon="ios"></ion-spinner>'

                                        });
                                        $http({
                                          method : 'POST',
                                          url : link,
                                          data: postData,
                                          headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                      }).success(function(res){
                                         $ionicLoading.hide();
                                         $scope.statusImgs = [];
                                         $scope.postImage = [];
                                          $scope.urlPreview = {};
                                          $scope.urlPreviewClass = '';
                                          $scope.showUrlPreview = false;



                                          $scope.userStatus = res.userstatus;
                                          $scope.getFriendsActivity = res.userstatus;
                                          $scope.content = $scope.getFriendsActivity.status;
                                          $scope.urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
                                          $scope.url = $scope.content.match($scope.urlRegex);
                                          // console.log($scope.url);
                                          $scope.getFriendsActivity.status = $scope.getFriendsActivity.status.replace($scope.url, '');
                                          $scope.urlPreviewPosted = res.userstatus.status_url;

                                          $scope.status.text="";
                                          if($scope.userStatus.status.match("completed")) {
                                            $scope.completed = 'WorkoutPost';
                                            $state.reload();
                                          } else {
                                            $scope.completed = '';
                                          }


                                    })
                                        }else{
                                         $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
                                        }


                                  }


  /**
   * @name editPost
   * @todo To Edit Post
   * @return void function
   *
   **/
                                  //user edit post
  $scope.editPost = function(id,text,image){
    if ($scope.isOnline == true){
      var status_id = id;
      var status_text = text;
      var status_images = image;

      var link = baseUrl+'/dashboard/update_post_status';
      var formData = {uu_id : uuid, user_id : user_id, status_text : status_text, status_id : status_id, status_images : $scope.edit_upload_post_images, status_url : $scope.editurlPreview }
      var postData = 'myData='+JSON.stringify(formData);

      $ionicLoading.show({
        template: '<ion-spinner icon="ios"></ion-spinner>'

      });
      $http({
        method : 'POST',
        url : link,
        data: postData,
        headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

      }).success(function(res){
        $scope.comment.text = '';
        $ionicLoading.hide();


        $scope.edit_status_images = [];
        $scope.edit_post_images = [];
        $scope.edit_upload_post_images = [];

        // user activity json object
        $scope.userStatus = res.userstatus;

        //  friends activity json object
        $scope.getFriendsActivity = res.getFriendsActivity;

        $scope.content = $scope.getFriendsActivity.status;
        $scope.urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        $scope.url = $scope.content.match($scope.urlRegex);
        // console.log($scope.url);
        $scope.getFriendsActivity.status = $scope.getFriendsActivity.status.replace($scope.url, '');
     //   console.log()
        $scope.urlPreviewPosted = res.getFriendsActivity.status_url;





        if($scope.userStatus.status.match("completed")) {
          $scope.completed = 'WorkoutPost';
          $state.reload();
        } else {
          $scope.completed = '';
        }



      })
    }else{
      $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
    }


  };

  /**
   * @name Npermission
   * @todo To Check if User have Permissions or not according to his package
   * @return void function
   *
   **/

                                  //user image Upload
                                  $scope.Npermission = function(is_perm)
                                  {

                                      if(is_perm != true)
                                       {

                                          var alertPopup = $ionicPopup.alert({
                                            title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                            template: "You don't have permission to do this.Please upgrade your package.",
                                            cssClass: 'loginErrorPopUp'
                                          });
                                       }

                                  }
  /**
   * @name selectUploadMethod
   * @todo To Select Upload Method
   * @return void function
   *
   **/

  $scope.selectUploadMethod = function(){

    if($scope.inArray('Upload Photos To Activity Feed')==true)
    {

      var myPopup = $ionicPopup.show({
        template: '<div class="uploadMethodSack"><ul><li><a class="ink" ng-click="takePicture();"><span class="ion-camera"></span><span>Upload from Camera</span></a></li><li><a class=ink"" ng-click="selectPicture();"><span class="ion-images"></span><span>Upload from Gallery</span></a></li></ul></div>',
        title: 'Choose Upload Method',
        subTitle: 'Select from Camera or Gallery',
        scope: $scope
      });

      myPopup.then(function(res) {

      });

      $timeout(function() {
        myPopup.close(); //close the popup after 3 seconds for some reason
      }, 8000);
    }
    else
    {

      var alertPopup = $ionicPopup.alert({
        title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
        template: "You don't have permission to do this.Please upgrade your package.",
        cssClass: 'loginErrorPopUp'
      });
    }
  }
  /**
   * @name takePicture
   * @todo To Take Picture
   * @return void function
   *
   **/
  $scope.takePicture = function(){
    document.addEventListener("deviceready", function () {

      var options = {

        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.CAMERA,
        allowEdit: true
      };

      $cordovaCamera.getPicture(options).then(function(imageURI) {

          $scope.uploadPicturex(imageURI);
          window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {

            $scope.ftLoad = true;
            //                                var image = document.getElementById('myImage');
            //                                image.src = fileEntry.nativeURL;
          });
          $ionicLoading.show({template: 'Uploading Picture', duration:500});
        },
        function(err){
          $ionicLoading.show({template: 'Errore di caricamento...', duration:500});
        })

    }, false);
  }

  /**
   * @name selectPicture
   * @todo To Select Picture
   * @return void function
   *
   **/

  $scope.selectPicture = function() {
    document.addEventListener("deviceready", function () {
      var options = {
        quality: 50,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 250,
        targetHeight: 250,
        allowEdit: true
      };

      $cordovaCamera.getPicture(options).then(
        function(imageURI) {
          $scope.uploadPicturex(imageURI);

          window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {
            // $scope.status.postImage = fileEntry.nativeURL;
            $scope.ftLoad = true;
            //				var image = document.getElementById('myImage');
            //				image.src = fileEntry.nativeURL;
          });
          $ionicLoading.show({template: 'Uploading Picture...', duration:500});
        },
        function(err){
          $ionicLoading.show({template: 'Errore di caricamento...', duration:500});
        })
    }, false);
  };





  /**
   * @name removeStatusIMG
   * @todo To Delete Status Image
   * @return void function
   *
   **/                                   //for refresh
  $scope.removeStatusIMG = function(index)
  {

    $scope.statusImgs.splice(index, 1);
    $scope.postImage.splice(index, 1);


  };
  /**
   * @name uploadPicturex
   * @todo To Upload Pictures
   * @return void function
   *
   **/
  $scope.uploadPicturex = function(imageURI) {



    var options = {

      fileKey: "picture",

      filename: "p1.png",

      chunkedMode: false,

      mimeType: "image/png",
      trustAllHosts:true,
      headers:{Connection:"close"}

    };
    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner><p>Uploading Image!</p>'

    });
    $cordovaFileTransfer.upload(encodeURI(baseUrl+"/dashboard/uploadStatusImge"), imageURI, options)

      .then(function(result) {

        $scope.postImage.push(imageURI);
        var  resk  = angular.fromJson(result.response);

        $scope.statusImgs.push(resk.image_name) ;


        $ionicLoading.hide();


      }, function(err) {


        $ionicLoading.hide();
        $ionicLoading.show({template: 'An error occured while uploading your picture.<br>Please try again.', duration:500});
        var alertPopup = $ionicPopup.alert({
          title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
          template: err,
          cssClass: 'loginErrorPopUp'
        });

      }, function (progress) {

      });



  };
  /**
   * @name doRefresh
   * @todo To Refresh Data
   * @return void function
   *
   **/
  $scope.doRefresh = function() {
  if ($scope.isOnline == true){
    var link = baseUrl+'/dashboard/userprofile';
     var formData = {uu_id : uuid, user_id : user_id}
     var postData = 'myData='+JSON.stringify(formData);
         $ionicLoading.show({
                   template: '<ion-spinner icon="ios"></ion-spinner>'

               });
          $http({
                 method : 'POST',
                 url : link,
                 data: postData,
                 headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

         }).success(function(res){
           $scope.checked = true;
          $ionicLoading.hide();
$window.localStorage['Permissions']  = JSON.stringify(res.dashboard_data.permissions);
          // user activity json object

          $scope.userStatus = res.dashboard_data.userStatus;

          //  friends activity json object
          $scope.getFriendsActivity = res.dashboard_data.getFriendsActivity;


            //for url preview

            $scope.content = $scope.getFriendsActivity.status;
            $scope.urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
            $scope.url = $scope.content.match($scope.urlRegex);
            // console.log($scope.url);
            $scope.getFriendsActivity.status = $scope.getFriendsActivity.status.replace($scope.url, '');
            $scope.urlPreviewPosted = res.dashboard_data.getFriendsActivity.status_url;


            for(var x in $scope.getFriendsActivity.comments){
              console.log($scope.getFriendsActivity.comments[x].comment);
              $scope.content = $scope.getFriendsActivity.comments[x].comment;
              $scope.urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
              $scope.url = $scope.content.match($scope.urlRegex);
              // console.log($scope.url);
              $scope.getFriendsActivity.comments[x].comment = $scope.getFriendsActivity.comments[x].comment.replace($scope.url, '');
            }




          // friends list json object

          $scope.friendsList = res.dashboard_data.myFriends;

          // news feed json object

          $scope.newsFeed = res.dashboard_data.newsFeed;

          //progress pictures json object

          $scope.userProgressPhotos = res.dashboard_data.userProgressPhotos;

          //levels

          $scope.level = res.dashboard_data.level;

          //points

          $scope.points = res.dashboard_data.points;

          //friends

          $scope.countFriends = res.dashboard_data.countFriends;

          //weight

          $scope.weight = res.dashboard_data.weight;

          //waist

          $scope.waist = res.dashboard_data.waist;

          //calorie graph json object

          $scope.calories_graph = parseInt(res.dashboard_data.calories_graph);
          $scope.Ccalories = res.dashboard_data.calories_graph.calories;
          $scope.Cfats = res.dashboard_data.calories_graph.fats;
          $scope.Cproteins = res.dashboard_data.calories_graph.proteins;

        // charts

          $scope.chartConfig = {
                  options: {
                      chart: {
                          type: 'line',
                          zoomType: 'x',
                          backgroundColor : '#fff',
                          height: '200'
                      }
                  },
                  credits: {
                    enabled : false
                  },
                  series: [{
                      name: 'Calories',
                      data: [$scope.Ccalories]
                  },{
                      name: 'Fats',
                      data: [$scope.Cfats]
                  },{
                      name: 'Proteins',
                      data: [$scope.Cproteins]
                  }],
                  xAxis: {
                  categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                  },
                  title: {
                      text: ''
                  },
                  yAxis: {
                    plotLines: [{
                      value: 0,
                      width: 1,
                      color: '#808080'
                    }]
                  },
                  loading: false
              }

              //my_graph data

              $scope.my_graph = res.dashboard_data.my_graph;
              $scope.my_graph.x_axis = res.dashboard_data.my_graph.x_axis;
              $scope.my_graph.y_axis = res.dashboard_data.my_graph.y_axis;

              $scope.chartConfig2 = {
                options: {
                    chart: {
                        type: 'line',
                        zoomType: 'x',
                        backgroundColor : '#fff',
                        height : '200'
                    }
                },
                credits: {
                  enabled : false
                },
                series: $scope.my_graph.y_axis,
                xAxis: {
                categories: $scope.my_graph.x_axis
                },
                title: {
                    text: ''
                },
                yAxis: {
                  plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                  }]
                },
                loading: false
            }

            notification.get(uuid, user_id).success(function (res) {

              var notifications = res;
              $window.localStorage['notifications']  = JSON.stringify(notifications);
              var localNoti = $window.localStorage['notifications'];
              $scope.notification = angular.fromJson(localNoti);
              $scope.notificationFull = $scope.notification.notification;
              if($scope.notificationFull.length > 0){
                if($scope.notificationFull[0].count == 0){
                  $scope.green = "green";
                } else if($scope.notificationFull[0].count > 0){
                  $scope.green = "red";
                }
              }

              $scope.changeColor = function(){
                $scope.green = "green";

                //;


              }
            });

}).finally(function() {
// Stop the ion-refresher from spinning
$scope.$broadcast('scroll.refreshComplete');
});
}else{
$cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
}


};




//news feed route



  /**
   * @name removeCommentIMG
   * @todo To Delete Comment Image
   * @return void function
   *
   **/

$scope.removeCommentIMG = function(index)
{

$scope.commentImgs.splice(index, 1);
$scope.commentImage.splice(index, 1);
console.log($scope.postImage);

};
///////////////////////// For Comment Images //////////////////////
  /**
   * @name selectUploadMethodComment
   * @todo To Select Upload Method Comment
   * @return void function
   *
   **/
$scope.selectUploadMethodComment = function(){

                                    var myPopup = $ionicPopup.show({
                                        template: '<div class="uploadMethodSack"><ul><li><a class="ink" ng-click="takePictureComment();"><span class="ion-camera"></span><span>Upload from Camera</span></a></li><li><a class=ink"" ng-click="selectPictureComment();"><span class="ion-images"></span><span>Upload from Gallery</span></a></li></ul></div>',
                                        title: 'Choose Upload Method',
                                        subTitle: 'Select from Camera or Gallery',
                                        scope: $scope
                                      });

                                      myPopup.then(function(res) {

                                      });

                                      $timeout(function() {
                                         myPopup.close(); //close the popup after 3 seconds for some reason
                                      }, 8000);
                                  };

                                    $scope.takePictureComment = function(){

                                       document.addEventListener("deviceready", function () {

                                          var options = {

                                            destinationType: Camera.DestinationType.FILE_URI,
                                            sourceType: Camera.PictureSourceType.CAMERA,
                                            allowEdit: true
                                          };

                                          $cordovaCamera.getPicture(options).then(function(imageURI) {

                                          $scope.uploadPicturexComment(imageURI);
                                            window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {

                                              $scope.ftLoad = true;
              //
                                              });
                                            $ionicLoading.show({template: 'Uploading Picture...', duration:500});
                                          },
                                          function(err){
                                            $ionicLoading.show({template: 'Error Uploading...', duration:500});
                                          })

                                        }, false);
                                    };


  /**
   * @name selectPictureComment
   * @todo To Select Pictures Comment
   * @return void function
   *
   **/
                                      $scope.selectPictureComment = function() {

                                       document.addEventListener("deviceready", function () {
                                          var options = {
                                            quality: 50,
                                            destinationType: Camera.DestinationType.FILE_URI,
                                            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                                            encodingType: Camera.EncodingType.JPEG,
                                            targetWidth: 250,
                                            targetHeight: 250,
                                            allowEdit: true
                                          };

                                          $cordovaCamera.getPicture(options).then(
                                          function(imageURI) {
                                          $scope.uploadPicturexComment(imageURI);

                                            window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {
                                             // $scope.status.postImage = fileEntry.nativeURL;
                                              $scope.ftLoad = true;
                                      //				var image = document.getElementById('myImage');
                                      //				image.src = fileEntry.nativeURL;
                                              });
                                            $ionicLoading.show({template: 'Uploading Picture...', duration:500});
                                          },
                                          function(err){
                                            $ionicLoading.show({template: 'Error Uploading...', duration:500});
                                          })
                                           }, false);
                                        };
  /**
   * @name uploadPicturexComment
   * @todo To Uplaod Pictures Comment
   * @return void function
   *
   **/
$scope.uploadPicturexComment = function(imageURI) {

  var options = {

  fileKey: "picture",

  filename: "p1.png",

  chunkedMode: false,

  mimeType: "image/png",
  trustAllHosts:true,
  headers:{Connection:"close"}

  };
 $ionicLoading.show({
                                             template: '<ion-spinner icon="ios"></ion-spinner><p>Uploading Image!</p>'

                                         });
  $cordovaFileTransfer.upload(encodeURI(baseUrl+"/dashboard/uploadCommentmge"), imageURI, options)

  .then(function(result) {

  //$scope.status.commentImage = {};
    //    $scope.commentImage = [];

   $scope.commentImage.push(imageURI);

 var  resk  = angular.fromJson(result.response);
    console.log(resk);
          $scope.commentImgs.push(resk.image_name) ;


$ionicLoading.hide();


  }, function(err) {


  $ionicLoading.hide();
  $ionicLoading.show({template: 'An error occured while uploading your picture.<br>Please try again.', duration:500});

  }, function (progress) {

            });



  }


//Edit Comment
  /**
   * @name selectUploadMethodCommentEdit
   * @todo To Select Upload Method Comment Edit
   * @return void function
   *
   **/
$scope.selectUploadMethodCommentEdit = function(){

                                    var myPopup = $ionicPopup.show({
                                        template: '<div class="uploadMethodSack"><ul><li><a class="ink" ng-click="takePictureCommentEdit();"><span class="ion-camera"></span><span>Upload from Camera</span></a></li><li><a class=ink"" ng-click="selectPictureCommentEdit();"><span class="ion-images"></span><span>Upload from Gallery</span></a></li></ul></div>',
                                        title: 'Choose Upload Method',
                                        subTitle: 'Select from Camera or Gallery',
                                        scope: $scope
                                      });

                                      myPopup.then(function(res) {

                                      });

                                      $timeout(function() {
                                         myPopup.close(); //close the popup after 8 seconds for some reason
                                      }, 8000);
                                  };

                                    $scope.takePictureCommentEdit = function(){

                                       document.addEventListener("deviceready", function () {

                                          var options = {

                                            destinationType: Camera.DestinationType.FILE_URI,
                                            sourceType: Camera.PictureSourceType.CAMERA,
                                            allowEdit: true
                                          };

                                          $cordovaCamera.getPicture(options).then(function(imageURI) {

                                          $scope.uploadPicturexCommentedit(imageURI);
                                            window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {

                                              $scope.ftLoad = true;

                                              });
                                            $ionicLoading.show({template: 'Uploading Picture...', duration:500});
                                          },
                                          function(err){
                                            $ionicLoading.show({template: 'Error Uploading...', duration:500});
                                          })

                                        }, false);
                                    };



                                      $scope.selectPictureCommentEdit = function() {

                                       document.addEventListener("deviceready", function () {
                                          var options = {
                                            quality: 50,
                                            destinationType: Camera.DestinationType.FILE_URI,
                                            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                                            encodingType: Camera.EncodingType.JPEG,
                                            targetWidth: 250,
                                            targetHeight: 250,
                                            allowEdit: true
                                          };

                                          $cordovaCamera.getPicture(options).then(
                                          function(imageURI) {
                                          $scope.uploadPicturexCommentedit(imageURI);

                                            window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {

                                              $scope.ftLoad = true;

                                              });
                                            $ionicLoading.show({template: 'Uploading Picture...', duration:500});
                                          },
                                          function(err){
                                            $ionicLoading.show({template: 'Error Uploading...', duration:500});
                                          })
                                           }, false);
                                        };

  /**
   * @name uploadPicturexCommentedit
   * @todo To Upload Pictures Comment Edit
   * @return void function
   *
   **/
$scope.uploadPicturexCommentedit = function(imageURI) {


  var options = {

  fileKey: "picture",

  filename: "p1.png",

  chunkedMode: false,

  mimeType: "image/png",
  trustAllHosts:true,
  headers:{Connection:"close"}

  };
 $ionicLoading.show({
                                             template: '<ion-spinner icon="ios"></ion-spinner><p>Uploading Image!</p>'

                                         });
  $cordovaFileTransfer.upload(encodeURI(baseUrl+"/dashboard/uploadCommentmge"), imageURI, options)

  .then(function(result) {



   $scope.edit_comment_images.push(imageURI);
 var  resk  = angular.fromJson(result.response);

          $scope.edit_upload_comment_images.push(resk.image_name) ;


$ionicLoading.hide();


  }, function(err) {


  $ionicLoading.hide();
  $ionicLoading.show({template: 'An error occured while uploading your picture.<br>Please try again.', duration:500});

  }, function (progress) {

            });



  };


//end edit comment image


  /**
   * @name selectUploadMethodPostEdit
   * @todo To Select Upload Method Post Edit
   * @return void function
   *
   **/

  $scope.selectUploadMethodPostEdit = function(){

    var myPopup = $ionicPopup.show({
      template: '<div class="uploadMethodSack"><ul><li><a class="ink" ng-click="takePicturePostEdit();"><span class="ion-camera"></span><span>Upload from Camera</span></a></li><li><a class=ink"" ng-click="selectPicturePostEdit();"><span class="ion-images"></span><span>Upload from Gallery</span></a></li></ul></div>',
      title: 'Choose Upload Method',
      subTitle: 'Select from Camera or Gallery',
      scope: $scope
    });

    myPopup.then(function(res) {

    });

    $timeout(function() {
      myPopup.close(); //close the popup after 8 seconds for some reason
    }, 8000);
  };
  /**
   * @name takePicturePostEdit
   * @todo To Take Pictures Post Edit
   * @return void function
   *
   **/
  $scope.takePicturePostEdit = function(){

    document.addEventListener("deviceready", function () {

      var options = {

        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.CAMERA,
        allowEdit: true
      };

      $cordovaCamera.getPicture(options).then(function(imageURI) {

          $scope.uploadPicturexPostedit(imageURI);
          window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {

            $scope.ftLoad = true;

          });
          $ionicLoading.show({template: 'Uploading Picture...', duration:500});
        },
        function(err){
          $ionicLoading.show({template: 'Error Uploading...', duration:500});
        })

    }, false);
  }


  /**
   * @name selectPicturePostEdit
   * @todo To Select Pictures Post Edit
   * @return void function
   *
   **/
  $scope.selectPicturePostEdit = function() {

    document.addEventListener("deviceready", function () {
      var options = {
        quality: 50,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 250,
        targetHeight: 250,
        allowEdit: true
      };

      $cordovaCamera.getPicture(options).then(
        function(imageURI) {
          $scope.uploadPicturexPostedit(imageURI);

          window.resolveLocalFileSystemURL(imageURI, function(fileEntry) {

            $scope.ftLoad = true;

          });
          $ionicLoading.show({template: 'Uploading Picture...', duration:500});
        },
        function(err){
          $ionicLoading.show({template: 'Error Uploading...', duration:500});
        })
    }, false);
  };
  /**
   * @name uploadPicturexPostedit
   * @todo To Upload Pictures Post Edit
   * @return void function
   *
   **/
  $scope.uploadPicturexPostedit = function(imageURI) {


    var options = {

      fileKey: "picture",

      filename: "p1.png",

      chunkedMode: false,

      mimeType: "image/png",
      trustAllHosts:true,
      headers:{Connection:"close"}

    };
    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner><p>Uploading Image!</p>'

    });
    $cordovaFileTransfer.upload(encodeURI(baseUrl+"/dashboard/uploadStatusImge"), imageURI, options)

      .then(function(result) {



        $scope.edit_status_images.push(imageURI);
        var  resk  = angular.fromJson(result.response);

        $scope.edit_upload_post_images.push(resk.image_name) ;


        $ionicLoading.hide();


      }, function(err) {


        $ionicLoading.hide();
        $ionicLoading.show({template: 'An error occured while uploading your picture.<br>Please try again.', duration:500});

      }, function (progress) {

      });



  };


  /**
   * @name shareViaTwitter
   * @todo To Take Share via Twitter
   * @return void function
   *
   **/
$scope.shareViaTwitter = function(message, image, link) {
$ionicLoading.show({
       template: '<ion-spinner icon="ios"></ion-spinner><p>Sharing on Twitter</p>'
   });
        $cordovaSocialSharing.canShareVia("twitter", message, image, link).then(function(result) {
            $cordovaSocialSharing.shareViaTwitter(message, image, link);
            $ionicLoading.hide();
        }, function(error) {
//            alert("Cannot share on Twitter");
        });
    };

  /**
   * @name screenshotFacebook
   * @todo To Take Screenshot and Post it to facebook
   * @return void function
   *
   **/
    $scope.screenshotFacebook = function(message, image, link){
      $ionicLoading.show({template: '<ion-spinner icon="ios"></ion-spinner>'});
      html2canvas(document.getElementsByClassName("myActivityHead"), {onclone: function(document) {
        // console.log('is it working or not');
        document.querySelector('.userstatusInfo').style.margin = "10px 20px";
        document.querySelector('.userstatusInfo').style.padding = "10px";
        document.querySelector('.userstatusInfo').style.flex = "0 0 85%";
        document.querySelector('.userstatusInfo').style.background = "#ffffff";
        document.querySelector('.userstatusInfo').style.border = "1px solid #2a2a2a";
        document.querySelector('.userstatusInfo').style.borderRadius = "5px";
        document.querySelector('.statusText p').style.color = '#333';
        document.querySelector('.statusText p').style.fontSize = '14px';
        document.querySelector('.stLikes').style.display = 'none';
        document.querySelector('.postEdit').style.display = 'none';
        document.querySelector('.hideOnApp').style.display = 'block';
        document.querySelector('.myActivityHead .userImg').style.display = 'none';
      },
          allowTaint : true,
          logging : true,
          useCORS : true,
          proxy : 'http://www.fitnessbasecamp.com'}).then(function (canvas) {

                  var context = canvas.getContext("2d");


                   context.fillStyle = "#ffffff";



                  var imgDataUrl = canvas.toDataURL("image/png");

                  //share the image via phonegap plugin
                  window.plugins.socialsharing.share(
                      'Fitnessbasecamp',
                      'Fitnessbasecamp!',
                      imgDataUrl,
                      'http://www.fitnessbasecamp.com/',
                      function(){
                         $ionicLoading.hide();

                      },
                      function(err){
                          //error callback
                           $ionicLoading.hide();
                          console.error('error in share', err)
                      }
                  );
          });


      };
  /**
   * @name shareProg
   * @todo To Share Progress Photos to Social Sites
   * @return void function
   *
   **/
    $scope.shareProg = function(message, image, link){
      $ionicLoading.show({template: '<ion-spinner icon="ios"></ion-spinner>'});
      $scope.picsToShare = [];
      html2canvas(document.querySelector(".progressPics"), {onclone: function(document) {
        document.querySelector('.progressPics').style.borderRadius = "0";
        document.querySelector('.progressPics').style.border = "0";
        document.querySelector('.progressHead').style.display = 'none';
        document.querySelector('.progressMain').style.padding = "10px";
        document.querySelector('.progressMain').style.background = "#ffffff";
        document.querySelector('.progressMain').style.border = "0";
        document.querySelector('.progressMain').style.margin = "0 0 10px 0";
        document.querySelector('.progressMain .hideOnApp').style.display = 'block';
        document.querySelector('.progressMain:last-child').style.display = 'none';
      },
          allowTaint : true,
          logging : true,
          useCORS : true,
          proxy : 'http://www.fitnessbasecamp.com'}).then(function (canvas) {
                  var context = canvas.getContext("2d");
                   context.fillStyle = "#ffffff";
                  var imgDataUrl = canvas.toDataURL("image/png");
        window.plugins.socialsharing.share(
          'Fitnessbasecamp',
          'Fitnessbasecamp!',
          imgDataUrl,
          'http://www.fitnessbasecamp.com/',
          function(){
            $ionicLoading.hide();

          },
          function(err){
            //error callback
            $ionicLoading.hide();


          });

      });
    }
  /**
   * @name shareStat
   * @todo To Share Stats to Social Sites
   * @return void function
   *
   **/
    $scope.shareStat = function(message, image, link){
      $ionicLoading.show({template: '<ion-spinner icon="ios"></ion-spinner>'});
      html2canvas(document.getElementsByClassName("highcharts-container"), {
          allowTaint : true,
          logging : true,
          useCORS : true,
          proxy : 'http://www.fitnessbasecamp.com'}).then(function (canvas) {

                  var context = canvas.getContext("2d");


                   context.fillStyle = "#ffffff";



                  var imgDataUrl = canvas.toDataURL("image/png");

                  //share the image via phonegap plugin
                  window.plugins.socialsharing.share(
                      'Fitnessbasecamp',
                      'Fitnessbasecamp!',
                      imgDataUrl,
                      'http://www.fitnessbasecamp.com/',
                      function(){
                         $ionicLoading.hide();

                      },
                      function(err){
                          //error callback
                           $ionicLoading.hide();
                          console.error('error in share', err)
                      }
                  );
          });


      };


//remove comment
  /**
   * @name removeComment
   * @todo To Delete Comment
   * @return void function
   *
   **/
$scope.removeComment = function(commentID, statusID){

    if ($scope.isOnline == true){
      $scope.showConfirm = function () {
        var confirmPopup = $ionicPopup.confirm({
          title: 'Warning',
          template: 'Are you Sure You Want To Delete your Comment ?',
          cssClass: 'loginErrorPopUp logoutCpop'

        });

        confirmPopup.then(function (res) {
          if (res) {
            var comment_id = commentID;
            var status_id = statusID;

            var link = baseUrl+'/dashboard/remove_comment';

            var formData = {uu_id : uuid, user_id : user_id, comment_id : comment_id, status_id : status_id}
            var postData = 'myData='+JSON.stringify(formData);

            $ionicLoading.show({
              template: '<ion-spinner icon="ios"></ion-spinner>'

            });
            $http({
              method : 'POST',
              url : link,
              data: postData,
              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

            }).success(function(res){
              $ionicLoading.hide();

              for(var x in $scope.getFriendsActivity.comments){
                console.log($scope.getFriendsActivity.comments[x].comment);
                $scope.content = $scope.getFriendsActivity.comments[x].comment;
                $scope.urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
                $scope.url = $scope.content.match($scope.urlRegex);
                // console.log($scope.url);
                $scope.getFriendsActivity.comments[x].comment = $scope.getFriendsActivity.comments[x].comment.replace($scope.url, '');
              }

              $scope.getFriendsActivity.comments =  res.status_comments.comments;
              var alertPopup = $ionicPopup.alert({
                title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                template: res.message,
                cssClass: 'loginErrorPopUp hideOk'
              });
              $timeout(function () {
                alertPopup.close();
              }, 3000);




            })
          } else {

          }
        });
      };
      $scope.showConfirm();

        }else{
         $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        }


};


  /**
   * @name userdashboard
   * @todo To Move to User's Dashboard
   * @return void function
   *
   **/

                                    $scope.userdashboard = function(result)
                                        {
                                          $state.go('app.userdashboard', {result: result});
                                        };


  $ionicModal.fromTemplateUrl('templates/modal.html', {
    id: '1', // We need to use and ID to identify the modal that is firing the event!
    scope: $scope,
    backdropClickToClose: false,
    animation: 'animated bounceInUp'
  }).then(function(modal) {
    $scope.Modal1 = modal;
  });

  // Modal 2
  // $ionicModal.fromTemplateUrl('templates/crop.html', {
  //   id: '2', // We need to use and ID to identify the modal that is firing the event!
  //   scope: $scope,
  //   backdropClickToClose: false,
  //   animation: 'animated slideInRight'
  // }).then(function(modal) {
  //   $scope.Modal2 = modal;
  // });

  $scope.openModal = function(index) {
    if (index == 1) $scope.Modal1.show();
    else $scope.Modal2.show();
  };

  $scope.closeModal = function(index) {
    if (index == 1) $scope.Modal1.hide();
    else $scope.Modal2.hide();
  };


  /**
   * @name moveNotification
   * @todo To Show Notifications in Notifications
   * @return void function
   *
   **/

$scope.moveNotification = function(id){
  $scope.Modal1.hide();
  var bit = $scope.notificationFull[id].bit;
  var workout_id = $scope.notificationFull[id].extra_info;
  var status_id = $scope.notificationFull[id].status_id;
  var noti_id = $scope.notificationFull[id].id;
  var person = $scope.notificationFull[id].sender;
  if(bit === 'status' && (workout_id === null || workout_id === '')){
    $state.go('app.singlePost', {cid: status_id, did : noti_id});
  } else if(bit === 'share_workout' && workout_id !== null){
    $state.go('app.myWorkoutDetailMain', {result: workout_id, cid : bit, did : person, nid : noti_id});
  } else if(bit === 'friend_request' && (workout_id === null || workout_id === '')){
    $state.go('app.friends.myFriends.friendsR', {cid: status_id, did : noti_id});
  }else if(bit === 'accept_friend_request' && (workout_id === null || workout_id === '')){
    $state.go('app.friends.myFriends.myFriendsI', {cid: status_id, did : noti_id});
  }
  var link = baseUrl+'/dashboard/notificationStatus';
  // console.log('userID---'+ user_id + '--UUID--' + uuid);
  var formData = {uu_id : uuid, user_id : user_id, status_id : status_id, noti_id : noti_id};
  var postData = 'myData='+JSON.stringify(formData);


  $http({
    method : 'POST',
    url : link,
    data: postData,
    headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

  }).success(function(res){

    // console.log(res);
    $scope.post = res.dashboard_data.userStatus;
    if($scope.post === '' || $scope.post === undefined){
      $scope.noStatus = true;
    }


  })
    .error(function(error){
      // console.log('request not completed')
    })
    .finally(function($ionicLoading) {
      // On both cases hide the loading
      $scope.hide($ionicLoading);
    });

};

  /**
   * @name expired
   * @todo To Check User's status of Subscription if it is expired or not
   * @return void function
   *
   **/

  $scope.expired = function(){
    var myPopup = $ionicPopup.show({
      template: '<div class="packageExpiredInner"><div class="selectOption" ng-click="closePopup()"><span>Not Now</span></div><div class="selectOption" ng-click="openBrowser()"><span>Renew Now</span></div></div>',
      subTitle: 'Oops! Your subscription has expired. Please renew your subscription.',
      cssClass: 'packageExpiredPopup',
      scope: $scope
    });

    myPopup.then(function(res) {

    });

    $timeout(function() {
      // myPopup.close(); //close the popup after 3 seconds for some reason
    }, 8000);
    $scope.closePopup = function() {

      myPopup.close();
    };
  };
  /**
   * @name openBrowser
   * @todo To Open In App Browser
   * @return void function
   *
   **/
  $scope.openBrowser = function(){
    var user_id = $scope.userData.user_id;
    var options = {
      location: 'yes',
      clearcache: 'yes',
      toolbar: 'no'
    };
      $cordovaInAppBrowser.open('http://www.fitnessbasecamp.com/loginUser_admin/'+user_id+'/app', '_system', options)
        .then(function(event) {

        })
        .catch(function(event) {
          // error
        });


      // $cordovaInAppBrowser.close();
  }


});
// dashboard Controller



