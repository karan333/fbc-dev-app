/**
 * Purpose:
 *@This file is used to view user's Workouts.
 *
 **/




angular.module('starter').controller('myWorkoutsCtrl', function($cordovaInAppBrowser, friendsList, $ionicModal, $ionicScrollDelegate ,focus, $cordovaToast, quote, $rootScope,  $cordovaNetwork, $window, deviceInformation, $ionicPopover , $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading) {


  /**
   *
   * Initializing Variables, Arrays, and Objects
   *
   **/


  $scope.deviceInfo = deviceInformation.getInfo();
  var uuid = $scope.deviceInfo.uuid
  $scope.layout = 'list';
  $scope.packageExpired = {};
  $scope.refine = {};
  $scope.searchStrman = {};
  $scope.baseUrlForWorkoutImg = 'http://www.fitnessbasecamp.com/uploads/workouts/';
//console.log($scope.workouts.search);
  $scope.moreworkouts=false;
  $scope.allWorkoutss=[];
  $scope.postlimit = 12;
  $scope.workoutss=[];
  $scope.currentrecordsview = 0;
  var user_id="";
  $scope.isOnline = [];
  $scope.dontShow = {};
  $scope.sharedWorkoutID = {};
  $scope.roles = {};
  $scope.share = {};
  $scope.searchF = {};
//popover
$ionicPopover.fromTemplateUrl('templates/popover.html', {
    scope: $scope,
  }).then(function(popover) {
    $scope.popover = popover;
  });


  $scope.siteUrl = siteUrl;
$scope.isOnline = [];
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();


        /**
         *
         * Listen for Online event
         *
         **/
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        /**
         *
         * Listen for ofline event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            // console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);
    var userData = $window.localStorage['userData'];
     $scope.userData = angular.fromJson(userData);

  var packageExpired = $window.localStorage['expired'];
  $scope.packageExpired = angular.fromJson(packageExpired);


                  var user_id = $scope.userData.user_id;

  /**
   *
   * get quotes
   *
   **/


                  quote.getQuote(uuid, user_id).success(function (res) {
                                      // console.log(res);
                                      $scope.quote = res;
                                      });


  /**
   *
   * getting friend list
   *
   **/
  friendsList.get(uuid, user_id).success(function (res) {
    // console.log(res.friends);
    $scope.roles = res.friends;
    $ionicLoading.hide();
  });


  /**
   *
   * Get workouts from service api call
   *
   **/



                  var link = baseUrl+'/workouts/myworkouts';
                  var formData = {uu_id : uuid, user_id : user_id};
                  var postData = 'myData='+JSON.stringify(formData);
                      $ionicLoading.show({
                                template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                       $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                      }).success(function(res){

                        // console.log(res);
                        $scope.NoResultFound =  res.code;
                        $scope.workouts = res.myworkouts;
                         $ionicLoading.hide();

                      }) .error(function(error){
                        $ionicLoading.hide();
                        // console.log('request not completed');
                      });



  /**
   * @name toggleGroup
   * @todo To show hide workout labels
   * @return void function
   *
   **/

                      $scope.toggleGroup = function(id) {
                          if ($scope.isGroupShown(id)) {
                            $scope.shownGroup = null;
                          } else {
                            $scope.shownGroup = id;
                          }
                        };
                        $scope.isGroupShown = function(id) {
                          return $scope.shownGroup === id;
                        };

  /**
   * @name focussearch
   * @todo To show search box
   * @return void function
   *
   **/
  $scope.focussearch = function() {
    // do something awesome
    $ionicScrollDelegate.scrollTop();
    focus('searchStrman');
    $scope.show = "showInput";

  };


  $scope.showSearchIcon = true;

  /**
   * @name change
   * @todo To check value inside search box
   * @return void function
   *
   **/

  $scope.change = function(text) {
    if($scope.searchStrman.search == ''){
      $scope.showSearchIcon = true;
    }
    // console.log(text);
    $ionicScrollDelegate.resize();
    angular.element(document.querySelectorAll('.allWorkoutsStack')).triggerHandler('click');
    $scope.showSearchIcon = false;
  };



  /**
   * @name emptySearch
   * @todo To empty check box
   * @return void function
   *
   **/


  $scope.emptySearch = function(text){
    $scope.showSearchIcon = true;
    $scope.searchStrman = {};
    $scope.searchStrman.search = null;
    $scope.searchStrman.search = '';
    text = '';
    $scope.searchF = {};
    $scope.searchF.friend = null;
    $scope.searchF.friend = '';

  };



  /**
   *
   * to check package expiry
   *
   **/


  $scope.permissions= angular.fromJson($window.localStorage['Permissions']);
  $scope.inArray = function (needle) {
    haystack  = $scope.permissions;
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
      if(haystack[i] == needle){ return true;

      } ;
    }
    return "noPermission";
  }
  $scope.Npermission = function(is_perm)
  {

    if(is_perm != true)
    {

      var alertPopup = $ionicPopup.alert({
        title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
        template: "You don't have permission to do this.Please upgrade your package.",
        cssClass: 'loginErrorPopUp'
      });
    }

  }
  $scope.addWorkoutx = function() {
    if ($scope.inArray('Add Custom Workout') == true) {

      $state.go('app.addCustomWorkout');
    }
    else {

      var alertPopup = $ionicPopup.alert({
        title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
        template: "You don't have permission to do this.Please upgrade your package.",
        cssClass: 'loginErrorPopUp'
      });

    }
  }


  /**
   * @name removeCustomWorkout
   * @todo To remove user workout
   * @return void function
   *
   **/


      $scope.removeCustomWorkout = function(id)
        {
        // console.log("mani");
$scope.showConfirm = function() {
                         var confirmPopup = $ionicPopup.confirm({
                           title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-close"></span></div></div>',
                           cssClass: 'animated jello progPicDelPop',
                           template: '<div class="PPdelBody"><span class="title">Warning</span><span class="popmessage">Are you sure you want to delete this Workout ? You will not be able to retrieve this once deleted.</span></div>'
                         });

                         confirmPopup.then(function(res) {
                           if(res) {
         /////// Code For Removing Exercise ///////////

var link = baseUrl+'/workouts/remove_workout';
                  var formData = {uu_id : uuid, user_id : user_id,workout_id:id}
                  var postData = 'myData='+JSON.stringify(formData);
                      $ionicLoading.show({
                                template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                       $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                      }).success(function(res){
                      // console.log(res);
                         var alertPopup = $ionicPopup.alert({
                           title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                           template: 'Your workout is removed',
                           cssClass: 'loginErrorPopUp hideOk'
                         });
                         $timeout(function () {
                           alertPopup.close();
                         }, 3000);
                       $state.go($state.current, {}, {reload: true});

                        $ionicLoading.hide();

                      });
                      }
                      });


        }
         $scope.showConfirm();
        };


  /**
   * @name filterWorkouts
   * @todo To filter workout
   * @return void function
   *
   **/

  $scope.filterWorkouts = function() {

    var w_type = $scope.refine.Selectedtype;
    var machines = $scope.refine.Selectedequip;
    var level = $scope.refine.Selectedlevel;
    var Selectedintensity = $scope.refine.Selectedintensity;
    var Selectedduration = $scope.refine.Selectedduration;
    ////////////////// Filter Request Start/////////////////

    var link = baseUrl+'/workouts/search_custom_workouts';
    var formData = {uu_id : uuid, user_id : user_id ,  workout_type : w_type,  wLevels : level, exmachines : machines,duration:Selectedduration,wGoals:Selectedintensity}
    var postData = 'myData='+JSON.stringify(formData);
    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner>'

    });
    $http({
      method : 'POST',
      url : link,
      data: postData,
      headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

    }).success(function(res){

      // console.log(res);
      $scope.NoResultFound =  res.code;
      $scope.workouts = res.workouts;
      $scope.modal.hide();
      $ionicLoading.hide();

    }) .error(function(error){
      $ionicLoading.hide();
      // console.log('request not completed');
    }).finally(function ($ionicLoading) {
      // On both cases hide the loading
      $ionicLoading.hide();
    });


    //favorite workout



    ///////////////// Filter Request End ///////////////////



  };


  /**
   * @name fromTemplateUrl
   * @todo To load workout filters from service api call
   * @return void function
   *
   **/



  $ionicModal.fromTemplateUrl('templates/modal.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
    var user_id = $scope.userData.user_id;
    var link = baseUrl+'/workouts/workoutfilter';
    var formData = {uu_id : uuid, user_id : user_id};
    var postData = 'myData='+JSON.stringify(formData);
    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner>'

    });
    $http({
      method : 'POST',
      url : link,
      data: postData,
      headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

    }).success(function(res){

      $scope.refine = res.workoutsfilter;
      // console.log($scope.refine);

    }) .error(function(error){
      $ionicLoading.hide();
      // console.log('request not completed');
    })
  });

  /**
   * @name toggleGroup
   * @todo To show hide workout labels
   * @return void function
   *
   **/


  $scope.toggleGroup = function(id) {
    if ($scope.isGroupShown(id)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = id;
    }
  };
  $scope.isGroupShown = function(id) {
    return $scope.shownGroup === id;
  };




  $scope.checkedFriend = {};

  $scope.share = {
    roles: [$scope.roles[1]]
  };
  $scope.checkAll = function() {
    $scope.share.roles = angular.copy($scope.roles);
  };
  $scope.uncheckAll = function() {
    $scope.share.roles = [];
  };
  $scope.checkFirst = function() {
    $scope.share.roles.splice(0, $scope.share.roles.length);
    $scope.share.roles.push($scope.roles[0]);
  };


  /**
   *
   * to initialize modals
   *
   **/




  $scope.modal2Data = {};
  $ionicModal.fromTemplateUrl('templates/modal2.html', {
    scope: $scope
  }).then(function(modal) {
    //Fix this line, changed the variable name to different name.
    $scope.modal2 = modal;
  });

  $scope.closeModal2 = function() {
    $scope.modal2.hide();
    $scope.share.email = '';
    $scope.share.roles = {};
    $scope.share.suggText = '';
  };



  /**
   * @name doShare
   * @todo To share custom workout with friends
   * @return void function
   *
   **/




  $scope.doShare = function(email, friends, text){
    // delete $scope.share.roles[0];
    friends.filter(function(n){ return n != undefined });
    // console.log(email);
    // console.log(friends);
    $scope.sharedEmail = email;
    $scope.sharedFriends = friends;
    $scope.sharedText = text;

    if((email !== undefined && email !== '') || friends.length > 1){
      var link = baseUrl+'/workouts/share_custom_workout';
      var formData = {uu_id : uuid, user_id : user_id ,  email : email,  friends_list : friends, message : text, workout_id : $scope.sharedWorkoutID};
      // console.log(formData);
      var postData = 'myData='+JSON.stringify(formData);
      $ionicLoading.show({
        template: '<ion-spinner icon="ios"></ion-spinner>'

      });
      $http({
        method : 'POST',
        url : link,
        data: postData,
        headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

      }).success(function(res){

        // console.log(res);
        $ionicLoading.hide();
        $scope.modal2.hide();
        var alertPopup = $ionicPopup.alert({
          title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
          template: res.message,
          cssClass: 'loginErrorPopUp hideOk'
        });

        $timeout(function () {
          alertPopup.close();
        }, 3000);

      }) .error(function(error){
        $ionicLoading.hide();
        // console.log('request not completed');
      }).finally(function ($ionicLoading) {
        // On both cases hide the loading
        $ionicLoading.hide();
      });
    } else{
      var alertPopup = $ionicPopup.alert({
        title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
        template: 'Please add a Friend',
        cssClass: 'loginErrorPopUp hideOk'
      });

      $timeout(function () {
        alertPopup.close();
      }, 3000);
    }


  };


  /**
   *
   * to initialize modals
   *
   **/

  $scope.model2 = function(id) {
    $scope.modal2.show();
    $scope.sharedWorkoutID = id;
    // console.log(id);
  };

  $scope.modal3Data = {};
  $ionicModal.fromTemplateUrl('templates/modal3.html', {
    scope: $scope
  }).then(function(modal) {
    //Fix this line, changed the variable name to different name.
    $scope.modal3 = modal;
  });

  $scope.closeModal3 = function() {
    $scope.modal3.hide();
    $scope.$on('$destroy', function() {
      $scope.modal3.remove();
    });
  };
  $scope.$on('$scope.modal3.hidden', function() {
    // console.log('modal 2 hidden');
    $scope.$on('$destroy', function() {
      $scope.modal3.remove();
    });
  });
  $scope.$on('$scope.modal2.hidden', function() {
    // console.log('modal 2 hidden');
    $scope.share.email = '';
    $scope.share.roles = {};
    $scope.share.suggText = '';
    $scope.$on('$destroy', function() {
      $scope.modal2.remove();
    });
  });

  $scope.model3 = function(id) {
    $scope.modal3.show();
    $scope.checkFirst();
  };



  /**
   * @name expired
   * @todo To show package expiry package
   * @return void function
   *
   **/



  $scope.expired = function(){
    var myPopup = $ionicPopup.show({
      template: '<div class="packageExpiredInner"><div class="selectOption" ng-click="closePopup()"><span>Not Now</span></div><div class="selectOption" ng-click="openBrowser()"><span>Renew Now</span></div></div>',
      subTitle: 'Oops! Your subscription has expired. Please renew your subscription.',
      cssClass: 'packageExpiredPopup',
      scope: $scope
    });

    myPopup.then(function(res) {

    });

    $timeout(function() {
      // myPopup.close(); //close the popup after 3 seconds for some reason
    }, 8000);
    $scope.closePopup = function() {

      myPopup.close();
    };
  };




  /**
   * @name openBrowser
   * @todo To open InAppBrowser
   * @return void function
   *
   **/



  $scope.openBrowser = function(){
    var user_id = $scope.userData.user_id;
    var options = {
      location: 'yes',
      clearcache: 'yes',
      toolbar: 'no'
    };
    $cordovaInAppBrowser.open('http://www.fitnessbasecamp.com/loginUser_admin/'+user_id+'/app', '_system', options)
      .then(function(event) {
        // console.log(event);
      })
      .catch(function(event) {
        // error
      });


    // $cordovaInAppBrowser.close();
  }

});
