/**
 * Purpose:
 *@This file is used to select time for start a workout
 *
 **/

angular.module('starter').controller('startWorkout4Ctrl', function(blur, focus, $cordovaToast, $rootScope,  $cordovaNetwork,$window, ionicDatePicker,$ionicModal, $ionicPopover, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading) {
 // console.log(GuserData.user_fname);

  /**
   *
   * to keep awake while timer is running
   *
   **/
$scope.deviceInfo = deviceInformation.getInfo();
var uuid = $scope.deviceInfo.uuid;
$scope.siteUrl = siteUrl;
$scope.selectType = {
  time : '60'
};
$scope.static = true;
$scope.dynamic = false;
$scope.dynamicInput = false;
$scope.isOnline = [];
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();


        /**
         *
         * Listen for Online event
         *
         **/
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        /**
         *
         * Listen for ofline event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            // console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);
                  var userData = $window.localStorage['userData'];
                 $scope.userData = angular.fromJson(userData);

                                    // after getting data from sqlite getting user id and then all news feed service

                                    var user_id = $scope.userData.user_id;
  /**
   * @name selectTime
   * @todo To select time in timer
   * @return void function
   *
   **/

             $scope.selectTime = function(value) {
               $scope.selectType.time = value;
               // console.log(value);
               blur('searchStrman');
               $window.localStorage['selectTime'] = value;
               // console.log('type---' + $window.localStorage['selectType'] + '--level----' + $window.localStorage['selectLevel'] + '--intensity--' + $window.localStorage['selectIntensity'] + '--time---' + $window.localStorage['selectTime']);
               // if($scope.selectType.time == '60+'){
               //   $scope.selectType.time = 70;
               //   var sixtyPlus = $scope.selectType.time;
               // }
             };


  /**
   * @name filterWorkouts
   * @todo To filter workout based on previous selected options
   * @return void function
   *
   **/


                                        $scope.filterWorkouts = function() {

                                            var w_type = [$window.localStorage['selectType']];
                                            var level =  [$window.localStorage['selectLevel']];
                                            var Selectedintensity =  [$window.localStorage['selectIntensity']];
                                            // console.log($scope.selectType.time);
                                          if($scope.selectType.time != '60+'){
                                            var Selectedduration =  $scope.selectType.time * 60;
                                          } else{
                                            var Selectedduration =  'all';
                                          }

                                            ////////////////// Filter Request Start/////////////////

                                            var link = baseUrl+'/workouts/search_workouts';
                                            var formData = {uu_id : uuid, user_id : user_id ,  workout_type : w_type,  wLevels : level,duration:Selectedduration,wGoals:Selectedintensity}
                                            var postData = 'myData='+JSON.stringify(formData);
                                          $ionicLoading.show({
                                                    template: '<ion-spinner icon="ios"></ion-spinner>'

                                                });
                                           $http({
                                                  method : 'POST',
                                                  url : link,
                                                  data: postData,
                                                  headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                                          }).success(function(res){

                                            // console.log(res.workouts);
                                            var result = JSON.stringify(res);
                                            $state.go('app.workouts', {result: result});
                                            $ionicLoading.hide();




                                              $timeout(function() {
                                                  ionicMaterialMotion.fadeSlideInRight({
                                                      startVelocity: 3000
                                                  });
                                              }, 700);

                                              // Set Ink
                                              ionicMaterialInk.displayEffect();


                                          }) .error(function(error){
                                            $ionicLoading.hide();
                                            // console.log('request not completed');
                                          });







                                            ///////////////// Filter Request End ///////////////////



                                           };


});
