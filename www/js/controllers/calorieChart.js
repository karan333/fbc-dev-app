/**
 * Purpose:
 *@This file is used to Make Calorie Charts
 *
 **/
angular.module('starter').controller('calorieChartCtrl', function(quote, $rootScope,  $cordovaNetwork, $window, deviceInformation, $scope, $cordovaSQLite, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $ionicLoading, $ionicPopup) {
  $scope.siteUrl = siteUrl;
   $scope.deviceInfo = deviceInformation.getInfo();
   var uuid = $scope.deviceInfo.uuid;

   $scope.calorieChart = {}
$scope.isOnline = [];
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();

        /**
         *
         * Listen for Online event
         *
         **/
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        /**
         *
         * Listen for Offline event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);


                          var userData = $window.localStorage['userData'];
                           $scope.userData = angular.fromJson(userData);

                           var user_id = $scope.userData.user_id;
                            quote.getQuote(uuid, user_id).success(function (res) {
                                                                               console.log(res);
                                                                               $scope.quote = res;
                                                                               });
                            var link = baseUrl+'/nutrition/calorie_chart';
                            console.log('userID---'+ user_id + '--UUID--' + uuid);
                            var formData = {uu_id : uuid, user_id : user_id}
                            var postData = 'myData='+JSON.stringify(formData);

                            $ionicLoading.show({
                            template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                            $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                          }).success(function(res){

                              console.log(res.calorie_chart);
                              $scope.calorieChart = res.calorie_chart;

                          $ionicLoading.hide();
                          $timeout(function() {
                              ionicMaterialMotion.fadeSlideInRight({
                                  startVelocity: 3000
                              });
                          }, 700);

                          // Set Ink
                          ionicMaterialInk.displayEffect();


                        })


});
