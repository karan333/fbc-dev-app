/**
 * Purpose:
 *@This file is used to Show Fav Exercises
 *
 **/

angular.module('starter').controller('FavExercisesController', function($sce, $filter, $cordovaInAppBrowser, quote, $rootScope,  $cordovaNetwork, $window, $ionicModal,focus,$ionicScrollDelegate, $ionicPopover, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading) {
 // console.log(GuserData.user_fname);


$scope.deviceInfo = deviceInformation.getInfo();
var uuid = $scope.deviceInfo.uuid
$scope.layout = 'list';
  $scope.packageExpired = {};

  $scope.searchStrman = {};

$scope.refine = {};
$scope.baseUrlForWorkoutImg = 'http://www.fitnessbasecamp.com/uploads/workouts/';

$scope.permissions= angular.fromJson($window.localStorage['Permissions']);
$scope.inArray = function (needle) {
haystack  = $scope.permissions;
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle){ return true;

        } ;
    }
    return "noPermission";
}
  /**
   * @name Npermission
   * @todo To Check if user have the permission or not
   * @return void function
   *
   **/
 $scope.Npermission = function(is_perm)
                                  {

                                      if(is_perm != true)
                                       {

                                          var alertPopup = $ionicPopup.alert({
                                            title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                            template: "You don't have permission to do this.Please upgrade your package.",
                                            cssClass: 'loginErrorPopUp'
                                          });
                                       }

                                  }

  /**
   * @name addExer
   * @todo To Add Exercise to custom Exercise
   * @return void function
   *
   **/
$scope.addExer = function()
 {
if($scope.inArray('Add Custom Exercise')==true)
{

 $state.go('app.addCustomExercise');
}
else
                                      {

 var alertPopup = $ionicPopup.alert({
                                        title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                        template: "You don't have permission to do this.Please upgrade your package.",
                                        cssClass: 'loginErrorPopUp'
                                      });
                                      }
 }

//console.log($scope.workouts.search);
$scope.moreexercise=false;
$scope.allWorkouts=[];
     $scope.postlimit = 6;
      $scope.exercise=[];
      $scope.currentrecordsview = 0;
//popover
$ionicPopover.fromTemplateUrl('templates/popover.html', {
    scope: $scope,
  }).then(function(popover) {
    $scope.popover = popover;
  });


  $scope.siteUrl = siteUrl;
$scope.isOnline = [];
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();



        /**
         *
         * Listen for Online event
         *
         **/
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })


        /**
         *
         * Listen for Offline event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            // console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);
                           var userData = $window.localStorage['userData'];
                          $scope.userData = angular.fromJson(userData);

  var packageExpired = $window.localStorage['expired'];
  $scope.packageExpired = angular.fromJson(packageExpired);


                  var user_id = $scope.userData.user_id;
                   quote.getQuote(uuid, user_id).success(function (res) {
                                                                      // console.log(res);
                                                                      $scope.quote = res;
                                                                      });

                  var link = baseUrl+'/exercise/myFavtExercise';
                  var formData = {uu_id : uuid, user_id : user_id}
                  var postData = 'myData='+JSON.stringify(formData);
                      $ionicLoading.show({
                                template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                       $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                      }).success(function(res){
                      // console.log(res);
                      $scope.allWorkouts = res.exercise;
                      var vimeo_id = res.vimeo_id;



                         $ionicLoading.hide();
                      }) .error(function(error){
                        $ionicLoading.hide();
                        // console.log('request not completed');
                      });
  /**
   * @name favorite
   * @todo To Add Favorite Exercise
   * @return void function
   *
   **/

                      $scope.favorite = function(exercise_id, index){
                      // console.log('favorite');
                      var exercise_id = exercise_id;
                        var link = baseUrl+'/exercise/Togglefavrtunfavrtexercise';
                       var formData = {uu_id : uuid, user_id : user_id , exercise_id : exercise_id}
                       var postData = 'myData='+JSON.stringify(formData);
                        $ionicLoading.show({
                               template: '<ion-spinner icon="ios"></ion-spinner>'

                           });
                      $http({
                             method : 'POST',
                             url : link,
                             data: postData,
                             headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                     }).success(function(res){

                       // console.log(res);
                        $scope.allWorkouts = res.exercise;
                        var alertPopup = $ionicPopup.alert({
                        title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                        template: res.message,
                        cssClass: 'loginErrorPopUp hideOk'
                      });
                        $timeout(function () {
                          alertPopup.close();
                        }, 3000);

                        $ionicLoading.hide();
                     }) .error(function(error){
                       $ionicLoading.hide();
                       // console.log('request not completed');
                     });

                      }

  /**
   * @name filterExercise
   * @todo To Apply filters on Exercises
   * @return void function
   *
   **/
   $scope.filterExercise = function() {

                        var ex_type = $scope.refine.Selectedtype;
                        var body_part = $scope.refine.bodyPartid;
                        var machines = $scope.refine.Selectedequip;
                        var level = $scope.refine.Selectedlevel;

                        ////////////////// Filter Request Start/////////////////

                        var link = baseUrl+'/exercise/search_exercises';
                  var formData = {uu_id : uuid, user_id : user_id ,  exerciseType : ex_type, bodyPart : body_part  ,  wLevels : level, exmachines : machines}
                  var postData = 'myData='+JSON.stringify(formData);
                      $ionicLoading.show({
                                template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                       $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                      }).success(function(res){

                        // console.log(res);
                         $scope.NoResultFound =  res.code;
                        $scope.exercise = [];
                      $scope.allWorkouts = res.exercises;
                      for(var i=0; i< $scope.postlimit ; i++) {
                      if(i < res.exercises.length)
                      {
                        $scope.exercise.push(res.exercises[i]);
                       }

                      }
                      $scope.currentrecordsview=$scope.postlimit;
                      $scope.modal.hide();


                        // console.log($scope.exercise);



                          //
                          // $timeout(function() {
                          //     ionicMaterialMotion.fadeSlideInRight({
                          //         startVelocity: 3000
                          //     });
                          //     $ionicLoading.hide();
                          // }, 700);
                          //
                          // // Set Ink
                          // ionicMaterialInk.displayEffect();



                      }) .error(function(error){
                        $ionicLoading.hide();
                        // console.log('request not completed');
                      });







                        ///////////////// Filter Request End ///////////////////



                       };

  /**
   * @name loadmoreexercise
   * @todo To Load More Exercises
   * @return void function
   *
   **/
                      $scope.loadmoreexercise = function() {

                                            // console.log("In Scroll");
                                            var lim = $scope.currentrecordsview + $scope.postlimit;
                                            // console.log($scope.allWorkouts.length);
                                            for(var i=$scope.currentrecordsview;i< lim;i++)
                                            {
                                              if(i < $scope.allWorkouts.length)
                                              {
                                                $scope.exercise.push($scope.allWorkouts[i]);
                                              }
                                              else
                                              {
                                                $scope.moreexercise=false;
                                                break;
                                              }
                                            }
                                            // console.log($scope.exercise);
                                          $scope.currentrecordsview=lim;
                                          // $timeout(function() {
                                          //                                 ionicMaterialMotion.fadeSlideInRight({
                                          //                                     startVelocity: 3000
                                          //                                 });
                                          //                             }, 1000);
                                          //
                                          //                             // Set Ink
                                          //                             ionicMaterialInk.displayEffect();
                                            $scope.$broadcast('scroll.infiniteScrollComplete');
                                        };
                                        $scope.$on('$stateChangeSuccess', function() {
                                          $scope.loadmoreexercise();
                                        });

                        //refine modal
                      $ionicModal.fromTemplateUrl('templates/modal.html', {
                          scope: $scope
                        }).then(function(modal) {
                          $scope.modal = modal;
                          var user_id = $scope.userData.user_id;
                          var link = baseUrl+'/exercise/exercise_filters';
                          var formData = {uu_id : uuid, user_id : user_id}
                          var postData = 'myData='+JSON.stringify(formData);
                              $ionicLoading.show({
                                        template: '<ion-spinner icon="ios"></ion-spinner>'

                                    });
                               $http({
                                      method : 'POST',
                                      url : link,
                                      data: postData,
                                      headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                              }).success(function(res){

                                $scope.refine = res.exercise_filters;
                                // console.log($scope.refine);
                                  // $timeout(function() {
                                  //     ionicMaterialMotion.fadeSlideInRight({
                                  //         startVelocity: 3000
                                  //     });
                                  //     $ionicLoading.hide();
                                  // }, 700);
                                  //
                                  // // Set Ink
                                  // ionicMaterialInk.displayEffect();


                              }) .error(function(error){
                                $ionicLoading.hide();
                                // console.log('request not completed');
                              })
                        });


  /**
   * @name toggleGroup
   * @todo To Show or hide Group
   * @return void function
   *
   **/
                      $scope.toggleGroup = function(id) {
                          if ($scope.isGroupShown(id)) {
                            $scope.shownGroup = null;
                          } else {
                            $scope.shownGroup = id;
                          }
                        };
                        $scope.isGroupShown = function(id) {
                          return $scope.shownGroup === id;
                        };
  /**
   * @name focussearch
   * @todo To Focus input on Search Bar
   * @return void function
   *
   **/
  $scope.focussearch = function() {
    // do something awesome
    $ionicScrollDelegate.scrollTop();
    focus('searchStrman');
    $scope.show = "showInput";

  };
  $scope.showSearchIcon = true;
  $scope.change = function(text) {
    if($scope.searchStrman.search == ''){
      $scope.showSearchIcon = true;
    }
    // console.log(text);
    $scope.showSearchIcon = false;
  };
  $scope.emptySearch = function(text){
    $scope.showSearchIcon = true;
    $scope.searchStrman = {};
    $scope.searchStrman.search = null;
    $scope.searchStrman.search = '';
    text = '';

  }
  $scope.content = function(id){
    var dataWithHtml = $scope.allWorkouts[id].description;
    $scope.dotdot = '...';
    $scope.workDescription = $filter('limitTo')(dataWithHtml, 60, 0);
    if($scope.workDescription.length > 58){
      $scope.workDescription = $scope.workDescription + $scope.dotdot;
    }
    return $scope.workDescription = $sce.trustAsHtml($scope.workDescription);
  };
  /**
   * @name expired
   * @todo To Check User's Subscription if it is Expired or not
   * @return void function
   *
   **/
  $scope.expired = function(){
    var myPopup = $ionicPopup.show({
      template: '<div class="packageExpiredInner"><div class="selectOption" ng-click="closePopup()"><span>Not Now</span></div><div class="selectOption" ng-click="openBrowser()"><span>Renew Now</span></div></div>',
      subTitle: 'Oops! Your subscription has expired. Please renew your subscription.',
      cssClass: 'packageExpiredPopup',
      scope: $scope
    });

    myPopup.then(function(res) {

    });

    $timeout(function() {
      // myPopup.close(); //close the popup after 3 seconds for some reason
    }, 8000);
    $scope.closePopup = function() {

      myPopup.close();
    };
  };
  /**
   * @name openBrowser
   * @todo To Open In App Browser
   * @return void function
   *
   **/
  $scope.openBrowser = function(){
    var user_id = $scope.userData.user_id;
    var options = {
      location: 'yes',
      clearcache: 'yes',
      toolbar: 'no'
    };
    $cordovaInAppBrowser.open('http://www.fitnessbasecamp.com/loginUser_admin/'+user_id+'/app', '_system', options)
      .then(function(event) {
        // console.log(event);
      })
      .catch(function(event) {
        // error
      });


    // $cordovaInAppBrowser.close();
  }



});
