
	/**
	* Purpose:
	*@This file is used to save and create Custom Exercises.
	*
	**/

angular.module('starter').controller('addCustomExercises', function(quote, $rootScope,  $cordovaNetwork, $window, $ionicModal,focus,$ionicScrollDelegate, $ionicPopover, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading,$cordovaFileTransfer) {

$scope.deviceInfo = deviceInformation.getInfo();
var uuid = $scope.deviceInfo.uuid
$scope.siteUrl = siteUrl;
$scope.refine = {};
$scope.refine.Exname="";
$scope.refine.pic = 'img/no-image.jpg'
$scope.ex_pic_name="";
$scope.baseUrlForWorkoutImg = 'http://www.fitnessbasecamp.com/uploads/workouts/'
	/**
	* @name ionicPopover
	* @todo To Show Popover
	* @return void function
	*
	**/ 
$ionicPopover.fromTemplateUrl('templates/popover.html', {
    scope: $scope,
  }).then(function(popover) {
    $scope.popover = popover;
  });
	/**
	* @name isOnline
	* @todo To Check If Internet is connected or not
	* @return void function
	*
	**/  
$scope.isOnline = [];
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();


        // listen for Online event
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })

        // listen for Offline event
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
          //  console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);



                  var userData = $window.localStorage['userData'];
                   $scope.userData = angular.fromJson(userData);

	/**
	*@This Function is used to get Qoutes in custom Exercise Page.
	*
	**/ 

                  user_id = $scope.userData.user_id;
                   quote.getQuote(uuid, user_id).success(function (res) {
                                                                      
                                                                      $scope.quote = res;
                                                                      });
	/**
	*@This is used to get Exercise Filter.
	*
	**/ 
                  var link = baseUrl+'/exercise/exercise_filters';
                  var formData = {uu_id : uuid, user_id : user_id}
                  var postData = 'myData='+JSON.stringify(formData);
                      $ionicLoading.show({
                                template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                       $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                      }).success(function(res){
                         $ionicLoading.hide();
                      }) .error(function(error){
                        $ionicLoading.hide();
                      })
	/**
	* @name selectPicture
	* @todo To Select Pictures
	* @return void function
	*
	**/ 

                      $scope.selectPicture = function() {
                       document.addEventListener("deviceready", function () {
                          var options = {
                            quality: 100,
                            destinationType: Camera.DestinationType.FILE_URI,
                            sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
                            encodingType: Camera.EncodingType.JPEG,
                             targetWidth: 250,
                             targetHeight: 250,
                            allowEdit: true
                          };

                          $cordovaCamera.getPicture(options).then(
                          function(imageURI) {
                           $scope.uploadPicturex(imageURI);
                            $ionicLoading.show({template: 'Uploading Photo...', duration:500});
                          },
                          function(err){
                            $ionicLoading.show({template: 'Error While Uploading Image...', duration:500});
                          })
                           }, false);
                        };
	/**
	* @name addExercise
	* @todo To Add Exercises
	* @return void function
	*
	**/ 

                     $scope.addExercise = function(name, type, part, equip, level, url, description)
					 {
						 var user_id = $scope.userData.user_id;
                         var link = baseUrl+'/exercise/addCustomExercise';
                         var formData = {uu_id : uuid, user_id : user_id, exercise_name : name, exercise_type : type, embed_link : url, equipment_id : equip, difficulty_level : level, body_parts : part, description : description,exercise_image:$scope.ex_pic_name}
                         var postData = 'myData='+JSON.stringify(formData);
                             $ionicLoading.show({
                                       template: '<ion-spinner icon="ios"></ion-spinner>'

                                   });
                              $http({
                                     method : 'POST',
                                     url : link,
                                     data: postData,
                                     headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                             }).success(function(res){
                             if(res.code == 400)
                             {
                                $ionicLoading.show({template: 'All the fields must be filled.', duration:500});
                             }
                             else
                             {

                               $state.go('app.myexercises');
                              }

                             }) .error(function(error){
                               $ionicLoading.hide();
                   
                             })

                     };


	/**
	* @name uploadPicturex
	* @todo To Upload Pictures
	* @return void function
	*
	**/ 
 $scope.uploadPicturex = function(imageURI) {


  var options = {

  fileKey: "picture",

  filename: "p1.png",

  chunkedMode: false,

  mimeType: "image/png",
  trustAllHosts:true,
  headers:{Connection:"close"}

  };
 $ionicLoading.show({
                                             template: '<ion-spinner icon="ios"></ion-spinner><p>Uploading Image!</p>'

                                         });
  $cordovaFileTransfer.upload(encodeURI(baseUrl+"/exercise/upload_exercise_images"), imageURI, options)

  .then(function(result) {
   $scope.refine.pic= imageURI;;
 var  resk  = angular.fromJson(result.response);

          $scope.ex_pic_name = resk.image_name;

$ionicLoading.hide();


  }, function(err) {

  $ionicLoading.hide();
  $ionicLoading.show({template: 'An error occured while uploading your picture.<br>Please try again.', duration:500});

  }, function (progress) {

            });



  }




});
