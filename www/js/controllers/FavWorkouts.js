/**
 * Purpose:
 *@This file is used to Show Favorite Workout
 *
 **/

angular.module('starter').controller('FavWorkoutsCtrl', function($sce, $filter,$cordovaInAppBrowser, $cordovaToast, quote, $rootScope,  $cordovaNetwork, $window, $ionicModal,focus,$ionicScrollDelegate, $ionicPopover, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading) {
 // console.log(GuserData.user_fname);


$scope.deviceInfo = deviceInformation.getInfo();
var uuid = $scope.deviceInfo.uuid
$scope.layout = 'list';
  $scope.packageExpired = {};
$scope.refine = {};
  $scope.searchStrman = {};
$scope.baseUrlForWorkoutImg = 'http://www.fitnessbasecamp.com/uploads/category-images/'
//console.log($scope.workouts.search);
$scope.moreworkouts=false;
$scope.allWorkoutss={};
     $scope.postlimit = 6;
      $scope.workoutss=[];
      $scope.currentrecordsview = 0;
       var user_id="";
       $scope.isOnline = [];
             document.addEventListener("deviceready", function () {
               $scope.network = $cordovaNetwork.getNetwork();
               $scope.isOnline = $cordovaNetwork.isOnline();



               /**
                *
                * Listen for Online event
                *
                **/
               $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
                   $scope.isOnline = true;
                   $scope.network = $cordovaNetwork.getNetwork();
                   $cordovaToast.showLongBottom('You are Online !')

               })


               /**
                *
                * Listen for Offline event
                *
                **/
               $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
                   // console.log("got offline");
                   $scope.isOnline = false;
                   $scope.network = $cordovaNetwork.getNetwork();
                   $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
               })

             }, false);
if ($stateParams.result) {

            //////////// My Coding Starts /////////
                var userData = $window.localStorage['userData'];
                $scope.userData = angular.fromJson(userData);
              user_id = $scope.userData.user_id;


            $stateParams.result =angular.fromJson($stateParams.result);
            $scope.allWorkoutss = $stateParams.result.workouts;


            $scope.NoResultFound =  $stateParams.result.code;


            for(var i=0; i< $scope.postlimit ; i++) {
             if(i < $stateParams.result.workouts.length)
              {
                $scope.workoutss.push($stateParams.result.workouts[i]);
              }
            }
            $scope.currentrecordsview=$scope.postlimit;
           $scope.moreworkouts=true;
              if($stateParams.result.workouts.length > 6)
              {

              $scope.moreworkouts=true;

              }



            //////////// End ///////////////////
        } else{

//popover
$ionicPopover.fromTemplateUrl('templates/popover.html', {
    scope: $scope,
  }).then(function(popover) {
    $scope.popover = popover;
  });


  $scope.siteUrl = siteUrl;

                  var userData = $window.localStorage['userData'];
                   $scope.userData = angular.fromJson(userData);

                   var packageExpired = $window.localStorage['expired'];
                    $scope.packageExpired = angular.fromJson(packageExpired);

                  user_id = $scope.userData.user_id;
                  quote.getQuote(uuid, user_id).success(function (res) {
                                                    // console.log(res);
                                                    $scope.quote = res;
                                                    });
                  var link = baseUrl+'/workouts/favtWorkouts';
                  var formData = {uu_id : uuid, user_id : user_id}
                  var postData = 'myData='+JSON.stringify(formData);
                      $ionicLoading.show({
                                template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                       $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                      }).success(function(res){

                        // console.log(res);


                        $scope.allWorkoutss = res.allworkouts;
                        res.allworkouts  =  angular.fromJson(res.allworkouts);

                        // console.log("Mani");
                        if(res.allworkouts !== undefined){
                          for(var i=0; i< $scope.postlimit ; i++) {
                            if(i < $scope.allWorkoutss.length)
                            {
                              $scope.workoutss.push(res.allworkouts[i]);
                            }
                          }
                        }

                        $scope.currentrecordsview=$scope.postlimit;
                       $scope.moreworkouts=true;


                         $ionicLoading.hide();

                      }) .error(function(error){
                        $ionicLoading.hide();
                        // console.log('request not completed');
                      });
  /**
   * @name favorite
   * @todo To Add Favorite Workouts
   * @return void function
   *
   **/
$scope.favorite = function(workout_id){
                      // console.log('favorite');
                      var workout_id = workout_id;
                        var link = baseUrl+'/workouts/Togglefavrtunfavrtworkout';
                       var formData = {uu_id : uuid, user_id : user_id ,  workout_id : workout_id}
                       var postData = 'myData='+JSON.stringify(formData);
                        $ionicLoading.show({
                               template: '<ion-spinner icon="ios"></ion-spinner>'

                           });
                      $http({
                             method : 'POST',
                             url : link,
                             data: postData,
                             headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                     }).success(function(res){
                        $scope.allWorkoutss={};
                        $scope.workoutss=[];
                       // console.log(res);
                        $scope.allWorkoutss = res.allworkouts;
                        var alertPopup = $ionicPopup.alert({
                        title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                        template: res.message,
                          cssClass: 'loginErrorPopUp hideOk'
                        });
                        $timeout(function() {
                          alertPopup.close();

                        }, 2000);
                        $scope.allWorkoutss = res.allworkouts;
                        res.allworkouts  =  angular.fromJson(res.allworkouts);

                        // console.log("Mani");
                        // console.log(res.allworkouts[0]);
                        for(var i=0; i< $scope.postlimit ; i++) {
                          if(i < $scope.allWorkoutss.length)
                          {
                            $scope.workoutss.push(res.allworkouts[i]);
                          }
                        }
                        $scope.currentrecordsview=$scope.postlimit;
                        $scope.moreworkouts=true;
                        $ionicLoading.hide();
                     }) .error(function(error){
                       $ionicLoading.hide();
                       // console.log('request not completed');
                     });

                      }

  /**
   * @name focussearch
   * @todo To Focus input in search bar
   * @return void function
   *
   **/
  $scope.focussearch = function() {
    // do something awesome
    $ionicScrollDelegate.scrollTop();
    focus('searchStrman');
    $scope.show = "showInput";

  };
  $scope.showSearchIcon = true;
  $scope.change = function(text) {
    if($scope.searchStrman == ''){
      $scope.showSearchIcon = true;
    }
    // console.log(text);
    $scope.showSearchIcon = false;
  };
  $scope.emptySearch = function(text){
    $scope.showSearchIcon = true;
    $scope.searchStrman = {};
    $scope.searchStrman.search = null;
    $scope.searchStrman.search = '';
    text = '';

  }



}
  /**
   * @name content
   * @todo To Render service data as html and applying character limit
   * @return void function
   *
   **/
  $scope.content = function(id){
    var dataWithHtml = $scope.allWorkoutss[id].description;
    $scope.dotdot = '...';
    $scope.workDescription = $filter('limitTo')(dataWithHtml, 60, 0);
    if($scope.workDescription.length > 58){
      $scope.workDescription = $scope.workDescription + $scope.dotdot;
    }
    return $scope.workDescription = $sce.trustAsHtml($scope.workDescription);
  };
$scope.permissions= angular.fromJson($window.localStorage['Permissions']);
  /**
   * @name inArray
   * @todo To Render service data as html and applying character limit
   * @return void function
   *
   **/
$scope.inArray = function (needle) {
haystack  = $scope.permissions;
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle){ return true;

        } ;
    }
    return "noPermission";
}
  /**
   * @name Npermission
   * @todo To Check Package Expiry
   * @return void function
   *
   **/
 $scope.Npermission = function(is_perm)
                                  {

                                      if(is_perm != true)
                                       {

                                          var alertPopup = $ionicPopup.alert({
                                            title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                            template: "You don't have permission to do this.Please upgrade your package.",
                                            cssClass: 'loginErrorPopUp'
                                          });
                                       }

                                  }
  /**
   * @name addWorkoutx
   * @todo To Add Workout
   * @return void function
   *
   **/
$scope.addWorkoutx = function()
 {
if($scope.inArray('Add Custom Workout')==true)
{

 $state.go('app.addCustomWorkout');
}
else
                                      {

 var alertPopup = $ionicPopup.alert({
                                        title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
                                        template: "You don't have permission to do this.Please upgrade your package.",
                                        cssClass: 'loginErrorPopUp'
                                      });
                                      }
 }
  /**
   * @name loadmoreworkouts
   * @todo To Load More Workouts
   * @return void function
   *
   **/
$scope.loadmoreworkouts = function() {

            // console.log("In Scroll Workouts");
            if($scope.allWorkoutss !== undefined){
              var lim = $scope.currentrecordsview + $scope.postlimit;

              for(var i=$scope.currentrecordsview;i< lim;i++)
              {
                if(i < $scope.allWorkoutss.length)
                {
                  $scope.workoutss.push($scope.allWorkoutss[i]);
                }
                else
                {
                  $scope.moreworkouts=false;
                  break;
                }
              }
            }

             // console.log($scope.workouts);
            $scope.currentrecordsview=lim;

              $scope.$broadcast('scroll.infiniteScrollComplete');
        };
        $scope.$on('$stateChangeSuccess', function() {
          $scope.loadmoreworkouts();
        });
                      $scope.filterWorkouts = function() {

                        var w_type = $scope.refine.Selectedtype;
                        var machines = $scope.refine.Selectedequip;
                        var level = $scope.refine.Selectedlevel;
                        var Selectedintensity = $scope.refine.Selectedintensity;
                        var Selectedduration = $scope.refine.Selectedduration;
                        ////////////////// Filter Request Start/////////////////

                        var link = baseUrl+'/workouts/search_workouts';
                        var formData = {uu_id : uuid, user_id : user_id ,  workout_type : w_type,  wLevels : level, exmachines : machines,duration:Selectedduration,wGoals:Selectedintensity}
                        var postData = 'myData='+JSON.stringify(formData);
                      $ionicLoading.show({
                                template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                       $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                      }).success(function(res){

                        // console.log(res);
                        $scope.NoResultFound =  res.code;
                        $scope.workoutss = [];
                        $scope.allWorkoutss=[];
                      $scope.allWorkoutss = res.workouts;
                      for(var i=0; i< $scope.postlimit ; i++) {

                      if(i < $scope.allWorkoutss.length )
                        {
                          $scope.workoutss.push(res.workouts[i]);
                        }


                      }
                      if(res.workouts.length > 6)
                      {
                        $scope.moreworkouts=true;

                      }
                      else
                      {

                      $scope.moreworkouts=false;

                      }
                      $scope.currentrecordsview=$scope.postlimit;
                      $scope.modal.hide();
                      }) .error(function(error){
                        $ionicLoading.hide();
                        // console.log('request not completed');
                      });


                      //favorite workout

                      $scope.favorite = function(){
                        // console.log('favorite');
                      }

                        ///////////////// Filter Request End ///////////////////



                       };

                        //refine modal
                      $ionicModal.fromTemplateUrl('templates/modal.html', {
                          scope: $scope
                        }).then(function(modal) {
                          $scope.modal = modal;
                          var user_id = $scope.userData.user_id;
                          var link = baseUrl+'/workouts/workoutfilter';
                          var formData = {uu_id : uuid, user_id : user_id}
                          var postData = 'myData='+JSON.stringify(formData);
                              $ionicLoading.show({
                                        template: '<ion-spinner icon="ios"></ion-spinner>'

                                    });
                               $http({
                                      method : 'POST',
                                      url : link,
                                      data: postData,
                                      headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                              }).success(function(res){

                                $scope.refine = res.workoutsfilter;



                              }) .error(function(error){
                                $ionicLoading.hide();
                                // console.log('request not completed');
                              })
                        });
                        $scope.$on('modal.shown', function() {

                                   });

  /**
   * @name toggleGroup
   * @todo To Hide or Show Workout Groups
   * @return void function
   *
   **/
                      $scope.toggleGroup = function(id) {
                          if ($scope.isGroupShown(id)) {
                            $scope.shownGroup = null;
                          } else {
                            $scope.shownGroup = id;
                          }
                        };
                        $scope.isGroupShown = function(id) {
                          return $scope.shownGroup === id;
                        };
  /**
   * @name expired
   * @todo To Check if user subscription is expired or not
   * @return void function
   *
   **/
  $scope.expired = function(){
    var myPopup = $ionicPopup.show({
      template: '<div class="packageExpiredInner"><div class="selectOption" ng-click="closePopup()"><span>Not Now</span></div><div class="selectOption" ng-click="openBrowser()"><span>Renew Now</span></div></div>',
      subTitle: 'Oops! Your subscription has expired. Please renew your subscription.',
      cssClass: 'packageExpiredPopup',
      scope: $scope
    });

    myPopup.then(function(res) {

    });

    $timeout(function() {
      // myPopup.close(); //close the popup after 3 seconds for some reason
    }, 8000);
    $scope.closePopup = function() {

      myPopup.close();
    };
  };
  $scope.openBrowser = function(){
    var user_id = $scope.userData.user_id;
    var options = {
      location: 'yes',
      clearcache: 'yes',
      toolbar: 'no'
    };
    $cordovaInAppBrowser.open('http://www.fitnessbasecamp.com/loginUser_admin/'+user_id+'/app', '_system', options)
      .then(function(event) {
        // console.log(event);
      })
      .catch(function(event) {
        // error
      });


    // $cordovaInAppBrowser.close();
  }

});
