/**
 * Purpose:
 *@This file is used as a wrapper to timer section
 *
 **/



angular.module('starter').controller('timerController', function(notification, $cordovaInAppBrowser, $window, quote, deviceInformation, $scope, $cordovaSQLite, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $ionicLoading, $ionicPopup) {
var userData = $window.localStorage['userData']
               $scope.userData = angular.fromJson(userData);
  $scope.packageExpired = {};


  /**
   *
   * to keep awake while timer is running
   *
   **/



  window.plugins.insomnia.keepAwake();
             //  console.log("Omar User data consoling "+JSON.stringify($scope.userData));

  /**
   *
   * Initializing Variables, Arrays, and Objects
   *
   **/
                                $scope.userImg = $scope.userData.image;
                                $scope.username = $scope.userData.user_fname;
                                $scope.Lusername = $scope.userData.user_lname;

                               // console.log("Userimg Source== "+$scope.userImg);

                                var user_id = $scope.userData.user_id;

  /**
   *
   * for quotes
   *
   **/

                                quote.getQuote(uuid, user_id).success(function (res) {
                                // console.log(res);
                                  $ionicLoading.hide();
                                $scope.quote = res;
                                });

  $scope.notificationFull = [];
  $scope.notification = [];
  $scope.$on('$ionicView.beforeEnter', function(){
    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner>'

    });
    notification.get(uuid, user_id).success(function (res) {

      var notifications = res;
      $window.localStorage['notifications']  = JSON.stringify(notifications);
      var localNoti = $window.localStorage['notifications'];
      $scope.notification = angular.fromJson(localNoti);
      $scope.notificationFull = $scope.notification.notification;
      if($scope.notificationFull.length > 0){
        if($scope.notificationFull[0].count == 0){
          $scope.green = "green";
        } else if($scope.notificationFull[0].count > 0){
          $scope.green = "red";
        }
      }

      $scope.changeColor = function(){
        $scope.green = "green";

        //;


      }
    });
  });
  /**
   * @name expired
   * @todo To check if package expired
   * @return void function
   *
   **/

  var packageExpired = $window.localStorage['expired'];
  $scope.packageExpired = angular.fromJson(packageExpired);
  $scope.expired = function(){
    var myPopup = $ionicPopup.show({
      template: '<div class="packageExpiredInner"><div class="selectOption" ng-click="closePopup()"><span>Not Now</span></div><div class="selectOption" ng-click="openBrowser()"><span>Renew Now</span></div></div>',
      subTitle: 'Oops! Your subscription has expired. Please renew your subscription.',
      cssClass: 'packageExpiredPopup',
      scope: $scope
    });

    myPopup.then(function(res) {

    });

    $timeout(function() {
      // myPopup.close(); //close the popup after 3 seconds for some reason
    }, 8000);
    $scope.closePopup = function() {

      myPopup.close();
    };
  };


  /**
   * @name openBrowser
   * @todo To open InAppBrowser
   * @return void function
   *
   **/




  $scope.openBrowser = function(){
    var user_id = $scope.userData.user_id;
    var options = {
      location: 'yes',
      clearcache: 'yes',
      toolbar: 'no'
    };
    $cordovaInAppBrowser.open('http://www.fitnessbasecamp.com/loginUser_admin/'+user_id+'/app', '_system', options)
      .then(function(event) {
        // console.log(event);
      })
      .catch(function(event) {
        // error
      });


    // $cordovaInAppBrowser.close();
  }
});
