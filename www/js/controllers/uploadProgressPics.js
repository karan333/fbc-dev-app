/**
 * Purpose:
 *@This file is used to upload progress photos.
 *
 **/

angular.module('starter').controller('uploadProgressPics', function($ionicModal, quote, $rootScope,  $cordovaNetwork, $cordovaToast, $cordovaNetwork, $rootScope, $cordovaPreferences, $ionicBackdrop, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading, $cordovaFileTransfer,$q) {


  /**
   *
   * Initializing Variables, Arrays, and Objects
   *
   **/


$scope.upload = [];
$scope.upload.pic = [];
$scope.upload.imagename1="";
$scope.upload.imagename2="";
$scope.upload.imagename3="";
$scope.upload.imagename4="";
$scope.imageID = null;
$scope.imageID2 = null;
$scope.imageID3 = null;
$scope.imageID4 = null;

$scope.finalImage = {
  id : null,
  document : null,
  filName : null,
  fileType : null

}
$scope.finalImage2 = {
  id : null,
  document : null,
  filName : null,
  fileType : null

}
$scope.finalImage3 = {
  id : null,
  document : null,
  filName : null,
  fileType : null

}
$scope.finalImage4 = {
  id : null,
  document : null,
  filName : null,
  fileType : null

}

$scope.isOnline = [];
      document.addEventListener("deviceready", function () {
        $scope.network = $cordovaNetwork.getNetwork();
        $scope.isOnline = $cordovaNetwork.isOnline();



        /**
         *
         * Listen for Online event
         *
         **/



        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.isOnline = true;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('You are Online !')

        })


        /**
         *
         * Listen for Offline event
         *
         **/
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            // console.log("got offline");
            $scope.isOnline = false;
            $scope.network = $cordovaNetwork.getNetwork();
            $cordovaToast.showLongBottom('No Internet Connection ! Please Connect to Internet')
        })

      }, false);



  var userData = $window.localStorage['userData'];
  $scope.userData = angular.fromJson(userData);
  // console.log($scope.userData);


  //  console.log("Omar User data consoling "+JSON.stringify($scope.userData));

  // after getting data from sqlite getting user id and then running dashboard service
  $scope.userImg = $scope.userData.image;
  $scope.username = $scope.userData.user_fname;
  $scope.Lusername = $scope.userData.user_lname;

  // console.log("Userimg Source== "+$scope.userImg);

  var user_id = $scope.userData.user_id;
  /**
   *
   * initialize modal
   *
   **/
  $scope.createModal = function() {
    $ionicModal.fromTemplateUrl('templates/crop.html', {
      id: '1', // We need to use and ID to identify the modal that is firing the event!
      scope: $scope,
      backdropClickToClose: false,
      animation: 'animated slideInRight'
    }).then(function(modal) {
      $scope.Modal1 = modal;
      $scope.Modal1.show();
    });
  }

  /**
   *
   * initialize modal
   *
   **/
  $ionicModal.fromTemplateUrl('templates/crop2.html', {
    id: '2', // We need to use and ID to identify the modal that is firing the event!
    scope: $scope,
    backdropClickToClose: false,
    animation: 'animated slideInRight'
  }).then(function(modal) {
    $scope.Modal2 = modal;
  });
  /**
   *
   * initialize modal
   *
   **/
  $ionicModal.fromTemplateUrl('templates/crop3.html', {
    id: '3', // We need to use and ID to identify the modal that is firing the event!
    scope: $scope,
    backdropClickToClose: false,
    animation: 'animated slideInRight'
  }).then(function(modal) {
    $scope.Modal3 = modal;
  });
  /**
   *
   * initialize modal
   *
   **/
  $ionicModal.fromTemplateUrl('templates/crop4.html', {
    id: '4', // We need to use and ID to identify the modal that is firing the event!
    scope: $scope,
    backdropClickToClose: false,
    animation: 'animated slideInRight'
  }).then(function(modal) {
    $scope.Modal4 = modal;
  });


  /**
   *
   * show modal
   *
   **/
  $scope.openModal = function(index) {
    if (index == 1) $scope.Modal1.show();
    else if (index == 2) $scope.Modal2.show();
    else if (index == 3) $scope.Modal3.show();
    else if (index == 4) $scope.Modal4.show();
  };
  /**
   *
   * close modal
   *
   **/
  $scope.closeModal = function(index) {
    if (index == 1) $scope.Modal1.remove();
    else if(index == 2) $scope.Modal2.hide();
    else if(index == 3) $scope.Modal3.hide();
    else if(index == 4) $scope.Modal4.hide();
  };


  /**
   * @name selectUploadMethod
   * @todo To show popup for adding photo from camera or gallery
   * @return void function
   *
   **/

                              $scope.selectUploadMethod = function(id){
                                var id = id;
                                // console.log(id);
                                var myPopup = $ionicPopup.show({
                                    template: '<div class="uploadMethodSack"><ul><li><a class="ink" ng-click="takePicture('+id+');"><span class="ion-camera"></span><span>Upload from Camera</span></a></li><li><a class=ink"" ng-click="selectPicture('+id+');"><span class="ion-images"></span><span>Upload from Gallery</span></a></li></ul></div>',
                                    title: 'Choose Upload Method',
                                    subTitle: 'Select from Camera or Gallery',
                                    scope: $scope
                                  });

                                  myPopup.then(function(res) {
                                    // console.log('Tapped!', res);
                                  });

                                  $timeout(function() {
                                     myPopup.close(); //close the popup after 3 seconds for some reason
                                  }, 8000);
                              }



  /**
   * @name takePicture
   * @todo To add photo from camera
   * @return void function
   *
   **/


                                $scope.takePicture = function(id){
                                // console.log(id);
                                   document.addEventListener("deviceready", function () {

                                      var options = {

                                        destinationType: Camera.DestinationType.FILE_URI,
                                        sourceType: Camera.PictureSourceType.CAMERA,
                                        allowEdit: false
                                      };

                                     $cordovaCamera.getPicture(options).then(function(imageURI) {


                                         if(id == 0)
                                         {
                                           console.log(imageURI)
                                           $scope.imageURI = imageURI;
                                           $scope.createModal();
                                           $scope.imageID = id;

                                           $scope.$on('modal.shown', function(event, modal) {
                                             if(modal.id === '1'){
                                               $scope.cropPicture(id);
                                             }
                                           });
                                         }
                                         if(id == 1)
                                         {
                                           $scope.imageURI2 = imageURI;
                                           $scope.Modal2.show();
                                           $scope.imageID2 = id;

                                           $scope.$on('modal.shown', function(event, modal) {
                                             if(modal.id === '2'){
                                               $scope.cropPicture2(id);
                                             }
                                           });
                                         }
                                         if(id == 2)
                                         {
                                           $scope.imageURI3 = imageURI;
                                           $scope.Modal3.show();
                                           $scope.imageID3 = id;

                                           $scope.$on('modal.shown', function(event, modal) {
                                             if(modal.id === '3'){
                                               $scope.cropPicture3(id);
                                             }
                                           });
                                         }
                                         if(id == 3)
                                         {
                                           $scope.imageURI4 = imageURI;
                                           $scope.Modal4.show();
                                           $scope.imageID4 = id;

                                           $scope.$on('modal.shown', function(event, modal) {
                                             if(modal.id === '4'){
                                               $scope.cropPicture4(id);
                                             }
                                           });
                                         }





                                        $ionicLoading.show({template: 'Uploading Photo...', duration:500});
                                      },
                                      function(err){
                                        $ionicLoading.show({template: 'Error While Uploading Image...', duration:500});
                                      })

                                    }, false);
                                }

  /**
   * @name selectPicture
   * @todo To add photo from gallery
   * @return void function
   *
   **/
                                  $scope.selectPicture = function(id) {
                                  // console.log(id);
                                   document.addEventListener("deviceready", function () {
                                      var options = {
                                        quality: 100,
                                        destinationType: Camera.DestinationType.FILE_URI,
                                        sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
                                        encodingType: Camera.EncodingType.JPEG,
                                        targetWidth: 346,
                                        targetHeight: 508,
                                        allowEdit: false
                                      };

                                      $cordovaCamera.getPicture(options).then(
                                      function(imageURI) {
                                     // console.log("Mani");
                                        if(id == 0)
                                        {
                                          console.log(imageURI)
                                          $scope.imageURI = imageURI;
                                          $scope.createModal();
                                          $scope.imageID = id;

                                          $scope.$on('modal.shown', function(event, modal) {
                                            if(modal.id === '1'){
                                              $scope.cropPicture(id);
                                            }
                                          });
                                        }
                                        if(id == 1)
                                        {
                                          $scope.imageURI2 = imageURI;
                                          $scope.Modal2.show();
                                          $scope.imageID2 = id;

                                          $scope.$on('modal.shown', function(event, modal) {
                                            if(modal.id === '2'){
                                              $scope.cropPicture2(id);
                                            }
                                          });
                                        }
                                        if(id == 2)
                                        {
                                          $scope.imageURI3 = imageURI;
                                          $scope.Modal3.show();
                                          $scope.imageID3 = id;

                                          $scope.$on('modal.shown', function(event, modal) {
                                            if(modal.id === '3'){
                                              $scope.cropPicture3(id);
                                            }
                                          });
                                        }
                                        if(id == 3)
                                        {
                                          $scope.imageURI4 = imageURI;
                                          $scope.Modal4.show();
                                          $scope.imageID4 = id;

                                          $scope.$on('modal.shown', function(event, modal) {
                                            if(modal.id === '4'){
                                              $scope.cropPicture4(id);
                                            }
                                          });
                                        }
                                        $ionicLoading.show({template: 'Uploading Photo...', duration:500});
                                      },
                                      function(err){
                                        $ionicLoading.show({template: 'Error While Uploading Image...', duration:500});
                                      })
                                       }, false);
                                    };




  /**
   * @name cropPicture
   * @todo To crop pictures
   * @return void function
   *
   **/
  var imageNameInc = 0;
  var cropper;
  $scope.cropPicture = function(id){
    cropper = null;
    var image = document.getElementById('cropImg');
    cropper = new Cropper(image, {
      modal: true,
      guides: true,
      highlight: false,
      background: true,
      autoCrop: true,
      responsive: true,
      ready: function () {

        // this.cropper.crop();
      },
      cropend : function () {
        console.log('crop ended');
      }
    });
  };


  /**
   * @name cropPicture
   * @todo convert cropped canvas to image
   * @return void function
   *
   **/

  $scope.finishCrop = function (id) {
    imageNameInc++;
    console.log(cropper);
    var image = document.getElementById('cropImg');
    console.log(image);
    var croppedImg = cropper.getCroppedCanvas({
      width: 500,
      height: 500
    });
    $scope.finalImage.id = id;
    $scope.finalImage.document = croppedImg.toDataURL("image/png");
    $scope.finalImage.fileName = makeid();
    $scope.finalImage.fileType = "png";
    cropper.reset();
    cropper.destroy();
    // image.cropper('destroy');
    // $('#cropImg').cropper('destroy');
    window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function(dirEntry) {

      // Setup filename and assume a jpg file
      var filename =  ($scope.finalImage.fileName ? $scope.finalImage.fileName : 'image') + "." + ($scope.finalImage.fileType ? $scope.finalImage.fileType : "png");
      dirEntry.getFile(filename, { create: true, exclusive: false }, function(fileEntry) {
        // attachment.document holds the base 64 data at this moment
        var binary = base64toBlob($scope.finalImage.document, 'image/png');
        writeFile(fileEntry, binary).then(function() {
          // Store file url for later reference, base 64 data is no longer required
          $scope.finalImage.document = fileEntry.nativeURL;

          $scope.closeModal(1);
          $scope.uploadPicturex($scope.finalImage.document,id);

        }, function(error) {
          WL.Logger.error("Error writing local file: " + error);
          reject(error.code);
        });

      }, function(errorCreateFile) {
        WL.Logger.error("Error creating local file: " + JSON.stringify(errorCreateFile));
        reject(errorCreateFile.code);
      });

    }, function(errorCreateFS) {
      WL.Logger.error("Error getting filesystem: " + errorCreateFS);
      reject(errorCreateFS.code);
    });


  };


  /**
   * @name cropPicture
   * @todo To crop pictures
   * @return void function
   *
   **/
  var cropper2;
  $scope.cropPicture2 = function(id){
    cropper2 = null;
    console.log(cropper2);

    var image = document.getElementById('cropImg2');
    cropper2 = new Cropper(image, {
      modal: true,
      guides: true,
      highlight: false,
      background: true,
      autoCrop: true,
      responsive: true,

      ready: function () {

        // this.cropper2.crop();
  }
    });
  };

  /**
   * @name cropPicture
   * @todo convert cropped canvas to image
   * @return void function
   *
   **/
  $scope.finishCrop2 = function (id) {
    imageNameInc++;
    console.log(cropper2);
    var image = document.getElementById('cropImg2');
    var croppedImg = cropper2.getCroppedCanvas({
      width: 500,
      height: 500
    });
    console.log(croppedImg);
    $scope.finalImage2.id = id;
    $scope.finalImage2.document = croppedImg.toDataURL("image/png");
    $scope.finalImage2.fileName = makeid();
    $scope.finalImage2.fileType = "png";
    cropper2.reset();
    cropper2.destroy();
    window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function(dirEntry) {

      // Setup filename and assume a jpg file
      var filename =  ($scope.finalImage2.fileName ? $scope.finalImage2.fileName : 'image') + "." + ($scope.finalImage2.fileType ? $scope.finalImage2.fileType : "png");
      dirEntry.getFile(filename, { create: true, exclusive: false }, function(fileEntry) {
        // attachment.document holds the base 64 data at this moment
        var binary = base64toBlob($scope.finalImage2.document, 'image/png');
        writeFile(fileEntry, binary).then(function() {
          // Store file url for later reference, base 64 data is no longer required
          $scope.finalImage2.document = fileEntry.nativeURL;

          $scope.closeModal(2);
          $scope.uploadPicturex($scope.finalImage2.document,id);

        }, function(error) {
          WL.Logger.error("Error writing local file: " + error);
          reject(error.code);
        });

      }, function(errorCreateFile) {
        WL.Logger.error("Error creating local file: " + JSON.stringify(errorCreateFile));
        reject(errorCreateFile.code);
      });

    }, function(errorCreateFS) {
      WL.Logger.error("Error getting filesystem: " + errorCreateFS);
      reject(errorCreateFS.code);
    });

  };


  /**
   * @name cropPicture
   * @todo To crop pictures
   * @return void function
   *
   **/
  var cropper3;
  $scope.cropPicture3 = function(id){
    cropper3 = null;
    var image = document.getElementById('cropImg3');

    cropper3 = new Cropper(image, {
      modal: true,
      guides: true,
      highlight: false,
      background: true,
      autoCrop: true,
      responsive: true,

      ready: function () {

        // this.cropper3.crop();
      }
    });
  };

  /**
   * @name cropPicture
   * @todo convert cropped canvas to image
   * @return void function
   *
   **/
  $scope.finishCrop3 = function (id) {
    imageNameInc++;
    console.log(cropper3);
    var image = document.getElementById('cropImg3');
    var croppedImg = cropper3.getCroppedCanvas({
      width: 500,
      height: 500
    });
    console.log(croppedImg);
    $scope.finalImage3.id = id;
    $scope.finalImage3.document = croppedImg.toDataURL("image/png");
    $scope.finalImage3.fileName = makeid();
    $scope.finalImage3.fileType = "png";
    cropper3.reset();
    cropper3.destroy();
    window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function(dirEntry) {

      // Setup filename and assume a jpg file
      var filename =  ($scope.finalImage3.fileName ? $scope.finalImage3.fileName : 'image') + "." + ($scope.finalImage3.fileType ? $scope.finalImage3.fileType : "png");
      dirEntry.getFile(filename, { create: true, exclusive: false }, function(fileEntry) {
        // attachment.document holds the base 64 data at this moment
        var binary = base64toBlob($scope.finalImage3.document, 'image/png');
        writeFile(fileEntry, binary).then(function() {
          // Store file url for later reference, base 64 data is no longer required
          $scope.finalImage3.document = fileEntry.nativeURL;

          $scope.closeModal(3);
          $scope.uploadPicturex($scope.finalImage3.document,id);

        }, function(error) {
          WL.Logger.error("Error writing local file: " + error);
          reject(error.code);
        });

      }, function(errorCreateFile) {
        WL.Logger.error("Error creating local file: " + JSON.stringify(errorCreateFile));
        reject(errorCreateFile.code);
      });

    }, function(errorCreateFS) {
      WL.Logger.error("Error getting filesystem: " + errorCreateFS);
      reject(errorCreateFS.code);
    });

  };


  /**
   * @name cropPicture
   * @todo To crop pictures
   * @return void function
   *
   **/
  var cropper4;
  $scope.cropPicture4 = function(id){
    cropper4 = null;
    var image = document.getElementById('cropImg4');
    cropper4 = new Cropper(image, {
      modal: true,
      guides: true,
      highlight: false,
      background: true,
      autoCrop: true,
      responsive: true,

      ready: function () {

        // this.cropper4.crop();
      }
    });
  };

  /**
   * @name cropPicture
   * @todo convert cropped canvas to image
   * @return void function
   *
   **/
  $scope.finishCrop4 = function (id) {
    imageNameInc++;
    var image = document.getElementById('cropImg4');
    console.log(cropper4);
    var croppedImg = cropper4.getCroppedCanvas({
      width: 500,
      height: 500
    });
    console.log(croppedImg);
    $scope.finalImage4.id = id;
    $scope.finalImage4.document = croppedImg.toDataURL("image/png");
    $scope.finalImage4.fileName = makeid();
    $scope.finalImage4.fileType = "png";
    cropper4.reset();
    cropper4.destroy();
    window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function(dirEntry) {

      // Setup filename and assume a jpg file
      var filename =  ($scope.finalImage4.fileName ? $scope.finalImage4.fileName : 'image') + "." + ($scope.finalImage4.fileType ? $scope.finalImage4.fileType : "png");
      dirEntry.getFile(filename, { create: true, exclusive: false }, function(fileEntry) {
        // attachment.document holds the base 64 data at this moment
        var binary = base64toBlob($scope.finalImage4.document, 'image/png');
        writeFile(fileEntry, binary).then(function() {
          // Store file url for later reference, base 64 data is no longer required
          $scope.finalImage4.document = fileEntry.nativeURL;

          $scope.closeModal(4);
          $scope.uploadPicturex($scope.finalImage4.document,id);

        }, function(error) {
          WL.Logger.error("Error writing local file: " + error);
          reject(error.code);
        });

      }, function(errorCreateFile) {
        WL.Logger.error("Error creating local file: " + JSON.stringify(errorCreateFile));
        reject(errorCreateFile.code);
      });

    }, function(errorCreateFS) {
      WL.Logger.error("Error getting filesystem: " + errorCreateFS);
      reject(errorCreateFS.code);
    });

  };
  function base64toBlob(base64Data, contentType) {
    contentType = contentType || '';
    var sliceSize = 1024;
    var byteCharacters = atob(base64Data.replace(/^data:image\/(png|jpg);base64,/,''));
    var bytesLength = byteCharacters.length;
    var slicesCount = Math.ceil(bytesLength / sliceSize);
    var byteArrays = new Array(slicesCount);

    for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
      var begin = sliceIndex * sliceSize;
      var end = Math.min(begin + sliceSize, bytesLength);

      var bytes = new Array(end - begin);
      for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
        bytes[i] = byteCharacters[offset].charCodeAt(0);
      }
      byteArrays[sliceIndex] = new Uint8Array(bytes);
    }
    return new Blob(byteArrays, { type: contentType });
  }
  function writeFile(fileEntry, dataObj) {
    return $q(function(resolve, reject) {
      // Create a FileWriter object for our FileEntry (log.txt).
      fileEntry.createWriter(function(fileWriter) {

        fileWriter.onwriteend = function() {
          console.log('file created successfully');
          resolve();
        };

        fileWriter.onerror = function(e) {
          console.log("Failed file write: " + e.toString());
          reject(e);
        };

        // If data object is not passed in,
        // create a new Blob instead.
        if (!dataObj) {
          dataObj = new Blob(['missing data'], { type: 'text/plain' });
        }

        fileWriter.write(dataObj);
      });
    })
  }

  /**
   * @name makeid
   * @todo To generate random name for images
   * @return void function
   *
   **/
  function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }
  /**
   * @name uploadPicturex
   * @todo To upload series of photos to remote server
   * @return void function
   *
   **/
 $scope.uploadPicturex = function(imageURI,id) {

// console.log("In Upload Picture Function");
console.log(imageURI);
  var options = {

  fileKey: "picture",

  filename: "p1.png",

  chunkedMode: false,

  mimeType: "image/jpg",
  trustAllHosts:true,
  headers:{Connection:"close"}

  };
 $ionicLoading.show({
                                             template: '<ion-spinner icon="ios"></ion-spinner><p>Uploading Image!</p>'

                                         });


  $cordovaFileTransfer.upload(encodeURI("http://www.fitnessbasecamp.com/services/dashboard/upload_progress_images"), imageURI, options)

  .then(function(result) {
  console.log(result.response);
  $scope.upload.pic[id] = imageURI;
 var  resk  = angular.fromJson(result.response);
  if(id == 0)
  {
    $scope.upload.imagename1 = resk.image_name;
    cropper.reset();
  }
  if(id == 1)
    {
      $scope.upload.imagename2 = resk.image_name;
      cropper2.reset();
    }
    if(id == 2)
      {
        $scope.upload.imagename3 = resk.image_name;
        cropper3.reset();
      }
      if(id == 3)
        {
          $scope.upload.imagename4 = resk.image_name;
          cropper4.reset();
        }
// console.log($scope.upload);
$ionicLoading.hide();


  }, function(err) {

  // console.log(err);
  $ionicLoading.hide();
  $ionicLoading.show({template: 'An error occured while uploading your picture.<br>Please try again.', duration:500});

  }, function (progress) {

            });



  }
  /**
   * @name $stateChangeStart
   * @todo To destroy crop on state change
   * @return void function
   *
   **/
  $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
    if(cropper){
      cropper.reset();
      cropper.destroy();
      cropper2.reset();
      cropper2.destroy();
      cropper3.reset();
      cropper3.destroy();
      cropper4.reset();
      cropper4.destroy();
    }


    console.log('print change')
  });

  /**
   * @name AddProgressPhoto
   * @todo To add progress pictures to database
   * @return void function
   *
   **/



$scope.AddProgressPhoto = function() {

 var userData = $window.localStorage['userData'];
                    $scope.userData = angular.fromJson(userData);

              // after getting data from sqlite getting user id and then all news feed service

              var user_id = $scope.userData.user_id;
              var workout_id = $stateParams.cid;

              var link = baseUrl+'/dashboard/add_progress_photos';
              var formData = {uu_id : uuid, user_id : user_id, workout_id : workout_id,p1:$scope.upload.imagename1,p2:$scope.upload.imagename2,p3:$scope.upload.imagename3,p4:$scope.upload.imagename4}
              var postData = 'myData='+JSON.stringify(formData);
                  $ionicLoading.show({
                            template: '<ion-spinner icon="ios"></ion-spinner>'

                        });
                   $http({
                          method : 'POST',
                          url : link,
                          data: postData,
                          headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                  }).success(function(res){
                  $ionicLoading.hide();
                  $state.go('app.allProgressPhotos');


                   });



}


});
