/**
 * Purpose:
 *@This file is used to Edit Custom Workout
 *
 **/

angular.module('starter').controller('editCustomWorkout', function ($cordovaToast, quote, $rootScope, $cordovaNetwork, $window, $ionicModal, focus, $ionicScrollDelegate, $ionicPopover, deviceInformation, $ionicPopup, $cordovaFile, $cordovaCamera, $state, loginService, $scope, $window, $stateParams, $http, $timeout, ionicMaterialMotion, ionicMaterialInk, $ionicSideMenuDelegate, $cordovaSQLite, $ionicLoading, $cordovaFileTransfer) {
// console.log(GuserData.user_fname);
//popover
  $ionicPopover.fromTemplateUrl('templates/popover.html', {
    scope: $scope,
  }).then(function (popover) {
    $scope.popover = popover;
  });

  $scope.deviceInfo = deviceInformation.getInfo();
  var uuid = $scope.deviceInfo.uuid;
  $scope.searchStrman = {};
  $scope.layout = 'list';
  $scope.refine = {};
  $scope.baseUrlForWorkoutImg = 'http://www.fitnessbasecamp.com/uploads/workouts/'
//console.log($scope.workouts.search);
  $scope.moreworkouts = false;
  $scope.allWorkoutss = [];
  $scope.myExercises = [];
  $scope.sharedExercises = [];
  $scope.merged_array = [];
  $scope.showReorder = false;
  $scope.refine2 = {};
  $scope.postlimit = 20;
  $scope.workoutss = [];
  $scope.currentrecordsview = 0;
  var user_id = "";
  $scope.total = '';

  $scope.user = {};
  $scope.previewWorkout = [];

  $scope.uncheckAll = function () {
    $scope.previewWorkout = [];
    $scope.total = "";
  };
  $scope.clearData = function(){
    $scope.showConfirm = function() {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Confirm',
        template: 'Are you sure you want to reset all the data in this custom workout?”  ',
        cssClass: 'loginErrorPopUp',
        buttons: [
          { text: 'No' },
          {
            text: 'Yes',
            type: 'button-positive',
            onTap: function(e) {
              $ionicLoading.show({
                template: '<ion-spinner icon="ios"></ion-spinner>'

              });
              $scope.uncheckAll();
              $scope.modal_info.hide();
              $scope.modal.hide();
              $scope.modal.remove();
              $scope.modal_info.remove();
              $scope.refine.Exname = '';
              $scope.refine.Selectedtype = '';
              $scope.refine.Selectedequip = '';
              $scope.refine.Selectedlevel = '';
              $scope.refine.Selectedintensity = '';
              $scope.refine.SelectedPrivacy = '';
              $scope.refine.ExUrl = '';
              $scope.refine.ExDescription = '';
              $scope.refine.pic = '';
              $ionicLoading.hide();
            }
          }
        ]

      });

      confirmPopup.then(function(res) {
        // console.log('tapped !' + res);
        if(res) {
        } else {
        }
      });
    };

    $scope.showConfirm();

  };
  $scope.cancelAddingWorkout = function(){
    $scope.showConfirm = function() {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Confirm',
        template: 'Are you sure you want to cancel this custom workout?',
        cssClass: 'loginErrorPopUp',
        buttons: [
          { text: 'No' },
          {
            text: 'Yes',
            type: 'button-positive',
            onTap: function(e) {
              $ionicLoading.show({
                template: '<ion-spinner icon="ios"></ion-spinner>'

              });
              $scope.modal_info.hide();
              $scope.modal.hide();
              $scope.modal.remove();
              $scope.modal_info.remove();
              $state.go('app.myWorkouts');
              $ionicLoading.hide();
            }
          }
        ]

      });

      confirmPopup.then(function(res) {
        if(res) {


        } else {

        }
      });
    };

    $scope.showConfirm();
  };
  $scope.active = true;
  $scope.active1 = true;
  $scope.refine = {};
  $scope.refine.Exname = "";
  $scope.refine.pic = 'img/no-image.jpg'
  $scope.ex_pic_name = "";
  $scope.workput_id = $stateParams.result;
  $scope.previewWorkout = [];

  $scope.siteUrl = siteUrl;

  var userData = $window.localStorage['userData'];
  $scope.userData = angular.fromJson(userData);


  user_id = $scope.userData.user_id;
  /**
   * @name getQuote
   * @todo To Get Random Quotes
   * @return void function
   *
   **/
  quote.getQuote(uuid, user_id).success(function(res) {
    //console.log(res);
    $scope.quote = res;
  });
  var link = baseUrl + '/exercise/custom_workoutexercises';
  var formData = {
    uu_id: uuid,
    user_id: user_id
  }
  var postData = 'myData=' + JSON.stringify(formData);
  $ionicLoading.show({
    template: '<ion-spinner icon="ios"></ion-spinner>'

  });
  $http({
    method: 'POST',
    url: link,
    data: postData,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    }

  }).success(function(res) {

    //console.log(res);
    $scope.checked = true;

    $scope.allWorkoutss = res.exercise;
    $scope.myExercises = res.myexercises;
    $scope.sharedExercises = res.shared_exercises;
    res.exercise = angular.fromJson(res.exercise);


    for (var i = 0; i < $scope.postlimit; i++) {
      if (i < $scope.allWorkoutss.length) {
        $scope.workoutss.push(res.exercise[i]);
      }
    }
    $scope.currentrecordsview = $scope.postlimit;
    $scope.moreworkouts = true;


    $ionicLoading.hide();
  }).error(function(error) {
    $ionicLoading.hide();
    // console.log('request not completed');
  });

  //next form for workouot

  $scope.nextForm = function() {

  };





  /**
   * @name swipeToAdd
   * @todo To Enable the Feature of Swipe, In this Function We will add Workout on Swipe
   * @return void function
   *
   **/

  $scope.swipeToAdd = function (workout) {
    var workout = JSON.stringify( workout, function( key, value ) {
      if( key === "$$hashKey" ) {
        return undefined;
      }
      return value;
    });
    workout = angular.fromJson(workout);
    $scope.previewWorkout.push(workout);
    // $scope.finalPreview = angular.fromJson($scope.previewWorkout);
    // console.log( $scope.previewWorkout);
    var alertPopup = $ionicPopup.alert({
      title: '<div class="titleIcon titleErrorIcon animated bounceInDown"><div class="titleShake animated tada"><span class="ion-ios-checkmark"></span></div></div>',
      template: workout.workout_name + " Added to Workout",
      cssClass: 'loginErrorPopUp notOK'
    });
    $timeout(function () {
      alertPopup.close();

    }, 2000);
  }

///////////////////////////// Workout Filters ////////////////
  /**
   *
   * Workout Filter
   *
   **/
var link = baseUrl+'/workouts/workoutfilter';
                  var formData = {uu_id : uuid, user_id : user_id};
                  var postData = 'myData='+JSON.stringify(formData);
                      $ionicLoading.show({
                                template: '<ion-spinner icon="ios"></ion-spinner>'

                            });
                       $http({
                              method : 'POST',
                              url : link,
                              data: postData,
                              headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                      }).success(function(res){

                        $scope.refine = res.workoutsfilter;
                       // console.log("Filters Data");
                        //console.log($scope.refine);
                          $timeout(function() {
                        //       ionicMaterialMotion.fadeSlideInRight({
                        //           startVelocity: 3000
                        //       });
                              $ionicLoading.hide();

                           }, 700);
                        //
                        //   // Set Ink
                        //   ionicMaterialInk.displayEffect();



                      }) .error(function(error){
                        $ionicLoading.hide();
                       // console.log('request not completed');
                      })


////////////////////////// End Workout Filters ///////////////
  ///////////////// For Getting Custom Workout Exercises ///////

  var link = baseUrl+'/workouts/customWorkoutExercises';
  var formData = {uu_id : uuid, user_id : user_id,workout_id:$scope.workput_id};
  var postData = 'myData='+JSON.stringify(formData);
  $ionicLoading.show({
    template: '<ion-spinner icon="ios"></ion-spinner>'

  });
  $http({
    method : 'POST',
    url : link,
    data: postData,
    headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

  }).success(function(res){
    //console.log("In It");
    // console.log(res);
    $scope.checked = true;


    $scope.previewWorkout = res.SystemExercise;
    // console.log($scope.previewWorkout);
    $scope.sumDuration();
    $scope.fwType = [];
    $scope.fwEquip = [];
    $scope.fwLevel = [];
    $scope.fwIntensity = [];
    $scope.refine.Exname = res.other_info[0].w_name;
    $scope.refine.ExDescription = res.other_info[0].description;
    $scope.refine.Selectedtype = res.other_info[0].type;
    $scope.refine.Selectedequip = res.other_info[0].equipments_used;
    $scope.refine.Selectedlevel = res.other_info[0].diff_level;
    $scope.refine.Selectedintensity = res.other_info[0].goal;
    $scope.refine.SelectedPrivacy = res.other_info[0].status;
    $scope.refine.ExUrl = res.other_info[0].embed_video;
    $scope.ex_pic_name  = res.other_info[0].custom_image;
    $scope.refine.pic  = $scope.baseUrlForWorkoutImg + res.other_info[0].custom_image;
    $scope.workoutName  = res.other_info[0].w_name;

    //coverting string to array

    $scope.fwType = $scope.refine.Selectedtype.split(",");
    $scope.fwEquip = $scope.refine.Selectedequip.split(",");


    $scope.fwLevel = $scope.refine.Selectedlevel;
    $scope.fwIntensity = $scope.refine.Selectedintensity;

    $scope.modal.show();










  }) .error(function(error){
    $ionicLoading.hide();
    //console.log('request not completed');
  });




/////////////////////////////////////////////////////////////
  /**
   * @name selectPicture
   * @todo To Select Pictures from Gallery
   * @return void function
   *
   **/
$scope.selectPicture = function() {
                       document.addEventListener("deviceready", function () {
                          var options = {
                            quality: 100,
                            destinationType: Camera.DestinationType.FILE_URI,
                            sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
                            encodingType: Camera.EncodingType.JPEG,
                            targetWidth: 250,
                            targetHeight: 250,
                            allowEdit: true
                          };

                          $cordovaCamera.getPicture(options).then(
                          function(imageURI) {
                           $scope.uploadPicturex(imageURI);

                            $ionicLoading.show({template: 'Uploading Photo...', duration:500});
                          },
                          function(err){
                            $ionicLoading.show({template: 'Error While Uploading Image...', duration:500});
                          })
                           }, false);
                        };
  /**
   * @name uploadPicturex
   * @todo To Upload Pictures
   * @return void function
   *
   **/
$scope.uploadPicturex = function(imageURI) {

// console.log("In Upload Picture Function");
// console.log(imageURI);
  var options = {

  fileKey: "picture",

  filename: "p1.png",

  chunkedMode: false,

  mimeType: "image/png",
  trustAllHosts:true,
  headers:{Connection:"close"}

  };
 $ionicLoading.show({
                                             template: '<ion-spinner icon="ios"></ion-spinner><p>Uploading Image!</p>'

                                         });
  $cordovaFileTransfer.upload(encodeURI(baseUrl+"/exercise/upload_exercise_images"), imageURI, options)

  .then(function(result) {
  // console.log(result.response);
   $scope.refine.pic= imageURI;;
 var  resk  = angular.fromJson(result.response);

          $scope.ex_pic_name = resk.image_name;

// console.log($scope.upload);
$ionicLoading.hide();


  }, function(err) {

  // console.log(err);
  $ionicLoading.hide();
  $ionicLoading.show({template: 'An error occured while uploading your picture.<br>Please try again.', duration:500});

  }, function (progress) {

            });



  }


  /**
   * @name EditWorkout
   * @todo To Edit Custom Workout
   * @return void function
   *
   **/
  $scope.EditWorkout = function(name, type, equip, level,intensity,privacy_status, url, description){
                          user_id = $scope.userData.user_id;
                          var exercises = $scope.previewWorkout;
                           exercises.forEach(function(v){
                             delete v.description;
                             delete v.c_description;
                             delete v.download_url;
                             delete v.download_url_linl;
                           });
                           var link = baseUrl+'/workouts/edit_custom_workout';
                           var formData = {
                             uu_id : uuid,
                             user_id : user_id,
                             workout_id: $scope.workput_id,
                             workout_name : name,
                             embed_url : url,
                             workout_type : type,
                             workout_intensity : intensity,
                             difficulty_level : level,
                             privacy_status : privacy_status,
                             equipment_used:equip,
                             description : description,
                             exercise_image:$scope.ex_pic_name,
                             exercises: exercises,
                             is_custom : 1
                           };
                           var postData = 'myData='+JSON.stringify(formData);
                               $ionicLoading.show({
                                         template: '<ion-spinner icon="ios"></ion-spinner>'

                                     });
                                $http({
                                       method : 'POST',
                                       url : link,
                                       data: postData,
                                       headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

                               }).success(function(res){
                                $ionicLoading.hide();
                               // console.log(res);
                               if(res.code == 400)
                               {
                                  $ionicLoading.show({template: 'All the fields must be filled.', duration:500});
                               }
                               else
                               {
                                    $ionicLoading.show({template: 'Workout Updated Successfully', duration:500})
//
                                      $timeout(function() {
                                        $scope.modal_info.hide();
                                        $scope.modal.hide();
                                        $scope.modal.remove();
                                        $scope.modal_info.remove();
                                        $state.go('app.myWorkouts');

                                      }, 500);


//

                                }

                               }) .error(function(error){
                                 $ionicLoading.hide();
                                 // console.log('request not completed');
                               })

                       }

  /////////////////////// End Add Workout //////////////////////////

  /**
   * @name previewNext
   * @todo To Preview Next Exercise in Workout
   * @return void function
   *
   **/
  $scope.previewNext = function(){
    $ionicModal.fromTemplateUrl('templates/modal.html', {
      id: 'previewModal',
      scope: $scope,
      animation: 'animated slideInRight',
      backdropClickToClose: false

    }).then(function (modal) {
      $scope.modal = modal;
      modal.show();


    });
  };
                      $ionicModal.fromTemplateUrl('templates/modal.html', {
                        id: 'previewModal',
                        scope: $scope,
                        animation: 'animated slideInRight',
                        backdropClickToClose : false

                      }).then(function(modal) {
                        $scope.modal = modal;




                      });


                      $ionicModal.fromTemplateUrl('templates/modal_info.html', {
                        id: 'infoModal',
                        scope: $scope,
                        animation: 'animated slideInRight',
                        backdropClickToClose : false

                      }).then(function(modal) {
                        $scope.modal_info = modal;




                      });



  $scope.$on('modal.hidden', function() {

  });

  /**
   * @name goToDetailPage
   * @todo To Move Detail Page of Workout
   * @return void function
   *
   **/
  $scope.goToDetailPage = function () {
    $ionicModal.fromTemplateUrl('templates/modal_info.html', {
      id: 'infoModal',
      scope: $scope,
      animation: 'animated slideInRight',
      backdropClickToClose: false

    }).then(function (modal) {
      $scope.modal_info = modal;
      modal.show();


    });

  };
  /**
   * @name goBackExList
   * @todo To Move Back to Exercise List
   * @return void function
   *
   **/
  $scope.goBackExList = function () {
    $scope.modal.hide();
    $scope.modal_info.hide();
    $ionicModal.fromTemplateUrl('templates/modal.html', {
      id: 'previewModal',
      scope: $scope,
      animation: 'animated slideInRight',
      backdropClickToClose: false

    }).then(function (modal) {
      $scope.modal = modal;
      modal.show();

    });

  };
  /**
   * @name reorderItem
   * @todo To Change the order of the exercises
   * @return void function
   *
   **/
  $scope.reorderItem = function (item, fromIndex, toIndex) {
    // console.log("in Reorder");
    $scope.previewWorkout.splice(fromIndex, 1);
    $scope.previewWorkout.splice(toIndex, 0, item);
  };
  /**
   * @name onItemDelete
   * @todo To Delete the exercise
   * @return void function
   *
   **/
  $scope.onItemDelete = function (item) {
    // console.log("All PreviewWork Length : " + $scope.previewWorkout.length);
    $scope.previewWorkout.splice($scope.previewWorkout.indexOf(item), 1);
    $scope.sumDuration();
  };
  /**
   * @name focussearch
   * @todo To Focus the Input in search
   * @return void function
   *
   **/
  $scope.focussearch = function() {
    // do something awesome
    $ionicScrollDelegate.scrollTop();
    focus('searchStrman');
    $scope.show = "showInput";

  };
  $scope.showSearchIcon = true;
  $scope.change = function(text) {
    if($scope.searchStrman.search == ''){
      $scope.showSearchIcon = true;
    }
    // console.log(text);
    $ionicScrollDelegate.resize();
    $scope.loadmoreworkouts();
    angular.element(document.querySelectorAll('.allWorkoutsStack')).triggerHandler('click');
    $scope.showSearchIcon = false;
  };
  $scope.emptySearch = function(text){
    $scope.showSearchIcon = true;
    $scope.searchStrman = {};
    $scope.searchStrman.search = null;
    $scope.searchStrman.search = '';
    text = '';

  };
  /**
   * @name loadmoreworkouts
   * @todo To Load More Workouts
   * @return void function
   *
   **/
$scope.loadmoreworkouts = function() {

           // console.log("In Scroll Workouts");
              var lim = $scope.currentrecordsview + $scope.postlimit;

              for(var i=$scope.currentrecordsview;i< lim;i++)
              {
                if(i < $scope.allWorkoutss.length)
                {
                  $scope.workoutss.push($scope.allWorkoutss[i]);
                }
                else
                {
                  $scope.moreworkouts=false;
                  break;
                }
              }
             // console.log($scope.workouts);
            $scope.currentrecordsview=lim;
            // $timeout(function() {
            //                                 ionicMaterialMotion.fadeSlideInRight({
            //                                     startVelocity: 3000
            //                                 });
            //                             }, 1000);
            //
            //                             // Set Ink
            //                             ionicMaterialInk.displayEffect();
              $scope.$broadcast('scroll.infiniteScrollComplete');
        };

  /**
   * @name sumDuration
   * @todo To Count and Sum the Duration of the Workout
   * @return void function
   *
   **/

  $scope.sumDuration = function(){
    var total = 0;
    for(var i = 0; i < $scope.previewWorkout.length; i++){
      var duration = $scope.previewWorkout[i];
      // console.log(duration.EXduration);
      if (duration.EXduration !== undefined) {
        total += (parseFloat(duration.EXduration));
      }
    }
    // console.log(total);
    var date = new Date(null);
    date.setSeconds(total); // specify value for SECONDS here
    $scope.total = date.toISOString().substr(11, 8);
  };
//filter
  /**
   * @name filterExercise
   * @todo To Filter Exercise
   * @return void function
   *
   **/
  $scope.filterExercise = function() {

    var ex_type = $scope.refine2.Selectedtype;
    var body_part = $scope.refine2.bodyPartid;
    var machines = $scope.refine2.Selectedequip;
    var level = $scope.refine2.Selectedlevel;
    var custom = 'custom';

    ////////////////// Filter Request Start/////////////////

    var link = baseUrl+'/exercise/search_exercises';
    var formData = {uu_id : uuid, user_id : user_id ,  exerciseType : ex_type, bodyPart : body_part  ,  wLevels : level, exmachines : machines, bit : custom}
    var postData = 'myData='+JSON.stringify(formData);
    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner>'

    });
    $http({
      method : 'POST',
      url : link,
      data: postData,
      headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

    }).success(function(res){

     // console.log(res);
      $scope.NoResultFound =  res.code;
      $scope.workoutss = res.exercises;
      $scope.modal_filter.hide();
      $ionicLoading.hide();



    }) .error(function(error){
      $ionicLoading.hide();
     // console.log('request not completed');
    }).finally(function ($ionicLoading) {
      // On both cases hide the loading
      $ionicLoading.hide();
    });


  };

  $scope.modal_filter = function() {
    $scope.modal_filter.hide();
  };

  $ionicModal.fromTemplateUrl('templates/modal_filter.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal_filter = modal;
    var user_id = $scope.userData.user_id;
    var link = baseUrl+'/exercise/exercise_filters';
    var formData = {uu_id : uuid, user_id : user_id};
    var postData = 'myData='+JSON.stringify(formData);
    $ionicLoading.show({
      template: '<ion-spinner icon="ios"></ion-spinner>'

    });
    $http({
      method : 'POST',
      url : link,
      data: postData,
      headers : {'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'}

    }).success(function(res){

      $scope.refine2 = res.exercise_filters;
      // console.log($scope.refine2);
      $timeout(function() {
          // ionicMaterialMotion.fadeSlideInRight({
          //     startVelocity: 3000
          // });
          $ionicLoading.hide();
      }, 700);

      // Set Ink
      // ionicMaterialInk.displayEffect();


    }) .error(function(error){
      $ionicLoading.hide();
      //console.log('request not completed');
    })
  });

  $scope.modal_filter = function() {
    $scope.modal_filter.show();
  };
        $scope.$on('$stateChangeSuccess', function() {
          $scope.loadmoreworkouts();
        });



});
