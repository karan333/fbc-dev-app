/**
 * Purpose:
 *@This file is used to Manage Routes of All App
 *
 **/

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ngEmbed' , 'monospaced.elastic', 'ionicImgCache', 'ionic', 'ion-gallery' ,'checklist-model', 'ionic.rating' , 'ionic-material', 'fancyboxplus', 'ionMdInput', 'ngCordova', 'angular-svg-round-progress','ionic-datepicker', 'highcharts-ng', 'ionicLazyLoad', 'ngVideo'])

.run(function($ionicPlatform, $cordovaSQLite, $state, $location, $timeout, $cordovaToast, $ionicHistory) {
   $ionicPlatform.ready(function() {




     if(window.cordova && window.cordova.plugins.Keyboard) {

         cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

         cordova.plugins.Keyboard.disableScroll(false)
         window.cordova.plugins.Keyboard.disableScroll(false);
       }
       if (cordova.platformId == 'android') {
           StatusBar.backgroundColorByHexString("#c3d737");
         StatusBar.styleBlackTranslucent();
       }


     });

     var backbutton = 0;
     $ionicPlatform.registerBackButtonAction(function() {
         if ($state.current.name == 'app.home' || $state.current.name == 'app.login') {
           if(backbutton == 0){
             backbutton++;
             $cordovaToast.showLongBottom('Press again to exit');
             // $timeout(function(){
             //   backbutton=1;
             // },5000);
           }else{
             navigator.app.exitApp();
           }
         }else{
           $ionicHistory.goBack();
         }
     }, 100);
})

.config(function(ionicImgCacheProvider, $sceProvider, $stateProvider , $urlRouterProvider , $ionicConfigProvider, ionicDatePickerProvider ) {


  ionicImgCacheProvider.debug(false);

  $ionicConfigProvider.scrolling.jsScrolling(true);

  // Set storage size quota to 100 MB.
  ionicImgCacheProvider.quota(100);

  // Set foleder for cached files.
  ionicImgCacheProvider.folder('fbc-cache');


$sceProvider.enabled(false);
  var datePickerObj = {
    inputDate: new Date(),
    titleLabel: 'Select a Date',
    setLabel: 'Set',
    todayLabel: 'Today',
    closeLabel: 'Close',
    mondayFirst: false,
    weeksList: ["S", "M", "T", "W", "T", "F", "S"],
    monthsList: ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"],
    templateType: 'popup',
    from: new Date(2012, 8, 1),
    to: new Date(2018, 8, 1),
    showTodayButton: false,
    dateFormat: 'dd MMMM yyyy',
    closeOnSelect: false,
    disableWeekdays: []
    };

    ionicDatePickerProvider.configDatePicker(datePickerObj);

    $ionicConfigProvider.tabs.position('bottom');


    // Turn off caching for demo simplicity's sake
    $ionicConfigProvider.views.maxCache(0);

    /*
    // Turn off back button text
    $ionicConfigProvider.backButton.previousTitleText(false);
    */
  $stateProvider

    .state('app', {
    url: '/app',
    cache: false,
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })
    /**
     * @name app.login
     * @todo To Route login
     *
     **/
  .state('app.login', {
        url: '/login',

        views: {
            'menuContent': {
                templateUrl: 'templates/login.html',
                controller: 'LoginCtrl'
            }
        }
    })
  /**
   * @name app.forgot-password
   * @todo To Route forgot-password
   *
   **/
  .state('app.forgot-password', {
    url: '/forgot-password',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/forgot-password.html',
        controller: 'forgotPassword'
      }
    }
  })
  /**
   * @name app.terms-conditions
   * @todo To Route terms-conditions
   *
   **/
  .state('app.terms-conditions', {
      url: '/terms-conditions',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/terms-conditions.html',
          controller: 'termsConditions'
        }
      }
    })
  /**
   * @name app.signup
   * @todo To Route signup
   *
   **/
  .state('app.signup', {
  url: "/signup?cid",
  cache: false,
    views: {
      'menuContent': {
       templateUrl: "templates/signup.html",
       controller: 'signupController'
    }
  }
})
  /**
   * @name app.profile
   * @todo To Route profile
   *
   **/
  .state('app.profile', {
  url: "/profile",
  cache: false,
    views: {
      'menuContent': {
       templateUrl: "templates/profile.html",
       controller: 'profileController'
    }
  }
})
  /**
   * @name app.splash
   * @todo To Route splash
   *
   **/
  .state('app.splash', {
  url: "/splash",
  cache: false,
    views: {
      'menuContent': {
       templateUrl: "templates/splash.html",
       controller: 'splashCtrl'
    }
  }
})
  /**
   * @name app.allNewsFeed
   * @todo To Route allNewsFeed
   *
   **/
.state('app.allNewsFeed', {
  url: "/allNewsFeed",
  cache: false,
    views: {
      'menuContent': {
       templateUrl: "templates/allNewsFeed.html",
       controller: 'allNewsFeed'
    }
  }
})
/**
 * @name app.Feedback
 * @todo To Route Feedback
 *
 **/
.state('app.Feedback', {
  url: "/Feedback",
  cache: false,
  views: {
    'menuContent': {
      templateUrl: "templates/Feedback.html",
      controller: 'FeedbackCtrl'
    }
  }
})
/**
 * @name app.contact
 * @todo To Route contact
 *
 **/
.state('app.contact', {
  url: "/contact",
  cache: false,
  views: {
    'menuContent': {
      templateUrl: "templates/contact.html",
      controller: 'contactCtrl'
    }
  }
})
/**
 * @name app.allProgressPhotos
 * @todo To Route allProgressPhotos
 *
 **/
.state('app.allProgressPhotos', {
  url: "/allProgressPhotos",
  cache: false,
    views: {
      'menuContent': {
       templateUrl: "templates/allProgressPhotos.html",
       controller: 'allProgressPhotos'
    }
  }
})
/**
 * @name app.uploadProgressPics
 * @todo To Route uploadProgressPics
 *
 **/
.state('app.uploadProgressPics', {
  url: "/uploadProgressPics",
  cache: false,
    views: {
      'menuContent': {
       templateUrl: "templates/uploadProgressPics.html",
       controller: 'uploadProgressPics'
    }
  }
})
/**
 * @name app.exercises
 * @todo To Route exercises
 *
 **/
.state('app.exercises', {
  url: '/exercises',
  cache: false,
  views: {
    'menuContent': {
      templateUrl: 'templates/exercises.html',
      controller: 'exercisescontroller'
    }
  }
})
/**
 * @name app.FavExercises
 * @todo To Route FavExercises
 *
 **/
.state('app.FavExercises', {
      url: '/FavExercises',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/FavExercises.html',
          controller: 'FavExercisesController'
        }
      }
    })
/**
 * @name app.myexercises
 * @todo To Route myexercises
 *
 **/
.state('app.myexercises', {
  url: '/myexercises',
  cache: false,
  views: {
    'menuContent': {
      templateUrl: 'templates/myexercises.html',
      controller: 'myexercisesCtrl'
    }
  }
})
/**
 * @name app.addCustomExercise
 * @todo To Route addCustomExercise
 *
 **/
.state('app.addCustomExercise', {
  url: '/addCustomExercise',
  cache: false,
  views: {
    'menuContent': {
      templateUrl: 'templates/addCustomExercise.html',
      controller: 'addCustomExercises'
    }
  }
})
/**
 * @name app.myexerciseDetail
 * @todo To Route myexerciseDetail
 *
 **/
.state('app.myexerciseDetail', {
  url: '/myexerciseDetail?cid?did',
  cache: false,
  views: {
    'menuContent': {
      templateUrl: 'templates/myexerciseDetail.html',
      controller: 'myexerciseDetailCtrl'
    }
  }
})
/**
 * @name app.allMyActivities
 * @todo To Route allMyActivities
 *
 **/
.state('app.allMyActivities', {
  url: "/allMyActivities",
  cache: false,
    views: {
      'menuContent': {
       templateUrl: "templates/allMyActivities.html",
       controller: 'allMyActivitiesCtrl'
    }
  }
})

/**
 * @name app.selectPackage
 * @todo To Route selectPackage
 *
 **/

  .state('app.selectPackage', {
  url: "/selectPackage?cid",
  cache: false,
    views: {
      'menuContent': {
       templateUrl: "templates/select_package.html",
       controller: 'selectPackageController'
    }
  }
})
  /**
   * @name app.browse
   * @todo To Route browse
   *
   **/
  .state('app.browse', {
      url: '/browse',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html'
        }
      }
    })
  /**
   * @name app.home
   * @todo To Route home
   *
   **/
    .state('app.home', {
      url: '/home',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/home.html',
          controller: 'HomeCtrl'
        }
      }
    })
    /**
     * @name app.workouts
     * @todo To Route workouts
     *
     **/
    .state('app.workouts', {
      url: '/workouts?result',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/workouts.html',
          controller: 'workoutsCtrl'
        }
      }
    })
    /**
     * @name app.singlePost
     * @todo To Route singlePost
     *
     **/
.state('app.singlePost', {
  url: '/singlePost?cid?did',
  cache: false,
  views: {
    'menuContent': {
      templateUrl: 'templates/singlePost.html',
      controller: 'singlePostCtrl'
    }
  }
})
/**
 * @name app.workoutLog
 * @todo To Route workoutLog
 *
 **/
     .state('app.workoutLog', {
              url: '/workoutLog?result',
              cache: false,
              views: {
                'menuContent': {
                  templateUrl: 'templates/workoutLog.html',
                  controller: 'workoutLogCtrl'
                }
              }
            })
     /**
      * @name app.FavWorkouts
      * @todo To Route FavWorkouts
      *
      **/
    .state('app.FavWorkouts', {
          url: '/FavWorkouts?result',
          cache: false,
          views: {
            'menuContent': {
              templateUrl: 'templates/FavWorkouts.html',
              controller: 'FavWorkoutsCtrl'
            }
          }
        })
    /**
     * @name app.addCustomWorkout
     * @todo To Route addCustomWorkout
     *
     **/
     .state('app.addCustomWorkout', {
       url: '/addCustomWorkout',
       cache: false,
       views: {
         'menuContent': {
           templateUrl: 'templates/addCustomWorkout.html',
           controller: 'addCustomWorkout'
         }
       }
     })
     /**
      * @name app.workoutDetail
      * @todo To Route workoutDetail
      *
      **/

  .state('app.workoutDetail', {
       url: '/workoutDetail?cid',
       cache: false,
       views: {
         'menuContent': {
           templateUrl: 'templates/workoutDetail.html',
           controller: 'workoutDetailCtrl'
         }
       }
     })
  /**
   * @name app.workoutDetailMain
   * @todo To Route workoutDetailMain
   *
   **/
  .state('app.workoutDetailMain', {
       url: '/workoutDetailMain?cid',
       cache: false,
       views: {
         'menuContent': {
           templateUrl: 'templates/workoutDetailMain.html',
           controller: 'workoutDetailCtrlMain'
         }
       }
     })
  /**
   * @name app.preWorkoutTimer
   * @todo To Route preWorkoutTimer
   *
   **/
  .state('app.preWorkoutTimer', {
       url: '/preWorkoutTimer?cid',
       cache: false,
       views: {
         'menuContent': {
           templateUrl: 'templates/preWorkoutTimer.html',
           controller: 'preWorkoutTimerCtrl'
         }
       }
     })
  /**
   * @name app.MyPreWorkoutTimer
   * @todo To Route MyPreWorkoutTimer
   *
   **/
    .state('app.MyPreWorkoutTimer', {
      url: '/MyPreWorkoutTimer?result',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/MyPreWorkoutTimer.html',
          controller: 'MyPreWorkoutTimerCtrl'
        }
      }
    })
    /**
     * @name app.myWorkoutDetailMain
     * @todo To Route myWorkoutDetailMain
     *
     **/
 .state('app.myWorkoutDetailMain', {
      url: '/myWorkoutDetailMain?result?cid?did?nid',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/myWorkoutDetailMain.html',
          controller: 'myWorkoutDetailMainCtrl'
        }
      }
    })
 /**
  * @name app.exerciseDetail
  * @todo To Route exerciseDetail
  *
  **/
  .state('app.exerciseDetail', {
       url: '/exerciseDetail?cid?did',
       cache: false,
       views: {
         'menuContent': {
           templateUrl: 'templates/exerciseDetail.html',
           controller: 'exerciseDetailCtrl'
         }
       }
     })
  /**
   * @name app.myWorkouts
   * @todo To Route myWorkouts
   *
   **/
.state('app.myWorkouts', {
  url: '/myWorkouts?result',
  cache: false,
  views: {
    'menuContent': {
      templateUrl: 'templates/myWorkouts.html',
      controller: 'myWorkoutsCtrl'
    }
  }
})
/**
 * @name app.timer
 * @todo To Route timer
 *
 **/
.state('app.timer', {
    url: '/timer',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/timer.html',
        controller: 'timerController'

      }
    }
})
/**
 * @name app.timer.stopWatch
 * @todo To Route timer.stopWatch
 *
 **/
.state('app.timer.stopWatch', {
    url: '/stopWatch',
    cache: false,
    views: {
      'stopWatch': {
        templateUrl: 'templates/stopwatch.html',
        controller: 'stopWatchController'

      }
    }
})
/**
 * @name app.timer.countdown
 * @todo To Route timer.countdown
 *
 **/
.state('app.timer.countdown', {
  url: '/countdown',
  cache: false,
  views: {
    'countdown': {
      templateUrl: 'templates/countdown.html',
      controller: 'countdowntimer'

    }
}
})
/**
 * @name app.timer.interval
 * @todo To Route timer.interval
 *
 **/
.state('app.timer.interval', {
  url: '/interval',
  cache: false,
  views: {
    'interval': {
      templateUrl: 'templates/interval.html',
      controller: 'intervalCtrl'

    }
}
})
/**
 * @name app.friends
 * @todo To Route friends
 *
 **/
.state('app.friends', {
    url: '/friends',
    views: {
      'menuContent': {
        templateUrl: 'templates/friends.html',
        controller: 'friendsMain'
      }
    }
  })
/**
 * @name app.friends.activities
 * @todo To Route friends.activities
 *
 **/
.state('app.friends.activities', {
    url: '/friends/activities',
    cache: false,
    views: {
      'activities': {
        templateUrl: 'templates/activities.html',
        controller: 'FriendsActivityCtrl'
      }
    }
  })
/**
 * @name app.friends.myFriends
 * @todo To Route friends.myFriends
 *
 **/
 .state('app.friends.myFriends', {
    url: '/friends/myFriends',
     cache: false,
    views: {
      'myFriends': {
        templateUrl: 'templates/myFriends.html',
        controller: 'myFriendsCtrl'
      }
    }
  })
 /**
  * @name app.friends.fFriends
  * @todo To Route friends.fFriends
  *
  **/
 .state('app.friends.fFriends', {
    url: '/friends/fFriends',
     cache: false,
    views: {
      'fFriends': {
        templateUrl: 'templates/fFriends.html',
        controller: 'fFriendsCtrl'
      }
    }
  })
 /**
  * @name app.friends.inbox
  * @todo To Route friends.inbox
  *
  **/
 .state('app.friends.inbox', {
    url: '/friends/inbox',
    views: {
      'inbox': {
        templateUrl: 'templates/inbox.html',
        controller: 'inboxCtrl'
      }
    }
  })
 /**
  * @name app.friends.myFriends.myFriendsI
  * @todo To Route friends.myFriends.myFriendsI
  *
  **/
 .state('app.friends.myFriends.myFriendsI', {
    url: '/friends/myFriendsI',
    views: {
      'myFriendsI': {
        templateUrl: 'templates/myFriendsI.html',
        controller: 'myFriendsICtrl'
      }
    }
  })
 /**
  * @name app.friends.myFriends.friendsR
  * @todo To Route friends.myFriends.friendsR
  *
  **/
 .state('app.friends.myFriends.friendsR', {
    url: '/friends/friendsR',
    views: {
      'friendsR': {
        templateUrl: 'templates/friendsR.html',
        controller: 'friendsRCtrl'
      }
    }
  })
 /**
  * @name app.nutrition
  * @todo To Route nutrition
  *
  **/
 .state('app.nutrition', {
    url: '/nutrition',
    views: {
      'menuContent': {
        templateUrl: 'templates/nutrition.html',
        controller: 'nutritionCtrl'
      }
    }
  })
 /**
  * @name app.calorieChart
  * @todo To Route calorieChart
  *
  **/
   .state('app.calorieChart', {
      url: '/calorieChart',
      views: {
        'menuContent': {
          templateUrl: 'templates/calorieChart.html',
          controller: 'calorieChartCtrl'
        }
      }
    })
   /**
    * @name app.meal
    * @todo To Route meal
    *
    **/
   .state('app.meal', {
      url: '/meal',
      views: {
        'menuContent': {
          templateUrl: 'templates/meal.html',
          controller: 'mealCtrl'
        }
      }
    })
   /**
    * @name app.water
    * @todo To Route water
    *
    **/
 .state('app.water', {
    url: '/water',
    views: {
      'menuContent': {
        templateUrl: 'templates/water.html',
        controller: 'waterCtrl'
      }
    }
  })
 /**
  * @name app.stats
  * @todo To Route stats
  *
  **/
.state('app.stats', {
        url: '/stats',
        views: {
          'menuContent': {
            templateUrl: 'templates/stats.html',
            controller: 'statsCtrl'
          }
        }
      })
/**
 * @name app.startWorkout
 * @todo To Route startWorkout
 *
 **/
    .state('app.startWorkout', {
  url: '/startWorkout',
  views: {
    'menuContent': {
      templateUrl: 'templates/startWorkout.html',
      controller: 'startWorkoutCtrl'
    }
  }
})
/**
 * @name app.startWorkout2
 * @todo To Route startWorkout2
 *
 **/
.state('app.startWorkout2', {
  url: '/startWorkout2',
  views: {
    'menuContent': {
      templateUrl: 'templates/startWorkout2.html',
      controller: 'startWorkout2Ctrl'
    }
  }
})
/**
 * @name app.startWorkout3
 * @todo To Route startWorkout3
 *
 **/
.state('app.startWorkout3', {
  url: '/startWorkout3',
  views: {
    'menuContent': {
      templateUrl: 'templates/startWorkout3.html',
      controller: 'startWorkout3Ctrl'
    }
  }
})
/**
 * @name app.startWorkout4
 * @todo To Route startWorkout4
 *
 **/
.state('app.startWorkout4', {
  url: '/startWorkout4',
  views: {
    'menuContent': {
      templateUrl: 'templates/startWorkout4.html',
      controller: 'startWorkout4Ctrl'
    }
  }
})
/**
 * @name app.editprogressphotos
 * @todo To Route editprogressphotos
 *
 **/
.state('app.editprogressphotos', {
      url: '/editprogressphotos?result',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/editProgressPics.html',
          controller: 'editProgressPics'
        }
      }
    })
/**
 * @name app.editexercise
 * @todo To Route editexercise
 *
 **/
.state('app.editexercise', {
      url: '/editexercise?result',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/editexercise.html',
          controller: 'editexercise'
        }
      }
    })
/**
 * @name app.userdashboard
 * @todo To Route userdashboard
 *
 **/
 .state('app.userdashboard', {
           url: '/userdashboard?result',
           cache: false,
           views: {
             'menuContent': {
               templateUrl: 'templates/userdashboard.html',
               controller: 'userdashboard'
             }
           }
         })
 /**
  * @name app.allFriendsActivities
  * @todo To Route allFriendsActivities
  *
  **/
 .state('app.allFriendsActivities', {
   url: "/allFriendsActivities?result",
   cache: false,
     views: {
       'menuContent': {
        templateUrl: "templates/allFriendsActivities.html",
        controller: 'allFriendsActivities'
     }
   }
 })
 /**
  * @name app.editcustomworkout
  * @todo To Route editcustomworkout
  *
  **/
.state('app.editcustomworkout', {
    url: "/editcustomworkout?result",
    cache: false,
      views: {
        'menuContent': {
         templateUrl: "templates/editCustomWorkout.html",
         controller: 'editCustomWorkout'
      }
    }
  })
/**
 * @name app.customWorkoutDetail
 * @todo To Route customWorkoutDetail
 *
 **/
  .state('app.customWorkoutDetail', {
        url: "/customWorkoutDetail?result",
        cache: false,
          views: {
            'menuContent': {
             templateUrl: "templates/customWorkoutDetail.html",
             controller: 'customWorkoutDetail'
          }
        }
      })
.state('app.notifications', {
  url: "/notifications?result",
  cache: false,
  views: {
    'menuContent': {
      templateUrl: "templates/notifications.html",
      controller: 'notifications'
    }
  }
});
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/splash');
});
